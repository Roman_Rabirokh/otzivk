<?php

class Page
{
	var $Id = 0;
	var $meta_title = "";
	var $meta_description = "";
	var $meta_keywords = "";
	var $scripts = array();
	var $css = array();
	var $meta = array();
	var $h1 = "";
	var $breadcrumb = array();
	var $footer = array();
	var $isIndex = FALSE;
	var $url = "";
	var $classes = array();
	var $contentTypeId = 0;
	var $notFound = FALSE;
    var $data = array();



	public function Page()
	{

	}

	public function addJS($script)
	{
		if($script != "")
		{
			$this->scripts[] = $script;
		}
	}
    public function addCss($script)
	{
		if($script != "")
		{
			$this->css[] = $script;
		}
	}

	public function addFooter($data)
	{
		if($data != "")
		{
			$this->footer[] = $data;
		}
	}

	public function getJS()
	{
		foreach($this->scripts as $script)
		{
			?><script type="text/javascript" src="<?= $script ?>"></script><?php
		}
	}

	public function getCss()
	{
		foreach($this->css as $css)
		{
			?><link rel="stylesheet" type="text/css" href="<?= $css?>" ><?php
		}
	}

	public function getMeta()
	{
		?><title><?php echo $this->meta_title; ?></title><meta content="<?php echo $this->meta_keywords;?>" name="keywords" /><meta content="<?php echo $this->meta_description;?>" name="description" /><?php
		print implode($this->meta);
	}

	public function getTitle()
	{
		return $this->h1;
	}

	public function getHeader()
	{
		$this->getMeta();
		$this->getCss();
	}

	public function getFooter()
	{
		$this->getJS();
		print implode("",$this->footer);
	}

	public function addPathItem($title,$path = "")
	{
		$this->breadcrumb[] = array("title"=>$title,"path"=>$path);
	}

	public function getPath()
	{
          
		if(count($this->breadcrumb) > 0)
		{ ?>
                    <ol class="breadcrumb"  itemscope itemtype="http://schema.org/BreadcrumbList">
			<li class='home' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a  href="/"><span itemprop="name">Главная</span></a>&nbsp;</li><?php
			foreach($this->breadcrumb as $item) { if($item["path"] != "")
                            { ?>
                                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">&nbsp;<a  href="<?=$item["path"]?>"><span itemprop="name"><?=$item["title"]?></span></a>&nbsp;</li><?php }
				else
				{
					?><li  class="active" >&nbsp; <span itemprop="name"><?=$item["title"]?></span></li><?php
				}
			}
			?></ol><?php
		}
	}

	function getBodyClass()
	{
		return implode(' ',$this->classes);
	}

	function getTemplateUrl()
	{
		return _TEMPLATE_URL;
	}

}
