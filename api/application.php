<?php
class APP
{
	var $events = array();
	var $routes = array();
	var $contentType = "";
	var $actionResult = FALSE;
	var $loadTemplate = TRUE;
    var $page;

	function APP()
	{
		$this->contentType = isset($_REQUEST["content"]) ? $_REQUEST["content"] : "";
        $this->page = new Page();
	}

	function setStatus($actionResult,$loadTemplate)
	{
		$this->actionResult = $actionResult;
		$this->loadTemplate = $loadTemplate;
	}

	function addEventHandler($event,$function)
	{
		$this->events[$event][] = $function;
	}

	function getEventHandlers($event)
	{
		if(isset($this->events[$event]))
		{
			return 	$this->events[$event];
		}
		else
		{
			return array();
		}
	}

	function getCache(&$data,$key) {
        if (!_ENABLE_CACHING) return false;
		$result = false;
		$cache_file = _CACHE_PATH . "components/" . $key . ".tmp";
		if(file_exists($cache_file) && ((time() - filemtime($cache_file)) <= _CACHE_TIME))
		{
			$data = unserialize(file_get_contents($cache_file));
			$result  = true;
		}

		return $result;

	}

	function setCache($data,$key)
	{
		$cache_file = _CACHE_PATH . "components/" . $key . ".tmp";
		file_put_contents($cache_file,serialize($data));
	}

	function includeComponent($name,$params = array(),$template = "")
	{
		global $page;
		global $user;

		$filename = _COMPONENTS . $name . "/module.php";
		if(file_exists($filename))
		{
			$cache_key = md5($name . serialize($params));

			$data = array();

			return (include($filename));


		}
		else
		{
			return false;
		}
	}

	function loadComponent($name,$params = array(),$template = "")
	{
		global $page;

		$id = "component_" . uniqid();
		echo "<div id='$id'></div>";

		$url_params = array();

		foreach($params as $key=>$value)
		{
			$url_params[] = $key . "=" . urlencode($value);
		}

		$page->addFooter("<script type='text/javascript'>$('#$id').load('/component/$name".(count($url_params) > 0 ? "?" . implode("&",$url_params) : "")."');</script>");
	}

	function includeFile($filename,$absPath = FALSE,$params = array())
	{

		if($absPath)
		{
			include($filename);
		}
		else
		{
			if(file_exists(_INCLUDES_PATH . $filename))
			{
				include(_INCLUDES_PATH . $filename);
			}
		}

	}

    function loadModule($module)
    {
        $filename =  _DIR . "modules/" . $module . ".php";
        if(file_exists($filename))
        {
            include_once($filename);
        }
    }

	function getComponentPath($name)
	{
		return "/components/" . $name . "/";
	}

	function mime_header_encode($str, $data_charset, $send_charset) {
	if($data_charset != $send_charset) {
		$str = iconv($data_charset, $send_charset, $str);
	}
	return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
	}

	function sendEmail($name_from,$email_from,$name_to,$email_to,$subject,$body)
	{
		$data_charset = "UTF-8";
		$send_charset = "UTF-8";
		$to = $this->mime_header_encode($name_to, $data_charset, $send_charset)
		. ' <' . $email_to . '>';
		$subjectOriginal = $subject;
		$subject = $this->mime_header_encode($subject, $data_charset, $send_charset);
		$from =  $this->mime_header_encode($name_from, $data_charset, $send_charset)
		.' <' . $email_from . '>';
		if($data_charset != $send_charset) {
			$body = iconv($data_charset, $send_charset, $body);
		}
		$headers = "From: $from\r\n";
		$headers .= "Content-type: text/html; charset=$send_charset\r\n";
		$headers .= "Mime-Version: 1.0\r\n";
		$headers .= "X-Mailer: PHP/".phpversion()."\r\n";

		if(!_MAIL_TO_LOG)
		{
			return mail($to, $subject, $body, $headers);
		}
		else
		{
			dbNonQuery("INSERT INTO `". _DB_TABLE_PREFIX ."mail_log` (`created`, `name_form`, `email_form`, `name_to`, `email_to`, `subject`, `body`) VALUES (now(), :name_form, :email_from, :name_to, :email_to, :subject, :body);

			",array(
				":name_form"=>$name_from,
				":email_from"=>$email_from,
				":name_to"=>$name_to,
				":email_to"=>$email_to,
				":subject"=>$subjectOriginal,
				":body"=>$body
			));
		}


	}

	function sendEmailTemplate($name_from,$email_from,$name_to,$email_to,$subject,$template,$params = array())
	{
		$mailTemplate = _MAIL_TEMPLATE . $template;
		$body = "";
		if(file_exists($mailTemplate))
		{
			$tpl = file_get_contents($mailTemplate);
			foreach($params as $key=>$param)
			{
				$tpl = str_replace($key,$param,$tpl);
			}
			$body = $tpl;
		}

		$this->sendEmail($name_from,$email_from,$name_to,$email_to,$subject,$body);

	}

	function createFormToken($key)
	{
		$result = "";
		if(isset($_SESSION["token_" . $key]))
		{
			$result  = 	$_SESSION["token_" . $key];
		}
		else
		{
			$result = $key."_".uniqid();
			$_SESSION["token_" . $key] = $result;
		}
		return $result;
	}

	function checkToken($key,$value)
	{
		if(isset($_SESSION["token_" . $key]))
		{
			return ($_SESSION["token_" . $key] == $value);
		}
		else
		{
			return false;
		}
	}

	function processRouting()
	{
		foreach($this->routes as $route)
		{
			preg_match($route["pattern"], $_SERVER["REQUEST_URI"], $matches);
			if(count($matches) > 0)
			{
				if(!empty($route["action"]))
				{
					call_user_func($route["action"],$matches);
				}
				if(!empty($route["contentType"]))
				{
					$this->contentType = $route["contentType"];
				}

				break;
			}
		}
	}

	function processTemplate($output)
	{
		global $user;

		$events = $this->getEventHandlers("beforeProcessTemplate");
		foreach($events as $event)
		{
			call_user_func($event);
		}

		if($this->loadTemplate)
		{
			if($this->contentType  != "component" && file_exists(_TEMPL . "header.php")) {
				include(_TEMPL . "header.php");
			}

			print $output;

			if($this->contentType  != "component"  && file_exists(_TEMPL . "footer.php")) {
				include(_TEMPL . "footer.php");
			}
		}
		else
		{
			print $output;
		}

		if($this->contentType == "component") {
			$this->page->getJS();
		}

		$events = $this->getEventHandlers("afterProcessTemplate");
		foreach($events as $event)
		{
			call_user_func($event);
		}
	}


	function addSessionMessage($code,$txt,$type /*success,danger,warning,info*/)
	{
		if(!isset($_SESSION['MESSAGES']))
		{
			$_SESSION['MESSAGES'] = '';
		}

		$messages = unserialize($_SESSION['MESSAGES']);
		$messages[$code] = array('TEXT'=>$txt,'TYPE'=>$type);

		$_SESSION['MESSAGES'] = serialize($messages);
	}

	function getSessionMessage($code,$unset = false)
	{
		if(isset($_SESSION['MESSAGES']))
		{
			$messages = unserialize($_SESSION['MESSAGES']);
			if(isset($messages[$code]))
			{
				$message = $messages[$code];

				if($unset)
				{
					unset($messages[$code]);
					$_SESSION['MESSAGES'] = serialize($messages);
				}

				return $message;
			}
			else
			{
				return '';
			}

		}
		else
		{
			return '';
		}
	}

	function getSetting($code)
	{
		return dbGetOne('SELECT value FROM #__settings WHERE code = :code',array(':code'=>$code));
	}

	static function getServerProtocol()
	{
		$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https' : 'http';
		return $protocol;
	}

}
