<?php

function random_password($length = 8, $allow_uppercase = true, $allow_numbers = true)
{
    $out = '';
    $arr = array();
    for($i=97; $i<123; $i++) $arr[] = chr($i);
    if ($allow_uppercase) for($i=65; $i<91; $i++) $arr[] = chr($i);
    if ($allow_numbers) for($i=0; $i<10; $i++) $arr[] = $i;
    shuffle($arr);
    for($i=0; $i<$length; $i++)
    {
        $out .= $arr[mt_rand(0, sizeof($arr)-1)];
    }
    return $out;
}



function validateEmail($email)
{
	if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

function printR($obj, $print = false) {
    $str = "<pre>" . print_r($obj,TRUE) . "</pre>";
    if ($print) {
        echo $str;
	} else {
        return $str;
    }
}

function get404()
{
	header("HTTP/1.0 404 Not Found");
	header("HTTP/1.1 404 Not Found");
	header("Status: 404 Not Found");
}

function redirect($new_url)
{
	header('Location: ' . $new_url);
	exit();
}

function redirect301($new_url)
{
	$refer = $_SERVER['QUERY_STRING'];
	if ($refer != '') $refer = '?'.$refer;
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: ' . $new_url .$refer);
	exit();
}

function setUrlParams($add = array(),$rm = array())
{
	$resultUrl = "";
	$url = explode("?",$_SERVER["REQUEST_URI"]);
	$dataParams = array();
	if(isset($url[1]))
	{
		$params = explode("&",$url[1]);
		foreach($params as $current)
		{
			$data = explode("=",$current);
			if(in_array($data[0],$rm))
			{
				continue;
			}
			$dataParams[] = $data[0] . "=" . (isset($data[1]) ? $data[1] : "");
		}
	}
	else
	{
		$resultUrl = $url[0];
	}

	foreach($add as $current)
	{
		$dataParams[] = $current;
	}

	$resultUrl .= (count($dataParams) > 0 ? "?" . implode("&",$dataParams) : "" );

	return $resultUrl != '' ? $resultUrl : $url[0];
}
function ruslat ($string)
{
	$string = mb_eregi_replace("ж","zh",$string);
	$string = mb_eregi_replace("ё","yo",$string);
	$string = mb_eregi_replace("й","i",$string);
	$string = mb_eregi_replace("ю","yu",$string);
	$string = mb_eregi_replace("ь","",$string);
	$string = mb_eregi_replace("ч","ch",$string);
	$string = mb_eregi_replace("щ","sh",$string);
	$string = mb_eregi_replace("ц","c",$string);
	$string = mb_eregi_replace("у","u",$string);
	$string = mb_eregi_replace("к","k",$string);
	$string = mb_eregi_replace("е","e",$string);
	$string = mb_eregi_replace("н","n",$string);
	$string = mb_eregi_replace("г","g",$string);
	$string = mb_eregi_replace("ш","sh",$string);
	$string = mb_eregi_replace("з","z",$string);
	$string = mb_eregi_replace("х","h",$string);
	$string = mb_eregi_replace("ъ","",$string);
	$string = mb_eregi_replace("ф","f",$string);
	$string = mb_eregi_replace("ы","y",$string);
	$string = mb_eregi_replace("в","v",$string);
	$string = mb_eregi_replace("а","a",$string);
	$string = mb_eregi_replace("п","p",$string);
	$string = mb_eregi_replace("р","r",$string);
	$string = mb_eregi_replace("о","o",$string);
	$string = mb_eregi_replace("л","l",$string);
	$string = mb_eregi_replace("д","d",$string);
	$string = mb_eregi_replace("э","ye",$string);
	$string = mb_eregi_replace("я","ja",$string);
	$string = mb_eregi_replace("с","s",$string);
	$string = mb_eregi_replace("м","m",$string);
	$string = mb_eregi_replace("и","i",$string);
	$string = mb_eregi_replace("т","t",$string);
	$string = mb_eregi_replace("б","b",$string);
	$string = mb_eregi_replace("Ё","yo",$string);
	$string = mb_eregi_replace("Й","I",$string);
	$string = mb_eregi_replace("Ю","YU",$string);
	$string = mb_eregi_replace("Ч","CH",$string);
	$string = mb_eregi_replace("Ь","",$string);
	$string = mb_eregi_replace("Щ","SH",$string);
	$string = mb_eregi_replace("Ц","C",$string);
	$string = mb_eregi_replace("У","U",$string);
	$string = mb_eregi_replace("К","K",$string);
	$string = mb_eregi_replace("Е","E",$string);
	$string = mb_eregi_replace("Н","N",$string);
	$string = mb_eregi_replace("Г","G",$string);
	$string = mb_eregi_replace("Ш","SH",$string);
	$string = mb_eregi_replace("З","Z",$string);
	$string = mb_eregi_replace("Х","H",$string);
	$string = mb_eregi_replace("Ъ","",$string);
	$string = mb_eregi_replace("Ф","F",$string);
	$string = mb_eregi_replace("Ы","Y",$string);
	$string = mb_eregi_replace("В","V",$string);
	$string = mb_eregi_replace("А","A",$string);
	$string = mb_eregi_replace("П","P",$string);
	$string = mb_eregi_replace("Р","R",$string);
	$string = mb_eregi_replace("О","O",$string);
	$string = mb_eregi_replace("Л","L",$string);
	$string = mb_eregi_replace("Д","D",$string);
	$string = mb_eregi_replace("Ж","Zh",$string);
	$string = mb_eregi_replace("Э","Ye",$string);
	$string = mb_eregi_replace("Я","Ja",$string);
	$string = mb_eregi_replace("С","S",$string);
	$string = mb_eregi_replace("М","M",$string);
	$string = mb_eregi_replace("И","I",$string);
	$string = mb_eregi_replace("Т","T",$string);
	$string = mb_eregi_replace("Б","B",$string);
	return $string;
}
function create_urlname($name)
{
	$name = htmlspecialchars_decode($name,ENT_QUOTES);
	$name = trim($name);
	$name = str_replace(array("+","_","/","\\","(",")","*",":","'",".",";","`","'"," ","	","#","`","~","+","=","-","*",",","<",">","!","?","@","¶","%","{","}","_","[","]","|","®","©","\"","&","»","«","—"),"!",$name);
	$name = mb_strtolower($name, 'UTF-8');
	$name = ruslat($name);

	$n = array();
	$new_name = explode("!",$name);
	foreach($new_name as $current)
	{
		if($current != "")
		{
			$n[] = $current;
		}
	}

    return implode("-",$n);
}
function getNumEnding($number, $endingArray)
{
    $number = $number % 100;
    if ($number>=11 && $number<=19) {
        $ending=$endingArray[2];
    }
    else {
        $i = $number % 10;
        switch ($i)
        {
            case (1): $ending = $endingArray[0]; break;
            case (2):
            case (3):
            case (4): $ending = $endingArray[1]; break;
            default: $ending=$endingArray[2];
        }
    }
    return $ending;
}

function uploadImage($source,$parentid,$image,$title = "",$description = "")
{
  
	$allowed_extension = array("jpg","png","jpeg","gif");

	if(!empty($image["name"]))
	{
		$ext = explode(".",$image["name"]);
		if(isset($ext[1]) && in_array($ext[1],$allowed_extension))
		{
			$filename = uniqid().".".$ext[1];
			$imageinfo = @getimagesize($image["tmp_name"]);
			if($imageinfo != NULL)
			{
				if(move_uploaded_file($image["tmp_name"],_IMAGES_PATH . $filename))
				{
					$mime = explode("/",$imageinfo["mime"]);
					dbNonQuery("INSERT INTO " . _DB_TABLE_PREFIX . "images (source,parentid,title,description,originalname,filename,width,height,mime) VALUES(:source,:parentid,:title,:description,:originalname,:filename,:width,:height,:mime
					)",array(
						":source"=>$source,
						":parentid"=>$parentid,
						":title"=>$title,
						":description"=>$description,
						":originalname"=>$image["name"],
						":filename"=>$filename,
						":width"=>$imageinfo[0],
						":height"=>$imageinfo[1],
						":mime"=>$mime[1]
					));
				}
			}
		}
	}
}

function uploadFile($id,$originalname,$filename,$value = array('title'=>'','description'=>''),$source = 1)
{
	if(empty($filename)) {  return; }

	$filesize = filesize($filename);
	$format = explode(".",$originalname);
	$strFormat = $format[count($format) - 1];
	$newName = $id . '_' . uniqid() . '.' . $strFormat;

	if(move_uploaded_file($filename,_FILES_PATH . $newName
	))
	{
		$fileid = dbNonQuery(
		'INSERT INTO #__files (source,parentid,title,description,originalname,filename,filesize)
		VALUES(:source,:parentid,:title,:description,:originalname,:filename,:filesize
		)',
		array(
		":source"=>$source,
		":parentid"=>$id,
		":title"=>$value["title"],
		":description"=>$value["description"],
		":originalname"=>$originalname,
		":filename"=>$newName,
		':filesize'=>$filesize
		),TRUE
		);

		return $fileid;

	}

}

function loadFileById($id)
{
	return dbGetRow('SELECT * FROM #__files WHERE id = :id',array(':id'=>$id));
}


function truncateText($string, $limit, $break=" ", $pad="...")
{
  if(strlen($string) <= $limit) return $string;
  $string = strip_tags(htmlspecialchars_decode($string,ENT_QUOTES));
  $string = substr($string, 0, $limit);
  if(false !== ($breakpoint = strrpos($string, $break))) {
    $string = substr($string, 0, $breakpoint);
  }
  return $string . $pad;
}

function genFormID()
{
	return 'form_' . uniqid();
}
function genUniqueID($prefix = '')
{
	return $prefix . uniqid();
}
function getMonthName($index,$type /*short,long*/)
{
	$monthShort = array(
	"1" => "янв",
	"2" => "февр",
	"3" => "марта",
	"4" => "апр",
	"5" => "мая",
	"6" => "июня",
	"7" => "июля",
	"8" => "авг",
	"9" => "сент",
	"10" => "окт",
	"11" => "нояб",
	"12" => "дек");

	$monthFull = array(
	"1" => "января",
	"2" => "февраля",
	"3" => "марта",
	"4" => "апреля",
	"5" => "мая",
	"6" => "июня",
	"7" => "июля",
	"8" => "августа",
	"9" => "сентября",
	"10" => "октября",
	"11" => "ноября",
	"12" => "декабря");

	switch($type)
	{

		case 'short':
			if(isset($monthShort[intval($index)]))
			{
				return $monthShort[intval($index)];
			}
			break;
		case 'long':
			if(isset($monthFull[intval($index)]))
			{
				return $monthFull[intval($index)];
			}
			break;
	}

}
function createYoutubeIframe($url)
{
    if(strripos($url,'youtube'))
    {
       
        $keyVideo=explode('=',$url);
        
    }
    else if(strripos($url,'youtu.be'))
    {
         $keyVideo=explode('/',$url);
        
    }
    $keyVideo=$keyVideo[count($keyVideo)-1];
    $iframe='<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$keyVideo.'" frameborder="0" allowfullscreen></iframe>';
    return $iframe;
}


