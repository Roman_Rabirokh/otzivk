<?php

define('_VERSION','1.3.2'); //13.12.2016
define('_SITE',$_SERVER['HTTP_HOST']);
define('_DIR',$_SERVER['DOCUMENT_ROOT'] . '/');
define('_FILES_PATH',_DIR.'files/');
define('_IMAGES_PATH',_FILES_PATH.'images/');
define('_THUMBS_PATH',_FILES_PATH.'thumbs/');
define('_INCLUDES_PATH',_FILES_PATH.'includes/');
define('_IMAGES_URL','/files/images/');
define('_THUMBS_URL','/files/thumbs/');
define('_FILES_URL','/files/');
define('_TMP_PATH',_FILES_PATH.'tmp/');
define('_TEMPL',_DIR . 'templates/');
define('_TEMPLATE_URL','/templates/');
define('_COMPONENTS',_DIR . '/components/');
define('_MAIL_TEMPLATE',_DIR . '/templates/mail/');
define('_MAIL_TO_LOG',FALSE);
define('_LANG','ru');
define('_CACHE_PATH',_DIR . 'files/cache/');
define('_CACHE_TIME',3600);
define('_ENABLE_CACHING', FALSE);

$modules = array('system/content','system/cron');

include_once('custom.config.php');

$modules[] = 'system/user';
$modules[] = 'system/engine';
