<?php if(!defined("_APP_START")) { exit(); } 
if($user->Authorized())
{
	include("messages.php");
	if(isset($_REQUEST["action"]))
	{
		$result = array("RESULT"=>FALSE,"MESSAGE"=>"");
		
		
		$validateToken = $this->checkToken("rpf",$_REQUEST["token"]);
		$errors = 0;
		if(!$validateToken)
		{
			$result["MESSAGE"] = $MSG["ERROR_TOKEN"];	
			$errors++;	
		}
		else
		{
			$fields = array("old_password","new_password","renew_password");		
			
			foreach($fields as $field)
			{
					if(trim($_REQUEST[$field]) == "")
					{
						$errors++;	
						$result["MESSAGE"] = $MSG["ERROR_EMPTY_FIELD"];	
					}
			}
		
			if($errors == 0)
			{
				if(strlen($_REQUEST["new_password"]) < 7)
				{
					$result["MESSAGE"] = $MSG["PASSWORD_WRONG_LENGTH"];	
					$errors++;						
				}
			}
		
			if($errors == 0)
			{
				if($_REQUEST["new_password"] != $_REQUEST["renew_password"])
				{
					$errors++;	
					$result["MESSAGE"] = $MSG["ERROR_EQUAL_PASSWORD_FIELDS"];		
				}				
			}
			
			if($errors == 0)
			{
				if(!$user->checkPassword($_REQUEST["old_password"]))
				{
					$errors++;	 
					$result["MESSAGE"] = $MSG["ERROR_CURRENT_PASSWORD"];		
				}
				else
				{
					$user->updatePasswordByUser($_REQUEST["new_password"]);
					$result["MESSAGE"] = $MSG["PASSWORD_UPDATE_SUCCESS"];
					$result["RESULT"] = TRUE;					
				}					
			}
		
		}
		
		echo json_encode($result);	
	}
	else
	{
		$data["TOKEN"] = $this->createFormToken("rpf");
		$this->page->addJS($this->getComponentPath($name) . "script.js");
		include("template.php");
	}	
}