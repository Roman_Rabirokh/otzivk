<?php if(!defined("_APP_START")) { exit(); } 
$data = dbQuery("SELECT m.path,cm.vars FROM " . _DB_TABLE_PREFIX . "content_modules as cm 
		INNER JOIN " . _DB_TABLE_PREFIX . "modules m ON m.id = cm.moduleid AND m.publish = 1
		WHERE cm.contentid = :id AND cm.publish = 1 ORDER BY cm.showorder",array(
			":id"=>$params["CONTENT_ID"]
		));
return $data;		