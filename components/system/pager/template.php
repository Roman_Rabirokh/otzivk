<?php 
if (!defined("_APP_START")) {
    exit();
} 
if ($data["PAGECOUNT"] > 1) 
{ ?>
    <ul>
        <?php if ($data["CURRENT_PAGE"] > 1) { ?>
            <li ><a  href="<?= setUrlParams(array(), array("page")) ?>">Первая</a></li>
            <li ><a  href="<?= setUrlParams(array("page=" . ($data["CURRENT_PAGE"] - 1)), array("page")) ?>">&lsaquo;</a></li>
        <?php } ?>
        <?php
        for ($i = 0; $i < $data["INTERVALS"]; $i++) {

            $finish = ($i + 1) * $params["PAGESIZE"] * $data['PAGER_SIZE'];
            $start = $finish - ($params["PAGESIZE"] * $data['PAGER_SIZE']) + 1;
            if (($data["CURRENT_PAGE"] * $params["PAGESIZE"] ) >= $start && $data["CURRENT_PAGE"] * $params["PAGESIZE"] <= $finish) {
                for ($p = 0; $p < $data['PAGER_SIZE']; $p++) {
                    $end_value = $start + ($p * $params["PAGESIZE"]) + $params["PAGESIZE"] - 1;
                    $pageID = ceil($end_value / $params["PAGESIZE"]);


                    if ($i > 0 && $p == 0) {
                        ?>
                        <li ><a href="<?= setUrlParams(array("page=" . ( $pageID - 1)), array("page")) ?>">&laquo;</a></li>
                        <?php
                    }

                    if ($end_value >= $data["ALL"]) {
                        $end_value = $data["ALL"];
                    }
                    ?>
                    <li><a <?= ($data["CURRENT_PAGE"] == $pageID ? "class='active'" : "") ?> href="<?= setUrlParams(array("page=" . $pageID), array("page")) ?>"><?= $pageID ?></a></li>
                    <?php
                    if ($end_value >= $data["ALL"]) {
                        break;
                    }


                    if ((($data['PAGER_SIZE'] - $p) == 1) && (($data["INTERVALS"] - $i) > 1)) {
                        ?>
                        <li ><a href="<?= setUrlParams(array("page=" . ( $pageID + 1)), array("page")) ?>">&raquo;</a></li>
                        <?php
                    }
                }
            }
        }
        ?>
        <?php if ($data["CURRENT_PAGE"] < $data["PAGECOUNT"] && $data["PAGECOUNT"] > 1) { ?>
            <li ><a  href="<?= setUrlParams(array("page=" . ($data["CURRENT_PAGE"] + 1)), array("page")) ?>">&rsaquo;</a></li>
        <?php } ?>
    </ul><?php 
} ?>
