function doLogin()
{
	$("#btnLogin").attr("disabled","disabled");
		$.post("/component/load/system/auth",{
			email:$("#tbEmail").val(),
			passw:$("#tbPassw").val(),
			action:'login'
		},function(data){
			if(data.RESULT)
			{
				//console.log('login '+data.RESULT)
				document.location.href ='/personal/';
			}
			else
			{

				alert("Ошибка авторизации !");
			}
			$("#btnLogin").removeAttr("disabled");
		},"json");
}

$(function(){
	$("#btnLogin").click(function(){
		doLogin();
		return false;
	});
	$("#tbPassw").keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			doLogin();
		}
	});
	$("#tbEmail").keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$("#tbPassw").focus();
		}
	});
	$("#rPassword").click(function(){
		$("#rPassword_block").slideToggle();
		return false;
	});
	$("#btnRestorePassword").click(function(){


		if($("#tbRestoreEmail").val() == "")
		{
			return false;
		}
		$(this).attr("disabled","disabled");

		$.post("/component/load/system/auth",{
			email:$("#tbRestoreEmail").val(),
			action:'restore'
			},function(data){
			if(data.RESULT)
			{

				alert("Данные для восстановления пароля отправлены на указанный Вами E-mail.");

			}
			else
			{
				alert(data.MESSAGE);
			}

			$("#btnRestorePassword").removeAttr("disabled");
		},"json");



	});
});
