<?php 
$MSG = array();
$MSG["TITLE_EMAIL"] = "E-mail";
$MSG["TITLE_PASSWORD"] = "Пароль";
$MSG["TITLE_RESTORE_PASSWORD"] = "Забыли пароль ?";
$MSG["RESTORE_EMAIL"] = "Введите Ваш E-mail";
$MSG["RESTORE_EMAIL_SUBJECT"] = "Восстановление пароля на сайте ";
$MSG["RESTORE_EMAIL_NOT_FOUND"] = "E-mail не найден в базе данных !";
$MSG["RESTORE_EMAIL_ERROR"] = "Введите корректный E-mail";