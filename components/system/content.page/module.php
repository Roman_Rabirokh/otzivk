<?php if(!defined("_APP_START")) { exit(); }
$pageUrl = "";
if(isset($params["URL"]))
{
	$pageUrl = $params["URL"];
}
else
{
	$cache_key = md5($_SERVER["REQUEST_URI"]);
	
	if($_SERVER["REQUEST_URI"] == "" || $_SERVER["REQUEST_URI"] == "/")
	{
		$pageUrl = "index";
		$this->page->isIndex = TRUE;
	}
	else
	{
		$mainPart = explode("?",$_SERVER["REQUEST_URI"]);
		
		if(substr($mainPart[0],0,1) == "/")
		{
			$pageUrl = substr($mainPart[0],1);
		}
		
		
		if(substr($pageUrl,count($pageUrl)-2) == "/")
		{
			$pageUrl = substr($pageUrl,0,count($pageUrl)-2);
		}
	}
}

if($pageUrl == "")
{
	$pageUrl = 'index';
	$this->page->isIndex = TRUE;
    $this->page->classes[] = 'home';          
}

$data = Content::GetByUrl(urldecode($pageUrl));

if(empty($data))
{
	return false;
}
else
{	
	$this->page->meta[] = $data["meta_head"];
	$this->page->meta_title = $data["meta_title"] != "" ? $data["meta_title"] : $data["name"];
	$this->page->meta_description = $data["meta_description"];
	$this->page->meta_keywords = $data["meta_keywords"];
	$this->page->h1 = $data["meta_h1"] != "" ? $data["meta_h1"] : $data["name"];
	$this->page->url = $data["url"];
	$this->page->classes[] = 'content-type-' . $data['content_type'];
	$this->page->classes[] = 'content-' . $data['id'];
	
	$this->page->data = $data;
	

	$path = array();
	
	
	if(!empty($data['content_type_url']))
	{
		$parentPage = Content::GetByUrl($data['content_type_url']);
		$this->page->addPathItem($parentPage['name'],Content::contentUrl($parentPage['url']));
	}
	
	
	Content::getOpenContentTree($data['id'],$path);
	$path = array_reverse($path,TRUE);
	foreach($path as $key=>$value)
	{
		if($key == $data['id'])
		{
			$this->page->addPathItem($value['NAME']);
		}
		else
		{
			$this->page->addPathItem($value['NAME'],$value['LINK']);
		}
	}

	if($data["protected"] == 1 && !$user->Authorized())
	{
		$this->IncludeComponent("system/auth");
	}
	else
	{
		if($this->page->isIndex && file_exists(_TEMPL . "home.php"))
		{
			include(_TEMPL . "home.php");
		}
		else
		{
			if(file_exists(_TEMPL . "content-" . $data["id"] . ".php"))
			{
				include(_TEMPL . "content-" . $data["id"] . ".php");	
			}
			else if(file_exists(_TEMPL . "content-type-" . $data["content_type"] . ".php"))
			{
				include(_TEMPL . "content-type-" . $data["content_type"] . ".php");	
			}
			else
			{
				@include(_TEMPL . "content.php");
			}
		}
	}
	
	return true;
}