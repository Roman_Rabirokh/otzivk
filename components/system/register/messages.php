<?php
$MSG = array();
$MSG["EMAIL_EXISTS"] = "Указанный Вами E-mail уже зарегистрирован в базе данных.";
$MSG["EMAIL_WRONG_LENGTH"] = "Длина пароля меньше 7 символов !";
$MSG["PASSWORD_NOT_REPASSWORD"] = "Поля 'Пароль' и 'Повторите пароль' не совпадают !";
$MSG["SUCCESS_REGISTER"] = "<div>Регистрация успешно завершена !</div><div>Регистрационные данные и информация об активации аккаунта отправлена на указанный Вами E-mail.</div>
<div><a href='/personal/'>Перейти в личный кабинет</a></div>";
$MSG["SUCCESS_ACTIVATED"] = "Ваш аккаунт успешно активирован !";
$MSG["ERROR_EMAIL"] = "Необходимо указать корректный E-mail.";
$MSG["ERROR_CODE"] = "Вы ввели неправильный код картинки.";
$MSG["ERROR_TOKEN"] = "Время сессии истекло. Необходимо обновить страницу.";
$MSG["CAPTION_R_EMAIL"] = "Адрес электронной почты";
$MSG["CAPTION_R_PASSWORD"] = "Пароль";
$MSG["CAPTION_R_REPASSWORD"] = "Повторите пароль";
$MSG["CAPTION_R_IMAGECODE"] = "Введите код на картинке";
