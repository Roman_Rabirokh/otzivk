	<?php if(!defined("_APP_START")) { exit(); }
	if(!empty($data['POSTS_RECALLS']))
	{
		// echo "<pre>"; print_r($data['POSTS_RECALLS']);
		$counter = 0; ?>
                
                <br/>
                <?php
		foreach($data['POSTS_RECALLS'] as $review_item)
		{ ?>
			<div class="block clearfix">
				<div class="col-md-9 col-sm-8 theme-cont">
					<h3><a class="recall-name" href="<?php echo Content::contentUrl($review_item['url']); ?>"><?php echo truncateText(htmlspecialchars_decode($review_item['name']), 250); ?> </a></h3>
				</div>
				<div class="col-md-3 col-sm-3"><h4><?php echo $review_item['date']; ?></h4></div>
                                
                                <div class="col-md-12 col-sm-12 line">
                                    <div class="stars">Рейтинг темы&nbsp;<?php
					if(!empty($review_item['rating']))
					{
						for($i = 1; $i <= round($review_item['rating']); $i++)
						{ ?>
						<img src="<?php echo $this->page->getTemplateUrl(); ?>img/star1_active.png" alt=""><?php
						}
					} ?>
					</div>
                                </div>
				<div class="col-md-2 topic_profile"><?php
				if(!empty($review_item['images'][0]))
				{ ?>
                                    <a href="/<?=$review_item['url']?>/"><img class="image-recall-profile" src="<?php echo $review_item['images'][0]; ?>" alt=""> </a><?php
				}
				else
				{ ?>
                                    <a href="/<?=$review_item['url']?>/"><img class="image-recall-profile" src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""></a>    <?php
				} ?>
					
					
				</div>
				<div class="col-md-10 theme-new-p-redact"><?php
					if(!empty($review_item['detail_text']))
					{ ?>
						<p><a href="<?php echo Content::contentUrl($review_item['url']); ?>"><?php echo truncateText(htmlspecialchars_decode($review_item['detail_text']), 250); ?></a<</p><?php
					}
					else
					{ ?>
						<p>Текст отзыва отсутствует</p><?php
					}
					if(!empty($review_item['images']))
					{
						$count_img = 0;
						foreach($review_item['images'] as $img)
						{
                                                    if($count_img!=0)
                                                    {
                                                        if($count_img > 4) break; ?>
                                                            <a href="javascript:void(0);"><img src="<?php echo $img; ?>" alt=""></a><?php
                                                        
                                                    }
                                                    $count_img++;
						}
						if($review_item['more_fotos'] > 0)
						{ ?>
						<span>+ <?php echo $review_item['more_fotos']; ?> фото</span><?php
						}
					}
					//else
					//{ ?>
						<!--<p><img height="50" width="50" src="<?php //echo _IMAGES_URL; ?>no-foto.png" alt=""></p> --> <?php
					//} ?>
				</div>
                                <div class="stars stars-list-recall new-recal-stars">Рейтинг отзыва &nbsp;<?php
					if(!empty($review_item['rating-recall']))
					{
						for($i = 1; $i <= round($review_item['rating-recall']); $i++)
						{?>
						<img src="<?php echo $this->page->getTemplateUrl(); ?>img/star1_active.png" alt=""><?php
						}
					} ?>
					</div>
			</div><?php
			if($counter == 0 && !empty($params['CATEGORY']))
			{ ?>
				<?php echo getBannerBlockById(7); ?><?php
			}
			$counter++;
		}
 ?>
		<div class="col-md-12 col-sm-12 col-xs-12 pagination">
			<?php $this->includeComponent("system/pager",array("ALL"=>$data['ALL'],"PAGESIZE"=> $data["PAGESIZE"])); ?>
		</div><?php

	}
	else
	{ ?>
		<?php echo getBannerBlockById(7); ?><?php
	}	?>
