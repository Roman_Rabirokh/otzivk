<?php if(!defined("_APP_START")) { exit(); }
   	if(!empty($params['CATEGORY']) || !empty($params['LAST']))
	{
            
		$sort = array();
		$filters = array();
		$sqlParams = array();
		$allCount = 0;

		if(!empty($params['CATEGORY']))
		{
			$pagesize = 10;
			$currentPage = isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;
			if($currentPage < 1)
			{
				$currentPage = 1;
			}

			$start = ($currentPage - 1) * $pagesize;
			$filters['FIELDS']['1'] = intval($params['CATEGORY']);
		}

		if(!empty($params['LAST']))
		{
			$sort = array('c.content_date' => 'DESC');
			$start = 0;
			$pagesize = 4;
		}
		$fields = array('c.id', 'c.name', 'c.userid', 'c.content_date', 'c.detail_text', 'cd.f1 AS category', 'c.mainimage', 'c.url', 'cd.f3 AS rating');
		$data['POSTS_RECALLS'] = Content::GetList(3,$fields,$filters,$sort,array('OFFSET'=>$start,'ROWS'=>$pagesize,'COUNT_ALL'=>TRUE),$allCount,$sqlParams);

		foreach($data['POSTS_RECALLS'] as &$recall)
		{
			$recall['avatar'] = getImageById($recall['mainimage']);
			$recall['user_nickname'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $recall['userid']));
			$recall['avatar'] = dbGetOne('SELECT profile_foto FROM #__profiles WHERE userid = :uid', array(':uid' => $recall['userid']));
			$date = date_parse($recall['content_date']);
			$month = getMonthName($date['month'],'long');
			$recall['date'] = $date['day'].' '.$month.' '.$date['year']. ' года';
			$recall['images_ids'] = dbQueryToArray('SELECT id FROM #__images WHERE parentid = :pid',
										array(':pid' => $recall['id'] ));

			$recall['images'] = array();
			foreach($recall['images_ids'] as $image)
			{
				if($image != $recall['mainimage'])
				{
					//$img = getImageById($image, array('height' => 50, 'width' => 50));
					$img = getImageById($image,array('width'=>80,'crop'=>array(0,0,50,50)));

					$recall['images'][] = $img;
				}
			}

			$recall['more_fotos'] = count($recall['images']) - 5;
                        $recall['rating-recall']= dbGetOne('SELECT SUM(measuring)/COUNT(measuring) as rating FROM #__measurings_review WHERE review_id = :id', [':id' =>$recall['id'] ]);
		}
                unset($recall);
		$data['ALL'] = $allCount;
		if(!empty($params['LAST']))
		{
			$data['ALL'] = 4;
		}
		$data['PAGESIZE'] = $pagesize;
		//echo "<pre>"; print_r($data['POSTS_RECALLS']); die;
		include 'template.php';
	}
	else
	{
		echo "Не корректный запрос";
	}
