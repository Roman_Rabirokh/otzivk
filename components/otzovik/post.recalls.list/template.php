	<?php if(!defined("_APP_START")) { exit(); }
	if(!empty($data['POSTS_RECALLS']))
	{
		// echo "<pre>"; print_r($data['POSTS_RECALLS']);
		$counter = 0;
		foreach($data['POSTS_RECALLS'] as $review_item)
		{ ?>
			<div class="block clearfix">
				<div class="col-md-9 col-sm-8">
					<h3><a class="recall-name" href="<?php echo Content::contentUrl($review_item['url']); ?>"><?php echo truncateText(htmlspecialchars_decode($review_item['name']), 150); ?></a></h3>
				</div>
				<div class="col-md-3"><h4><?php echo $review_item['date']; ?></h4></div>
                                
                                <div class="col-md-12 line">
                                    <div class="stars">Оценка предмета темы&nbsp;<?php
					if(!empty($review_item['rating']))
					{
						for($i = 1; $i <=5 ; $i++)
						{ 
                                                    if($i <= round($review_item['rating'], 1, PHP_ROUND_HALF_UP))
                                                    { ?>
                                                        <img style="width:12px;" src="<?php echo $this->page->getTemplateUrl(); ?>img/or-star.png" alt=""><?php
                                                    }
                                                    else
                                                    { ?>
                                                        <img style="width:12px;" src="<?php echo $this->page->getTemplateUrl(); ?>img/ow-star.png" alt=""><?php
                                                    }
						}
					} ?>
					</div>
                                </div>
				<div class="col-md-2 topic_profile"><?php
				if(!empty($review_item['avatar']))
				{ ?>
					<img class="avatar" src="<?php echo $review_item['avatar']; ?>" alt=""><?php
				}
				else
				{ ?>
					<img class="avatar" src="<?php echo _IMAGES_URL; ?>nofoto.png" alt=""><?php
				} ?>
					<h5><a href="/user/<?php echo $review_item['userid']; ?>/"><?php if(!empty($review_item['user_nickname'])) echo $review_item['user_nickname']; else echo "Инкогнито"; ?></a></h5>
					
				</div>
				<div class="col-md-10"><?php
					if(!empty($review_item['detail_text']))
					{ ?>
						<p class="textp-line-topic"><?php echo truncateText(htmlspecialchars_decode($review_item['detail_text']), 250); ?></p><?php
					}
					else
					{ ?>
						<p>Текст отзыва отсутствует</p><?php
					}
					if(!empty($review_item['images']))
					{
						$count_img = 0;
						foreach($review_item['images'] as $img)
						{
						if($count_img > 4) break; ?>
						<a href="javascript:void(0);"><img src="<?php echo $img; ?>" alt=""></a><?php
						$count_img++;
						}
						if($review_item['more_fotos'] > 0)
						{ ?>
						<span>+ <?php echo $review_item['more_fotos']; ?> фото</span><?php
						}
					}
					else
					{ ?>
						<p class="foto-img-recall-topic"><img height="50" width="50" src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""></p><?php
					} ?>
				</div>
                                    <div class="stars stars-list-recall"><?php
                                    if(!empty($review_item['rating-recall']))
                                    { ?>
                                        <span>Рейтинг отзыва &nbsp;</span><?php
                                        for($i = 1; $i <= 5; $i++)
                                        {
                                            if($i <= round($review_item['rating-recall'], 1, PHP_ROUND_HALF_UP)) 
                                            { ?>
                                                <img style="width:12px;" src="<?php echo $this->page->getTemplateUrl(); ?>img/or-star.png" alt=""><?php
                                            }
                                            else
                                            {
                                                if($i - round($review_item['rating-recall'], 1, PHP_ROUND_HALF_UP) < 1)
                                                { ?>
                                                    <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/hp-star.png" alt=""><?php
                                                }
                                                else
                                                { ?>
                                                    <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/ow-star.png" alt=""><?php  
                                                }
                                                
                                            }
                                        }
                                    } ?>
                                    </div>
			</div><?php
			if($counter == 0 && !empty($params['CATEGORY']))
			{ ?><?php echo getBannerBlockById(7); ?><?php
			}
			$counter++;
		}
		if(!empty($params['CATEGORY']))
		{ ?>
		<div class="col-md-12 col-sm-12 col-xs-12 pagination">
			<?php $this->includeComponent("system/pager",array("ALL"=>$data['ALL'],"PAGESIZE"=> $data["PAGESIZE"])); ?>
		</div><?php
		}
	}
	else
	{ ?>
		<?php echo getBannerBlockById(7); ?><?php
	}	?>
