<?php
if (!defined("_APP_START"))
{
    exit();
}
$token = $this->createFormToken('pf');
global $app;
?>
<iframe name="pf-info" style="width: 100%; height: 200px;display:none;"></iframe>
<div class="row personal_area profile profile_edit">
    <form enctype="multipart/form-data" action="/redact/profile/" method="post" class="form" target="pf-info" id="pf-edit">
    <div class="box">
        <div class="block">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 profile-avatar" style="position:relative;">
                    <input style="display:none;" onChange="uploadImg(this);" type="file" name="avatar_profile" />
                        <i class="fa fa-upload" aria-hidden="true"></i>
                            <div class="avatar_foto_wrapper"><?php
                        if(!empty($data['PROFILE_INFO']['upload_foto_src']))
                        { ?>
                            <img height="190" width="190" src="<?php echo $data['PROFILE_INFO']['upload_foto_src']; ?>" alt=""><?php
                        }
                        else
                        {
                            if (!empty($data['PROFILE_INFO']['profile_foto']))
                            {
                                ?>
                                <img height="190" width="190" src="<?php echo $data['PROFILE_INFO']['profile_foto']; ?>" alt=""><?php
                            } else
                            {
                                ?>
                                <img src="<?php echo _TEMPLATE_URL; ?>img/profile_img1.png" alt=""><?php 
                                
                            }
                            
                        } ?>
                            </div>
                    
                    
                </div>
                
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h3><?php if (!empty($data['PROFILE_INFO']['name'])) { echo $data['PROFILE_INFO']['name'];}
                    else { echo 'Инкогнито'; } ?></h3>
                        <iframe style="width:100%;height:150px;display:none;" name="pf-info" ></iframe>
                        <div id="registerMessages"></div>

                        <div class="form-group">
                            <input type="hidden" name="token" value="<?php echo $token; ?>" >
                            <input type="hidden" name="pf_id" value="<?php if (!empty($data['PROFILE_INFO']['id'])) {echo $data['PROFILE_INFO']['id'];} ?>" >
                            <input type="text" placeholder="Ваше имя" name="user_name" class="form-input" value="<?php if (!empty($data['PROFILE_INFO']['name'])) { echo $data['PROFILE_INFO']['name'];} ?>">
                        </div>
                        <div class="form-group">
                            <input type="email" <?php if (!empty($data['PROFILE_INFO']['user']['email'])) { echo 'readonly'; }
                        else { echo 'placeholder="Ваш e-mail" name="user_email"'; } ?>  class="form-input" value="<?php if (!empty($data['PROFILE_INFO']['user']['email'])) { echo $data['PROFILE_INFO']['user']['email']; } ?>" >
                            <span id="error-user-email" ></span>
                        </div>
<?php
if (empty($data['PROFILE_INFO']['user']['social_code']))
{
    ?>
                            <form class="form"> <?php $app->includeComponent('system/change.password'); ?>
                            </form>
    <?php }
?>
                    </div>
                    <div class="col-md-3 col-sm-2 col-xs-12">
                        <a href="javascript:void(0);" id="save-pf" class="amend">Сохранить изменения</a>
                    </div><?php
if (empty($data['PROFILE_INFO']['user']['social_code']))
{
    ?>
                        <div class="col-md-3 col-sm-2 col-xs-12">
                            <a href="javascript:void(0);" id="change-psw" class="amend">Изменить пароль</a>
                        </div><?php }
?>
            </div>
        </div>
        <div class="block">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4>Страна: <input type="text" name="country" value="<?= $data['PROFILE_INFO']['country'] ?>"></h4>
                    <h4>Город: <input type="text" name="city" value="<?= $data['PROFILE_INFO']['city'] ?>"></h4>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4>Отзывы: <span>2</span></h4>
                    <h4>Комментарии: <span>0</span></h4>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4>Регистрация: <span>2.08.2016</span></h4>
                    <h4>Рейтинг: <span>+21</span></h4>
                </div>
            </div>
        </div>
        </form>
        <div class="block">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4>Мои подписки:</h4>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div id="block-sbs"></div><?php
                        if (!empty($data['SUBSCRIBES_TYPE']))
                        {
                            ?>
                        <form class="delivery" id="personal-subscribes" method="post" target="edit-pf-sbs" action="/subscribe/edit/personal/"><?php
                        $counter = 1;
                        foreach ($data['SUBSCRIBES_TYPE'] as $subscribe)
                        {
                            ?>
                                <input <?php if (!empty($data['SUBSCRIBES_PROFILE']) && in_array($subscribe['id'], $data['SUBSCRIBES_PROFILE'])) { echo 'checked'; } ?> type="checkbox" value="<?php echo $subscribe['id']; ?>" name="checkboxG<?php echo $counter; ?>" id="checkboxG<?php echo $counter; ?>" class="css-checkbox" disabled="disabled" />
                                <label for="checkboxG<?php echo $counter; ?>" class="css-label radGroup"></label>
                                <p><?php echo $subscribe['title']; ?></p><?php
                            $counter++;
                        }
                        ?>
                            <input type="hidden" name="token" value="<?php echo $this->createFormToken('se'); ?>" />
                        </form><?php }
                    ?>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 manage-subscribe">
                    <a href="javascript:void(0);" id="edit-subcribe" class="manage_subscriptions">Управление подписками</a>
                </div>
            </div>
        </div>
    </div>
</div>
