	<?php if(!defined("_APP_START")) { exit(); }
	global $user;
	$this->page->addJS($this->getComponentPath($name) . "script.js");
	$this->page->meta_title = 'Редактирование профиля';

	if($user->Authorized())
	{
		$userid = $user->getID();
		$data['PROFILE_INFO'] = dbGetRow('SELECT id, name,city, country, profile_foto, upload_foto FROM #__profiles WHERE userid = :uid',
																	array(
																		':uid' => $userid
																	));
		$data['PROFILE_INFO']['user'] = dbGetRow('SELECT email, social_code FROM #__users WHERE id = :id', array(':id' => $userid));
		$data['PROFILE_INFO']['upload_foto_src'] = getImageById($data['PROFILE_INFO']['upload_foto']);

// echo "<pre>"; print_r($data['PROFILE_INFO']); die;

		$data['SUBSCRIBES_TYPE'] = dbQuery('SELECT * FROM #__subscribe_types');
		$data['SUBSCRIBES_PROFILE'] = dbQueryToArray('SELECT subcribe_id FROM #__subscribes_profile WHERE userid = :uid', array(':uid' => $userid));

		include 'template.php';
	}
