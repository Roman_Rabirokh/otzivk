$(document).ready(function(){
    
    $('.profile-avatar').find('img').click(function(){
       $('input[name="avatar_profile"]').click(); 
    });
    
     $('.profile-avatar').find('.fa-upload').click(function(){
       $('input[name="avatar_profile"]').click(); 
    });
    
//    $('input[name="avatar_profile"]').change(function(){
//        console.log('upload foto');
//    });
    
    
    $('.profile-avatar').mouseover(function(){
        $('.profile-avatar').find('.fa-upload').css('display', 'block');
    }).mouseleave(function(){
        $('.profile-avatar').find('.fa-upload').css('display', 'none');
    });
	$('#save-pf').click(function(){
		var form = $('#pf-edit');

		var name = $('input[name="user_name"]').val();
		var email = $('input[name="user_email"]').val();
		if(email == '')
		{
			$('input[name="readonly"]').val('0');
		}
		else
		{
			$('input[name="readonly"]').val('1');
		}
		if(name == "")
		{
			$('input[name="user_name"]').attr('placeholder', 'Введите своё имя');
			$('input[name="user_name"]').css({
																				border:'1px solid red',
																				color: 'red'
																			});
		}
		else
		{
			form.submit();
		}
	});

	$('#change-psw').click(function(){
		$('#btnUpdatePassword').click();
	});

	$('#edit-subcribe').click(function(){
		var edit = '<a href="javascript:void(0);" onclick="saveInfoSubscribes()" id="save-subcribe" class="manage_subscriptions" >Изменить</a>';
		$('.manage-subscribe').html(edit);
		$('#personal-subscribes').find('input[type="checkbox"]').each(function(){
			$(this).removeAttr('disabled');
		});
		var submit = '<input type="submit" id="sendSubscribes" style="display:none"/>';
		$('#personal-subscribes').append(submit);
    $('#block-sbs').append('<iframe name="edit-pf-sbs" style="height:150px;width:100%;display: none;" ></iframe>');
	});
});

function saveInfoSubscribes()
{
	$('#sendSubscribes').click();
}

function uploadImg(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var preview = $('.profile-avatar').find('img');
        console.log(preview.attr('src'));
        //console.log(preview);

        reader.onload = function(e) {

           //console.log(e.target.result);
            preview.attr('src', e.target.result);

            //var img_block  = '<div class="col-md-3"><label for="description" >Загруженное фото</label></div>';
            //var img_block = '<img style="border-radius:50%;" src="' +  e.target.result + '" />';

            //$('.avatar_foto_wrapper').html('');
            //$('.avatar_foto_wrapper').html(img_block);

        };

        reader.readAsDataURL(input.files[0]);
    }
 }

