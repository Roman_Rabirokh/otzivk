	<?php if(!defined("_APP_START")) { exit(); } ?>
	<div class="row personal_area profile_subscribes"><?php
	if(!empty($data['PROFILE_USER_ON']))
	{ ?>
		<div class="box"><?php
		foreach($data['PROFILE_USER_ON'] as $subscr)
		{ ?>
			<div class="block">
				<div class="row">
					<div class="col-md-2 col-sm-2 col-xs-12"><?php
                                        if(!empty($subscr['profile_foto']))
                                        { ?>
                                            <a href="/user/<?php echo $subscr['id']; ?>/"><img height="100" src="<?php echo $subscr['profile_foto']; ?>" alt="" /></a><?php
                                        }
                                        else
                                        { ?>
                                            <img height="100" src="<?php echo _IMAGES_URL; ?>nofoto.png" alt="" /><?php
                                        } ?>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
                                            <h3><a style="text-align: left;" href="/user/<?php echo $subscr['id']; ?>/" ><?php echo $subscr['name']; ?></a></h3>
						<h4><?php
						if(!empty($subscr['qnt_days']))
						{ ?> На сайте: <span></span> <?php GetPeriodOnSite($subscr['qnt_days']); } else echo "Пользователь на сайте с сегодняшнего дня"; ?></h4>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<a href="/form/message/<?php echo $subscr['id']; ?>/" class="send_message">Отправить сообщение</a>
						<div class="block-btn-pfpage">
						<a href="javascript:void(0);" data-uid="<?php echo $subscr['id']; ?>" onclick="subscribe_off(this, true);" class="unfollow">Отписаться</a>
					</div>
					</div>
				</div>
			</div><?php
		} ?>
		</div><?php
	}
	else
	{ ?>
		<h3>У вас нет подписок.</h3><?php
	} ?>
	</div>
