	<?php if(!defined("_APP_START")) { exit(); }
	global $user;

	if($user->Authorized())
	{
		$userid = $user->getID();
		$ids_user_on = dbQueryToArray('SELECT userid_subscr FROM #__subscribes_on_user WHERE userid = :uid', array(':uid'=> $userid));
		$data['PROFILE_USER_ON'] = array();
		foreach ($ids_user_on as &$uid)
		{
			$data['PROFILE_USER_ON'][] = dbGetRow('SELECT u.created, u.id, DATEDIFF(NOW(), u.created) AS qnt_days, p.name, p.profile_foto FROM #__users u
                                                            LEFT JOIN #__profiles p ON p.userid = u.id
                                                            WHERE u.id = :uid',
                                                                    array(
                                                                            ':uid' => $uid
                                                                    ));
		}
		//echo "<pre>"; print_r($data['PROFILE_USER_ON']);
		include 'template.php';
	}
