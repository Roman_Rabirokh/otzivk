	<?php if(!defined("_APP_START")) { exit(); } ?>
	<div class="row personal_area profile_draft"><?php
	if(!empty($data['REVIEW_DRUFTS']))
	{
		foreach($data['REVIEW_DRUFTS'] as $draft)
		{ ?>
		<div class="block clearfix">
			<div class="col-md-9 col-sm-8">
				<a href="/personal/drafts/<?php echo $draft['id']; ?>/"><h2><?php if(!empty($draft['name'])) echo $draft['name']; ?></h2>
			</div>
			<div class="col-md-3"><h3><?php echo $draft['date'] ?></h3></div>
			<div class="col-md-12 line"></div>
			<div class="col-md-12">
				<p><?php if(!empty($draft['detail_text'])) echo  truncateText(htmlspecialchars_decode($draft['detail_text']), 100); ?></p>
			</div>
			<div class="col-md-12"><?php
			if(!empty($draft['images']))
			{
				$count_img = 0;
				foreach($draft['images'] as $img)
				{
				if($count_img > 4) break; ?>
				<a href="javascript:void(0);"><img src="<?php echo $img; ?>" alt=""></a><?php
				$count_img++;
				}
				if($draft['more_fotos'] > 0)
				{ ?>
				<span>+ <?php echo $draft['more_fotos']; ?> фото</span><?php
				}
			}
			else
			{ ?>
				<p><img class="full-img-l" height="50" width="50" src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""></p><?php
			} ?>
			</div>
		</div><?php
		}
	}
	else
	{ ?>
		<h4 style="margin-bottom:20px;" >У вас нет отзывов в черновиках.</h4><?php
	} ?>
	</div>
