	<?php if(!defined("_APP_START")) { exit(); }
	global $user;

	if($user->Authorized())
	{
		$userid = $user->getID();
		$data['REVIEW_DRUFTS'] = dbQuery('SELECT c.id, c.name, c.detail_text, c.mainimage, c.content_date, c.url, cd.f1 AS topic_id, cd.f8 AS recommend, cd.f9 AS youtube, cd.f3 AS rating FROM #__content c
		                                  LEFT JOIN #__content_data_3 cd ON cd.id = c.id
																			WHERE c.userid = :uid AND c.active = 0',
																				array(
																					':uid' => $userid
																				));
		if(!empty($data['REVIEW_DRUFTS']))
		{
			foreach($data['REVIEW_DRUFTS'] as &$recall)
			{
				$date = date_parse($recall['content_date']);
				$month = getMonthName($date['month'],'long');
				$recall['date'] = $date['day'].' '.$month.' '.$date['year']. ' года';
				$recall['images_ids'] = dbQueryToArray('SELECT id FROM #__images WHERE parentid = :pid',
											array(':pid' => $recall['id'] ));
				$recall['images'] = array();
				foreach($recall['images_ids'] as $image)
				{
					if($image != $recall['mainimage'])
					{
						$img = getImageById($image, array('width'=>200,'crop'=>array(0,0,50,50)));
						$recall['images'][] = $img;
					}
				}
				$recall['more_fotos'] = count($recall['images']) - 5;
			}

		}
		//echo "<pre>"; print_r($data['REVIEW_DRUFTS']); die;
		include 'template.php';
	}
