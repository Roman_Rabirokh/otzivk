$(document).ready(function(){
  $('#sendFromChat').click(function(){
    var text_msg = $('textarea[name="msg_from_chat_text"]').val();
    var to_user = $('input[name="user_to"]').val();
    var token = $('input[name="token"]').val();
    //console.log(to_user)
    if(text_msg == '')
    {
      $('textarea[name="msg_from_chat_text"]').attr('placeholder', 'Введите текст сообщеия');
      return false;
    }
    else
    {
      $.ajax({
        url: '/messages/chat/answer/',
        method: 'POST',
        data: {
          user_to: to_user,
          text_msg: text_msg,
          token: token
        },
        success: function(data){
          console.log(data)
          if(data)  window.location.reload();
        }
      });
    }
  });
});
