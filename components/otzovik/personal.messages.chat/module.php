<?php if(!defined("_APP_START")) { exit(); }
global $user;
$this->page->addJS($this->getComponentPath($name) . "script.js");

if($user->Authorized() && !empty($params['USER_FROM']))
{
  $user_from = $params['USER_FROM'];
  $user_to = $user->getID();
//  echo $user_to;
  $data['MESSAGES_CHAT'] = dbQuery('SELECT * FROM #__users_messages
                                    WHERE (userid_to = :uid AND userid_from = :user_from) OR (userid_to = :user_from_to AND userid_from = :user_to)
                                    ORDER BY created',
                                    array(
                                      ':uid' => $user_to,
                                      ':user_from_to' => $user_from,
                                      ':user_to' => $user_to,
                                      ':user_from' => $user_from
                                   ));

  if(!empty($data['MESSAGES_CHAT']))
  {
    foreach($data['MESSAGES_CHAT'] as &$item_chat)
    {

  		$date_msg = date_parse($item_chat['created']);
      $month = getMonthName($date_msg['month'],'long');
      $item_chat['date'] = $date_msg['day'].' '.$month.' '.$date_msg['year']. ' года';
      $item_chat['user_name'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $item_chat['userid_from']));
    }
  }
// echo "<pre>"; print_r($data['MESSAGES_CHAT']); die;


  include "template.php";
}
