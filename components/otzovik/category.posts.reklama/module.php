	<?php if(!defined("_APP_START")) { exit(); }

	if(!empty($params['CATEGORY']))
	{
		$sort = array();
		$filters = array();
		$sqlParams = array();
		
		$fields = array('c.id', 'c.name', 'cd.f4 AS site', 'cd.f5 AS price', 'cd.f6 AS link');
		$data['REKLAMA_GOODS'] = Content::GetList(5,$fields,$filters,$sort,array('OFFSET'=>0,'ROWS'=>3,'COUNT_ALL'=>TRUE),$allCount,$sqlParams);
		
		include 'template.php';
	}
	else
	{
		echo "Нет данных для вывода";
	}
