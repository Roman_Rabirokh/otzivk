$(document).ready(function(){
  $('.del-bookmark-lk').click(function(){
    var tid = $(this).attr('data-id');
    if(tid != '')
    {
      $.ajax({
        url: '/bookmark/del/topic/',
        method: 'POST',
        data:{
          tid: tid
        },
        success: function(data){
            window.location.reload();
        }
      });
    }
  });
});
