	<?php if(!defined("_APP_START")) { exit(); }
	$this->page->addJS($this->getComponentPath($name)."script.js");
	global $user;

	if($user->Authorized())
	{
		$userid = intval($user->getID());

		$data['USER_BOOKMARKS'] = dbQueryToArray('SELECT topic_id FROM #__users_bookmarks WHERE userid = :uid', array(':uid' => $userid));

		$data['BOOKMARKS_IDS'] = implode($data['USER_BOOKMARKS'], ',');

		include 'template.php';
	}
