	<?php if(!defined("_APP_START")) { exit(); } 
	if(!empty($data['POSTS_RECALLS']))
	{ 
		$counter = 0;
		foreach($data['POSTS_RECALLS'] as $review_item)
		{ ?>
			<div class="block clearfix">
				<div class="col-md-9 col-sm-8">
					<h3><a href="<?php echo Content::contentUrl($review_item['url']); ?>"><?php echo $review_item['name']; ?></a></h3>
				</div>
				<div class="col-md-3"><h4><?php echo $review_item['date']; ?></h4></div>
				<div class="col-md-12 line"></div>
				<div class="col-md-2 topic_profile"><?php
				if(!empty($review_item['avatar']))
				{ ?>
					<img class="avatar" src="<?php echo $review_item['avatar']; ?>" alt=""><?php
				}
				else
				{ ?>
					<img class="avatar" src="<?php echo _IMAGES_URL; ?>nofoto.png" alt=""><?php
				} ?>
					<h5><?php if(!empty($review_item['user_nickname'])) echo $review_item['user_nickname']; else echo "Инкогнито"; ?></h5>
					<div class="stars"><?php 
					if(!empty($review_item['rating']))
					{ 
						for($i = 1; $i <= round($review_item['rating']); $i++)
						{ ?>
						<img src="<?php echo $this->page->getTemplateUrl(); ?>img/star1_active.png" alt=""><?php
						}
					} ?>
					</div>
				</div>
				<div class="col-md-10"><?php
					if(!empty($review_item['detail_text']))
					{ ?>
						<p><?php echo truncateText(htmlspecialchars_decode($review_item['detail_text']), 250); ?></p><?php
					}
					else
					{ ?>
						<p>У этого товара пока нет отзывов</p><?php
					}
					if(!empty($review_item['images']))
					{
						$count_img = 0;
						foreach($review_item['images'] as $img)
						{ 
						if($count_img > 4) break; ?>
						<a href="javascript:void(0);"><img height="50" width="50" src="<?php echo $img; ?>" alt=""></a><?php
						$count_img++;
						}
						if($review_item['more_fotos'] > 0)
						{ ?>
						<span>+ <?php echo $review_item['more_fotos']; ?> фото</span><?php
						}
					}
					else
					{ ?>
						<p><img height="50" width="50" src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""></p><?php
					} ?>
				</div>
			</div><?php
		} 
	}
	else
	{ ?>
		<?php //echo getBannerBlockById(7); ?><?php
	}	?>
