<?php if(!defined("_APP_START")) { exit(); }

	if(!empty($params['PID']))
	{
		$sort = array();
		$filters = array();
		$sqlParams = array();

		$filters['FIELDS']['1'] = intval($params['PID']);
		$filters["c.id != (".$params['RID'].")"] = "";
		$sort['c.content_date'] = 'DESC'; 
		$fields = array('c.id', 'c.name', 'c.userid', 'c.content_date', 'c.detail_text', 'cd.f1 AS category', 'c.mainimage', 'c.url', 'cd.f3 AS rating');
		$data['POSTS_RECALLS'] = Content::GetList(3,$fields,$filters,$sort,array('OFFSET'=>0,'ROWS'=>2,'COUNT_ALL'=>TRUE),$allCount,$sqlParams);

		foreach($data['POSTS_RECALLS'] as &$recall)
		{
			$recall['avatar'] = getImageById($recall['mainimage']);
			$recall['user_nickname'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $recall['userid']));
			$date = date_parse($recall['content_date']);
			$month = getMonthName($date['month'],'long');
			$recall['date'] = $date['day'].' '.$month.' '.$date['year']. ' года';
			$recall['images_ids'] = dbQueryToArray('SELECT id FROM #__images WHERE parentid = :pid',
										array(':pid' => $recall['id'] ));
				
			$recall['images'] = array();
			foreach($recall['images_ids'] as $image)
			{
				if($image != $recall['mainimage'])
				{
					$img = getImageById($image, array('height' => 50, 'width' => 50));
					$recall['images'][] = $img;
				}
			}
			
			$recall['more_fotos'] = count($recall['images']) - 5;
		}
		//echo "<pre>"; print_r($data['POSTS_RECALLS']); die;
		include 'template.php';
	}
	else
	{
		echo "Не корректный запрос";
	}
