	<?php if(!defined("_APP_START")) { exit(); }

	if(!empty($params['CATEGORY']))
	{
		$sort = array();
		$filters = array();
		$sqlParams = array();
		
		$filters['FIELDS']['2'] = intval($params['CATEGORY']);
		$filters["c.id != (".$params['PID'].")"] = "";
		$fields = array('c.id', 'c.name', 'c.userid', 'c.preview_text', 'c.detail_text', 'c.content_date', 'c.mainimage, c.url');
		$sort['c.content_date'] = 'DESC'; 
		$data['VIEW_ALSO'] = Content::GetList(4,$fields,$filters,$sort,array('OFFSET'=>0,'ROWS'=>2,'COUNT_ALL'=>TRUE),$allCount,$sqlParams);
		if(!empty($data['VIEW_ALSO']))
		{
			foreach($data['VIEW_ALSO'] as &$item)
			{
				$item['user_nickname'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $item['userid']));
				$item['main_image'] = getImageById($item['mainimage']);
				
                                // данные о последнем отзыве
                                $item['last-review']= dbGetRow('SELECT c.* FROM #__content AS c '
                                        . ' INNER JOIN #__content_data_3 AS f ON(f.id=c.id) '
                                        . ' WHERE f.f1= :id AND c.content_type=3 '
                                        . ' ORDER BY c.created DESC ', [':id' =>$item['id'] ]);
                                $item['last-review']['detail_text']=mb_strimwidth( strip_tags($item['last-review']['detail_text']), 0, 150, "...");
                                if(!empty($item['last-review']['id']))
                                    $images_ids = dbQueryToArray('SELECT id FROM #__images WHERE parentid = :pid',
                                                                                                                                                            array(':pid' => $item['last-review']['id']));
				$item['images'] = array();
                                
				if(!empty($images_ids))
				{
					foreach($images_ids as $img_id)
					{
						if($img_id != $item['mainimage'])
						{
							$item['images'][] = getImageById($img_id);
						}
					}
				}
				
				$item['more_fotos'] = count($item['images']) - 5;
				$item['recalls'] = dbGetRow('SELECT COUNT(c.id) AS qnt, AVG(cd.f3) AS average FROM #__content c
																				 LEFT JOIN #__content_data_3 cd ON cd.id = c.id
																				 WHERE c.content_type = 3 AND cd.f1 = :pid',
																					array(
																						':pid' => $item['id']
																					));
			}
                        unset($item);
		}
		
		include 'template.php';
	}
	else
	{
		echo "Не корректный запрос";
	}