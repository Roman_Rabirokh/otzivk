	<?php if(!defined("_APP_START")) { exit(); } 
	if(!empty($data['VIEW_ALSO']))
	{ 
		foreach($data['VIEW_ALSO'] as $view_item)
		{ ?>
			<div class="row block">
				<div class="col-md-4 col-sm-4 left"><?php
				if(!empty($view_item['main_image']))
				{ ?>
					<a href="/<?php echo $view_item['url']; ?>"><img height="279" src="<?php echo $view_item['main_image'] ; ?>" alt=""></a><?php
				}
				else
				{ ?>
					<img style="width:100%"; src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""><?php
				} ?>
					<div class="comments_box">
					<div class="comments">
					<p><?php echo $view_item['recalls']['qnt']; ?></p>
					</div>
					</div>
				</div>
				<div class="col-md-8 right">
					<div class="row">
						<div class="col-md-7 col-sm-8">
							<h3><a style="color:#FF8B3E;text-decoration:none; font-weight: bold;" href="<?php echo Content::contentUrl($view_item['url']); ?>"><?php echo $view_item['name']; ?></a></h3>
						</div>
						<div class="col-md-5 col-sm-8 rating"><?php
						if(!empty($view_item['recalls']['average']))
						{ ?>
							<p><?php echo round($view_item['recalls']['average'], 1, PHP_ROUND_HALF_UP); ?> / 5</p>
							<div class="stars"><?php
							for($i = 1; $i <= 5; $i++ )
							{ 
                                                            if($i <= round($view_item['recalls']['average'], 1, PHP_ROUND_HALF_UP))
                                                            { ?>
								<img style="width:12px;margin-top:3px" src="<?php echo $this->page->getTemplateUrl(); ?>img/or-star.png" alt=""><?php
                                                            }
                                                            else
                                                            { 
                                                                if($i - round($view_item['recalls']['average'], 1, PHP_ROUND_HALF_UP) < 1)
                                                                { ?>
                                                                    <img style="width:12px;margin-top:3px;" src="<?php echo $this->page->getTemplateUrl(); ?>img/hp-star.png" alt=""><?php
                                                                }
                                                                else
                                                                { ?>
                                                                    <img style="width:12px;margin-top:3px;" src="<?php echo $this->page->getTemplateUrl(); ?>img/ow-star.png" alt=""><?php
                                                                }
                                                            }
							} ?>
							</div><?php
						} ?>
						</div>
                                            <div class="col-md-12"><?php
						if(!empty($view_item['name']))
						{ ?>
							<p class="testtema"><?php echo truncateText(htmlspecialchars_decode($view_item['detail_text']), 150); ?></p><?php
						}
						else
						{ ?>
							<p class="testtema" style="margin-bottom:55px;" >У данного товара нет детального описания213</p><?php
						}?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="line"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12"><?php
						if(!empty($view_item['last-review']['detail_text']))
						{ ?>
                                                    <a href="/<?=$view_item['last-review']['url']?>"><h2><?=$view_item['last-review']['name']?> </h2></a>
                                                    <a href="/<?=$view_item['last-review']['url']?>"><h2 class="black-color-text"><?php echo truncateText(htmlspecialchars_decode($view_item['last-review']['detail_text']), 100);  ?></h2> </a><?php
						}
						else
						{ ?>
							<h2>У данного товара нет превью текста</h2><?php
						} ?>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-9 col-sm-8"><?php
						if(!empty($view_item['images']))
						{ 
							$counter_img = 0;
							foreach($view_item['images'] as $src)
							{ 
								if($counter_img == 4) break; ?>
								<a href="javascript:void(0);">
									<img style="margin-bottom:10px;" height="50" width="50" src="<?php echo $src; ?>" alt="">
								</a><?php
								$counter_img++;
							} 
							if($view_item['more_fotos'] > 0)
							{ ?>	
								<span>+ <?php echo $view_item['more_fotos']; ?> фото</span><?php
							}
						} 
						else
						{ ?>
							<img style="margin-bottom:10px;" height="50" width="50" src="<?php echo _IMAGES_URL;?>no-foto.png" alt=""><?php
						} ?>
						</div>
						<div class="col-md-3 autor"style="padding-left:45px;height: 50px; padding-top: 7px;"><?php
							if(!empty($view_item['user_nickname']))
							{ ?>
								<p><?php echo $view_item['user_nickname']; ?></p><?php
							}
							else
							{ ?>
								<p>Инкогнито</p><?php
							} ?>
						</div>
					</div>
				</div>
			</div><!-- .block --><?php
		}
	}
else
{ ?>
	<div class="row block">
		<h3 style="padding-top:15px;padding-bottom:30px;text-align: center;font-weight:bold;">Нет данных для вывода</h3>
	</div><?php	
}	?>