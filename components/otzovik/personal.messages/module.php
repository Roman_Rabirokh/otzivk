<?php

if (!defined("_APP_START"))
{
    exit();
}
global $user;

if ($user->Authorized())
{
    $userid = $user->getID();
    //Pagination
    $allCount = 0;
    $pagesize = 10;
    $currentPage = isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;
    if ($currentPage < 1)
    {
        $currentPage = 1;
    }
    $start = ($currentPage - 1) * $pagesize;

    $data['MESSAGES'] = dbQuery('SELECT *, (userid_to+userid_from- :usr) AS u_user FROM #__users_messages
				WHERE userid_to = :uid OR userid_from = :uid_fr
				GROUP BY u_user 
                                LIMIT :start, :pagesize', array(
        ':uid' => $userid,
        ':uid_fr' => $userid,
        ':start' => $start,
        ':pagesize' => $pagesize,
        ':usr' => $userid,
            ), TRUE, $allCount);
    foreach ($data['MESSAGES'] as &$msg)
    {
        $msg['name_user_from'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $msg['userid_from']));
        if ($msg['userid_from'] == $userid)
        {
            $msg['name_user_to'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $msg['userid_to']));
        }
        $msg['last_message'] = dbGetOne('SELECT msg_text FROM #__users_messages
		WHERE userid_from = :uid AND userid_to = :uid_to
		ORDER BY created DESC
		LIMIT 0, 1', array(
            ':uid' => $msg['userid_from'],
            ':uid_to' => $msg['userid_to']
        ));
    }
    unset($msg);
    //Pagination
    $data['ALL'] = $allCount;
    $data['PAGESIZE'] = $pagesize;
//    echo "<pre>"; print_r($data['MESSAGES']); die;
    include 'template.php';
}
