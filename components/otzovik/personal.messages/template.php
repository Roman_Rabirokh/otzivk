	<?php if(!defined("_APP_START")) { exit(); } ?>
	<div class="row personal_area profile_messages_list">
		<div class="col-md-12 box">
			<div class="table-responsive"><?php
			if(!empty($data['MESSAGES']))
			{
		//echo "<pre>"; print_r($data['MESSAGES']); ?>
				<table class="table">
					<thead>
						<tr>
<!--							<th>Тема</th>-->
							<th>Учасники</th>
							<th>Последние обновления</th>
						</tr>
					</thead>
					<tbody><?php
					$flag = 1;
					foreach($data['MESSAGES'] as $message)
					{ ?>
						<tr <?php if($flag%2 != 0) echo 'class="active"'; ?> >
							
							<td>
                                                            <a href="/user/<?php if($message['userid_from'] != $userid) echo $message['userid_from']; else echo $message['userid_to']; ?>/"><?php if(!empty( $message['name_user_from']) && $message['userid_from'] != $userid) echo $message['name_user_from']; else if($message['userid_from'] == $userid){ echo 'отправлено'.', '; if(!empty($message['name_user_to'])){ echo $message['name_user_to'];}else{ echo 'Инкогнито'; } }else { echo "Инкогнито";} ?></a>
                                                            <!--<a href="/personal/messages/<?php //if($message['userid_from'] != $userid) echo $message['userid_from']; else echo $message['userid_to']; ?>/"><?php //if(!empty( $message['name_user_from']) && $message['userid_from'] != $userid) echo $message['name_user_from']; else if($message['userid_from'] == $userid){ echo 'отправлено'.', '; if(!empty($message['name_user_to'])){ echo $message['name_user_to'];}else{ echo 'Инкогнито'; } }else { echo "Инкогнито";} ?></a>-->
                                                        </td>
                                                        <td style="width:80%;"><a href="/personal/messages/<?php if($message['userid_from'] != $userid) echo $message['userid_from']; else echo $message['userid_to']; ?>/" title="<?php echo $message['last_message']; ?>"><?php echo truncateText(htmlspecialchars_decode($message['last_message']), 85); ?><span style="display:inline-block; float:right;"><i>перейти к диалогу</i></span></a></td>
						</tr><?php
						$flag++;
					} ?>
					</tbody>
				</table>
				<div class="col-md-12 col-sm-12 col-xs-12 pagination">
						<?php $this->includeComponent("system/pager",array("ALL"=>$data['ALL'],"PAGESIZE"=>$data["PAGESIZE"])); ?>
				</div><?php
			}
			else
			{ ?>
				<h3>У вас пока нет сообщений.</h3><?php
			} ?>
			</div>
		</div>
	</div>
