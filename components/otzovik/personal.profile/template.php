<?php if (!defined("_APP_START")) {
    exit();
} ?>
<div class="row personal_area profile">
    <div class="box">
        <div class="block">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12"><?php
                if(!empty($data['PROFILE_INFO']['upload_foto_src']))
                { ?>
                    <img height="190" width="190" src="<?php echo $data['PROFILE_INFO']['upload_foto_src']; ?>" alt=""><?php
                }
                else
                {
                    if (!empty($data['PROFILE_INFO']['profile_foto'])) {
                    ?>
                        <img height="190" width="190" src="<?php echo $data['PROFILE_INFO']['profile_foto']; ?>" alt=""><?php
                    } else {
                        ?>
                        <img src="<?php echo _TEMPLATE_URL; ?>img/profile_img1.png" alt=""><?php }
                    
                } ?>
                

                </div>
                <div class="col-md-7 col-sm-7 col-xs-12">
                    <h3><?php if (!empty($data['PROFILE_INFO']['name'])) echo $data['PROFILE_INFO']['name']; ?></h3>
                    <p class="name"><?php if (!empty($data['PROFILE_INFO']['user_info']['name'])) echo $data['PROFILE_INFO']['user_info']['name']; ?></p>
                    <p class="email"><?php if (!empty($data['PROFILE_INFO']['user_info']['email'])) {
                        echo $data['PROFILE_INFO']['user_info']['email'];
                    } ?></p>
                    <?php if (empty($data['PROFILE_INFO']['user_info']['social_code'])) {
                        ?>
                        <p class="password">&bull; &bull; &bull; &bull; &bull;</p><?php }
                    ?>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                    <a href="/personal/profile/edit/" class="amend">Изменить</a>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4>Страна: <span> <?php
                            if (!empty($data['PROFILE_INFO']['country'])) {
                                echo $data['PROFILE_INFO']['country'];
                            } else {
                                echo 'Не указано';
                            }
                            ?> </span></h4>
                    <h4>Город: <span><?php
                            if (!empty($data['PROFILE_INFO']['city'])) {
                                echo $data['PROFILE_INFO']['city'];
                            } else {
                                echo 'Не указано';
                            }
                            ?>
                        </span></h4>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4>Отзывы: <span><?php echo $data['PROFILE_INFO']['qnt_reviews']; ?></span></h4>
                    <h4>Комментарии: <span><?php echo $data['PROFILE_INFO']['qnt_comments']; ?></span></h4>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-1">
                    <h4>Регистрация: <span><?php echo $data['PROFILE_INFO']['date_reg']; ?></span></h4>
                    <h4>Рейтинг: <span><?php if ($data['PROFILE_INFO']['rating'] > 0) echo '+' . $data['PROFILE_INFO']['rating'];
                            else echo $data['PROFILE_INFO']['rating']; ?></span></h4>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4>Мои подписки:</h4>
                </div>
                <div class="col-md-6 col-sm-5 col-xs-12">
                    <div id="block-sbs"></div><?php
                        if (!empty($data['SUBSCRIBES_TYPE'])) {
                            ?>
                        <form class="delivery" id="personal-subscribes" method="post" target="edit-pf-sbs" action="/subscribe/edit/personal/"><?php
                        $counter = 1;
                        foreach ($data['SUBSCRIBES_TYPE'] as $subscribe) {
                                ?>
                                <input <?php if (!empty($data['SUBSCRIBES_PROFILE']) && in_array($subscribe['id'], $data['SUBSCRIBES_PROFILE'])) echo 'checked'; ?> type="checkbox" value="<?php echo $subscribe['id']; ?>" name="checkboxG<?php echo $counter; ?>" id="checkboxG<?php echo $counter; ?>" class="css-checkbox" disabled="disabled" />
                                <label for="checkboxG<?php echo $counter; ?>" class="css-label radGroup"></label>
                                <p><?php echo $subscribe['title']; ?></p><?php
                    $counter++;
                }
                ?>
                            <input type="hidden" name="token" value="<?php echo $this->createFormToken('se'); ?>" />
                        </form><?php }
                        ?>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-12 manage-subscribe">
                    <a href="javascript:void(0);" id="edit-subcribe" class="manage_subscriptions">Управление подписками</a>
                </div>
            </div>
        </div>
    </div>
</div>
