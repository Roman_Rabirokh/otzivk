<?php

if (!defined("_APP_START"))
{
    exit();
}
$this->page->addJS($this->getComponentPath($name) . "script.js");
global $user;
if ($user->Authorized())
{
    $userid = $user->getID();
    $data['PROFILE_INFO'] = dbGetRow('SELECT id, profile_foto, city, country, rating, upload_foto FROM #__profiles WHERE userid = :uid', array(
        ':uid' => $userid
    ));
    if(empty($data['PROFILE_INFO']))
    {
        $data['PROFILE_INFO']['rating']=0;
    }
    if(!empty($data['PROFILE_INFO']['upload_foto']))
    {
        $data['PROFILE_INFO']['upload_foto_src'] = getImageById($data['PROFILE_INFO']['upload_foto']);
    }
    
    $data['PROFILE_INFO']['qnt_reviews'] = dbGetOne('SELECT COUNT(id) FROM #__content WHERE userid = :uid AND content_type = 3', array(':uid' => $userid));
    $data['PROFILE_INFO']['qnt_comments'] = dbGetOne('SELECT COUNT(id) FROM #__comments_review WHERE userid = :uid', array(':uid' => $userid));
    $data['PROFILE_INFO']['user_info'] = dbGetRow('SELECT created, email, social_code, name FROM #__users WHERE id = :id', array(':id' => $userid));
    $date = date_parse($data['PROFILE_INFO']['user_info']['created']);
    // $month = getMonthName($date['month'],'long');
    $data['PROFILE_INFO']['date_reg'] = $date['day'] . '.' . str_pad($date['month'], 2, '0', STR_PAD_LEFT) . '.' . $date['year'];
//     echo "<pre>"; print_r($data['PROFILE_INFO']); die;
    $data['SUBSCRIBES_TYPE'] = dbQuery('SELECT * FROM #__subscribe_types');

    $data['SUBSCRIBES_PROFILE'] = dbQueryToArray('SELECT subcribe_id FROM #__subscribes_profile WHERE userid = :uid', array(':uid' => $userid));
    include 'template.php';
}
