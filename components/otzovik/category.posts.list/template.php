	<?php if(!defined("_APP_START")) { exit(); }
	if(!empty($data['POST_LIST']))
	{
		//if(empty($params['TOP'])) { ?><div class="top_news"><?php //}
		foreach($data['POST_LIST'] as $value_item)
		{ ?>
			<div class="row block">
				<div class="col-md-4 col-sm-4 left"><?php
				if(!empty($value_item['main_image']))
				{ ?>
					<a href="/<?php echo $value_item['url']; ?>"><img src="<?php echo $value_item['main_image'] ; ?>" alt=""></a><?php
				}
				else
				{ ?>
					<img style="width:100%"; src="<?php echo _IMAGES_URL; ?>/no-foto.png" alt=""><?php
				} ?>
					<div class="comments_box">
					<div class="comments">
					<p><?php echo $value_item['qnt_reviews']; ?></p>
					</div>
					</div>
				</div>
				<div class="col-md-8 right">
					<div class="row">
						<div class="col-md-7 col-sm-8">
							<h3><a href="/<?php echo $value_item['url']; ?>"><?php echo $value_item['name']; ?></a></h3>
						</div>
                                            
						<div class="col-md-5 col-sm-8 rating"><?php
						if(!empty($value_item['rating']))
						{ ?>
							<p><?php echo round($value_item['rating'], 1, PHP_ROUND_HALF_UP); ?> / 5</p>
							<div class="stars"><?php
							for($i = 1; $i <= 5; $i++ )
							{ 
                                                            if($i <= intval(round($value_item['rating'], 1, PHP_ROUND_HALF_UP))) 
                                                            { ?>
                                                                <img style="width:12px;margin-top:3px;" src="<?php echo _TEMPLATE_URL; ?>img/or-star.png" alt=""><?php
                                                            }
                                                            else
                                                            { 
                                                                if($i - round($value_item['rating'], 1, PHP_ROUND_HALF_UP)  < 1 ) 
                                                                { ?>
                                                                    <img style="width:12px;margin-top:3px;" src="<?php echo _TEMPLATE_URL; ?>img/hp-star.png" alt=""><?php
                                                                }
                                                                else
                                                                { ?>
                                                                  <img style="width:12px;margin-top:3px;" src="<?php echo _TEMPLATE_URL; ?>img/ow-star.png" alt=""><?php  
                                                                }
                                                            }
							} ?>
							</div><?php
						} ?>
						</div>
                                            <div class="col-md-12"><?php
						if(!empty($value_item['detail_text']))
						{ ?>
							<p class="tem-test-descr"><?php echo truncateText(htmlspecialchars_decode($value_item['detail_text']), 100); ?></p><?php
						}
						else
						{ ?>
							<p class="tem-test-descr" style="margin-bottom:55px;" >У данного товара нет детального описания</p><?php
						}?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="line"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 min-heit-div"><?php
						if(!empty($value_item['last-review']['detail_text']))
						{ ?>
                                                    <a href="/<?=$value_item['last-review']['url']?>"><h2><?=$value_item['last-review']['name']?> </h2></a>
                                                    <a href="/<?=$value_item['last-review']['url']?>"><h2 class="black-color-text"><?php echo truncateText(htmlspecialchars_decode($view_item['last-review']['detail_text']), 200); ?></h2> </a><?php
						}
						else
						{ ?>
							<h2>У данного товара нет превью текста</h2><?php
						} ?>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-12 div-resize-tes"><?php
						if(!empty($value_item['images']))
						{
							$counter_img = 0;
							foreach($value_item['images'] as $src)
							{
								if($counter_img == 5) break; ?>
								<a href="javascript:void(0);">
									<img class="full-img-l" height="50" width="50" src="<?php echo $src; ?>" alt="">
								</a><?php
								$counter_img++;
							}
							if($value_item['more_fotos'] > 0)
							{ ?>
								<span>+ <?php echo $value_item['more_fotos']; ?> фото</span><?php
							}
						}
						else
						{ ?>
							<img class="no-fotoid" height="50" width="50" src="/files/images/no-foto.png" alt=""><?php
						} ?>
							<div class="autor"><?php
							if(!empty($params['BOOKMARKS_IDS']))
							{ ?>
                                                            <p class="del-bookmark-lk" data-id="<?php echo $value_item['id']; ?>" >Убрать из закладок</p><?php
							}
							else
							{
                                                            if(!empty($value_item['user_nickname']))
                                                            { ?>
									<p><?php echo $value_item['last-review']['user_name']; ?></p><?php
                                                            }
                                                            else
                                                            { ?>
                                                                <p>Инкогнито</p><?php
                                                            }
							} ?>
							</div>
						</div>
					</div>
				</div>
			</div><!-- .block --><?php
		}
		//if(empty($params['TOP'])) { ?></div><?php //}
		if(empty($params['TOP']))
		{ ?>
		<div class="col-md-12 col-sm-12 col-xs-12 pagination">
				<?php $this->includeComponent("system/pager",array("ALL"=>$data['ALL'],"PAGESIZE"=>$data["PAGESIZE"])); ?>
		</div><?php
		}
	}
else
{ ?>
	<div class="row">
            <div class="no-data-output col-md-12"><?php
            if(!empty($params['SEARCH']))
            { ?>
                    <h3>По вашему запросу ничего не найдено.</h3><?php
            }
            else
            { ?>
                    <h3>Нет данных для вывода</h3><?php
            } ?>
            </div>
	</div><?php
}	?>
