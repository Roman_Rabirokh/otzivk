	<?php if(!defined("_APP_START")) { exit(); }

	if(!empty($data['POST_LIST']))
	{
		// echo "<pre>"; print_r($data['POST_LIST']); die;
		?>
	<div class="news">
		<div class="row"><?php
		for($i = 0; $i < 2; $i++)
		{ ?>
			<a href="/<?php echo $data['POST_LIST'][$i]['url']; ?>">
				<div class="col-md-6 col-sm-6 col-xs-12 block"><?php
				if(!empty($data['POST_LIST'][$i]['mainimage']))
				{ ?>
					<img src="<?php echo getImageById($data['POST_LIST'][$i]['mainimage'],array('width'=>600,'crop'=>array(0,0,422,227))); ?>" alt=""><?php
				}
				else
				{ ?>
					<img width="422" height="227" src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""><?php
				} ?>
					<div class="info"><?php
					if(!empty($data['POST_LIST'][$i]['rating']))
					{
						for($j = 1;$j <= 5;$j++)
						{ 
                                                    if($i <= round($data['POST_LIST'][$i]['rating'], 1, PHP_ROUND_HALF_UP)) 
                                                    { ?>
							<img style="width:15px;" src="<?php echo $this->page->getTemplateUrl() ; ?>img/or-star.png" alt=""><?php
                                                    }
                                                    else
                                                    { ?>
                                                        <img style="width:15px;" src="<?php echo $this->page->getTemplateUrl() ; ?>img/ow-star.png" alt=""><?php
                                                    }
						}
					} ?>
						<p><?php echo truncateText($data['POST_LIST'][$i]['name'], 90); ?></p>
						<div class="comments"><p><?php echo $data['POST_LIST'][$i]['qnt_reviews']; ?></p></div>
					</div>
				</div>
			</a><?php
		} ?>
		</div>
		<div class="row"><?php
		for($i = 2; $i < 6; $i++)
		{ ?>
			<a href="<?php echo $data['POST_LIST'][$i]['url']; ?>">
				<div class="col-md-3 col-sm-3 col-xs-6 block"><?php

					if(!empty($data['POST_LIST'][$i]['mainimage']))
				{ ?>
					<img src="<?php echo getImageById($data['POST_LIST'][$i]['mainimage'],array('width'=>600,'crop'=>array(0,0,210,120))); ?>" alt=""><?php
				}
				else
				{ ?>
					<img width="210" height="120" src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""><?php
				} ?>
					<div class="info"><?php
					if(!empty($data['POST_LIST'][$i]['rating']))
					{
						for($j = 1; $j <= 5;$j++)
						{ 
                                                    if($j <= round($data['POST_LIST'][$i]['rating'], 1, PHP_ROUND_HALF_UP)) 
                                                    { ?>
							<img style="width:12px;" src="<?php echo $this->page->getTemplateUrl() ; ?>img/or-star.png" alt=""><?php
                                                    }
                                                    else
                                                    { ?>
                                                        <img style="width:12px;" src="<?php echo $this->page->getTemplateUrl() ; ?>img/ow-star.png" alt=""><?php
                                                    }
						}
					} ?>
						<p><?php echo truncateText(htmlspecialchars_decode($data['POST_LIST'][$i]['name']), 40); ?></p>
						<div class="comments"><p><?php echo $data['POST_LIST'][$i]['qnt_reviews']; ?></p></div>
					</div>
				</div>
				</a><?php
		} ?>
		</div>
	</div><?php
	} ?>
