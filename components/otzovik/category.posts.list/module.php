	<?php if(!defined("_APP_START")) { exit(); }

	if(!empty($params['CATEGORY']) || !empty($params['SEARCH']) || !empty($params['TOP']) || !empty($params['MAINPAGE']) || !empty($params['BOOKMARKS_IDS']) || !empty($params['TOP']))
	{
		$sort = array();
		$filters = array();
		$sqlParams = array();
                
		//Pagination
		$allCount = 0;
		$pagesize = 10;
		$currentPage = isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;
		if($currentPage < 1)
		{
			$currentPage = 1;
		}
		$start = ($currentPage - 1) * $pagesize;
		if(!empty($params['CATEGORY']))
		{
                        $str_ids="";
                        if(strripos($params['CATEGORY'],'ALL')===FALSE){
                            //проверка конечная ли категория (3 уровень) или нет
                            Content::getOpenContentTree($params['CATEGORY'],$count);
                            $count=count($count);
                            if($count<3)// если категория не конечная получить список всех конечных категорий для категории верхнего уровня
                            {
                                if($count==2)
                                {
                                    $categories= dbQuery('SELECT id FROM #__content WHERE parentid = :id', [':id' => $params['CATEGORY']]);

                                }
                                else if($count==1)
                                {
                                 $categories=dbQuery('SELECT level_3.id FROM #__content AS level_1 '
                                         . ' INNER JOIN #__content AS level_3 ON(level_3.parentid=level_1.id) '
                                         . ' WHERE level_1.parentid = :id ', [':id' => $params['CATEGORY']]);
                                }
                                $str_ids="";
                                    for($i=0;$i<count($categories);$i++)
                                    {
                                        if($i+1==count($categories))
                                            $str_ids.=$categories[$i]['id'];
                                        else
                                            $str_ids.=$categories[$i]['id'].", ";
                                    }
                                    if(!empty($str_ids))
                                    {
                                         $filters["cd.f2 IN (".$str_ids.")"] = '';
                                    }
                                    else
                                    {
                                       $str_ids= $params['CATEGORY'];
                                       $filters["cd.f2 IN (".$str_ids.")"] = ''; 
                                    }
                               
                            }
                            else {
                                $filters['FIELDS']['2'] = intval($params['CATEGORY']);
                            }
                        }
                        
                        
		}
		if(!empty($params['SEARCH']))
		{
                        if(!$params['SEARCH_TEXT'])
                            $filters["LOWER(c.name) RLIKE '".$params['SEARCH']. "'"] = '';
                        else
                            $filters["(LOWER(c.name) RLIKE '".$params['SEARCH']. "' OR "."LOWER(c.detail_text) RLIKE '".$params['SEARCH']."')"] = '';
		}

		if(!empty($params['TOP']))
		{
			$sort['cd.f10'] = 'DESC';
			$filters["TO_DAYS(NOW()) - TO_DAYS(content_date) <= 7"] = '';
		}



		$fields = array('c.id', 'c.name', 'c.userid', 'c.preview_text', 'c.detail_text', 'c.content_date', 'c.url', 'c.mainimage, cd.f7 AS rating, cd.f10 AS qnt_reviews, cd.f11 AS last_review');
		if(isset($_GET['sort']) && $_GET['sort'] == 'latest')
		{
			$sort['cd.f11'] = 'DESC';
		}
		else if(isset($_GET['sort']) && $_GET['sort'] == 'rating')
		{
			$sort['cd.f7'] = 'DESC';
		}
		else if(isset($_GET['sort']) && $_GET['sort'] == 'best')
		{
			//$filters['FIELDS']['7'] = 4;
		}
		else
		{
			$sort['c.content_date'] = 'ASC';
		}

		if(!empty($params['MAINPAGE']))
		{
			$sort = array('cd.f11' => 'DESC');
			//$filters["TO_DAYS(NOW()) - TO_DAYS(cd.f11) <= 7"] = '';
			$filters["mainimage != 0"] = '';
			$filters["(cd.f11) != 0"] = '';
			$filters["(cd.f10) != 0"] = '';
			$sort = array('c.id' => 'DESC');
			$start = 0;
			$pagesize = 6;
		}

		if(!empty($params['BOOKMARKS_IDS']))
		{
			$filters["c.id IN (".$params['BOOKMARKS_IDS'].")"] = '';
		}

		if(!empty($params['TOP']))
		{
			$filters = array();
			$sort = array('cd.f7' => 'DESC');
			$start = 0;
			$pagesize = 4;
		}


                
		// если сортировка по  "лучшие отзывы" - то нужен сложный запрос сортировки
                if (!empty($_GET['sort']) && $_GET['sort'] == 'best')
                {
                    if(!empty($str_ids))
                    {
                        $str_ids=' AND cd.f2 IN ('.$str_ids .')';
                    }
                    else 
                    {
                        $str_ids=' AND cd.f2 IN ('.$params['CATEGORY'] .')';
                    }
                    $data['POST_LIST']= dbQuery('SELECT c.*, cd.f7 AS rating, cd.f10 AS qnt_reviews, cd.f11 AS last_review,  '
                           . ' AVG(r.measuring)  AS ratingRecall  '
                           . ' FROM #__content AS c '
                           . ' INNER JOIN #__content_data_4 AS cd ON (cd.id=c.id) '
                           . ' LEFT OUTER JOIN  #__content_data_3  as f ON (f.f1=c.id)'
                           . ' LEFT OUTER JOIN #__measurings_review AS r ON (r.review_id=f.id) '
                           . ' WHERE c.content_type=4 '.$str_ids
                           . ' GROUP BY c.id '
                           . ' ORDER BY ratingRecall DESC  '
                           . ' LIMIT :start, :rows ', [':start' => $start,':rows' => $pagesize], TRUE, $allCount);
                    
                }

                else
                    $data['POST_LIST'] = Content::GetList(4,$fields,$filters,$sort,array('OFFSET'=>$start,'ROWS'=>$pagesize,'COUNT_ALL'=>TRUE),$allCount,$sqlParams);   
                if(!empty($data['POST_LIST']))
		{
			foreach($data['POST_LIST'] as &$item)
			{
				$item['user_nickname'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $item['userid']));
				if(!empty($item['mainimage']))
                                    $item['main_image'] = getImageById($item['mainimage'], array('width'=>600,'crop'=>array(0,0,282,279)));
				// данные о последнем отзыве
                                $item['last-review']= dbGetRow('SELECT c.* FROM #__content AS c '
                                        . ' INNER JOIN #__content_data_3 AS f ON(f.id=c.id) '
                                        . ' WHERE f.f1= :id AND c.content_type=3 '
                                        . ' ORDER BY c.created DESC ', [':id' =>$item['id'] ]);
                                $item['last-review']['detail_text']= mb_strimwidth( strip_tags($item['last-review']['detail_text']), 0, 150, "...");
                                $item['last-review']['user_name'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $item['last-review']['userid']));
                                if(!empty($item['last-review']['id']))
                                    $images_ids = dbQueryToArray('SELECT id FROM #__images WHERE parentid = :pid',
                                                                                                                                                            array(':pid' => $item['last-review']['id']
																			));
				$item['images'] = array();
				if(!empty($images_ids))
				{
					foreach($images_ids as $img_id)
					{
						if($img_id != $item['mainimage'])
						{
							$item['images'][] = getImageById($img_id, array('width'=>70,'crop'=>array(0,0,50,50)));
						}
					}
				}

				$item['more_fotos'] = count($item['images']) - 5;
//                                print_r('<pre>');
//                                print_r($item['id']);
//                                print_r('</pre>');
//                                print_r('<pre>');
//                                print_r($item['last-review']);
//                                print_r('</pre>');
			}
                        unset($item);
		}
    //echo "<pre>"; print_r($data['POST_LIST']); die;
		//Pagination
		$data['ALL'] = $allCount;
		if(!empty($params['TOP']))
		{
			$data['ALL'] = 4;
		}
		$data['PAGESIZE'] = $pagesize;
		if(!empty($params['MAINPAGE']))
		{
			include 'template-home.php';
		}
		else
		{
			include 'template.php';
		}

	}
	else
	{
		if(empty($params['BOOKMARKS_IDS']))
		{
                    echo '<div style="margin-top: 33px;" class="row personal_area"><h3>У вас пока нет закладок.</h3></div>';
		}
		else
		{
                    echo "Повашему запросу ничего не найдено";
		}
	}
