<?php if(!defined("_APP_START")) { exit(); }
$this->page->addJS($this->getComponentPath($name)."script.js");
global $user;

if(!empty($params['REVIEW_ID']))
{
    if($user->Authorized())
    {
       $userid = $user->getID();
       $data['ISSET_COMMENT'] = dbQuery('SELECT id FROM #__comments_review WHERE userid = :uid AND review_id = :rid', array(':uid' => $userid, ':rid' => $params['REVIEW_ID']));
    }
    // пагинация по комментариям
    $pagesize = 10;
    $currentPage = isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;
    if ($currentPage < 1)
    {
        $currentPage = 1;
    }

    $start = ($currentPage - 1) * $pagesize;
    if (!empty($params['LAST']))
    {
        $sort = array('c.content_date' => 'DESC');
        $start = 0;
        $pagesize = 4;
    }



    // echo $params['REVIEW_ID']; die;
  $data['RECALL_BLOCK'] = dbGetRow('SELECT c.id, c.userid, c.name, c.preview_text, c.detail_text, c.mainimage, c.content_date, cd.f9 as video, cd.f1 AS product_id, cd.f3 AS rating
                                    FROM #__content c
                                    LEFT JOIN #__content_data_3 cd ON cd.id = c.id
                                    WHERE c.id = :id',
                                      array(
                                        ':id' => intval($params['REVIEW_ID'])
                                      ));
  
  $date = date_parse($data['RECALL_BLOCK']['content_date']);
  $month = getMonthName($date['month'],'long');
  $data['RECALL_BLOCK']['date'] = $date['day'].' '.$month.' '.$date['year']. ' года';

  $data['RECALL_BLOCK']['user_nickname'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $data['RECALL_BLOCK']['userid']));
  $data['RECALL_BLOCK']['user_info']['avatar'] = dbGetOne('SELECT profile_foto FROM #__profiles WHERE userid = :uid', array(':uid' => $data['RECALL_BLOCK']['userid']));
  $data['RECALL_BLOCK']['user_info']['upload_foto'] = dbGetOne('SELECT upload_foto FROM #__profiles WHERE userid = :uid', array(':uid' => $data['RECALL_BLOCK']['userid']));
  if(!empty($data['RECALL_BLOCK']['user_info']['upload_foto']))
  {
      $data['RECALL_BLOCK']['user_info']['upload_foto_src'] = getImageById($data['RECALL_BLOCK']['user_info']['upload_foto']);
  }
  $images_ids = dbQueryToArray('SELECT id FROM #__images WHERE parentid = :pid', array(':pid' => $data['RECALL_BLOCK']['id']));
  $data['RECALL_BLOCK']['images'] = array();
  if(!empty($images_ids))
  {
    foreach($images_ids as $img_id)
    {
      $data['RECALL_BLOCK']['images'][] = getImageById($img_id);
    }
  }

  $data['RECALL_BLOCK']['product_info'] = dbGetRow('SELECT c.id, c.name, c.mainimage, c.url, c.detail_text, cd.f2 AS category, cd.f7 AS rating FROM #__content c
                                                      LEFT JOIN #__content_data_4 cd ON cd.id = c.id
                                                      WHERE content_type = 4 AND c.id = :id',
                                                      array(
                                                        ':id' => $data['RECALL_BLOCK']['product_id']
                                                      ));
  if($data['RECALL_BLOCK']['product_info']['mainimage'] != 0)
  {
    $data['RECALL_BLOCK']['product_info']['main_image'] = getImageById($data['RECALL_BLOCK']['product_info']['mainimage'], array('width'=>550,'crop'=>array(0,0,424,346)));
  }

  $data['RECALL_COMMENTS'] = dbQuery('SELECT userid, created, id,  detail_text FROM #__comments_review WHERE review_id = :rid ORDER BY created DESC '
          . 'LIMIT :start, :rows ', array(':rid' => intval($params['REVIEW_ID']), ':start' => $start, ':rows' => $pagesize), TRUE, $allCount );
  
  // пагинация
  $data['ALL'] = $allCount;
    if (!empty($params['LAST']))
    {
        $data['ALL'] = 4;
    }
    $data['PAGESIZE'] = $pagesize;

    $data['QNT_COMMENTS'] = dbGetOne('SELECT COUNT(id) AS qnt FROM #__comments_review WHERE review_id = :rid', array(':rid' => intval($params['REVIEW_ID'])));
  if(!empty($data['RECALL_COMMENTS']))
  {
    foreach($data['RECALL_COMMENTS'] AS &$comment)
    {
      $comment['user_info'] = dbGetRow('SELECT name, profile_foto AS avatar FROM #__profiles WHERE userid = :uid', array(':uid' => $comment['userid']));
      $date = date_parse($comment['created']);
      $month = getMonthName($date['month'],'long');
      $comment['date'] = $date['day'].' '.$month.' '.$date['year']. ' года';
    }
  }
  // echo "<pre>"; print_r($data['RECALL_BLOCK']); die;
  include 'template.php';
}
else
{

}
