$(document).ready(function(){
  $('.add-bookmarks-review').click(function(){
    var tid = $(this).attr('data-id');
    var uid = $(this).attr('data-uid');
    if(tid != '' && uid != '')
    {
      $.ajax({
        url: '/bookmark/add/review/',
        method: 'POST',
        data: {
          tid: tid,
          uid: uid
        },
        success: function(data){
          if(data)
          {
            var del_bookmark = '<a href="javascript:void(0);" data-id=' + tid + ' data-uid=' + uid +' class="del-bookmark">В закдадках</a>';
            $('.outer-button-bookmark').html(del_bookmark);
          }
        }
      });
    }
  });

  $('.savePreview').click(function(){
    var prewiew_id = $(this).attr('data-rid');
    $("#save-info").html('Ваш отзыв сохранён в черновиках, вы можете редактировать его <a href="/personal/drafts/' + prewiew_id +'/">личном кабинете.</a>');
    $("body").animate({scrollTop:0}, '500', 'swing');
    $("#save-info").addClass('alert alert-success');
  });
});
