<?php
if (!defined("_APP_START"))
{
    exit();
}
global $user;
global $app;
$app->page->breadcrumb = array();
$userid = $user->getID();
$data['CATEGORY_TREE'] = array();
Content::getOpenContentTree($data['RECALL_BLOCK']['product_info']['category'], $data['CATEGORY_TREE']);
foreach (array_reverse($data['CATEGORY_TREE']) as $path_item)
{
    $this->page->addPathItem($path_item['NAME'], $path_item['LINK']);
}

$app->page->addPathItem($data['RECALL_BLOCK']['product_info']['name'], '/' . $data['RECALL_BLOCK']['product_info']['url']);
$app->page->addPathItem($data['RECALL_BLOCK']['name']);
?>
<main>
    <div class="container">
        <div class="row">
            <!--<div class="col-md-3 sidebar">-->
                <?php //echo getBannerBlockById(2); ?>
                <?php //echo getBannerBlockById(3); ?>
            <!--</div>--><!-- .sidebar -->
            <div class="col-md-9 col-sm-8">
                    <?php $this->page->getPath(); ?>
                <div class="row review">
                    <div id="save-info"></div><?php
                        if (!empty($data['RECALL_BLOCK']))
                        {
                            ?>
                        <div class="col-md-6 col-sm-6 col-xs-6 home"><?php
                            if (!empty($data['RECALL_BLOCK']['product_info']['main_image']))
                            {
                                ?>
                                <img height="346" src="<?php echo $data['RECALL_BLOCK']['product_info']['main_image']; ?>" alt=""><?php
                            } else
                            {
                                ?>
                                <img height="346" src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""><?php }
                            ?>
                            <div class="col-md-6">
                                <h2> <a class="add-a-review" href="/<?=$data['RECALL_BLOCK']['product_info']['url']?>/" ><?php echo $data['RECALL_BLOCK']['product_info']['name']; ?> </a></h2>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12 new-class-div-star"><?php
                            if (!empty($data['RECALL_BLOCK']['product_info']['rating']))
                            {
                                ?>
                                        <p class="rating"><?php echo round($data['RECALL_BLOCK']['product_info']['rating'], 1, PHP_ROUND_HALF_UP); ?>/5</p>
                                        <div class="stars"><?php
                                for ($i = 1; $i <=5; $i++)
                                { 
                                    if($i <= round($data['RECALL_BLOCK']['product_info']['rating'], 1, PHP_ROUND_HALF_UP))
                                    { ?>
                                    <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/or-star.png" alt=""><?php 
                                    }
                                    else
                                    { 
                                        if($i - round($data['RECALL_BLOCK']['product_info']['rating'],1, PHP_ROUND_HALF_UP) < 1) 
                                        { ?>
                                            <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/hp-star.png" alt=""><?php
                                        }
                                        else
                                        { ?>
                                            <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/ow-star.png" alt=""><?php
                                        }
                                        
                                    } 
                                } ?>
                                        </div><?php }
                            ?>
                                </div>
                                <div class="col-md-12 ad-new-rev-form">
                                    <form action="/review/add/" id="add-review-more" method="POST" class="search" >
                                        <input type="hidden" name="post_id" value="<?php echo $data['RECALL_BLOCK']['product_info']['id']; ?>" />
                                    </form>
                                    <a href="javascript:void(0);" id="button-add-recall" >+ Добавить отзыв</a>
                                </div>
                            </div>
                               
                        </div>
                        <?php echo getBannerBlockById(5); ?>
                     <div class="col-md-12 description-subject">
                                <?php if(!empty($data['RECALL_BLOCK']['product_info']['detail_text'])) { ?>
                                <?=$data['RECALL_BLOCK']['product_info']['detail_text']?>
                                    <?php } else { ?>
                                    Нет описания
                                <?php } ?>
                                </div>
                        <div class="col-md-12 recall">
                            <div class="col-md-2">
                                <a href="/user/<?php if (!empty($data['RECALL_BLOCK']['userid'])) echo $data['RECALL_BLOCK']['userid'];
                                else echo "0" ?>/"><?php
                                if(!empty($data['RECALL_BLOCK']['user_info']['upload_foto_src']))
                                { ?>
                                    <img style="border-radius:50%;" src="<?php echo $data['RECALL_BLOCK']['user_info']['upload_foto_src']; ?>" alt=""><?php
                                }
                                else
                                {
                                    if (!empty($data['RECALL_BLOCK']['avatar']))
                                    {
                                    ?>
                                        <img style="border-radius:50%;" src="<?php echo $data['RECALL_BLOCK']['avatar']; ?>" alt=""><?php
                                    }
                                    else
                                    { ?>
                                        <img style="border-radius:50%;" src="<?php echo _IMAGES_URL; ?>nofoto.png" alt=""><?php 
                                        
                                    }
                                    
                                } ?>
                                
                                </a>
                            </div>
                            <div class="col-md-2 name">
                                <a href="/user/<?php if (!empty($data['RECALL_BLOCK']['userid'])) echo $data['RECALL_BLOCK']['userid'];
                                else echo "0" ?>/" style="text-decoration: none;"><?php
                                if (!empty($data['RECALL_BLOCK']['user_nickname']))
                                {
                                    ?>
                                        <p><?php echo $data['RECALL_BLOCK']['user_nickname']; ?></p><?php
                            } else
                            {
                                    ?>
                                        <p>Инкогнито</p><?php }
                                ?>
                                </a><?php
                            if (!empty($data['RECALL_BLOCK']['rating']))
                            { ?>
                                    <div class="stars"><?php
                                    for ($i = 1; $i <= 5; $i++)
                                    { 
                                        if($i <= round($data['RECALL_BLOCK']['rating'], 1, PHP_ROUND_HALF_UP))
                                        { ?>
                                         <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/or-star.png" alt=""><?php
                                        }
                                        else
                                        { 
                                            if($i - round($data['RECALL_BLOCK']['rating'], 1, PHP_ROUND_HALF_UP) < 1) 
                                            { ?>
                                                <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/hp-star.png" alt=""><?php
                                            }
                                            else
                                            { ?>
                                                <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/ow-star.png" alt=""><?php
                                            }
                                        }
                                    } ?>
                                    </div><?php
                            } ?>
                            </div>
                            <div class="col-md-5"></div>
                            <div class="col-md-3 date"><?php
                            if (!empty($data['RECALL_BLOCK']['date']))
                            {
                                    ?>
                                    <p><?php echo $data['RECALL_BLOCK']['date']; ?></p><?php }
                                ?>
                            </div>
                            <div class="col-md-12 line"></div>
                            <div class="col-md-12"><?php
                            if (!empty($data['RECALL_BLOCK']['name']))
                            {
                                    ?>
                                    <h3><?php echo $data['RECALL_BLOCK']['name']; ?></h3><?php
                                } else
                                {
                                    ?>
                                    <h3>Заголовок отзыва отсутствует</h3><?php
                        }
                        if (!empty($data['RECALL_BLOCK']['detail_text']))
                        {
                            echo htmlspecialchars_decode($data['RECALL_BLOCK']['detail_text']);
                        } else
                        {
                                    ?>
                                    <p>Содержание отзыва отсутствует</p><?php }
                                ?>
                            </div>
                            <div class="col-md-12">
                                <?php
                                if (!empty($data['RECALL_BLOCK']['video']))
                                {
                                    echo $data['RECALL_BLOCK']['video'];
                                }
                                ?> 
                            </div>
                            <div class="col-md-7">
                                 <div class="social-icons" style="margin-bottom:20px;">
<?php include 'yandex.php'; ?>
    </div>
                            </div>
                            <div class="col-md-5 recall_function"><?php
                            if ($user->Authorized())
                            {
                                $bid = dbGetOne('SELECT id FROM #__users_bookmarks WHERE userid = :uid AND topic_id = :tid', array(
                                    ':uid' => $userid,
                                    ':tid' => $data['RECALL_BLOCK']['product_info']['id']
                                ));
                                if (!empty($bid))
                                {
                                    ?>
                                        <span class="outer-button-bookmark"><a href="javascript:void(0);" class="adel-bookmark" data-id="<?php echo $data['RECALL_BLOCK']['product_info']['id']; ?>"  data-uid="<?php echo $data['RECALL_BLOCK']['userid']; ?>" >В закладках</a></span><?php
                                } else
                                {
                                        ?>
                                        <span class="outer-button-bookmark"><a href="javascript:void(0);" class="add-bookmarks-review" data-id="<?php echo $data['RECALL_BLOCK']['product_info']['id']; ?>"  data-uid="<?php echo $data['RECALL_BLOCK']['userid']; ?>" >Добавить отзыв в закладки</a></span><?php
                                } ?>
                                <a href="#message-moderator-form" class="fancybox" >(!) Написать модератору</a><?php
                            }
                            else
                            { ?>
                                <a href="javascript:void(0);" onclick="redirectToEnter();">Авторизуйтесь, чтобы добавить отзыв в закладки</a><?php 
                            } ?>
                                
                            </div>
                           
                            <?php if ($data['RECALL_BLOCK']['userid'] != $user->getID())
                            { 
                        if ($user->Authorized())
                        { 
                            if(empty($data['ISSET_COMMENT'])) 
                            { ?>
                            <div class="col-md-12">
                                    <p class="line"></p>
                                    <h3 style="text-align: center; font-size:18px; font-weight: bold; padding-top: 20px;">Оцените, насколько полезен этот отзыв</h3>
                                </div>
                            <div class="col-md-12 block-much">
                                <button type="button" data-rid="<?php echo $data['RECALL_BLOCK']['id']; ?>" data-measuring="1" class="col-md-2 measuring">Очень плохо</button>
                                <button type="button" data-rid="<?php echo $data['RECALL_BLOCK']['id']; ?>" data-measuring="2" class="col-md-2 measuring">Плохо</button>
                                <button type="button" data-rid="<?php echo $data['RECALL_BLOCK']['id']; ?>" data-measuring="3" class="col-md-2 measuring">Нормально</button>
                                <button type="button" data-rid="<?php echo $data['RECALL_BLOCK']['id']; ?>" data-measuring="4" class="col-md-2 measuring">Хорошо</button>
                                <button type="button" data-rid="<?php echo $data['RECALL_BLOCK']['id']; ?>" data-measuring="5" class="col-md-2 measuring">Отлично</button>
                            </div><?php
                            }
                } else
                { ?>
                    <div class="col-md-12 block-much">
                        <!--<h4 style="text-align: center" ><a href="/login/">Авторизуйтесь</a>, чтобы поставить свою оценку отзыву.</h4>-->

                    </div><?php }
                }
                            ?>
                        </div><?php
                    if (!empty($params['PREVIEW']))
                    {
                        ?>
                            <div class="form-group review-group review-buttons col-md-12" >
                                <div class="col-md-4 but-preview" >
                                    <button type="button" data-rid="<?php echo $data['RECALL_BLOCK']['id']; ?>" class="editPreview" >Редактировать</button>
                                </div>
                                <div class="col-md-4 but-preview" style="padding-right:20px;" >
                                    <button type="button" data-rid="<?php echo $data['RECALL_BLOCK']['id']; ?>" class="savePreview" >Сохранить</button>
                                </div>
                                <div class="col-md-4 but-publish"  >
                                    <button type="button" data-rid="<?php echo $data['RECALL_BLOCK']['id']; ?>" class="publishPreview" >Опубликовать</button>
                                </div>
                            </div><?php
                    } ?>
                                                
                    
                    
                       <?php if ($user->Authorized())
                                {
                                            if(empty($data['ISSET_COMMENT']))
                                            { ?>
                                            <div class="col-md-12 add-comment">
                                                <h4>Оставить комментарий:</h4>
                                                <div class="block comment-review">
                                                    <textarea data-reviewid="<?php echo $data['RECALL_BLOCK']['id']; ?>" id="comment-text-review" rows="6"></textarea>
                                                    <input type="hidden" name="measuring" value="">
                                                </div>
                                                <div style="padding-left: 0;" class="col-md-10"><p style="color: red;" class="error-comment-text"></p></div>
                                                <div class="col-md-2">
                                                    <button class="leave-comment measuring" type="button">Отправить</button>
                                                </div>
                                            </div><?php
                                            }
                                            else
                                            {?>
                                            <div class="col-md-12 add-comment">
                                                <h4 style="text-align:center;">Вы уже осталяли оценку и комментарий к этому отзыву</h4>
                                            </div> <?php 
                                                
                                            }
                                } else
                                    {
                                        ?>
                                            <div class="col-md-12 add-comment">
                                                <p><a href="#data-register" class="fancybox">Авторизируйтесь</a>, чтобы поставить оценку отзыву и оставить комментарий.<p>
                                            </div> <?php } ?>
                        <div class="col-md-12 comments-title">
                            <h3>Комментарии к отзыву <span>(<?php echo $data['QNT_COMMENTS']; ?>)<span></h3>
                                        </div><?php
                                                    if (!empty($data['RECALL_COMMENTS']))
                                                    {
                                                        ?>
                                            <div class="col-md-12 comments"><?php
                                                                    foreach ($data['RECALL_COMMENTS'] as $comment_block)
                                                                    {
                                                                        ?>
                                                    <div class="top_news">
                                                        <div class="block clearfix comment">
                                                            <div class="col-md-2 topic_profile"><?php
                                                                        if (!empty($comment_block['user_info']['avatar']))
                                                                        {
                                                                            ?>
                                                                    <img class="avatar-comments" src="<?php echo $comment_block['user_info']['avatar']; ?>" alt=""><?php
                                                        } else
                                                        {
                                                            ?>
                                                                    <img class="avatar-comments" src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""><?php }
                                                        ?>

                                                            </div>
                                                            <div class="col-md-10 name-date">
                                                                <div class="col-md-9 col-sm-9 col-xs-5" >
                                                                    <h5><?php
                                                if (!empty($comment_block['user_info']['name']))
                                                {
                                                    echo $comment_block['user_info']['name'];
                                                } else
                                                {
                                                    echo "Инкогнито";
                                                }
                                                ?>
                                                                    </h5>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3 col-xs-7"><h5><?php if (!empty($comment_block['date'])) echo $comment_block['date'];
                                    else echo "Дата неизвестна"; ?></h5></div>
                                                                <div class="col-md-12 col-sm-12 col-xs-12 comment-text"><?php if (!empty($comment_block['detail_text'])) echo $comment_block['detail_text']; ?></div>
                                                                <div class="col-md-12 comment-mark" idc="<?= $comment_block['id'] ?>"> <span class="not-tematic">Комментарий не по теме</span></div>
                                                            </div>
                                                        </div>
                                                    </div><?php }
                                ?>
                                            </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 pagination">
			<?php $this->includeComponent("system/pager",array("ALL"=>$data['ALL'],"PAGESIZE"=> $data["PAGESIZE"])); ?>
		</div><?php
                                        }
                                        else
                                        {
                                            ?>
                                            <div class="col-md-12 comments">
                                                <p>У этого отзыва пока нет комментариев. Вы может оставить свой комментарий воспользавшись формой выше.</p>
                                            </div><?php
                                        }
                                        
                                        ?>
                                        </div><?php }
                                    ?>
                                   <?php echo getBannerBlockById(1); ?>
                                    <div class="col-md-12" style="margin-top: 10px;padding-left: 0;margin-bottom:20px;">
                                        <h3 class="review-titles">Другие отзывы</h3>
                                    </div>
                                    <div class="col-md-12 row topic" style="padding-left:0;padding-right:0;"><?php $this->includeComponent('otzovik/review.show.other', array('PID' => $data['RECALL_BLOCK']['product_id'], 'RID' => intval($params['REVIEW_ID']))); ?>
                                    </div>
                                    <?php echo getBannerBlockById(7); ?>
                                    <div class="col-md-12" style="margin-top: 10px;padding-left: 0;margin-bottom:20px;">
                                        <h3 class="review-titles">Смотрите также</h3>
                                    </div>
                                    <div class="col-md-12" style="padding-left:0;padding-right:0;">
                                        <div class="top_news" style="margin-top:0;">
<?php
$category = dbGetOne('SELECT f2 AS idc FROM #__content_data_4 WHERE id = :id', array(':id' => $data['RECALL_BLOCK']['product_id']));
$this->includeComponent('otzovik/category.view.also', array('CATEGORY' => $category, 'PID' => $data['RECALL_BLOCK']['product_id']));
?>
                                        </div><!-- .see_also -->
                                    </div>
                                    </div>

                                    </div><!-- .container -->
                                    </main><!-- main -->
                                    
                                    <div style="display:none;" >
                                        <div id="message-moderator-form">
                                            <iframe style="width: 100%; height:100px;border:1px solid grey;display:none;" name="send-moderator-msg"></iframe>
                                            <form id="form-message-mod" name="moderator-message" method="post" target="send-moderator-msg" action="/msg/moderator/">
                                                <div id="wrap-moderator-mess">
                                                    <input id="first-moder-input" type="hidden" name="review_id" value="<?php echo $params['REVIEW_ID']; ?>"/>
                                                    <input id="second-moder-input" type="text" name="subject" placeholder="Тема сообщения" value="Жалоба на отзыв"  />
                                                    <p class="error error-subject" ></p>
                                                </div>
                                                <div>
                                                    <textarea name="msg_text" placeholder="текст сообщения"></textarea>
                                                    <p class="error error-text-msg"></p>
                                                    
                                                </div>
                                                <div>
                                                    <input id="subm-massdge-moder" type="submit" value="Отправить" />
                                                </div>
                                            </form>
                                        </div>
                                    </div>

