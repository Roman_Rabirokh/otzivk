$(document).ready(function(){
	var recommend = $('input[name="recommend"]:checked').val();
  if(recommend == 1)
  {
    $('.recommend-yes').css({
      'backgroundColor': 'grey',
      'color': 'white'
    });
  }
  if(recommend == 0)
  {
    $('.recommend-no').css({
      'backgroundColor': 'grey',
      'color': 'white'
    });
  }

  $('.del-img-review').click(function(){
    var imgId = $(this).attr('data-img-id');
    var reviewId = $(this).attr('data-recall-id');
    $.ajax({
      url: '/draft/image/delete/',
      method: 'POST',
      data:{
        img_id: imgId,
        rid: reviewId
      },
      success: function(data){
        window.location.reload();
      }
    })
  });

  $('.img-container').mouseover(function(){
    $(this).find('span').show();
  }).mouseleave(function(){
    $(this).find('span').hide();
  });

  $('#saveDraft').click(function(){
    $('.action-review').append('<input type="hidden" name="action" value="save" />');
    var form = $('#edit-review');
    form.submit();
  });

	$('#previewDraft').click(function(){
		$('.action-review').append('<input type="hidden" name="action" value="preview" />');
		var form = $('#edit-review');
		form.submit();
	});



});

function readFotoTopicURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      console.log(e.target.result);
      var	img_block = '<span class="img-container"><img src="' +  e.target.result + '" /><span class="del-img-review" >Удалить</span></span>';

      $('#draft-review-fotos').html('');
      $('#draft-review-fotos').html(img_block);

    };

    reader.readAsDataURL(input.files[0]);
  }
}
