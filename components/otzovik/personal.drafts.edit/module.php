	<?php if(!defined("_APP_START")) { exit(); }
  $this->page->addJS($this->getComponentPath($name)."script.js");
  global $useer;

  if($user->Authorized())
  {
    if(!empty($params['REVIEW_ID']))
    {
      //echo $params['REVIEW_ID']; die;
      $data['RECALL_BLOCK'] = dbGetRow('SELECT c.id, c.userid, c.name, c.preview_text, c.detail_text, c.mainimage, c.content_date, cd.f1 AS product_id, cd.f3 AS rating, cd.f8 AS recommend, cd.f9 AS youtube
                                        FROM #__content c
                                        LEFT JOIN #__content_data_3 cd ON cd.id = c.id
                                        WHERE c.id = :id',
                                          array(
                                            ':id' => intval($params['REVIEW_ID'])
                                          ));

      $date = date_parse($data['RECALL_BLOCK']['content_date']);
      $month = getMonthName($date['month'],'long');
      $data['RECALL_BLOCK']['date'] = $date['day'].' '.$month.' '.$date['year']. ' года';

      $data['RECALL_BLOCK']['user_nickname'] = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $data['RECALL_BLOCK']['userid']));
      $data['RECALL_BLOCK']['avatar'] = getImageById($data['RECALL_BLOCK']['mainimage']);
      $images_ids = dbQueryToArray('SELECT id FROM #__images WHERE parentid = :pid', array(':pid' => $data['RECALL_BLOCK']['id']));
      $data['RECALL_BLOCK']['images'] = array();
      if(!empty($images_ids))
      {
        for($i = 0, $cnt = count($images_ids); $i < $cnt; $i++)
        {
          $data['RECALL_BLOCK']['images'][$i]['img'] = getImageById($images_ids[$i], array('width'=>300,'crop'=>array(0,0,150,100)));
          $data['RECALL_BLOCK']['images'][$i]['img_id'] = $images_ids[$i];
        }
      }

      $data['RECALL_BLOCK']['product_info'] = dbGetRow('SELECT c.id, c.name, c.mainimage, cd.f7 AS rating FROM #__content c
                                                          LEFT JOIN #__content_data_4 cd ON cd.id = c.id
                                                          WHERE content_type = 4 AND c.id = :id',
                                                          array(
                                                            ':id' => $data['RECALL_BLOCK']['product_id']
                                                          ));
      $data['RECALL_BLOCK']['product_info']['main_image'] = getImageById($data['RECALL_BLOCK']['product_info']['mainimage']);
      $data['RECALL_COMMENTS'] = dbQuery('SELECT userid, created, detail_text FROM #__comments_review WHERE review_id = :rid ORDER BY created DESC', array(':rid' => intval($params['REVIEW_ID'])));
      $data['QNT_COMMENTS'] = dbGetOne('SELECT COUNT(id) AS qnt FROM #__comments_review WHERE review_id = :rid', array(':rid' => intval($params['REVIEW_ID'])));

      // echo "<pre>"; print_r($data['RECALL_BLOCK']); die;
      include 'template.php';
    }
    else
    {
      redirect('/personal/drafts/');
    }
  }
