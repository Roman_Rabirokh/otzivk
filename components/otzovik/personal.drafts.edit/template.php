	<?php if(!defined("_APP_START")) { exit(); }
  $token = $this->createFormToken('de');
	echo '<script> var tokenUpload="'.$token.'"</script>';?>
  <div class="row add_new_topic_review"><?php
  if(!empty($data['RECALL_BLOCK']))
  {
    if(isset($data['RECALL_BLOCK']['recommend'])) echo '<script> var recommend ='.$data['RECALL_BLOCK']['recommend'].'</script>'; ?>
    <h2 style="margin-bottom: 35px;">Редактирование отзыва</h2>
    <iframe style="width:100%;height:150px;display:none;" name="edit-topic-review" ></iframe>
    <form enctype="multipart/form-data" action="/save/draft/review/" method="post" id="edit-review" target="edit-topic-review" >
      <div class="form-group review-group" >
        <label>Лаконичный заголовок отзыва:</label>
        <input type="text" name="review_title" class="input-review" value="<?php if(!empty($data['RECALL_BLOCK']['name'])) echo $data['RECALL_BLOCK']['name']; ?>" placeholder="заголовок отзыва"/>
        <input type="hidden" name="topic_id" value="<?php if(!empty($data['RECALL_BLOCK']['product_id'])) echo $data['RECALL_BLOCK']['product_id']; ?>" />
        <input type="hidden" name="review_id" value="<?php if(!empty($data['RECALL_BLOCK']['id'])) echo $data['RECALL_BLOCK']['id']; ?>" />
        <input type="hidden" name="token" value="<?php echo $token; ?>" />
      </div>
      <div class="form-group review-group" >
        <label>Ваш отзыв:</label>
        <textarea id="text-review" name="review_text" placeholder="начните вводить тект..." ><?php if(!empty($data['RECALL_BLOCK']['detail_text'])) echo $data['RECALL_BLOCK']['detail_text']; ?></textarea>
      </div>
      <div class="form-group review-group review-rate" >
        <div class="col-md-6">
          <label>Ваша оценка:</label>
          <div class="add-rating"><?php
          for($i = 1; $i <=5; $i++)
          {
            if($i <= $data['RECALL_BLOCK']['rating'])
            { ?>
              <img height="35" class="star-rate-<?php echo $i; ?>" src="<?php echo _TEMPLATE_URL; ?>img/y-star.png" /><?php
            }
            else
            { ?>
              <img height="35" class="star-rate-<?php echo $i; ?>" src="<?php echo _TEMPLATE_URL; ?>img/w-star.png" /><?php
            } ?>
            <input type="radio" name="rating" value="<?php echo $i; ?>" <?php if($i == $data['RECALL_BLOCK']['rating']) echo 'checked'; ?> /><?php
          } ?>

          </div>
        </div>
        <div class="col-md-6">
          <label>Рекомендуете ли вы это своим друзьям?</label>
          <div class="recommend">
            <span class="recommend-yes">Да</span>
            <input type="radio" name="recommend" <?php if(isset($data['RECALL_BLOCK']['recommend']) && $data['RECALL_BLOCK']['recommend'] == 1) echo 'checked'; ?> value="1"/>
            <span class="recommend-no">Нет</span>
            <input type="radio" name="recommend" <?php if(isset($data['RECALL_BLOCK']['recommend']) && $data['RECALL_BLOCK']['recommend'] == 0) echo 'checked';?> value="0" />
          </div>
        </div>
      </div>
      <div class="form-group review-group review-foto" >
        <label>Добавьте фотографии:</label>
        <div id="draft-review-fotos"><?php
        if(!empty($data['RECALL_BLOCK']['images']))
        {
          foreach($data['RECALL_BLOCK']['images'] as $image)
          { ?>
            <span class="img-container"><img src="<?php echo $image['img']; ?>" /><span class="del-img-review" data-recall-id="<?php echo $data['RECALL_BLOCK']['id']; ?>" data-img-id="<?php echo $image['img_id']; ?>">Удалить</span></span><?php
          }
        } ?>
        </div>
        <p>! Пожалуйста, добавляйте здесь только сделанные вами уникальные фотографии и только по теме отзыва!</p>
				<div id="dropzone" style="border:2px dashed #4A90E2; margin:30px 0;background-color: #F6F9FD; height: 355px; width:100%;">
					<div class="dz-message needsclick">
							<p>Перетащите изображения сюда</p>
							<p>или</p>
							<span class="note needsclick"><button type="button" class="add-topic-foto">Загрузить с компьютера</button></span>
						</div>
				</div>
      </div>
      <div class="form-group review-group review-video" >
        <label>Добавьте видео к отзыву:</label>
        <p>Вставьте в поле ссылку с YuoTube</p>
        <input type="text" name="review_video" value="<?php if(!empty($data['RECALL_BLOCK']['youtube'])) echo $data['RECALL_BLOCK']['youtube']; ?>" class="input-review" placeholder="вставьте ссылку"/>
      </div>
      <div class="action-review" ></div>
      <div class="form-group review-group review-buttons" >
              <div class="col-md-4 but-preview" ><button type="button" id="saveDraft"  >Сохранить</button></div>
              <div class="col-md-4 but-preview" style="padding-right: 20px;" ><button type="button" id="previewDraft"  >Предпросмотр</button></div>
              <div class="col-md-4 but-publish" ><button type="submit" >Опубликовать</button></div>
      </div>
    </form> <?php
  }
  else
  { ?>
    <h3>У вас нет отзывов в черновиках.</h3><?php
  } ?>
  </div>
