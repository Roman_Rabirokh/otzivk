<?php if(!defined("_APP_START")) { exit(); }
$pageId= !empty($params['pageId']) ? $params['pageId'] : 0;

$filters = array();
$filters['c.parentid = 0'] = '';
$data['CATEGORY'] = Content::GetList(2,array('c.id','c.name','c.url'),$filters,array('showorder'=>'ASC'));

foreach($data['CATEGORY'] as &$category)
{
    $category['href'] = Content::contentUrl($category['url']);
    $category['subcats_2']= dbQuery('SELECT id, name, url FROM #__content WHERE content_type = 2 AND parentid = :pid',array(':pid' => $category['id']));
    foreach($category['subcats_2'] as &$cat_level_2)
    {
        $cat_level_2['href'] = Content::contentUrl($cat_level_2['url']);
        $cat_level_2['subcats_3']=dbQuery('SELECT id, name, url FROM #__content WHERE content_type = 2 AND parentid = :pid',array(':pid' => $cat_level_2['id']));
        foreach($cat_level_2['subcats_3']  as &$cat_level_3)
        {
            $cat_level_3['href'] = Content::contentUrl($cat_level_3['url']);
            if($cat_level_3['id']==$pageId)
            { 
                $cat_level_3['active']=TRUE;
                $category['active']=TRUE;
                $cat_level_2['active']=TRUE;
            }
        }
        if($cat_level_2['id']==$pageId)
        {
            $cat_level_2['active']=TRUE;
            $category['active']=TRUE;
        }
        unset($cat_level_3);
    }
    if($category['id']==$pageId)
        $category['active']=TRUE;
     unset($cat_level_2);
}
unset($category);

include('template.php');
