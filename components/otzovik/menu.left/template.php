<?php if(!defined("_APP_START")) { exit(); } ?>
<ul>
    <?php foreach($data['CATEGORY'] as $cat) { ?>
    <li class="menu-left-li <?php if(!empty($cat['active'])){ ?> active-item-menu <?php } ?>" > <a href="<?php echo $cat['href']; ?>"> <?php echo $cat['name']; ?> </a>
                <span></span>
            <?php if(!empty($cat['subcats_2'])) {  ?>
            <ul class="hide-ul-menu" <?php if(!empty($cat['active'])){ ?>style="display: block"  <?php } ?>>
                <?php foreach($cat['subcats_2'] as $cat_level_2){ ?>
                    <li class="menu-left-li-level-2 <?php if(!empty($cat_level_2['active'])){ ?> active-item-menu<?php }?>">
                 <a href="<?php echo $cat_level_2['href']; ?>"> <?php echo $cat_level_2['name']; ?> </a>
                     <span></span>
                       <?php if(!empty($cat_level_2['subcats_3'])) {  ?>
                       <ul class="hide-ul-menu" <?php if(!empty($cat_level_2['active'])){ ?> style="display: block"  <?php } ?>>
                            <?php foreach($cat_level_2['subcats_3'] as $cat_level_3) { ?>
                                <li class="<?php if(!empty($cat_level_3['active'])){ ?> active-item-menu<?php }?> " >
                                    <a href="<?php echo $cat_level_3['href']; ?>"><?php echo $cat_level_3['name']; ?></a><span></span>
                                </li>
                           <?php  } ?>
                       </ul>
                       <?php } ?>
                    </li>
                <?php  }  ?>
            </ul>
           <?php } ?>
        </li>
    <?php  } 
    ?>
</ul>