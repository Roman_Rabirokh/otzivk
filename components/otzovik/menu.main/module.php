<?php if(!defined("_APP_START")) { exit(); }

$lilters = array();
$filters['c.parentid = 0'] = '';
$data['CATEGORY'] = Content::GetList(2,array('c.id','c.name','c.url'),$filters,array('showorder'=>'ASC'),array());

foreach($data['CATEGORY'] as &$category)
{
    $category['href'] = Content::contentUrl($category['url']);
}

if(!empty($params['PLACE']) && $params['PLACE'] == 'header')
{
  include('template-header.php');
}

if(!empty($params['PLACE']) && $params['PLACE'] == 'footer')
{
  include('template-footer.php');
}
