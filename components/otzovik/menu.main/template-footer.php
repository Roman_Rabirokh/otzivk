<?php if(!defined("_APP_START")) { exit(); }

if(!empty($data['CATEGORY']))
{ ?>
<ul>
    <div class="col-md-6 col-sm-6 col-xs-6 ul_column"><?php
    for($i = 0; $i <4; $i++)
    { ?>
        <li><a href="<?php echo $data['CATEGORY'][$i]['href']; ?>"><?php echo $data['CATEGORY'][$i]['name']; ?></a></li><?php
    } ?>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 ul_column"><?php
    for($i = 4; $i < 7; $i++)
    { ?>
        <li><a href="<?php echo $data['CATEGORY'][$i]['href']; ?>"><?php echo $data['CATEGORY'][$i]['name']; ?></a></li></li><?php
    } ?>
    <li><a href="/catalog/" class="last">Ещё</a></li>
    </div>
</ul><?php
} ?>
