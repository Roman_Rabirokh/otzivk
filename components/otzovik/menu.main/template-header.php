<?php
if (!defined("_APP_START")) {
    exit();
}
?>

<ul id="gamb-hidden">
    <?php for ($i = 0; $i < 6; $i++) {
        ?>
        <span id="exitspan" onclick="hamBurgerClose();">x</span>
        <li <?php if (!empty($params['PLACE'] && $params['PLACE'] == 'header')) {
            ?>data-id-cat-header="<?php echo $data['CATEGORY'][$i]['id']; ?>" class="head-menu" <?php } ?>>
            <a  href="<?php echo $data['CATEGORY'][$i]['href']; ?>"><?php echo $data['CATEGORY'][$i]['name']; ?></a>
        </li>
    <?php } ?>
    <li><a href="/catalog/">Ещё</a>
    </li>
</ul>
<ul id="gamb-visible">

    <li id="head-menu" onclick="hamBurger();" id="first-elem"><a id="catalog-hamb"> Каталог</a> </li>
</ul>

<script>
    function hamBurger() {
        document.getElementById("head-menu").style.display = "none";
        document.getElementById("gamb-hidden").style.display = "block";
        document.getElementById("catalog-hamb").style.display = "none";
    }
        function hamBurgerClose(e) {
        
        document.getElementById("head-menu").style.display = "block";
        document.getElementById("gamb-hidden").style.display = "none";
        document.getElementById("catalog-hamb").style.display = "block";
       
    }
    
</script>
<style>

</style>
