<?php 


$title = "Меню";

$table = '#__menu';

$select_fields = '*,id as menuitems';

$controls['menuitems'] = new Control('menuitems','template');
$controls['menuitems']->template = "config/templates/menuitems.php";
$controls['menuitems']->template_mode = 'full';


function after_delete($id)
{
	dbNonQuery('DELETE FROM #__menu_items WHERE menuid = ' . intval($id));
}

function after_copy($id,$newid)
{
	$items = dbQuery('SELECT * FROM #__menu_items WHERE menuid = ' . intval($id));
	foreach($items as $item)
	{
		dbNonQuery('INSERT INTO #__menu_items (menuid,title,link,showorder) VALUES(:menuid,:title,:link,:showorder)',array(
			':menuid'=>$newid,
			':title'=>$item['title'],
			':link'=>$item['link'],
			':showorder'=>$item['showorder']
		));
	}
}