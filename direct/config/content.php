<?php
include_once(_DIR . "api/image.php");
include('api/content.php');
if(isAjax())
{
	if(!empty($_REQUEST["action"]))
	{
		switch($_REQUEST["action"])
		{
			case "set_active":
				if(strcmp($_REQUEST["state"],"1") == 0)
				{
					dbNonQuery("UPDATE #__content SET active = 1 WHERE id = :id",array(":id"=>$_REQUEST["id"]));
				}
				if(strcmp($_REQUEST["state"],"0") == 0)
				{
					dbNonQuery("UPDATE #__content SET active = 0 WHERE id = :id",array(":id"=>$_REQUEST["id"]));
				}
				break;
			case "load_images":
				include("templates/images.php");
				break;
			case "rm_file":
				rmFile(intval($_POST['fileid']));
				break;
			case "rm_image":
				rmImage(intval($_POST['fileid']));
				break;
		}
	}
}
else
{
	$table = _DB_TABLE_PREFIX . "content";


	$title = "Контент";

	$dataFields = array();
	$dataFieldsValues = array();


	if(isset($_GET["typeid"]))
	{
		$contentType =  dbGetRow("SELECT id,IF(pluralname != '',pluralname,name) as title_name,genitivename FROM #__content_types WHERE id = :id",array(":id"=>intval($_GET["typeid"])));

		$title .= ' / ' . $contentType['title_name'];

		if(!empty($contentType['genitivename']))
		{
			$top_buttons_titles["ADD"] = _ADD_NEW . ' ' . $contentType['genitivename'];
		}

		$dataFields = dbQuery("SELECT * FROM `#__content_fields` WHERE content_type = :content_type ORDER BY showorder",
		array(":content_type"=>$contentType['id'])
		);

		if(isset($_GET['id']))
		{
			$dataFieldsValues = dbGetRow('SELECT * FROM #__content_data_' . $contentType['id'] . ' WHERE id = :id',array(':id'=>$_GET['id']));
		}

		$row_actions = array();
		$row_actions[] = array('LINK'=>'http://' . get_url(1,null,array("parent"),array('typeid'=>$_GET["typeid"],'parent'=>'[ID]','id'=>'-1')),'TEXT'=>'<span class="glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;Добавить подуровень');
		$row_actions[] = array('LINK'=>'http://' . get_url(1,null,array("parent"),array('typeid'=>$_GET["typeid"],'parent'=>'[ID]')),'TEXT'=>'<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Перейти в подуровень');

		$controls['parentid'] = new Control('parentid','longlist','Владелец');
		$controls['parentid']->default_value = isset($_GET['parent']) ? intval($_GET['parent']) : 0;
		$controls['parentid']->tablename = '#__content';
		$controls['parentid']->config_name = 'content&typeid=' . intval($_GET["typeid"]);

	}
	else
	{
		if(!empty($_GET['id']) && intval($_GET['id']) > 0)
		{
			$currentContentType = dbGetOne('SELECT content_type FROM #__content WHERE id = :id',array(":id"=>intval($_GET['id'])));
			$dataFields = dbQuery("SELECT * FROM `#__content_fields` WHERE content_type = (SELECT content_type FROM #__content WHERE id = :id)",
			array(":id"=>intval($_GET['id']))
			);

		}
	}


	/* FILTERS */
	
	$fname = new Control('fname','text','Наименование');
	
	
	$filters = array($fname);
	/* FILTERS */


	$get_params = array("fast_edit","typeid");

	$source = 'SELECT id,created,active,mainimage,name,showorder FROM ' . $table;

	if(isset($_GET['parentcontrol']))
	{
		$source = 'SELECT id,name FROM ' . $table;
	}


	$where = " WHERE 1=1";

	if(isset($_GET['parent']))
	{
		$where .= " AND parentid = '" . intval($_GET['parent']) . "'";
	}
	else
	{
		$where .= " AND (parentid = '0' OR parentid = '-1')";
	}

	if(!empty($_GET["typeid"]))
	{
		 $where .= " AND content_type = '" . intval($_GET["typeid"]) . "'";
	}
	
	$fname_value = filters_get_value($fname); 
	if(!empty($fname_value))
	{
		 $where .= " AND LOWER(name) RLIKE '" . mb_strtolower($fname_value) . "'"; 
	}
	
	$source .= $where;
	$select_fields = "*, id as images,id as files";

	$title_fields["id"] = "ID";
	$edit_title_fields["id"] = "ID";
        
        $title_fields["created"] = "Дата";
	$edit_title_fields["created"] = "Дата";

	$title_fields["name"] = "Наименование";
	$edit_title_fields["name"] = "Наименование";

	$required_fields = array("name");

	$title_fields["mainimage"] = "Изображение";
	
	$title_fields["meta_title"] = "TITLE";
	$edit_title_fields["meta_title"] = "TITLE";

	$title_fields["meta_h1"] = "H1";
	$edit_title_fields["meta_h1"] = "H1";

	$title_fields["meta_description"] = "DESSCRIPTION";
	$title_fields["meta_keywords"] = "KEYWORDS";

	$title_fields["updated"] = "Дата изменения";
	$title_fields["active"] = "Активность";

	$title_fields["urlname"] = "Символьный код";
	$edit_title_fields["urlname"] = "Символьный код";

	$title_fields["showorder"] = "Порядок";
	$edit_title_fields["showorder"] = "Порядок";
        
        $unsorted_fields[] = 'mainimage';

	$eval_fields['name'] = "showEditLink(\$row);";
	$eval_fields['mainimage'] = "showMainImage(\$row);";
        $eval_fields['created'] = "showDateContentItem(\$row);";

	$exclude_fields_edit = array();
	$exclude_fields = array();

        
	$controls["active"] = new Control("active","checkbox","Активность");
	$controls["protected"] = new Control("protected","checkbox","Необходима авторизация");
	$controls["meta_keywords"] = new Control("meta_keywords","longtext","KEYWORDS");
	$controls["meta_description"] = new Control("meta_description","longtext","DESCRIPTION");
	$controls["meta_head"] = new Control("meta_head","longtext","Meta Head");
	$controls["extid"] = new Control("extid","text","Внешний код");
	$controls["id"] = new Control("id","label","ID");
	$controls["preview_text"] = new Control("preview_text","longtext","Краткий текст");
	$controls["tags"] = new Control("tags","longtext","Теги");

	$controls["detail_text"] = new Control("detail_text","richtext","Полный текст");
	$controls["detail_text"]->height = 300;

	$controls["content_date"] = new Control("content_date","date","Дата");
	$controls["content_date"]->default_value = date("Y-m-d H:i:s");

	$controls["images"] = new Control("images","template");
	$controls["images"]->template = "config/templates/images.php";
	$controls["images"]->template_mode = "full";

	$controls["files"] = new Control("files","template");
	$controls["files"]->template = "config/templates/files.php";
	$controls["files"]->template_mode = "full";


	$controls["content_type"] = new Control("content_type","hidden","Тип контента");
	$controls["content_type"]->default_value = !empty($_GET["typeid"]) ? intval($_GET["typeid"]) : 1;

	$controls['mainimage'] = new Control('mainimage','template','Основное изображение');
	$controls['mainimage'] ->template_mode = 'standart';
	$controls['mainimage']->template = "config/templates/mainimage.php";

	$controls['userid'] = new Control('userid','longlist','Автор');
	$controls['userid']->tablename = '#__users';
	$controls['userid']->textfield = 'email';
	$controls['userid']->config_name = 'users';

	
	


	if(!isFastEdit())
	{
		$eval_fields["active"] = "show_active(\$row);";
	}
	$tab1 = new Tab("Основная информация","",array("id","active","name","urlname","preview_text","detail_text","content_date","mainimage","content_type","showorder",'tags'));

	$tab2 = new Tab("SEO","",array("meta_title","meta_description","meta_keywords","meta_h1"));

	$tab3 = new Tab("Изображения","",array("images"));
	$tab31 = new Tab("Файлы","",array("files"));
	$tab4 = new Tab("Модули","",array());

	$tab10 = new Tab("Служебная информация","",array("protected","userid","parentid","meta_head","extid"));

	$tabs = array($tab1,$tab2,$tab3,$tab31,$tab4,$tab10);


	include('generate.fields.php');



	if(isset($_GET['parent']))
	{

		$path = array();
		getOpenContentTree($_GET['parent'],$path);
		$breadcrumbs[] = array("TEXT"=>$contentType['title_name'],"LINK"=>'http://' . get_url(1,null,array("parent"),array('typeid'=>$_GET["typeid"])));
		$path = array_reverse($path,TRUE);
		foreach($path as $key=>$value)
		{
			$breadcrumbs[] = array("TEXT"=>$value,"LINK"=>'http://' . get_url(1,null,array("parent"),array('typeid'=>$_GET["typeid"],'parent'=>$key)));
		}
	}


	function showEditLink($row)
	{
		?><a href="http://<?php echo get_url(1,null,array(),array('typeid'=>$_GET["typeid"],'id'=>$row['id'])); ?>"><?php echo $row['name']; ?></a><?php
	}
        
        function showDateContentItem($row)
        {
            $date = date_parse($row['created']);
            //echo "<pre>"; print_r($date); 
            $isodate = sprintf("%02d.%02d.%04d в %02d:%02d", $date['day'],$date['month'],$date['year'], $date['hour'], $date['minute']);
            echo $isodate;
            
        }


	$scripts[] = "config/js/content.js?v=1.1";

	global $engine_path;

	if(isset($_GET["typeid"]) && file_exists($engine_path . '/config/content-type-' . intval($_GET["typeid"]) . '.php'))
	{

		include('content-type-' . intval($_GET["typeid"]) . '.php');
	}
	if(isset($currentContentType) && file_exists($engine_path . '/config/content-type-' . $currentContentType . '.php'))
	{

		include('content-type-' . $currentContentType . '.php');
	}



}

