<?php

$title = 'Пользователи';

$table = '#__direct_users';

$source = "SELECT id,login,name,email,DATE_FORMAT(last_login,'%d.%m.%Y %H:%i:%s') as llogin,last_ip FROM " . $table;

$title_fields['login'] = 'Логин';
$title_fields['email'] = 'E-mail';
$title_fields['llogin'] = 'Вход';
$title_fields['last_ip'] = 'IP';

$controls['groupid'] = new Control('groupid','list','Группа прав','SELECT id,name FROM #__direct_usersgroup ORDER BY name');

$sort_changes['llogin'] = 'last_login';

$select_fields = 'id,login,passw,name,email,groupid';

$required_fields = array('login');

$controls['passw'] = new Control('passw','function','Пароль');
$controls['passw']->source = 'showPasswFields';

function showPasswFields()
{
    if(isset($_GET['id']) && $_GET['id'] == '-1')
    {
        ?>
            <input type="text" id="passw" required name="fields[passw]" class="form-control" />
        <?php
    }
    else
    {
        ?>
        <a class="btn btn-default" onclick="$(this).hide();$('#passw').show();return false;" href="">Изменить пароль</a>
        <input type="text" placeholder="Новый пароль" id="passw" style="display:none;" name="fields[passw]" class="form-control" />
        <?php
    }
}

function before_insert(&$data)
{
    if(!empty($data['passw']))
    {
        $data['passw'] = md5($data['passw']);
    }
    else
    {
        unset($data['passw']);
    }
    $data['code'] = md5(uniqid());
}

function before_update($id,$old,&$data)
{

    if(!empty($data['passw']))
    {
        $data['passw'] = md5($data['passw']);
    }
    else
    {
        unset($data['passw']);
    }
    $data['code'] = md5(uniqid());
}
