<?php

$title = 'Дополниетльные поля';

$table = '#__content_fields';


$source = 'SELECT id,name,code,caption,fieldtype,multiple,showorder FROM #__content_fields f WHERE content_type = ' . intval($_GET['parent']);

$title_fields['caption'] = 'Заголовок';
$title_fields['multiple'] = 'Множественное';
$title_fields['fieldtype'] = 'Тип поля';


$exclude_fields = array();

$controls['content_type'] = new Control('content_type','hidden');
$controls['content_type']->default_value = intval($_GET['parent']);


$controls['caption'] = new Control('caption','text','Заголовок');

$controls['multiple'] = new Control('multiple','checkbox','Множественное');
$controls['required'] = new Control('required','checkbox','Обязательное');

$controls['source'] = new Control('source','text','Источник');

$controls['fieldtype'] = new Control('fieldtype','list','Тип поля',
	array(
		'1'=>'Текст',
		'2'=>'Число',
		'3'=>'Дата',
		'4'=>'Список',
		'5'=>'Длинный текст'
	)
);

$controls['params'] = new Control('params','template','Параметры');
$controls['params']->template_mode = 'standart';
$controls['params']->template = 'config/templates/field_params.php';

function after_insert($id,$data)
{
	switch($data['fieldtype'])
	{
		case "1":
			dbNonQuery('ALTER TABLE `rs_content_data_' . $data['content_type'] . '` ADD `f' . $id . '` varchar(1000) NOT NULL;');
			break;
		case "2":
			dbNonQuery('ALTER TABLE `rs_content_data_' . $data['content_type'] . '` ADD `f' . $id . '` int(11) NOT NULL;');
			break;	
		case "3":
			dbNonQuery('ALTER TABLE `rs_content_data_' . $data['content_type'] . '` ADD `f' . $id . '` datetime NOT NULL;');
			break;
		case "4":
			dbNonQuery('ALTER TABLE `rs_content_data_' . $data['content_type'] . '` ADD `f' . $id . '` int(11) NOT NULL;');
			break;
		case "5":
			dbNonQuery('ALTER TABLE `rs_content_data_' . $data['content_type'] . '` ADD `f' . $id . '` text COLLATE \'utf8_general_ci\' NOT NULL;');
			break;
	}
}

function after_update($id,$data)
{
	
}

function before_delete($id)
{
	$content_type = dbGetOne('SELECT content_type FROM #__content_fields WHERE id = :id',array(':id'=>$id));
	dbNonQuery('ALTER TABLE `rs_content_data_' . $content_type . '` DROP `f' . $id . '`;');
}

function after_delete($id)
{
	
}


