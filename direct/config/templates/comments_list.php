<?php
$comments = dbQuery('SELECT review_id,detail_text FROM #__comments_review WHERE userid = :uid', array(':uid' => $_GET["id"]));
//echo "<pre>"; print_r($comments);

if(!empty($comments))
    { ?>
    <table class="t table table-hover">
        <tr>
            <th>Текст комментария</th>
            <th>Название отзыва</th>
            <th>Автор отзыва</th>
            <th>Название темы</th>
            <th>Автор темы</th>
            <th>Категория темы</th>
        </tr><?php
        $flag = 1;
        foreach($comments as $comment)
        { 
            $review = dbGetRow('SELECT c.userid,c.name,c.url,cd.f1 AS topic_id FROM #__content c '
                            . 'LEFT JOIN #__content_data_3 cd ON cd.id = c.id '
                            . 'WHERE c.id = :rid', array(':rid' => $comment['review_id']));
            $review_author = dbGetRow('SELECT u.id,u.name,p.name AS profile_name FROM #__users u '
                               . 'LEFT JOIN #__profiles p ON p.userid = u.id '
                               . 'WHERE u.id = :uid', array(':uid' => $review['userid']));
            $topic = dbGetRow('SELECT c.id,c.userid,c.name,c.url,cd.f2 AS category_id FROM #__content c '
                            . 'LEFT JOIN #__content_data_4 cd ON cd.id = c.id '
                            . 'WHERE c.id = :tid', array(':tid' => $review['topic_id']));
            $author_topic = dbGetRow('SELECT u.id,u.name,p.name AS profile_name FROM #__users u '
                               . 'LEFT JOIN #__profiles p ON p.userid = u.id '
                               . 'WHERE u.id = :uid', array(':uid' => $topic['userid'])); 
            $category = dbGetRow('SELECT name,url FROM #__content WHERE id = :cid AND content_type = 2', array(':cid' => $topic['category_id'])); ?>
        
            <tr class="<?php if($flag % 2 == 0) echo "atr"; else echo "tr"; ?>">
                <td><?php echo $comment['detail_text']; ?></td>
                <td><a target="_blank" href="<?php echo $review['url']; ?>"><?php echo $review['name'] ?></a></td>
                <td><a target="_blank" href="/user/<?php echo $review_author['userid']; ?>/"><?php if(!empty($review_author['name'])) echo $review_author['name']; else if(!empty($review_author['profile_name'])) echo $review_author['profile_name']; else echo 'Инкогнито'; ?></a></td>
                <td><a target="_blank" href="<?php echo $topic['url']; ?>"><?php echo $topic['name'] ?></a></td>
                <td><a target="_blank" href="/user/<?php echo $author_topic['userid']; ?>/"><?php if(!empty($author_topic['name'])) echo $author_topic['name']; else if(!empty($author_topic['profile_name'])) echo $author_topic['profile_name']; else echo 'Инкогнито'; ?></a></td>
                <td><?php if(!empty($category['name'])) echo '<a target="_blank" href="/'.$category['url'].'">'.$category['name'].'</a>'; else echo "Без категории" ?></td>
            </tr><?php
            $flag++;
        } ?>
    </table><?php
    }
    else
    {
        echo "У этого пользователя ещё нет отзывов к темам";
    } ?>

