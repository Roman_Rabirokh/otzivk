<?php

$reviews = dbQuery('SELECT c.name,c.url,cd.f1 AS topic_id FROM #__content c '
        . 'LEFT JOIN #__content_data_3 cd ON cd.id = c.id '
        . 'WHERE userid = :uid AND content_type = 3',array(":uid"=>$_GET["id"]));

if(!empty($reviews))
    { ?>
    <table class="t table table-hover">
        <tr>
            <th>Название отзыва</th>
            <th>Название темы</th>
            <th>Автор темы</th>
            <th>Категория темы</th>
        </tr><?php
        $flag = 1;
        foreach($reviews as $review)
        { 
            $topic = dbGetRow('SELECT c.userid,c.name,c.url,cd.f2 AS category_id FROM #__content c '
                            . 'LEFT JOIN #__content_data_4 cd ON cd.id = c.id '
                            . 'WHERE c.id = :tid', array(':tid' => $review['topic_id']));
        $author_topic = dbGetRow('SELECT u.id,u.name,p.name AS profile_name FROM #__users u '
                               . 'LEFT JOIN #__profiles p ON p.userid = u.id '
                               . 'WHERE u.id = :uid', array(':uid' => $topic['userid'])); 
        $category = dbGetRow('SELECT name,url FROM #__content WHERE id = :cid AND content_type = 2', array(':cid' => $topic['category_id'])); ?>
            <tr class="<?php if($flag % 2 == 0) echo "atr"; else echo "tr"; ?>">
                <td><a href="/<?php echo $review['url']; ?>"><?php echo $review['name']; ?></a></td>
                <td><a target="_blank" href="/<?php echo $topic['url']; ?>"><?php echo $topic['name'] ?></a></td>
                <td><a target="_blank" href="/user/<?php echo $topic['userid']; ?>/"><?php if(!empty($author_topic['name'])) echo $author_topic['name']; else if(!empty($author_topic['profile_name'])) echo $author_topic['profile_name']; else echo 'Инкогнито'; ?></a></td>
                <td><?php if(!empty($category['name'])) echo '<a target="_blank" href="/'.$category['url'].'">'.$category['name'].'</a>'; else echo "Без категории" ?></td>
            </tr><?php
            $flag++;
        } ?>
    </table><?php
    }
    else
    {
        echo "У этого пользователя ещё нет отзывов к темам";
    } ?>

