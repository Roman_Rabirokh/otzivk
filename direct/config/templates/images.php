<?php 

$images = dbQuery('SELECT * FROM #__images WHERE source = 1 AND parentid = :parentid ORDER BY showorder',array(":parentid"=>$_GET["id"]));
?>
<script>
	var strFileImage = '<div class="form-inline"><input type="file" multiple name="images[#index#][file]" class="form-control" >&nbsp;<input type="text" placeholder="Наименование" multiple name="images[#index#][title]" class="form-control" >&nbsp;<input placeholder="Описание" type="text" multiple name="images[#index#][description]" class="form-control" >&nbsp;<input type="text" value="500" multiple name="images[#index#][showorder]" placeholder="Описание" class="form-control" ></div><br />';
	var fileInd = 0;
	function addImage()
	{
		fileInd = fileInd  + 1;
		var newStrFile = strFileImage.replace(/#index#/g,fileInd);
		$("#imagesUpload").append(newStrFile );
	}
	
</script>
<div class="panel panel-default">
	<div class="panel-body">
		<div id="imagesUpload">
			<div class="form-inline">
				<input type="file" multiple name="images[0][file]" class="form-control" >
				<input type="text" multiple name="images[0][title]" placeholder="Наименование" class="form-control" >
				<input type="text" multiple name="images[0][description]" placeholder="Описание" class="form-control" >
				<input type="text" value="500" multiple name="images[0][showorder]" placeholder="Описание" class="form-control" >
			</div><br />
		</div>
		<div class="form-inline">
			<input type="button" onclick="addImage()" value="Добавить изображение" class="btn btn-default" />
		</div>	
	</div>
</div>
<?php if(!empty($images)) {?>
<div class="panel panel-default">
	<div class="panel-body">
		<table class="table">
			<?php foreach($images as $img) { 
			
			$thumb = getImageById($img["id"],array("k"=>80));
			
			?>
			<tr>
				<td><input type="checkbox" /></td>
				<td><input type="radio" name="mainimage" /></td>
				<td><a target="_blank" href="<?php echo _IMAGES_URL . $img["filename"] ; ?>"><img class="img-thumbnail" src="<?php echo $thumb; ?>" /></a> </td>
				<td><?php echo $img["originalname"]; ?></td>
				<td><?php echo $img["width"] . ' x ' . $img["height"]; ?></td>
				<td><input type="text" value="<?php echo $img["title"]; ?>" placeholder="Наименование" class="form-control" /></td>
				<td><input type="text" value="<?php echo $img["description"]; ?>" placeholder="Описание" class="form-control" /></td>
				<td><input type="text" value="<?php echo $img["showorder"]; ?>" placeholder="Порядок" class="form-control" /></td>
				<td><a href="javascript:void(0);" onclick="rmImage(<?php echo $img["id"]; ?>)" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a></td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>	
<?php } ?>
