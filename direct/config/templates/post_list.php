<?php
    include_once(_DIR . "api/image.php");
    $posts = dbQuery('SELECT c.id,c.name,c.url,c.mainimage,cd.f2 as category_id FROM #__content c '
                    .'LEFT JOIN #__content_data_4 cd ON cd.id = c.id '
                    . 'WHERE c.userid = :uid AND c.content_type = 4',
                        array(":uid"=>$_GET["id"])
                    );

    if(!empty($posts))
    { ?>
    <table class="t table table-hover">
        <tr>
            <th>Категория</th>
            <th>Название</th>
            <th>Изображение</th>
            <th>Отзыв к теме</th>
            <th>Комментарии к отзыву</th>
        </tr><?php
        $flag = 1;
        foreach($posts as $post)
        { 
            $post_image = dbGetOne('SELECT filename FROM #__images WHERE parentid = :iid', array(':iid' => $post['mainimage']));
            $category = dbGetRow('SELECT name,url FROM #__content WHERE id = :cid AND content_type = 2', array(':cid' => $post['category_id'])); 
            $reviews = dbQuery('SELECT c.id,c.name,c.url FROM #__content c '
                              .'LEFT JOIN #__content_data_3 cd ON cd.id = c.id '
                              .'WHERE cd.f1 = :tid AND content_type = 3 AND userid = :uid',
                                array(
                                    ':tid' => $post['id'],
                                    ':uid' => intval($_GET['id'])
                                )
                              );
            if(!empty($reviews))
            {
                $comments_list = array();
                foreach($reviews as $review)
                {
                    $comments = dbQuery('SELECT detail_text FROM #__comments_review WHERE review_id = :rid', array(':rid' => $review['id']));
                    if(!empty($comments))
                    {
                        $comments_list[] = $comments;
                    }
                }  
            } ?>
            <tr class="<?php if($flag % 2 == 0) echo "atr"; else echo "tr"; ?>">
                <td><?php if(!empty($category)) { ?> <a href="/<?php echo $category['url']; ?>"><?php echo $category['name']; ?></a> <?php } else { echo "Без категории"; } ?></td>
                <td><a target="_blank" href="/<?php echo $post['url']; ?>"><?php echo $post['name'] ?></a></td>
                <td><img src="<?php echo getImageById($post['mainimage'],array("k"=>150)); ?>" alt="Изображение темы" /></td>
                <td><?php
                    if(!empty($reviews))
                    { ?>
                    <ul class="review-list"><?php
                        foreach($reviews as $review)
                        { ?>
                        <li><a target="_blank" href="/<?php echo $review['url']; ?>" ><?php echo $review['name']; ?></a></li><?php
                        } ?>
                    </ul><?php
                    }
                    else
                    {
                        echo "У пользователя нет отзывов к этой теме";
                    } ?>
                </td>
                <td><?php
                    if($comments_list)
                    { ?>
                        <ul class="comments-review-list" ><?php
                        foreach($comments_list as $comment)
                        { ?>
                            <li><?php
                            if(!empty($comment))
                            { ?>
                                <ul class="comments-list"><?php
                                foreach($comment as $comment_item)
                                { ?>
                                <li><?php echo $comment_item['detail_text']; ?></li><?php
                                } ?>
                                </ul><?php
                            } ?>
                            </li><?php
                        } ?>
                        </ul><?php
                    }
                    else
                    {
                        echo "У пользователя нет комментариев к своему отзыву";
                    }
                    ?>
                </td>
            </tr><?php
            $flag++;
        } ?>
    </table><?php
    }
    else
    {
        echo "У этого пользователя ещё нет добавленных тем";

    } ?>

    <style>
        table tr th{
            text-align: left;
            height: 50px;
        }

         table tr td{
            text-align: left;
            padding-left: 15px;
            height: 35px;
        }
        .review-list{
            padding-left: 0;
            list-style: none;
        }
        .comments-review-list{
            list-style: none;
            padding-left: 0;
        }
        .comments-list{
            padding-left: 0;
        }
    </style>





