<?php

$messages = dbQuery('SELECT * FROM #__messages_from_admin WHERE userid = :uid ORDER BY created DESC', array('uid' => $_GET['id']));
if(!empty($messages))
    { ?>
    <table class="t table table-hover message_table_list">
        <tr>
            <th>Дата сообщения</th>
            <th>Тема сообщения</th>
            <th>Текст сообщения</th>
        </tr><?php
        $flag = 1;
        foreach($messages as $msg)
        { 
            $date = date_parse($msg['created']);
            //echo "<pre>"; print_r($date); 
            $isodate = sprintf("%02d.%02d.%04d в %02d:%02d", $date['day'],$date['month'],$date['year'], $date['hour'], $date['minute']);?>
            <tr class="<?php if($flag % 2 == 0) { echo "atr";} else { echo "tr"; } ?>">
                <td><?php echo $isodate; ?></td>
                <td><?php echo $msg['subject']; ?></td>
                <td><?php echo $msg['text_msg']; ?></td>
            </tr><?php
            $flag++;
        } ?>
    </table><?php
    }
    else
    {
        echo "Этому пользователю не отправлялись сообщения от администрации";
    } 

