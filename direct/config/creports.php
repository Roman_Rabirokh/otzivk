<?php

$title = 'Жалобы на комментарии';

$table = '#__comments_review';


$source = 'SELECT u.id, u.detail_text, u.review_id, '
        . ' (SELECT name FROM #__profiles WHERE userid = u.userid) as name_user, '
        //. ' u.id as name_user_report, '
        . ' u.id as reports, '
        . ' u.id as count_report, '
        . ' u.id as post '
	. ' FROM  ' . $table . ' u '
        . ' WHERE u.id = (SELECT  DISTINCT comment  FROM #__comments_reports WHERE comment = u.id) ';



$title_fields["name_user"] = "Комментарий оставил";
$title_fields["detail_text"] = "Текст комментария";
//$title_fields["name_user_report"] = "Жалобу отправил";
$title_fields["count_report"] = "Кол-во всех жалоб на комментарий";
$title_fields["post"] = "Оставлен к отзыву:";
$title_fields["reports"] = "Жалобы";

//$eval_fields['name_user_report'] = "getNameUser(\$row);";

$eval_fields['count_report'] = "getСountReport(\$row);";

$eval_fields['reports'] = "getUrlReport(\$row);";

$eval_fields['post'] = "getUrlPost(\$row);";


$unsorted_fields[] = 'name_user';
$unsorted_fields[] = 'text';
$unsorted_fields[] = 'idComment';
//$unsorted_fields[] = 'name_user_report';
$unsorted_fields[] = 'count_report';
$unsorted_fields[] = 'post';
$unsorted_fields[] = 'reports';


$exclude_fields[] = 'review_id';
$exclude_fields[] = 'id';


function getNameUser($row)
{
    $name= dbGetOne('SELECT p.name FROM #__profiles AS p '
            . ' INNER JOIN #__comments_reports  AS c ON(c.user=p.userid)  '
            . ' WHERE c.comment= :id ', array(':id' => $row['id']));
    if(empty($name))
	$name='Нет имени';
    print_r ($name);
}
function getСountReport($row)
{
    $count= dbGetOne('SELECT COUNT(id) FROM  #__comments_reports'
            . ' WHERE comment= :id ', array(':id' => $row['id']));
    if(empty($count))
	$count='Опапа 0!';
    print_r ($count);
}

function getUrlReport($row)
{
    ?><a href="/direct/index.php?t=creportscomment&idc=<?=$row['id']; ?>">Перейти к жалобам </a> <?php
}
function getUrlPost($row)
{
    $data= dbGetRow('SELECT url, name FROM #__content WHERE id= :id ', [':id' => $row['review_id']]);
    
    ?><a href="/<?php echo $data['url']; ?>/">Перейти : <?php echo $data['name'] ?>  </a> <?php
}

