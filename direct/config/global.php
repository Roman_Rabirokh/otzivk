<?php
	$language = "ru";//interface language
	$version_info = 'RS Engine v 3.7.7';//cms version
	$config_title = $version_info;//default title for all document
	$engine_path = dirname(getenv('SCRIPT_FILENAME'));//path to engine folder
	define('_ENGINE_PATH',$engine_path );

	$base_config_path = dirname(getenv('SCRIPT_FILENAME'))."/config/base.php"; //path to custom default config path
	$menu = array(); //menu for framework
	$titles = array();//array of titles, use when config file not found
	$sourceid = 0;
    $pagesize_array = array(10,20,50,100);
