<?php

$title = 'Пользователи';

$table = '#__users';

$source =   'SELECT u.id,u.email,u.blocked,u.name,u.money,u.created,u.socialservice,u.country,u.city,u.qnt_topic,u.qnt_review,u.qnt_comments,u.rating,u.qnt_warning,'
          . ' (SELECT social_page_link FROM #__profiles WHERE userid = u.id) AS social_page'
          . ' FROM  ' . $table . ' u';


$title_fields["name"] = "Имя";
$title_fields["email"] = "E-mail";
$title_fields["social_page"] = "Соцсеть";
$title_fields["money"] = "Бонусы";
$title_fields["created"] = "Дата регистрации";
$title_fields["rating"] = "Рейтинг";
$title_fields["country"] = "Страна";
$title_fields["city"] = "Город";
$title_fields["qnt_topic"] = "К-во созданных тем";
$title_fields["qnt_review"] = "К-во написанных отзывов";
$title_fields["qnt_comments"] = "К-во написанных комментариев";
$title_fields["qnt_warning"] = "К-во предупреждений";
$title_fields["blocked"] = "Блокировка";


$exclude_fields[] = 'socialservice';
$exclude_fields[] = 'id';
$exclude_fields[] = 'subscribes';
$exclude_fields[] = 'messages_from_admin';

$unsorted_fields[] = 'edit_profile';
$unsorted_fields[] = 'social_page';

$eval_fields['name'] = "getLinkUser(\$row);";
$eval_fields['social_page'] = "getLinkSocialPage(\$row);";
$eval_fields['created'] = "getDateRegister(\$row);";

function getLinkSocialPage($row)
{
    if(!empty($row['social_page']))
    {
        echo '<a target="_blank" href='.$row['social_page'].'>'.$row['socialservice'].'</a>';
    }
    
}

function getCountryUser($row)
{
    if(!empty($row['country'])) { echo $row['country']; }
}

function getCityUser($row)
{
    if(!empty($row['city'])) { echo $row['city']; }
}

function getDateRegister($row)
{
    $date = new DateTime($row['created']);
    echo $date->format('d.m.Y');
}

function getLinkUser($row)
{
    if(!empty($row['name']))
    {
        echo '<a target="_blank" href="/user/'.$row['id'].'/" >'.$row['name'].'</a>';
    }
    else
    {
        $user_name = dbGetOne('SELECT name FROM #__profiles WHERE userid = :uid', array(':uid' => $row['id']));
        if(!empty($user_name))
        {
            echo '<a target="_blank" href="/user/'.$row['id'].'/" >'.$user_name.'</a>';
        }
        else
        {
            echo '<a target="_blank" href="/user/'.$row['id'].'/" >Инкогнито</a>';
        }  
    }
}

$controls["post_list"] = new Control("post_list","template");
$controls['post_list'] ->template_mode = "full";
$controls["post_list"]->template = "config/templates/post_list.php";

$controls["subscribes"] = new Control("subscribes","template", "Подписки пользователя");
$controls['subscribes'] ->template_mode = "standart";
$controls["subscribes"]->template = "config/templates/subscribes_list.php";

$controls["review_list"] = new Control("review_list","template");
$controls['review_list'] ->template_mode = "full";
$controls["review_list"]->template = "config/templates/review_list.php";

$controls["comments_list"] = new Control("comments_list","template");
$controls['comments_list'] ->template_mode = "full";
$controls["comments_list"]->template = "config/templates/comments_list.php";

$controls["messages_from_admin"] = new Control("messages_from_admin","template");
$controls['messages_from_admin'] ->template_mode = "full";
$controls["messages_from_admin"]->template = "config/templates/messages_from_admin.php";

$controls["messages_from_user"] = new Control("messages_from_user","template");
$controls['messages_from_user'] ->template_mode = "full";
$controls["messages_from_user"]->template = "config/templates/messages_from_user.php";

$tab1 = new Tab("Основная информация","",array("id","email","created","blocked","country","city", "money","rating","qnt_topic", "qnt_review","qnt_comments", "subscribes"));
$tab2 = new Tab("Темы пользователя","",array("post_list"));
$tab3 = new Tab("Отзывы пользователя","",array("review_list"));
$tab4 = new Tab("Комментарии пользователя к отзывам","",array("comments_list"));
$tab5 = new Tab("Сообщения пользователю","",array("messages_from_admin"));
$tab6 = new Tab("Сообщения от пользователя","",array("messages_from_user"));

$tabs = array($tab1,$tab2,$tab3,$tab4,$tab5,$tab6);

$select_fields='id,email,created,blocked,money,country,city,rating,qnt_topic,qnt_review,qnt_comments,id AS post_list,id AS review_list,id AS comments_list,id AS subscribes,id AS messages_from_admin, id AS messages_from_user';

include('generate.fields.php');




