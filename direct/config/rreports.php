<?php

$title = 'Жалобы на отзывы';

$table = '#__message_to_moderator';


$source = 'SELECT cr.id, cr.created, cr.review_id, cr.subject,  cr.text_msg, cr.user_from, '
        . ' (SELECT name FROM #__profiles WHERE userid = cr.user_from) as name_user, '
        . ' cr.review_id as review, '
        . 'cr.is_read '
	. ' FROM  ' . $table . ' cr ';
       // . ' WHERE u.id = (SELECT  DISTINCT comment  FROM #__comments_reports WHERE comment = u.id) ';



$title_fields["created"] = "Дата";
$title_fields["name_user"] = "Жалобу оставил";
$title_fields["subject"] = "Тема";
$title_fields["text_msg"] = "Текст жалобы";
$title_fields["review"] = "Оставлен к отзыву";
$title_fields["is_read"] = "Статус / Изменить";

$title_fields["reports"] = "Жалобы";

//$eval_fields['name_user_report'] = "getNameUser(\$row);";

$eval_fields['count_report'] = "getСountReport(\$row);";

$eval_fields['reports'] = "getUrlReport(\$row);";

$eval_fields['review'] = "getUrlReview(\$row);";

$eval_fields['created'] = "getDateReport(\$row);";

$eval_fields['name_user'] = "getUserInfo(\$row);";

$eval_fields['is_read'] = "getStatusTitle(\$row);";




$unsorted_fields[] = 'name_user';
$unsorted_fields[] = 'review';



$exclude_fields[] = 'review_id';
$exclude_fields[] = 'id';
$exclude_fields[] = 'user_from';


function getNameUser($row)
{
    $name= dbGetOne('SELECT p.name FROM #__profiles AS p '
            . ' INNER JOIN #__comments_reports  AS c ON(c.user=p.userid)  '
            . ' WHERE c.comment= :id ', array(':id' => $row['id']));
    if(empty($name))
	$name='Нет имени';
    print_r ($name);
}
function getСountReport($row)
{
    $count= dbGetOne('SELECT COUNT(id) FROM  #__comments_reports'
            . ' WHERE comment= :id ', array(':id' => $row['id']));
    if(empty($count))
	$count='Опапа 0!';
    print_r ($count);
}

function getUrlReport($row)
{
    ?><a href="/direct/index.php?t=creportscomment&idc=<?=$row['id']; ?>">Перейти к жалобам </a> <?php
}
function getUrlReview($row)
{
    $data= dbGetRow('SELECT url, name FROM #__content WHERE id= :id ', [':id' => $row['review_id']]);
    
    ?><a href="/<?php echo $data['url']; ?>/">Перейти : <?php echo $data['name'] ?>  </a> <?php
}

function getUserInfo($row)
{
    echo '<a href="/user/'.$row['user_from'].'">'.$row['name_user'].'</a>';
}

function getStatusTitle($row)
{
    if($row['is_read'] == 0)
    {
        echo '<span class="report-no-read" data-id="'.$row['id'].'" style="color:red;cursor:pointer;" onclick="changeReadStatusOn(this);"><b>Не прочитано</b></span>';
    }
    else
    {
        echo '<span class="report-no-read" data-id="'.$row['id'].'" style="cursor:pointer;" onclick="changeReadStatusOff(this);"><b>Прочитано</b></span>';;
    }
}

function getDateReport($row)
{
    $date = new DateTime($row['created']);
    echo $date->format('d.m.Y в H:m');
}

$controls["text_msg"] = new Control("text_msg","longtext", "Текст жалобы");
$controls['text_msg'] ->height = 300;



$select_fields='id,created,subject, text_msg';

include('generate.fields.php');

