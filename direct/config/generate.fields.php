<?php


$fields = array();
if(!empty($dataFields)) {
foreach($dataFields as $field)
{
	$fname = 'f' . $field['id'];
	switch($field['fieldtype'])
	{
		
		case 1:
			
			if($field['multiple'] == 1)
			{
				
			}
			else
			{
				$controls[$fname] = new Control($fname,'text',$field['name']);
				$fields[$fname] = isset($dataFieldsValues[$fname]) ? $dataFieldsValues[$fname] : '';
			}
			
			$tab1->addField($fname);
			
			break;
		case 2:
			
			if($field['multiple'] == 1)
			{
				
			}
			else
			{
				$controls[$fname] = new Control($fname,'number',$field['name']);
				$fields[$fname] = isset($dataFieldsValues[$fname]) ? $dataFieldsValues[$fname] : '';
			}
			
			$tab1->addField($fname);
			
			break;
		case 3:
			
			if($field['multiple'] == 1)
			{
				
			}
			else
			{
				$controls[$fname] = new Control($fname,'date',$field['name']);
				$fields[$fname] = isset($dataFieldsValues[$fname]) ? $dataFieldsValues[$fname] : '';
			}
			
			$tab1->addField($fname);
			
			break;
		
		case 4:
			
			$fsource = '';
			if(intval($field['source']) > 0)
			{
				$fsource = 'SELECT id,name FROM #__content WHERE content_type = ' . intval($field['source']) . ' ORDER BY name,showorder';
			}
			else
			{
				$fsource = $field['source'];	
			}
			
			$controls[$fname] = new Control($fname,'list',$field['name'],$fsource);
		
			if($field['multiple'] == 1)
			{
				$controls[$fname]->is_multiple = TRUE;	
				$controls[$fname]->size = 10;
				if(!empty($_GET['id']))
				{
					$fields[$fname] = dbQueryToArray('SELECT valueid FROM #__content_field_' . $field['id'] . ' WHERE parentid = :id',array(':id'=>intval($_GET['id'])));
				}
			}
			else
			{
				$fields[$fname] = isset($dataFieldsValues[$fname]) ? $dataFieldsValues[$fname] : 0;
			}

			$tab1->addField($fname);

			break;
		case 5:

			if($field['multiple'] == 1)
			{
				
			}
			else
			{
				$controls[$fname] = new Control($fname,'longtext',$field['name']);
				$controls[$fname]->rows = 5;
				$fields[$fname] = isset($dataFieldsValues[$fname]) ? $dataFieldsValues[$fname] : '';
			}
			
			$tab1->addField($fname);
			
			break;
			
			
	}	
	
	
	if($field['required'] == 1)
	{
		$controls[$fname]->required = TRUE;
	}
}}


function updateContentFields($id,$data,$action)
{
//        echo "<pre>"; print_r($data); die;
	$contentType = dbGetOne('SELECT content_type FROM #__content WHERE id = :id',array(':id'=>$id));
	$dataFields = dbQuery("SELECT * FROM `#__content_fields` WHERE content_type = :id",
	array(":id"=>$contentType)
	);
	
	$dataTableName = '#__content_data_' . $contentType;
	
	
	if($action == 2)
	{
		
	}
	else
	{
		//check if exists row in fields table 
		
		$dataRow = dbGetOne('SELECT id FROM ' . $dataTableName . ' WHERE id = :id',array(':id'=>$id));
		
		if(empty($dataRow))
		{
			dbNonQuery('INSERT INTO ' . $dataTableName . ' (id) VALUES(:id) ',array(':id'=>$id));
		}
		
		foreach($dataFields as $field)
		{
			$fname = 'f' . $field['id'];
			switch($field['fieldtype'])
			{
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					if($field['multiple'] == 1)
					{
						$table = '#__content_field_' . $field['id'];
						dbNonQuery('DELETE FROM ' . $table . ' WHERE parentid = :id',array(':id'=>$id));
						if(isset($data[$fname]) && is_array($data[$fname]))
						{
							foreach($data[$fname]  as  $current)
							{
								dbNonQuery('INSERT INTO ' . $table . ' (parentid,valueid) VALUES(:parentid, :valueid)',array(':parentid'=>$id,':valueid'=>$current));	
							}						
						}
						
					}
					else
					{
						dbNonQuery('UPDATE ' . $dataTableName . ' SET ' . $fname . '=:' . $fname . ' WHERE id = :id ',
						array(':id'=>$id,
						':' . $fname =>$data[$fname]
						));	
					}
					break;	
			}
		}
	}
}

