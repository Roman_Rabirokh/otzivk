<html>
<head>
<title><?php echo $config->title?> | <?php echo $config_title?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="js/bootstrap/css/bootstrap-theme.min.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/admin.css" />
<link rel="stylesheet" type="text/css" href="js/chosen/chosen.min.css" />
<!--[if lt IE 9]>
	<script src="js/bootstrap/html5shiv.js"></script>
	<script src="js/bootstrap/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="js/jquery.js"></script>	
<script type="text/javascript" src="js/tinymce/tinymce.min.js?v=4.3.10"></script>
<script type="text/javascript" src="js/tinymce/jquery.tinymce.min.js?v=4.3.10"></script>
<script type="text/javascript" src="js/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="js/const.php"></script>	
<script type="text/javascript" src="js/global_function.js?v=1.2"></script>

<link rel="stylesheet" href="/templates/libs/fancybox/jquery.fancybox.css" />
</head>
<body >