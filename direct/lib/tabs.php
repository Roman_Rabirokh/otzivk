<?php 
class Tabs
{
	var $items = array();
	function Tabs($items)
	{
		$this->items = $items;
	}
	function AddTab($tab)
	{
		array_push($tab,$this->items);
	}
	function create()
	{
		?>
		<script language="JavaScript" type="text/javascript">
		var tabs = Array(<?php $i = 0; foreach($this->items as $current) {  echo "'tab_$i',"; $i++; } ?>'');
		var fields = new Array();
		<?php
		$i = 0;
		foreach($this->items as $current) 
		{
			?>
			fields[<?php echo $i?>] = new Array(<?php echo (implode(",",$current->fields))?>);
			<?php
			$i++;
		}
		?>
		</script>
		<div>&nbsp;</div>
		<ul class="nav nav-tabs" role="tablist">
			<?php $i = 0; foreach($this->items as $current) {  ?>
				<li <?php if($i == 0) { ?> class="active" <?php } ?>><a id="tab_<?php echo $i?>" onclick="SetCurrentTab(this,<?php echo $i?>);<?php echo $current->onclick; ?>" href="#home" role="tab" data-toggle="tab"><?php echo $current->caption?></a></li>
			<?php $i++; } ?>
		
		</ul>		
		<?php
	}

}
class Tab
{
	var $caption = ""; // tab's caption
	var $onclick = ""; // JavaScript function onclik
	var $fields = array();// array of controls aasigned with this tab
	function Tab($caption,$onclick,$fields = array())
	{
		$this->caption = $caption;
		$this->onclick = $onclick;
		foreach($fields as $current)
		{
			array_push($this->fields,"'tr_$current'");
		}
	}
	function addField($name)
	{
		array_push($this->fields,"'tr_$name'");	
	}
}
