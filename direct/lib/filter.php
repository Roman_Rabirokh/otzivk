<?php
class Filter
{
	var $filters = array();
	function Filter($filters)
	{
		$this->filters = $filters;
	}
	function init()
	{
		if(count($this->filters))
		{
			?>
			<div id="filter" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
	<form method="post" method="post">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Фильтры</h4>
      </div>
			<div class="filter modal-body"   ><table class="table" border="0">
			<?php
			$post_exists = false;
			foreach($this->filters as $current)
			{
				$current->name = $_GET["t"]."_".$current->id;
				
				
				if(isset($_POST[$current->name]))
				{
					$post_exists = true;
					if($current->is_multiple)
					{
						$current->value = implode(",",$_POST[$current->name]);
					}
					else
					{
						$current->value = $_POST[$current->name];
					}
					$_SESSION[$current->name] = $current->value;
					if($current->type == "checkbox")
					{
						$current->value = 1;
						$_SESSION[$current->name] = 1;
					}					
					
				}
				else
				{
					if(isset($_SESSION[$current->name]))
					{
						$current->value = $_SESSION[$current->name];
						if($current->type == "checkbox")
						{
							$current->value = 1;
						}	
					}	
					
				}
				if($post_exists)
				{
					if($current->type == "checkbox" && !isset($_POST[$current->name]))
					{
						$current->value = 0;
						unset($_SESSION[$current->name]);
					}
				}
				if($current->type == "checkbox")
				{
					echo "<tr><td ><label>".$current->caption."</label></td><td >";
					$current->create();
					echo "</td></tr>";
				}
				else
				{
					echo "<tr><td ><label>".$current->caption."</label></td><td >";
					$current->create();
					echo "</td></tr>";
				}	
			}
			?></table></div>
			  <div class="modal-footer">
			  <input type="submit" class="btn btn-default" value="<?php echo _APPLY?>" />&nbsp;
			   <button type="button" class="btn btn-default" class="close" data-dismiss="modal" aria-label="Close"><?php echo _CLOSE_FILTER?></button>
			  </div></form>
			</div>
			</div>
			</div>
			<?php
		}
	}
}
function filters_get_value($filter)
{
	$name = $_GET["t"]."_".$filter->id;
	if(isset($_POST[$name]))
	{
		if($filter->is_multiple)
		{
			return  implode(",",$_POST[$name]);
		}
		else
		{
			return  $_POST[$name];
		}	
		
	}
	else
	{
		if(isset($_SESSION[$name]))
		{
			return $_SESSION[$name];
		}
	}
}
