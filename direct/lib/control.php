<?php
class Control
{
	var $caption = "";
	var $type = "text";//text,email,tel,date,longtext,list,checkbox,label,template,radio,function,hidden,longlist,time,richtext
	var $source = "";
	var $keyfield = "id";
	var $textfield = "name";
	var $css_class = "";
	var $css_style = "";
	var $attr = "";
	var $js = ""; //javascript 
	var $id = ""; // control id
	var $name = ""; //control name
	var $required = false; // is required
	var $value = "";
	var $template = "";
	var $max_length = "";
	var $argument = array();//for function
	var $rows = 3;//for longtext
	var $default_value = "";
	var $is_multiple = "";//for list
	var $size = "";//for list
	var $onchange = "";//for list
	var $tooltip = "";
	var $date_display = "";
	var $template_mode = "standart"; //full,standart
	var $current_row = array();
	var $html = ""; //additional html in control area
	var $tablename = ""; //name of table for control with type 'longlist', this property is required
	var $config_name = ""; //name of config for control with type 'longlist', this property is required
	var $showselect = false; //option for longlist, in this case select field will  display
	var $windowheight = "400px";
	var $windowwidth = "600px";
	var $disabled = false;//now only for longlist
    var $parentcontrol = ""; //name of the parent control, only for longlist
    var $showTime = false; //only for date
	var $isNumber = false; //only for text
	var $height = "";
	
	
	function Control($name,$type = "",$caption = "",$source = "",$value = "",$text = "")
	{
		$this->name = 'fields[' . $name . ']';
		
		$this->id = $name;
		
		if($caption != "")
		{
			$this->caption = $caption;
		}
		else
		{
			$this->caption = $name;
		}
		if($source != "")
		{
			$this->source = $source;
		}
		if($type != "")
		{
			$this->type = $type;
		}
		if($value != "")
		{
			$this->keyfield = $value;
		}	
		if($text != "")
		{
			$this->textfield = $text;
		}
	}
	function set_base_params()
	{
		
	}
	function create()
	{
		
		global $hide_x_list;
		$hide_x_list = TRUE;
		
		switch($this->type)
		{
			case "text":
			case "email":
			case "tel":
			case "number":
				?>
				<input <?php echo  ($this->required ? "required" : "")?> <?php echo  $this->attr?> title="<?php echo  $this->tooltip?>" <?php echo  ($this->max_length != "maxlength='$this->max_length'" ? "" : "")?> type="<?php echo $this->type; ?>" <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> class="form-control<?php echo  ($this->css_class != "" ? $this->css_class : "")?>" <?php echo  $this->js?> name="<?php echo  $this->name?>" id="<?php echo  $this->id?>"  value="<?php echo  $this->value?>" />
				<?php
				break;	
			case "file":
				?>
				<input <?php echo  ($this->required ? "required" : "")?> <?php echo  $this->attr?> title="<?php echo  $this->tooltip?>"  type="file" <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> class="form-control<?php echo  ($this->css_class != "" ? $this->css_class : "")?>" <?php echo  $this->js?> name="<?php echo  $this->name?>_file" id="<?php echo  $this->id?>_file"  value="<?php echo  $this->value?>" />
				<input   type="hidden" name="<?php echo  $this->name?>" id="<?php echo  $this->id?>"  value="<?php echo  $this->value?>" />
				<?php
				break;		
			case "date":
				?>
				<div class='input-group control_date' id='<?php echo  $this->id?>_wrapper'>
                   <input <?php echo  ($this->required ? "required" : "")?> <?php echo  $this->attr?>  title="<?php echo  $this->tooltip?>" <?php echo  ($this->max_length != "maxlength='$this->max_length'" ? "" : "")?> type="text" <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> class="form-control<?php echo  ($this->css_class != "" ? $this->css_class : "")?>" <?php echo  $this->js?> name="<?php echo  $this->name?>" id="<?php echo  $this->id?>"  value="<?php echo  $this->value?>" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
				<?php
				break;
			case "longtext":
				?>
				<textarea <?php echo  ($this->required ? "required" : "")?> <?php echo  $this->attr?>  title="<?php echo  $this->tooltip?>" <?php echo  ($this->max_length != "" ? "onkeypress=\"javascript:return check_length(this,$this->max_length)\"" : "")?> <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> class="form-control<?php echo  ($this->css_class != "" ? $this->css_class : "")?>" <?php echo  $this->js?> name="<?php echo  $this->name?>" id="<?php echo  $this->id?>" rows="<?php echo  $this->rows?>" ><?php echo  $this->value?></textarea>
				<?php
				break;
			case "richtext":
				?>
				<textarea <?php echo  ($this->required ? "required" : "")?> <?php echo  $this->attr?>  title="<?php echo  $this->tooltip?>" <?php echo  ($this->max_length != "" ? "onkeypress=\"javascript:return check_length(this,$this->max_length)\"" : "")?> <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> class="form-control<?php echo  ($this->css_class != "" ? $this->css_class : "")?>" <?php echo  $this->js?> name="<?php echo  $this->name?>" id="<?php echo  $this->id?>" rows="<?php echo  $this->rows?>" ><?php echo  $this->value?></textarea>
				 <script type="text/javascript">tinymce.init({selector: "#<?php echo  $this->id?>", language: "ru"<?php echo  ($this->height != "" ? ',height: '.$this->height : '')?>,cache_suffix: '?v=4.3.10',plugins: "hr code table image imagetools colorpicker contextmenu media link searchreplace textcolor textpattern visualblocks visualchars wordcount charmap anchor advlist lists paste",file_browser_callback: RoxyFileBrowser});</script>
				<?php
				break;	
			case "list":
				?>
				<select <?php echo  ($this->required ? "required" : "")?> <?php echo  $this->attr?>  onchange="<?php echo  $this->onchange?>" title="<?php echo  $this->tooltip?>" <?php echo  ($this->size != "" ? "size='$this->size'" : "")?> <?php echo  ($this->is_multiple ? " multiple='true'" : "")?> <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> class="form-control <?php echo  ($this->css_class != "" ? $this->css_class : "")?>" <?php echo  $this->js?> name="<?php echo  $this->name?><?php echo  ($this->is_multiple ? "[]" : "")?>" id="<?php echo  $this->id?>" >
				<option value=""><?php echo  _UNSELECTED?></option>
				<?php 
					if(is_array($this->source))
					{
						foreach($this->source as $key=>$value)
						{
							if($this->is_multiple)
							{
								$selected = in_array($key,$this->value) ? " selected ": "";
							}
							else
							{
								$selected = $key == $this->value ? " selected ": "";
							}
							?>
							<option <?php echo  $selected?> value="<?php echo  $key?>" ><?php echo  $value?></option>
							<?php
						}
					}
					else
					{
						$result = dbQuery($this->source);
						
						foreach($result as $row)
						{
							$selected = "";
							if($this->is_multiple)
							{
								if(is_array($this->value))
								{
									$selected = in_array($row[$this->keyfield],$this->value) ? " SELECTED ": "";
								}
							}
							else
							{
								$selected = $row[$this->keyfield] == $this->value ? " SELECTED ": "";
							}
							?>
							<option <?php echo  $selected?> value="<?php echo  $row[$this->keyfield]?>"><?php echo  $row[$this->textfield]?></option>
							<?php
						}
					}
				?>
				</select><?php if(!$hide_x_list) { ?>
				&nbsp;<input type="button" value="X" class="btn btn-default" onclick="reset_select('<?php echo  $this->id?>')" style="width:30px;" /><?php }
				break;
			case "checkbox":
				
				?>
				<input <?php echo  ($this->required ? "required" : "")?> <?php echo  $this->attr?>  title="<?php echo  $this->tooltip?>" type="checkbox" <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> <?php echo  ($this->css_class != "" ? "class='$this->css_class'" : "class='cb'")?> <?php echo  $this->js?> name="<?php echo  $this->name?>" id="<?php echo  $this->id?>"  value="<?php echo  $this->value?>" <?php echo  ($this->value == 1  ? " CHECKED " : "")?> />
				<?php
				break;
			case "label":
				?>
				<span title="<?php echo  $this->tooltip?>" <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> <?php echo  ($this->css_class != "" ? "class='$this->css_class'" : "")?> <?php echo  $this->js?>  id="<?php echo  $this->id?>"  ><?php echo  $this->value?></span>
				<?php
				break;
			case "template":
				include($this->template);
				break;
			case "radio":
					if(is_array($this->source))
					{
						foreach($this->source as $key=>$value)
						{
							?>
							<input title="<?php echo  $this->tooltip?>" <?php echo  ( $key == $this->value ? " CHECKED ": "")?> type="radio" <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> <?php echo  ($this->css_class != "" ? "class='$this->css_class'" : "class='rb'")?> <?php echo  $this->js?> name="<?php echo  $this->name?>"   value="<?php echo  $key?>" />
							&nbsp;<?php echo  $value?>
							<?
						}
					}
					else
					{
						$result = dbQuery($this->source);
						foreach($result as $row)
						{
							?>
							<input title="<?php echo  $this->tooltip?>" <?php echo  ( $row[$this->keyfield] == $this->value ? " CHECKED ": "")?> type="radio" <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> <?php echo  ($this->css_class != "" ? "class='$this->css_class'" : "class='rb'")?> <?php echo  $this->js?> name="<?php echo  $this->name?>"   value="<?php echo  $row[$this->keyfield]?>" />
							&nbsp;<?php echo  $row[$this->textfield]?>
							<?php
						}
					}
					break;
			case "function":
				if(function_exists($this->source))
				{
					
					if(!empty($this->argument) && count($this->argument) > 0)
					{
						array_push($this->argument,$this);
						call_user_func_array($this->source,$this->argument);	
					}
					else
					{
						
						call_user_func($this->source,$this);	
					}
				}
				break;
			case "longlist":
				if(empty($this->tablename) || empty($this->config_name))
				{
					echo "Internal error: empty property 'tablename' or 'config_name'";
				}
				else
				{
					if($this->showselect)
					{
						?>
						<select <?php echo  $this->attr?>  name="<?php echo  $this->name?>]" title="<?php echo  $this->tooltip?>" <?php echo  ($this->size != "" ? "size='$this->size'" : "")?> <?php echo  ($this->is_multiple ? " multiple='true'" : "")?> <?php echo  ($this->css_style != "" ? "style='$this->css_style'" : "")?> <?php echo  ($this->css_class != "" ? "class='$this->css_class'" : "class='select_box'")?> <?php echo  $this->js?> name="<?php echo  $this->name?><?php echo  ($this->is_multiple ? "[]" : "")?>" id="<?php echo  $this->id?>">
						<option value="-1"><?php echo  _UNSELECTED?></option>
						<?php
							$result = dbQuery($this->source);
							$value_array = array();
							if($this->is_multiple)
							{
								$value_array = explode(",",$this->value);
							}
							foreach($result as $row)
							{
								$selected = "";
								if($this->is_multiple)
								{
									$selected = in_array($row[$this->keyfield],$value_array) ? " SELECTED ": "";
								}
								else
								{
									$selected = $row[$this->keyfield] == $this->value ? " SELECTED ": "";
								}
								?>
								<option <?php echo  $selected?> value="<?php echo  $row[$this->keyfield]?>"><?php echo  $row[$this->textfield]?></option>
								<?php
							}
						?>
						</select>
						<?php if(!$this->disabled) { ?>
						<input type="button"  onclick="edit_longlist('index.php?t=<?php echo  $this->config_name?>&noheader&nofooter&noaction&sql=<?php echo  urlencode($this->source)?>&parentcontrol=<?php echo  $this->id?>&keyfield=<?php echo  $this->keyfield?>&textfield=<?php echo  $this->textfield?>&rtype=<?php echo  ($this->showselect ? "1" : "0")?>&return&rtype=1&id=-1','<?php echo  $this->windowwidth?>','<?php echo  $this->windowheight?>',this);" class="buttons" value="+"/>
						<?php
						} 
					}
					else
					{
					?>
					<input type="hidden" name="<?php echo  $this->name?>" id="<?php echo  $this->id?>" value="<?php echo  $this->value?>" />
					<?php
					echo "<label id='t_".$this->id."' class='text_box'>".dbGetOne("SELECT ".$this->textfield." FROM ".$this->tablename." WHERE ".$this->keyfield. " = '".$this->value."'")."</label>";
					
					if(!$this->disabled) {
					?>&nbsp;
					<input type="button" onclick="edit_longlist('index.php?t=<?php echo  $this->config_name?>&noheader&nofooter&noaction&sql=<?php echo  urlencode($this->source)?>&parentcontrol=<?php echo  $this->id?>&keyfield=<?php echo  $this->keyfield?>&textfield=<?php echo  $this->textfield?>&rtype=<?php echo  ($this->showselect ? "1" : "0")?>&rtype=0','<?php echo  $this->windowwidth?>','<?php echo  $this->windowheight?>',this,'<?php echo  $this->parentcontrol?>');" class="btn btn-default" value="..."/>
					<?php
					} }
				if(!$this->disabled) {
				?>	
				&nbsp;
				<input type="button" class="btn btn-default" onclick="document.getElementById('<?php echo  $this->id?>').value = '-1';document.getElementById('t_<?php echo  $this->id?>').innerHTML = '';" value="X"/>
				<?php
				} }
				break;	
			case "time":
				$time = explode(":",$this->value);
				$h = $time[0];
				$m = $time[1];
				
				?>
				<input type="hidden" name="<?php echo  $this->name?>" id="<?php echo  $this->id?>" value="<?php echo  $this->value?>" />
				<input type="hidden" name="<?php echo  $this->name?>" id="<?php echo  $this->id?>" value="<?php echo  $this->value?>" />
				<select onchange="set_time('<?php echo  $this->id?>')" id="h_<?php echo  $this->id?>">
				<?php
				for($i = 0; $i < 24;$i++)
				{
					?>
					<option <?php echo  ($h == $i ? "SELECTED" : "")?> value="<?php echo  $i?>"><?php echo  $i?></option>
					<?php
				}
				?>
				</select>&nbsp;:&nbsp;<select onchange="set_time('<?php echo  $this->id?>')" id="m_<?php echo  $this->id?>">
				<?php
				for($i = 0; $i < 60;$i++)
				{
					?>
					<option <?php echo  ($m == $i ? "SELECTED" : "")?> value="<?php echo  $i?>"><?php echo  $i?></option>
					<?php
				}
				?>
				</select>
				<?php
				break;
		}
		echo $this->html;
	}


}