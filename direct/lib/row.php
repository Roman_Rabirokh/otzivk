<?php
class Row
{
	var $config = "";
	var $id = "-1";
	var $copy = "-1";
	function Row($config,$id)
	{
		$this->config  = $config;
		$this->id  = $id;
	}

	function edit()
	{
		global $engine_db;
		global $titles;
		$_SESSION["back_row_url"] = $_SERVER["SERVER_NAME"].(!empty($_SERVER["SERVER_PORT"]) ? ":".$_SERVER["SERVER_PORT"] : "").$_SERVER["REQUEST_URI"];

		if(empty($this->config->title) && !empty($titles[$_GET["t"]]))
		{
			$this->config->title = $titles[$_GET["t"]];
		}
		$coln = 0;
		$columns = array();
		$row = dbGetRow("SELECT ".$this->config->select_fields." FROM ".$this->config->table." WHERE ".$this->config->source_key_field." = :id",array(":id"=>$this->id),$coln,$columns);




		if($this->id != "-1" && empty($row))
		{
			header("Location: index.php?t=404");
			exit();
		}


		if(!empty($this->config->fields))
		{
			foreach($this->config->fields as $key=>$value)
			{
				$row[$key] = $value;
				$coln++;
				$columns[] = array('name'=>$key);
			}
		}



		$formId = uniqid();

		?>
		<div class="container-fluid">
			<div class="panel panel-default">
		  <div class="panel-body">
		<div class="page_title pull-left" style="float: left;"><h2><?php echo $this->config->title; ?> / <?php
		if($_GET["id"] == "-1")
		{
			echo _INSERT_NEW;
		}
		else
		{
			echo _EDITING;
		}
                ?></h2> 
                

		</div><?php 
                    if((isset($_GET['t']) && $_GET['t'] == 'susers') && isset($_GET['id'])) 
                    { ?>
                      <div class="msg_admin_wrapper">
                      <iframe style="width: 500px;height: 150px;display:none;" name="msg_admin"></iframe>
                        <form style="margin-bottom: -15px;margin-top: -4px;"action="/admin/send/msg/" target="msg_admin" method="post" class="col-md-6">
                            <input type="hidden" name="userid" value="<?php echo $_GET['id']; ?>"/>
                            <div class="col-md-4" >
                                <input style="height: 38px;width: 280px;padding-left: 9px;" type="text" name="subject" placeholder="Введите тему сообщения"/>
                                <p class="subject_msg_admin_error" style="display: none; color: red;margin-bottom:0;font-size: 10px;">Введите тему сообщения</p>
                            </div>
                            <div class="col-md-6">
                            <textarea style="width:400px;" col="10" rows="2" class="" name="text_msg" placeholder="Написать сообщение пользователю" ></textarea>
                            <p class="msg_admin_error" style="display: none; color: red;margin-bottom:0;font-size: 10px;">Введите сообщение</p>
                            </div>
                            <div class="col-md-1" >
                            <input style="margin-top:2px;margin-left: -16px;" class="btn btn-success" type="submit" id="send_msg_fr_admin" value="Отправить" />
                            </div>
                        </form>
                      </div>
                          <style>
                              .msg_admin_wrapper{
                                  position: relative;
                                  left: 30px;
                              }
                          </style><?php 
                      
                    }
                    if((isset($_GET['t']) && $_GET['t'] == 'rreports') && isset($_GET['id'])) 
                    {
                        dbQuery('UPDATE #__message_to_moderator SET is_read = 1 WHERE id = :id', array(':id' => intval($_GET['id'])));
                    } ?>
		</div>
		</div>
			<div class="panel panel-default">
		  <div class="panel-body">
		  <iframe style="display:none;" name="f<?php echo $formId; ?>"></iframe>
		<form action="db.php?t=<?php echo $_GET["t"]; ?>&id=<?php echo $_GET["id"]; ?>&save" method="post" target="f<?php echo $formId; ?>"  class="form-horizontal form-edit" role="form" enctype="multipart/form-data" >
		<input type="hidden" name="base_url" id="base_url" value="http://<?php echo get_url(1,$this->config); ?>" />
		<input type="hidden" name="token" value="<?php echo  createFormToken("edit" . $this->id); ?>" />
		<input type="hidden" name="is_close" value="0" id="is_close" />
		<?php
		//create hidden fields
		foreach($this->config->controls as $current)
		{
			if($current->type == "hidden")
			{
				?>
				<input value="<?php echo $current->default_value;?>"  type="hidden" id="<?php echo $current->id;?>" name="<?php echo $current->name; ?>" />
				<?php
				//unset($this->config->controls[$current->id]);
			}
		}
		//*****************

		if(!empty($this->config->edit_content_top))
		{
			include($this->config->edit_content_top);
		}

		$tr_controls = array();
		$tab_enabled = false;
		if(isset($this->config->tabs) && count($this->config->tabs) > 0)
		{
			$tabs = new Tabs($this->config->tabs);
			$tabs->create();
			$tab_enabled = true;
		}
		?>
		<table width="100%" border="0">
		<tr>
		<td style="vertical-align:top;padding-top:15px;padding-bottom:15px;">
		<?php
		$checkbox_array = array();
		for ($i = 0; $i < $coln; $i++)
		{
			$is_number = false;
			$field_name = $columns[$i]["name"];


			if(in_array($field_name,$this->config->exclude_fields_edit))
			{
				continue;
			}
			$ctr = new Control($field_name);


			if(in_array($field_name,$this->config->required_fields))
			{
				$ctr->required = true;
			}
			if(in_array($field_name,$this->config->number_fields))
			{
				$ctr->isNumber = TRUE;
			}
			if(isset($this->config->edit_title_fields[$field_name]))
			{
				$ctr->caption = $this->config->edit_title_fields[$field_name];
			}
			else
			{
				if(isset($this->config->title_fields[$field_name]))
				{
					$ctr->caption = $this->config->title_fields[$field_name];
				}
				else
				{
					$ctr->caption = $field_name;
				}
			}
			if(isset($this->config->controls[$field_name]))
			{
				$ctr  = $this->config->controls[$field_name];
				if($this->id == "-1")
				{
					$ctr->value = $ctr->default_value;
				}
				else
				{
					$ctr->value = $row[$field_name];
				}
				if(isset($_SESSION["last_post"][$field_name]))
				{
					$ctr->value = $_SESSION["last_post"][$field_name];
				}
				if($this->config->controls[$field_name]->type == "checkbox")
				{
					array_push($checkbox_array,$field_name);
				}
				if($this->config->controls[$field_name]->type == "date")
				{

					if($this->config->controls[$field_name]->date_display != "")
					{
						$ctr->date_display = $row[$this->config->controls[$field_name]->date_display];
					}
				}
			}
			else
			{
				$ctr->type = "text";
				if(!empty($row[$field_name]))
				{
					$ctr->value = $row[$field_name];
				}
				if(isset($_SESSION["last_post"][$field_name]))
				{
					$ctr->value = $_SESSION["last_post"][$field_name];
				}
			}
			if($ctr->type == "hidden")
			{
				continue;
			}

			if($tab_enabled)
			{
				array_push($tr_controls,"'tr_".$field_name."'");
			}


			$ctr->current_row = $row;

			if($ctr->type == "template" && $ctr->template_mode == "full")
			{
				echo "<div class='form-group' id='tr_".$field_name."'><div class='col-sm-12'>";
				$ctr->create();
				echo "</div></div>";
			}
			else
			{
				echo "<div class='form-group' id='tr_".$field_name."'>";
				echo  '<label for="'.$ctr->id.'" class="col-sm-2 control-label">'.$ctr->caption.'</label>';
				echo '<div class="col-sm-8">';
				$ctr->create();
				echo "</div>";
				echo "</div>";
			}


		}
		?>

		<?php
		if(isset($this->config->edit_content_after_fields))
		{
			if(!empty($this->config->edit_content_after_fields))
			{
				include($this->config->edit_content_after_fields);
			}
		}
		if($tab_enabled)
		{
			?>
			<script language="JavaScript">var tr_controls = Array(<?php echo implode(",",$tr_controls);?>);SetCurrentTab(document.getElementById("tab_0"),0); </script>
			<?php
		}
		?>
		<div class="line"></div>
		<table >
			<tr>
				<td colspan="2">
					<?php if(isset($this->config->edit_mode_buttons) && $this->config->edit_mode_buttons[0]) { ?>
					<input id="btnSave" class="btn btn-success" type="submit" value="<?php echo _BUTTON_SAVE; ?>" />&nbsp;&nbsp;
					<?php }
					if(isset($this->config->edit_mode_buttons) && $this->config->edit_mode_buttons[1] && !isset($_GET["return"])) {
					?>
					<input id="btnSaveClose" class="btn btn-success" type="submit" value="<?php echo _BUTTON_SAVE_CLOSE; ?>" />&nbsp;&nbsp;
					<?php }
					if(isset($_GET["return"]))
					{
						?>
						<input class="btn btn-default" onclick="close_longlist()" type="button" value="<?php echo _BUTTON_CLOSE; ?>" />
						<?php
					}
					else
					{
					if(isset($this->config->edit_mode_buttons) && $this->config->edit_mode_buttons[2]) {
					?>
						<input class="btn btn-default" onclick="close_form('<?php echo get_url(1,$this->config); ?>',this)" type="button" value="<?php echo _BUTTON_CLOSE; ?>" />
					<?php } } ?>
				</td>
			</tr>
		</table>
		</td>
		<td style="vertical-align:top;">
		<?php
		if(!empty($this->config->edit_content_right))
		{
			include($this->config->edit_content_right);
		}
		?>
		</td>
		</tr>
		<tr><td colspan="2">
		<?php
		if(isset($this->config->edit_content_bottom))
		{
			if(!empty($this->config->edit_content_bottom))
			{
				include($this->config->edit_content_bottom);
			}
		}

		if(count($checkbox_array) > 0)
		{
			$checkbox_array_string = "";
			foreach($checkbox_array as $current)
			{
				$checkbox_array_string .= $current.",";

			}
			?>
			<input type="hidden" name="checkbox_array" value="<?php echo $checkbox_array_string; ?>" />
			<?php
		}
		?>
		</td></tr></table>
		</form></div></div>
		</div>
		<div id="dlonglist" >
		<iframe id="ilonglist" frameborder="0" style=""></iframe>
        <input onclick="$('#dlonglist').fadeOut();" class="buttons" type="button" value="Закрыть" />
		</div>
		<?php
		if(isset($_SESSION["last_post"]))
		{
			unset($_SESSION["last_post"]);
		}
		if(isset($_SESSION["rowerror"]))
		{
			?>
			<script language="JavaScript">
			alert('<?php echo $_SESSION["rowerror"]; ?>');
			</script>
			<?php
			unset($_SESSION["rowerror"]);
		}
	}

}

?>
