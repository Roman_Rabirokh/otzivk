<?php
	//$type
	//0 - base url
	//1 - base url + sort (if exists)
	function get_url($type,$config = null,$rmget = array(),$addget = array())
	{
		$url = "";
		$base_url = $_SERVER["HTTP_HOST"]."$_SERVER[PHP_SELF]?t=$_GET[t]".(isset($_GET["noheader"]) ? "&noheader" : "").(isset($_GET["nofooter"]) ? "&nofooter" : "").(isset($_GET["parent"]) && !in_array("parent",$rmget) ? "&parent=$_GET[parent]" : "").(isset($_GET["noaction"]) ? "&noaction" : "").(isset($_GET["sql"]) && !in_array("sql",$rmget)  ? "&sql=$_GET[sql]" : "").(isset($_GET["parentcontrol"])  ? "&parentcontrol=$_GET[parentcontrol]" : "").(isset($_GET["textfield"])  ? "&textfield=$_GET[textfield]" : "").(isset($_GET["keyfield"])  ? "&keyfield=$_GET[keyfield]" : "").(isset($_GET["rtype"])  ? "&rtype=$_GET[rtype]" : "").(isset($_GET["return"])  ? "&return" : "").(isset($_GET["autoheight"])  ? "&autoheight=$_GET[autoheight]" : "");
		
		if(!empty($config))
		{
			foreach($config->get_params as $key)
			{
				if(in_array($key,$rmget))
				{
					continue;
				}
				$base_url .= (isset($_GET[$key])  ? "&$key=$_GET[$key]" : "");
			}		
		}
		if(!empty($addget))
		{
			foreach($addget as $key=>$value)
			{
				$base_url .= '&' . $key . '=' . $value;	
			}			
		}
		switch($type)
		{
			case "0":
				$url = $base_url.(isset($_GET["page"]) ? "&page=$_GET[page]" : "");
				break;
			case "1":
				$url = $base_url.(isset($_GET["sort"]) ? "&sort=".str_replace(" ","+",$_GET["sort"]) : "").(isset($_GET["page"]) ? "&page=$_GET[page]" : "");
				break;
			case "2":
				$url = $base_url.(isset($_GET["sort"]) ? "&sort=".str_replace(" ","+",$_GET["sort"]) : "");
				break;
			case "3":
				$url = $base_url.(isset($_GET["sort"]) ? "&sort=".str_replace(" ","+",$_GET["sort"]) : "");
				break;	
		}
		return $url;
	}

 function ruslat ($string) # Задаём функцию перекодировки кириллицы в транслит.
{
$string = mb_eregi_replace("ж","zh",$string);
$string = mb_eregi_replace("ё","yo",$string);
$string = mb_eregi_replace("й","i",$string);
$string = mb_eregi_replace("ю","yu",$string);
$string = mb_eregi_replace("ь","",$string);
$string = mb_eregi_replace("ч","ch",$string);
$string = mb_eregi_replace("щ","sh",$string);
$string = mb_eregi_replace("ц","c",$string);
$string = mb_eregi_replace("у","u",$string);
$string = mb_eregi_replace("к","k",$string);
$string = mb_eregi_replace("е","e",$string);
$string = mb_eregi_replace("н","n",$string);
$string = mb_eregi_replace("г","g",$string);
$string = mb_eregi_replace("ш","sh",$string);
$string = mb_eregi_replace("з","z",$string);
$string = mb_eregi_replace("х","h",$string);
$string = mb_eregi_replace("ъ","",$string);
$string = mb_eregi_replace("ф","f",$string);
$string = mb_eregi_replace("ы","y",$string);
$string = mb_eregi_replace("в","v",$string);
$string = mb_eregi_replace("а","a",$string);
$string = mb_eregi_replace("п","p",$string);
$string = mb_eregi_replace("р","r",$string);
$string = mb_eregi_replace("о","o",$string);
$string = mb_eregi_replace("л","l",$string);
$string = mb_eregi_replace("д","d",$string);
$string = mb_eregi_replace("э","ye",$string);
$string = mb_eregi_replace("я","ja",$string);
$string = mb_eregi_replace("с","s",$string);
$string = mb_eregi_replace("м","m",$string);
$string = mb_eregi_replace("и","i",$string);
$string = mb_eregi_replace("т","t",$string);
$string = mb_eregi_replace("б","b",$string);
$string = mb_eregi_replace("Ё","yo",$string);
$string = mb_eregi_replace("Й","I",$string);
$string = mb_eregi_replace("Ю","YU",$string);
$string = mb_eregi_replace("Ч","CH",$string);
$string = mb_eregi_replace("Ь","",$string);
$string = mb_eregi_replace("Щ","SH",$string);
$string = mb_eregi_replace("Ц","C",$string);
$string = mb_eregi_replace("У","U",$string);
$string = mb_eregi_replace("К","K",$string);
$string = mb_eregi_replace("Е","E",$string);
$string = mb_eregi_replace("Н","N",$string);
$string = mb_eregi_replace("Г","G",$string);
$string = mb_eregi_replace("Ш","SH",$string);
$string = mb_eregi_replace("З","Z",$string);
$string = mb_eregi_replace("Х","H",$string);
$string = mb_eregi_replace("Ъ","",$string);
$string = mb_eregi_replace("Ф","F",$string);
$string = mb_eregi_replace("Ы","Y",$string);
$string = mb_eregi_replace("В","V",$string);
$string = mb_eregi_replace("А","A",$string);
$string = mb_eregi_replace("П","P",$string);
$string = mb_eregi_replace("Р","R",$string);
$string = mb_eregi_replace("О","O",$string);
$string = mb_eregi_replace("Л","L",$string);
$string = mb_eregi_replace("Д","D",$string);
$string = mb_eregi_replace("Ж","Zh",$string);
$string = mb_eregi_replace("Э","Ye",$string);
$string = mb_eregi_replace("Я","Ja",$string);
$string = mb_eregi_replace("С","S",$string);
$string = mb_eregi_replace("М","M",$string);
$string = mb_eregi_replace("И","I",$string);
$string = mb_eregi_replace("Т","T",$string);
$string = mb_eregi_replace("Б","B",$string);
return $string;
}
function create_urlname($name)
{
	$name = htmlspecialchars_decode($name,ENT_QUOTES);
	$name = trim($name);
	$name = str_replace(array("+","_","/","\\","(",")","*",":","'",".",";","`","'"," ","	","#","`","~","+","=","-","*",",","<",">","!","?","@","¶","%","{","}","_","[","]","|","®","©","\"","&","»","«","—"),"!",$name);
	$name = mb_strtolower($name, 'UTF-8');
	$name = ruslat($name);
	
	$n = array();
	$new_name = explode("!",$name);
	foreach($new_name as $current)
	{
		if($current != "")
		{
			$n[] = $current;
		}
	}
	
    return implode("-",$n);
} 
function createFormToken($key)
	{
		$result = "";
		if(isset($_SESSION["token_" . $key]))
		{
			$result  = 	$_SESSION["token_" . $key];
		}
		else
		{
			$result = $key."_".uniqid();	
			$_SESSION["token_" . $key] = $result;
		}
		return $result;
	}
	
	function checkToken($key,$value)
	{
		if(isset($_SESSION["token_" . $key]))
		{
			return ($_SESSION["token_" . $key] == $value);	
		}
		else
		{
			return false;	
		}
	}
	
	function isAjax()
	{
		return isset($_REQUEST["ajax"]) ? TRUE : FALSE;
	}
	function isFastEdit()
	{
		return isset($_REQUEST["fast_edit"]) ? TRUE : FALSE;
	}
    function isNewRow()
	{
		return isset($_REQUEST["id"]) && $_REQUEST["id"] == '-1' ? TRUE : FALSE;
	}

	function mime_header_encode($str, $data_charset, $send_charset) {
	if($data_charset != $send_charset) {
		$str = iconv($data_charset, $send_charset, $str);
	}
	return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
	}

	function sendEmail($name_from,$email_from,$name_to,$email_to,$subject,$body)
	{
		$data_charset = "UTF-8"; 
		$send_charset = "UTF-8"; 
		$to = mime_header_encode($name_to, $data_charset, $send_charset)
		. ' <' . $email_to . '>';
		$subjectOriginal = $subject;
		$subject = mime_header_encode($subject, $data_charset, $send_charset);
		$from =  mime_header_encode($name_from, $data_charset, $send_charset) 
		.' <' . $email_from . '>'; 
		if($data_charset != $send_charset) {
			$body = iconv($data_charset, $send_charset, $body); 
		}
		$headers = "From: $from\r\n";
		$headers .= "Content-type: text/html; charset=$send_charset\r\n";
		$headers .= "Mime-Version: 1.0\r\n";
		$headers .= "X-Mailer: PHP/".phpversion()."\r\n";  
		
		if(!_MAIL_TO_LOG)
		{	
			return mail($to, $subject, $body, $headers);
		}
		else
		{
			dbNonQuery("INSERT INTO `". _DB_TABLE_PREFIX ."mail_log` (`created`, `name_form`, `email_form`, `name_to`, `email_to`, `subject`, `body`) VALUES (now(), :name_form, :email_from, :name_to, :email_to, :subject, :body);
			
			",array(
				":name_form"=>$name_from,
				":email_from"=>$email_from,
				":name_to"=>$name_to,
				":email_to"=>$email_to,
				":subject"=>$subjectOriginal,
				":body"=>$body
			));
		}
		
	
	}
	
	function sendEmailTemplate($name_from,$email_from,$name_to,$email_to,$subject,$template,$params = array())
	{
		$mailTemplate = _MAIL_TEMPLATE . $template;
		$body = "";
		if(file_exists($mailTemplate))
		{
			$tpl = file_get_contents($mailTemplate);
			foreach($params as $key=>$param)
			{
				$tpl = str_replace($key,$param,$tpl);
			}
			$body = $tpl;
		}
			
		sendEmail($name_from,$email_from,$name_to,$email_to,$subject,$body);
		
	}

?>