<?php

$db = null;

function setQueryTable($query)
{
	return str_replace("#__",_DB_TABLE_PREFIX,$query);
}

function initConnectionParams($params = array())
{
	if(!defined("_CMS"))
	{
		if(count($params) > 0)
		{
			define("_HOST",$params["_HOST"]);
			define("_DB",$params["_DB"]);
			define("_DBUSER",$params["_DBUSER"]);
			define("_DBPASSWORD",$params["_DBPASSWORD"]);
		}
		
		else
		{
			global $currentuser;
			define("_HOST",$currentuser->Host);
			define("_DB",$currentuser->DataBase);
			define("_DBUSER",$currentuser->Login);
			define("_DBPASSWORD",$currentuser->Passw);
		}
	}
}

function openConnection()
{
	global $db;
	try 
	{
		$db = new PDO("mysql" . _HOST . ";dbname=" . _DB, _DBUSER, _DBPASSWORD);
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$db->query("set names utf8");
		return $db;
    }
	catch(PDOException $e)
    {
		//send_mail_to_admin("http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"],$e->getMessage());
		echo "<p>У нас возникли небольшие технические проблемы. В ближайшее время они будут решены. Спасибо за понимание !</p>";
		return false;
    }
	
}

function closeConnection($db)
{
	$db = null;
}


function dbQuery($query,$params = array(),$count_all = false,&$all_count = 0,&$column_count = 0,&$columns = array()) {
	
	global $db;
	$query = setQueryTable($query);
	//echo $query;
	
	$already_open = false;
	if($db == null) { 
		$db = openConnection();
	} else {
		$already_open = true;
	}
	
	$result = array();
	
	if($count_all){
	
		$sql_query = explode("SELECT",$query,2);
		$query = "SELECT SQL_CALC_FOUND_ROWS" . $sql_query[1];
	}
	
	$sql = $db->prepare($query);
	
	if(!$sql)
	{
		print_r($db->errorInfo());
		return array();	
	}
	
	if(is_array($params)) {
		foreach($params as $key=>$value)
		{
			$sql->bindValue($key,$value);
		} 
	}
	$sql->execute();
	$column_count = $sql->columnCount();

	while($r = $sql->fetch(PDO::FETCH_ASSOC))
	{
		array_push($result,$r);
	}
	$columns = array();
	 
	for ($i = 0; $i < $sql->columnCount(); $i++) {
     $columns[] = $sql->getColumnMeta($i);
    }

	if($count_all){
		$all_count = dbGetOne("SELECT FOUND_ROWS();",true,$db);
	}
	
	if(!$already_open) {
		//closeConnection($db);
	}
	
	return $result;

}

function dbNonQuery($query,$params = array(),$is_insert = false) {
	
	global $db;
    
	$query = setQueryTable($query);
	
    $result = "";
	
	$already_open = false;
	
	if($db == null) { 
		$db = openConnection();
	} else {
		$already_open = true;
	}
	
	$result = array();
	
	$sql = $db->prepare($query);
	if(is_array($params))
	{
		foreach($params as $key=>$value)
		{
			$sql->bindValue($key,$value);
		} 
	}
	
	$sql->execute();
	
	
	if($is_insert)
	{
		$result = $db->lastInsertId(); 
	}
	
	if(!$already_open) {
		//closeConnection($db);
	}

	return $result;
}

function dbGetOne($query,$params = array()) {
	
	global $db;
	
	$query = setQueryTable($query);

    $already_open = false;
	
	if($db == null) { 
		$db = openConnection();
	} else {
		$already_open = true;
	}
	
	$result = $db->prepare($query);
	
	if(is_array($params))
	{
		foreach($params as $key=>$value)
		{
			$result->bindValue($key,$value);
		}
	}
	$result->execute();
	$row = $result->fetch(PDO::FETCH_BOTH);
	
	if(!$already_open) {
		//closeConnection($db);
	}
	return isset($row[0]) ? $row[0] : "";
}	

function dbGetRow($query,$params = array(),&$column_count = 0,&$columns = array()) 
{
    global $db;
	
	$query = setQueryTable($query);
	
    $already_open = false;
	
	if($db == null) { 
		$db = openConnection();
	} else {
		$already_open = true;
	}
	
	$result = $db->prepare($query);
	if(count($params) > 0) {
		foreach($params as $key=>$value)
		{
			$result->bindValue($key,$value);
		}
	}
	$result->execute();
	$row = $result->fetch(PDO::FETCH_ASSOC);
	
	$column_count = $result->columnCount();
	
	$columns = array();
	 
	for ($i = 0; $i < $result->columnCount(); $i++) {
     $columns[] = $result->getColumnMeta($i);
    }

	if(!$already_open) {
		//closeConnection($db);
	}
	return $row;
}	

function dbQueryToArray($query,$params = array())
{
	$arr = array();
	
	$result = dbQuery($query,$params);
	foreach($result as $r)
	{
		$arr[] = implode(",",$r);
	}
	
	return $arr;
}

function row($key,$row)
{
	return $row[$key];
}