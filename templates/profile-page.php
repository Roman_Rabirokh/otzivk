<?php if(!defined("_APP_START")) { exit(); }
include('sidebar.php');
global $user;
global $app;
$user_isset = dbGetRow('SELECT id, created,name, DATEDIFF(NOW(), created) AS qnt_days FROM #__users WHERE id = :uid', array('uid' => $userid));

if(!empty($user_isset))
{
  $data['USER_INFO'] = dbGetRow('SELECT name, profile_foto, upload_foto FROM #__profiles WHERE userid = :uid', array(':uid' => $userid));
  if(!empty($data['USER_INFO']['upload_foto']))
  {
      $data['USER_INFO']['upload_foto_src'] = getImageById($data['USER_INFO']['upload_foto']);
  }
}

//echo "<pre>"; print_r($user_isset);  ?>

<div class="col-md-9 col-sm-8"><?php
if(!empty($user_isset))
{ ?>
  <ol class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li class="active"><?php if(!empty($user_isset['name'])) echo $user_isset['name']; else { if(!empty($data['USER_INFO']['name'])) echo $data['USER_INFO']['name']; else echo 'Инкогнито'; } ?></li>
  </ol>
  <div class="row personal_area viewing_profile">
    <div class="box">
      <div class="block">
        <div class="row">
          <div class="col-md-2 col-sm-2 col-xs-12 profile-page-avatar-wrapper"><?php
          if(!empty($data['USER_INFO']['upload_foto_src']))
          { ?>
              <img height="89" src="<?php echo $data['USER_INFO']['upload_foto_src']; ?>" alt=""><?php
          }
          else
          {
              if(!empty($data['USER_INFO']['profile_foto']))
              { ?>
                  <img height="89" src="<?php echo $data['USER_INFO']['profile_foto']; ?>" alt=""><?php
              }
              else
              { ?>
                  <img height="89" src="<?php echo _TEMPLATE_URL; ?>img/nofoto.png" alt=""><?php
              }
              
          } ?>
          
          
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <h3><?php if(!empty($user_isset['name'])) echo $user_isset['name']; else { if(!empty($data['USER_INFO']['name'])) echo $data['USER_INFO']['name']; else echo 'Инкогнито'; } ?></h3>
            <h4><?php
            if(!empty($user_isset['qnt_days']))
            { ?>На сайте:&nbsp;
              <span><?php  GetPeriodOnSite($user_isset['qnt_days']); ?>
              </span><?php
            }
            else
            {
              echo "Пользователь на сайте с сегодняшнего дня";
            } ?></h4>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12"><?php
//          if($user->Authorized() || !empty($_SESSION['user']))
//          {
            $data['USERS_ON'] = dbQueryToArray('SELECT userid_subscr FROM #__subscribes_on_user WHERE userid = :uid', array(':uid' => $user->getID())); ?>
            <a href="<?php if($user->Authorized() || !empty($_SESSION['user'])) {  echo '/form/message/'.$userid;} else { echo '#data' ;} ?>" class="send_message <?php if(!$user->Authorized() || empty($_SESSION['user'])) {  echo 'fancybox'; } ?>">Отправить сообщение</a><?php
//          }
          if($user->Authorized())
          { ?>
            <div class="block-btn-pfpage"><?php
            if(in_array($userid, $data['USERS_ON']))
            { ?>
              <a href="javascript:void(0);" data-uid="<?php echo $userid; ?>" onclick="subscribe_off(this);" class="follow" >Отписаться</a><?php
            }
            else
            { ?>
              <a href="javascript:void(0);" data-uid="<?php echo $userid; ?>" onclick="subscribe_on(this);" class="follow" >Подписаться</a><?php
            } ?>
            </div><?php
          }
          else
          { ?>
            <a href="#data" class="follow fancybox" >Подписаться</a><?php
              
          } ?>
          </div>
        </div>
      </div>
    </div>
      <div class="row topic">
    <?php $app->includeComponent('otzovik/personal.reviews', array('USER' => $user_isset['id'])); ?>
      </div>
  </div><?php
}
else
{ ?>
  <div class="row personal_area viewing_profile">
    <h3>Такого пользователя не существует.</h3>
  </div><?php
} ?>
<?php echo getBannerBlockById(1); ?>
</div>
