/*var map;
 var markers = [];
 
 function initMap() {
 if (navigator.geolocation)
 {
 var currentLat;
 var currentLng;
 // alert('')
 navigator.geolocation.getCurrentPosition(function(position) {
 // Определение текущих координат.
 currentLat = position.coords.latitude;
 currentLng = position.coords.longitude;
 $('#review-lat').val(currentLat);
 $('#review-lng').val(currentLng);
 console.log(currentLat);
 console.log(currentLng);
 // Текущее местоположение пользователя
 var currentPosition = {lat: currentLat, lng: currentLng};
 
 if(currentPosition)
 {
 // Инициализация карты
 $('#map-block').show();
 map = new google.maps.Map(document.getElementById('map'), {
 zoom: 12,
 center: currentPosition,
 //mapTypeId: google.maps.MapTypeId.TERRAIN
 });
 
 // Добавление и обработка события клика по карте
 map.addListener('click', function(event) {
 clearMarkers();
 addMarker(event.latLng);
 $('#review-lat').val(event.latLng.lat());
 $('#review-lng').val(event.latLng.lng());
 console.log(event.latLng.lat());
 console.log(event.latLng.lng());
 });
 
 // Adds a marker at the center of the map.
 addMarker(currentPosition);
 
 }
 });
 }
 else
 {
 alert('Геолокация не доступна');
 }
 
 
 }
 
 // Adds a marker to the map and push to the array.
 function addMarker(location) {
 var marker = new google.maps.Marker({
 position: location,
 map: map
 });
 markers.push(marker);
 }
 
 // Sets the map on all markers in the array.
 function setMapOnAll(map) {
 for (var i = 0; i < markers.length; i++) {
 markers[i].setMap(map);
 }
 }
 
 // Removes the markers from the map, but keeps them in the array.
 function clearMarkers() {
 setMapOnAll(null);
 }
 
 // Shows any markers currently in the array.
 function showMarkers() {
 setMapOnAll(map);
 }
 
 // Deletes all markers in the array by removing references to them.
 function deleteMarkers() {
 clearMarkers();
 markers = [];
 } */

function getSubCats(obj)
{
    $('.select-category').find('select').each(function () {
        $(this).css('border', '1px solid #B9B9B9');
    });
    $(obj).next().remove();
    $(obj).next().remove();
    $('.select-category select').each(function () {
        $(this).removeAttr('required');
    });
    $('.select-category select:last').each(function () {
        $(this).attr('required', 'required');
    });

    var catId = $(obj).val();
    // console.log(catId);
    if (catId != 0)
    {
        $.ajax({
            type: "POST",
            url: "/topic/get/subcats/",
            dataType: "json",
            data: {
                cat_id: catId
            },
            success: function (result) {
                if (result)
                {
                    // console.log(result);
                    $('.categories').attr('name', '');
                    var select = '<select required name="topic_category" onchange="getSubCats(this);" class="input-review categories add-sub-cats">';
                    select += '<option value="0" >Выберите подкатегорию</option>';
                    for (var i in result)
                    {
                        select += '<option value="' + result[i]['id'] + '">' + result[i]['name'] + '</option>';
                    }
                    select += '</select>';
                    $('.select-category').append(select);
                } else
                {
                    $(obj).attr('name', 'topic_category');
                    var value = $(obj).val();
                    if (value != 0)
                    {
                        $('.select-category').find('select').each(function () {
                            $(this).css('border', '1px solid #B9B9B9');
                        });
                        $(".error-topic-category").html('');
                    }
                }
            }
        });
    }
}

function redirectToEnter()
{
    $.fancybox.close();
    var fb = $('#data').html();
    $.fancybox.open(fb);
    
}

function redirectToEnter()
{
    $.fancybox.close();
    $('#enter').click();
    
}

function redirectToRegister()
{
    $.fancybox.close();
    $('#register-a').click();
    
}

$(document).ready(function () {
//    $('a#enter').fancybox();
//$('a#register-a').fancybox();
    
    //$('#add-review-page').click(function(e){
        //$('a#register-a').fancybox();
        //if(auth == 0)
        //{
//            $('a#enter').fancybox();
//            e.preventDefault();

       // }
    //});
    
   
    var divDropzone = $('#dropzone');
    if (divDropzone.length > 0)
    {
        var myDropzone = new Dropzone("div#dropzone", {
            url: "/upload/review/images/",
            method: 'post',
            uploadMultiple: true,
            addRemoveLinks: true,
            parallelUploads: 10,
            renameFilename: 'review_fotos',
            dictRemoveFile: 'Удалить',
            dictCancelUpload: 'Отменить',
            removedfile: function (image) {
                console.log(image);
                image.previewElement.remove();
                $.ajax({
                    url: '/del/upload/',
                    method: 'POST',
                    data: {
                        img_name: image.name,
                        token_folder: tokenUpload
                    },
                    success: function () {

                    }
                });
            },
            sending: function (file, xhr, formData) {
                formData.append("token_folder", tokenUpload);
            },
            success: function (image, response) {
                console.log(image);
                console.log(response);
            }
        });

    }


    /*if (navigator.geolocation)
     {
     // alert('')
     navigator.geolocation.getCurrentPosition(function(position) {
     
     // Текущие координаты.
     currentLat = position.coords.latitude;
     
     currentLng = position.coords.longitude;
     
     });
     }
     else
     {
     alert('Геолокация не доступна');
     }*/
    //Таймер обратного отсчета
    //Документация: http://keith-wood.name/countdown.html
    //<div class="countdown" date-time="2015-01-07"></div>
    var austDay = new Date($(".countdown").attr("date-time"));
    $(".countdown").countdown({until: austDay, format: 'yowdHMS'});

    //Попап менеджер FancyBox
    //Документация: http://fancybox.net/howto
    //<a class="fancybox"><img src="image.jpg" /></a>
    //<a class="fancybox" data-fancybox-group="group"><img src="image.jpg" /></a>

    $('.fancybox').fancybox({
        centerOnScroll: true
    });

    //Навигация по Landing Page
    //$(".top_mnu") - это верхняя панель со ссылками.
    //Ссылки вида <a href="#contacts">Контакты</a>
    $(".top_mnu").navigation();

    //Добавляет классы дочерним блокам .block для анимации
    //Документация: http://imakewebthings.com/jquery-waypoints/
    $(".block").waypoint(function (direction) {
        if (direction === "down") {
            $(".class").addClass("active");
        } else if (direction === "up") {
            $(".class").removeClass("deactive");
        }
        ;
    }, {offset: 100});

    //Плавный скролл до блока .div по клику на .scroll
    //Документация: https://github.com/flesler/jquery.scrollTo
    $("a.scroll").click(function () {
        $.scrollTo($(".div"), 800, {
            offset: -90
        });
    });

    //Каруселька
    //Документация: http://owlgraphic.com/owlcarousel/
    var owl = $(".carousel");
    owl.owlCarousel({
        items: 4
    });
    owl.on("mousewheel", ".owl-wrapper", function (e) {
        if (e.deltaY > 0) {
            owl.trigger("owl.prev");
        } else {
            owl.trigger("owl.next");
        }
        e.preventDefault();
    });
    $(".next_button").click(function () {
        owl.trigger("owl.next");
    });
    $(".prev_button").click(function () {
        owl.trigger("owl.prev");
    });

    //Кнопка "Наверх"
    //Документация:
    //http://api.jquery.com/scrolltop/
    //http://api.jquery.com/animate/
    $("#top").click(function () {
        $("body, html").animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    //Аякс отправка форм
    //Документация: http://api.jquery.com/jquery.ajax/
    /*$("form").submit(function() {
     $.ajax({
     type: "GET",
     url: "mail.php",
     data: $("form").serialize()
     }).done(function() {
     alert("Спасибо за заявку!");
     setTimeout(function() {
     $.fancybox.close();
     }, 1000);
     });
     return false;
     });*/

    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    });

    $('input[name="find_post_for_review"]').keyup(function () {
        $('#add-review').find('input[name="post_id"]').val('');
    });
//        $('input[name="find_post_for_review"]').change(function(){
//           var name=$(this).val();
//            $.ajax
//                ({
//                    url: '/serch-similar-theme/',
//                    type: 'POST',
//                    data: {name: name},
//                    async: false,
//                    success: function (data)
//                    {
//                        $('.similar-theme').empty();
//                        data=JSON.parse(data);
//                        for(i=0;i<data.length;i++)
//                        {
//                            $('.similar-theme').append(data[i].name);
//                        }
//
//                    },
//                    error: function (jqXHR, textStatus, errorThrown)
//                    {
//                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
//                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
//                    }
//                });
//        });

    $('.comment-mark span').click(function () {

        var id = $(this).parent().attr('idc');
        var elem = $(this);
        $.ajax
                ({
                    url: '/send-discontent/',
                    type: 'POST',
                    data: {id: id},
                    success: function (data)
                    {
                        elem.empty();
                        elem.html(data);
                        elem.css('color', 'green');

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });

    });
    $('input[name="find_post_for_review"]').autocomplete({
        source: function (request, response) {
            $.ajax
                    ({
                        url: '/serch-similar-theme/',
                        type: 'POST',
                        data: {name: request.term},
                        //async: false,
                        success: function (data)
                        {
                            data = JSON.parse(data);
                            response($.map(data, function (item) {
                                $('#ui-id-2').css('background-color', '#fff');
                                $('#ui-id-2').css('height', '100%');
                                return {
                                    label: item.name,
                                    value: item.name,
                                    id: item.id
                                }
                            }));

                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                            alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                        }
                    });
        },
        minLength: 1,
        //appendTo: 'input[name="find_post_for_review"]',
        select: function (event, ui) {
            $('input[name=post_id]').val(ui.item.id);
        },

    });

    $('input[name="title_doctor"]').autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: "/find/posts/",
                dataType: "json",
                data: {
                    q: request.term
                },
                success: function (result) {
                    response($.map(result, function (item) {
                        $('#ui-id-1').css('background-color', '#fff');
                        $('#ui-id-1').css('height', '100%');
                        return {
                            label: item.name,
                            value: item.name,
                            id: item.id
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            $('#add-review').find('input[name="post_id"]').val(ui.item.id);
        },
    });

    $('#search-review-header').autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: "/find/posts/",
                dataType: "json",
                data: {
                    q: request.term
                },
                success: function (result) {
                    response($.map(result, function (item) {
                        $('#ui-id-1').css('background-color', '#fff');
                        $('#ui-id-1').css('height', '100%');
                        return {
                            label: item.name,
                            value: item.name,
                            url: item.url,
                            id: item.id
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            window.location.href = '/' + ui.item.url + '/';
        },
    });

    intTMCE('#text-review');
    intTMCE('#text-topic');

    $('[name="topic_title"]').blur(function () {
        var value = $(this).val();
        if (value != '') {
            $(".error-topic-title").html('');
            $('input[name="topic_title"]').css('border', '1px solid #B9B9B9');
        }
    });

    $('[name="review_title"]').keyup(function () {
        var value = $(this).val();
        if (value != '') {
            $(".error-review-title").html('');
            $('input[name="review_title"]').css('border', '1px solid #B9B9B9');
        }
    });

    $('#text-topic').prev().keyup(function () {
        $(".error-topic-details").html('');
        $('#text-topic').prev().css('border', 'none');
    });

    $('#text-review').prev().keyup(function () {
        $(".error-review-details").html('');
        $('#text-review').prev().css('border', 'none');
    });

    $('#send_output_money').click(function ()
    {
        $.ajax
                ({
                    url: '/send-request-money/',
                    type: 'POST',
                    data: {},
                    success: function (data)
                    {
                        alert(data);

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('статус:' + jqXHR.readyState + "  " + jqXHR.status + "    " + jqXHR.responseText + "   ");
                        alert("Что-то пошло не так. Повторите попытку позже! " + textStatus + " " + errorThrown + " " + jqXHR);
                    }
                });
    });
    $('.recommend').find('span').click(function () {
        $(".error-review-recommend").html('');
        $('.recommend').find('span').each(function () {
            $(this).css({
                backgroundColor: 'white',
                color: '#B9B9B9',
                border: '1px solid #B9B9B9'
            });
        });
        $(this).css({
            backgroundColor: 'grey',
            color: 'white'
        });
        $('.recommend').find('input').removeAttr('checked')
        $(this).next().attr('checked', 'checked');
    });

    $('.add-rating').find('img').click(function () {
        $(".error-review-rating").html('');
        $('.add-rating').find('input[type="radio"]').each(function () {
            $(this).removeAttr('checked');
        });
        $('.add-rating').find('img').each(function () {
            $(this).attr('src', '/templates/img/w-star.png');
        });
        var rating = $(this).next().val();
        // console.log(rating);
        for (i = 1; i <= rating; i++)
        {
            var class_img = '.star-rate-' + i;
            $('.add-rating').find(class_img).attr('src', '/templates/img/y-star.png');
        }
        $(this).next().attr('checked', 'checked');
    });

    $('.add-topic-foto').click(function () {
        $(this).next().click();
    });

    $('.block-much').find('button').click(function () {
        $('.block-much').find('button').removeClass('active');
        $(this).addClass('active');
        var measuring = $(this).attr('data-measuring');
        var review_id = $(this).attr('data-rid');
        $('input[name="measuring"]').val(measuring);
        $.fancybox.open('<div class="measuring-warning" >Ваша оценка будет учтена, если вы оставите комментарий.</div>');
        /*$.ajax({
            url: '/measuring/review/',
            method: 'POST',
            data: {
                rate: measuring,
                rid: review_id
            },
            success: function (data) {
                if (data)
                {
                    //console.log(data);
                    var result = JSON.parse(data);
                    if (result['MESSAGE'])
                    {
                        //console.log(result['MESSAGE']);
                    }
                }
            }
        }, 'json');*/
    });

    $('.leave-comment').click(function () {
        // alert('leave-comment');
        var comment_text = $('#comment-text-review').val();
        var review_id = $('#comment-text-review').attr('data-reviewid');
        var measuring = $('input[name="measuring"]').val();

        if (comment_text != '')
        {
            $('.error-comment-text').text('');
            if(measuring == '')
            {
                $('.error-comment-text').text('Для отправки комментария, поставьте оценку отзыву');
                return false;
            }
            else
            {
                $('.error-comment-text').text('');
            }
            $.ajax({
                url: '/comment/review/leave/',
                method: 'POST',
                data: {
                    comment: comment_text,
                    rate: measuring,
                    rid: review_id
                },
                success: function (data) {
                    if (data)
                    {
                        var result = JSON.parse(data);
                        //console.log(result['SUCCESS']);
                        if (result['SUCCESS'])
                        {
                            window.location.reload();
                        }
                        else
                        {
                            $.fancybox('<div class="comment_is_set_error" >' + result['MESSAGE'] + '</div>');
                        }
                    }
                }
            }, 'json');
        } else
        {
            $('.error-comment-text').text('Введите текст комментария');
            return false;
        }

    });

    $('#search-form-topic-review').find('input[type="button"]').click(function ()
    {
        var topic = $('#search-review-header').val();
        var topic_id = $('#search-post-id-header').val();
        var action = $('#search-form-topic-review').attr('action');
        if (topic != '')
        {
            $('#search-form-topic-review').submit();
        } else
        {
            $('#search-review-header').attr('placeholder', 'выберите тему для поиска');
        }
    });

    $('#button-add-recall').click(function () {
        var form = $('#add-review-more');
        form.submit();
    });


    $('.head-menu').mouseover(function () {
        
        var cat_id = $(this).attr('data-id-cat-header');
        $.ajax({
            url: '/get/subcategories/',
            method: 'POST',
            data: {
                cat: cat_id
            },
            success: function (data) {
                $('.sub-menu').html(data);
                $('.sub-menu').show();
            }
        });

    }).mouseleave(function () {
        $('.sub-menu').hide();
    });

    $('.sub-menu').mouseover(function () {
        $(this).show();
    }).mouseleave(function () {
        $(this).hide();
    });


    $('.left-menu-item').mouseover(function () {
        var cat_id = $(this).attr('data-id-cat-side');
        $.ajax({
            url: '/get/subcategories/',
            method: 'POST',
            data: {
                cat: cat_id
            },
            success: function (data) {

                //var smenu = '<div class="sub-left-menu" >'+ data +'</div>';
                //var selector = '[data-id-cat-side="'+ cat_id +'"]';
                //console.log(data);
                //$(selector).append(smenu);
                //$(selector).find('.sub-left-menu').show();
                if (data != '<ul></ul>')
                {
                    $('.sub-left-menu').attr('data-id', cat_id);
                    $('.sub-left-menu').html(data);
                    $('.sub-left-menu').show();
                }
            }
        });

    }).mouseleave(function () {
        $('.sub-left-menu').hide();
    });

//        $('.menu-left-li').click(function()
//        {
//            $('#menu-left-bar').find('.hide-ul-menu').hide();
//           /*var lists=*/$(this).children('ul').show();
//            //lists.next().show();
//        });
//        $('.menu-left-li-level-2').click(function(event)
//        {
//            $(this).parent('ul').find('.hide-ul-menu').hide();
//            $(this).children('ul').show();
//            
//            event.stopPropagation();
//        });


    $('.sub-left-menu').mouseover(function () {
        var cat = $(this).attr('data-id');
        var selector = '[data-id-cat-side="' + cat + '"]';
        //console.log(selector);
        //$(selector).mouseover();
        $(this).show();
    }).mouseleave(function () {
        $(this).hide();
    });

    $('.sendPreview').click(function () {

        var page = $(this).attr('data-page');
        if (page == "new-topic")
        {
            var form = $('#new-topic-review');
        } else
        {
            var form = $('#topic-review');
        }

        var action = $('input[name="action"]');


        //console.log(action.length);
        if (action.length == 0)
        {
            $('.action-review').append('<input type="hidden" name="action" value="preview" />');
        } else
        {
            action.val('preview');
        }

        form.submit();
    });

    $('.saveReview').click(function () {

        var page = $(this).attr('data-page');

        if (page == "new-topic")
        {
            var form = $('#new-topic-review');
        } else
        {
            var form = $('#topic-review');
        }

        var action = $('input[name="action"]');


        //console.log(action.length);
        if (action.length == 0)
        {
            $('.action-review').append('<input type="hidden" name="action" value="save" />');
        }

        form.submit();
    });

    $('.editPreview').click(function () {
        var review_id = $(this).attr('data-rid');
        window.location.href = "/personal/drafts/" + review_id + "/";
    });

    $('.publishPreview').click(function () {
        var review_id = $(this).attr('data-rid');
        $.ajax({
            url: '/review/publish/',
            method: 'POST',
            data: {
                rid: review_id
            },
            success: function (data) {
                if (data)
                {
                    window.location.href = '/' + data + '/';
                }
            }
        });
    });

});



function intTMCE(selector)
{
    $(selector).trumbowyg({lang: 'ru', autogrow: true, plugins: [], btns: [
            ['formatting'],
            'btnGrp-semantic',
            ['superscript', 'subscript'],
            //['link'],
            ['insertImage'],
            'btnGrp-justify',
            'btnGrp-lists',
            ['horizontalRule'],
            ['removeformat'],
        ]});
}


// Отрисовка загруженных изображений
function readFotoTopicURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //var img_block  = '<div class="col-md-3"><label for="description" >Загруженное фото</label></div>';
            var img_block = '<table style="margin-top:-20px;"><tr><td><img style="border-radius:5px;height:100px;width:150px;" src="' + e.target.result + '" /></td><td>&nbsp;&nbsp;<span class="btn btn-default" onclick="delFoto(this);" style="dislay:inline-block; margin-left:16px; margin-top:-17px; width:85px;" >Удалить</span></td></tr></table>';

            $('#load-img').html('');
            $('#load-img').html(img_block);

        };

        reader.readAsDataURL(input.files[0]);
    }
}

function delFoto()
{
    $('#load-img').html('');
    $('#input-img').html('');
    var inputFoto = '<input type="file" onchange="readURL(this);" name="topic_foto" id="bid-img" class="form-control" style="opacity:1; position:static;"/>';
    $('#input-img').html(inputFoto);

}

//Drag&Drop
var isAdvancedUpload = function ()
{
    var div = document.createElement('div');
    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

var $form = $('.box');

if (isAdvancedUpload)
{
    $form.addClass('has-advanced-upload');
}

if (isAdvancedUpload)
{
    var droppedFiles = false;

    $form.on('dragover dragenter', function () {
        $form.addClass('is-dragover');
    })
            .on('dragleave dragend drop', function () {
                $form.removeClass('is-dragover');
            })
            .on('drop', function (e) {
                droppedFiles = e.originalEvent.dataTransfer.files;
            });
}

var $input = $form.find('input[type="file"]'),
        $label = $form.find('label'),
        showFiles = function (files) {
            //$label.text(files.length > 1 ? ($label.html('<p>Выбрано '+ files.length + ' фото</p>'));

            $label.text(files.length > 1 ? ($input.attr('data-multiple-caption') || '').replace('{count}', files.length) : files[ 0 ].name);
        };

$input.on('change', function (e) {
    showFiles(e.target.files);
});

function showDelButton(obj) {
    $(obj).find('span').show();
}

function hideDelButton(obj)
{
    $(obj).find('span').hide();
}

function subscribe_on(obj, reload = false)
{
    var uid = $(obj).attr('data-uid');
    console.log(uid);
    $.ajax({
        url: '/subscribe/on/',
        method: 'POST',
        data: {
            user_on: uid
        },
        success: function (data) {
            if (data)
            {
                var unsubscr = '<a href="javascript:void(0);" data-uid="' + uid + '" onclick="subscribe_off(this);" class="follow" >Отписаться</a>'
                $('.block-btn-pfpage').html(unsubscr);
            }
        }

    });
}

function subscribe_off(obj, reload = false)
{
    var uid = $(obj).attr('data-uid');
    console.log(uid);
    $.ajax({
        url: '/subscribe/off/',
        method: 'POST',
        data: {
            user_on: uid
        },
        success: function (data) {
            if (data)
            {
                var unsubscr = '<a href="javascript:void(0);" data-uid="' + uid + '" onclick="subscribe_on(this);" class="follow" >Подписаться</a>'
                $('.block-btn-pfpage').html(unsubscr);
            }
        }
    });
    if (reload)
    {
        window.location.reload();
}
}





