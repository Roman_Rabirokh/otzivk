  <?php if(!defined("_APP_START")) { exit(); }
	include('sidebar.php');
	if(!empty($_GET['s']))
	{
		$search_target = mb_strtolower(trim($_GET['s']), 'UTF-8');	
	}
	else
	{
		$search_target = '';
	} 
        if(!empty($_GET['d']))
        {
            $search_text=1;
        }
        else
            $search_text=0;
                ?>
	<div class="col-md-9 col-sm-8">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><?php echo $data['name']; ?></li>
	</ol>
	<div class="row personal_area listing">
		<div class="listing_h2 clearfix">
			<div class="col-md-7 col-sm-7 col-xs-12"> 
				<h2><?php echo $data['name']; ?></h2>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12 sponsor">
				<p>Спонсор раздела</p>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 sorting">
			<ul>
<!--				<li <?php //if(!isset($_GET['sort'])) echo 'class="active"'; ?>><a href="/<?php //echo $data['url']; ?>/">Все</a></li>-->
				<li <?php if(isset($_GET['sort']) && $_GET['sort'] == 'rating') echo 'class="active"'; ?> ><a href="/<?php echo $data['url']; ?>/?sort=rating">По рейтингу</a></li>
				<li <?php if(isset($_GET['sort']) && $_GET['sort'] == 'latest') echo 'class="active"'; ?> ><a href="/<?php echo $data['url']; ?>/?sort=latest">Новые отзывы</a></li>
				<li <?php if(isset($_GET['sort']) && $_GET['sort'] == 'best') echo 'class="active"'; ?> ><a href="/<?php echo $data['url']; ?>/?sort=best">Лучшие отзывы</a></li>
			</ul>
		</div>
		<?php $this->includeComponent('otzovik/category.posts.list', array('SEARCH' => $search_target, 'SEARCH_TEXT'=> $search_text)); ?>
	</div>
        <?php echo getBannerBlockById(1); ?>
</div>