<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>libs/bootstrap/css/bootstrap.min.css" />
       <!-- <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>libs/bootstrap/css/bootstrap-theme.min.css" /> -->
     <!-- <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>libs/bootstrap/bootstrap-grid-3.3.1.min.css" /> -->
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>libs/font-awesome-4.2.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>libs/fancybox/jquery.fancybox.css" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>libs/owl-carousel/owl.carousel.css" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>libs/countdown/jquery.countdown.css" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>css/fonts.css" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>css/main.css?v=1.8" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>css/custom.css" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>css/media.css?v=5.5" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>css/dropzone.css" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>css/jquery-ui.css" />
        <link rel="stylesheet" href="<?php echo $this->page->getTemplateUrl(); ?>js/dist/ui/trumbowyg.min.css" />

                <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDt5swuSODxoVxtPxdMIBkzWF6bE_htcEA&callback=initMap" async defer></script> -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDt5swuSODxoVxtPxdMIBkzWF6bE_htcEA&callback=initMap" async defer ></script> -->
        <?php $this->page->getHeader(); ?>

    </head>
    <body class="<?php echo $this->page->getBodyClass(); ?>"><?php
    if($user->Authorized())
    {
        echo "<script>var auth = 1;</script>";
    }
    else
    {
        echo "<script>var auth = 0;</script>";
    } ?>
        <header>
            <div class="top_header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12 logo z-index-for-mob">
                            <a href="/">
                                <img src="<?php echo $this->page->getTemplateUrl(); ?>img/header_logo.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-md-5 col-sm-8 col-xs-12 header_search">
                            <div class="row profile">
                                <div class="col-md-4 col-sm-3 col-xs-12"></div>
                                <div class="col-md-2 col-sm-3 col-xs-3 z-index-for-mob"><?php
                                    global $user;
                                    if (!$user->Authorized()) {
                                        ?>
                                        <a id="enter" class="entrance button fancybox" href="#data">Вход</a><?php
                                    } else {
                                        ?>
                                        <a class="entrance button fancybox" href="/logout/">Выйти</a><?php }
                                    ?>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-4 z-index-for-mob"><?php
                                    if (!$user->Authorized()) {
                                        ?>
                                        <a id="register-a" class="registration button fancybox" href="#data-register">Регистрация</a><?php
                                    } else {
                                        ?>
                                        <a class="registration button fancybox" href="/personal/">Личный кабинет</a><?php }
                                    ?>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-3 col-xs-12 header_search_p">
                                    <p>Найти отзыв о:</p>
                                </div>
                                <div class="col-md-8 col-sm-9 col-xs-12 z-index-for-mob">
                                    <form action="/search/" method="get" id="search-form-topic-review" class="search">
                                        <input type="search" name="s" id="search-review-header" class="input" />
                                        <input type="button" name="" value="" class="submit" />
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="row">
                                <div class="col-md-12 col-sm-6 col-xs-12 p0 top-menu"><?php
                                    $top_menu = Content::getMenuByCode('header');
                                    if (!empty($top_menu)) {
                                        ?>
                                        <ul><?php
                                        foreach ($top_menu as $menu_item) {
                                            ?>
                                                <li><a href="<?php echo $menu_item['link'] ?>"><?php echo $menu_item['title']; ?></a></li><?php }
                                        ?>
                                        </ul><?php }
                                    ?>
                                </div>
                            </div>
                            
                            <div class="write_review">
                                <div class="col-md-12 col-sm-12 col-xs-12 left">
                                    <a id="add-review-page" href="<?php if($user->Authorized()) { echo '/new/'; } else { echo '#data'; } ?> " class="fancybox" >Написать отзыв</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .container -->
            </div><!-- .top_header -->
            <div class="header_nav">
                <div class="container">
                    <div class="row">
                        <?php $this->includeComponent('otzovik/menu.main', array('PLACE' => 'header')); ?>
                        <div class="sub-menu"></div>
                    </div>
                </div>
            </div><!-- .header_nav -->
        </header><!-- header --><?php
        if(!$user->Authorized())
        { ?>
            <div style="display:none">
                <div id="data">
                    <?php $this->includeComponent('system/auth'); ?> 
                </div>
            </div>
            <div style="display:none">
                <div id="data-register">
                    <?php $this->includeComponent('system/register'); ?> 
                </div>
            </div><?php
        } ?>
        <main>
            <div class="container">
                <div class="row">

          