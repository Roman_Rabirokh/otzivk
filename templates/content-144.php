<?php if(!defined("_APP_START")) { exit(); } 
include('sidebar.php'); ?>
<div class="col-md-9 col-sm-8">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><?php echo $data['name']; ?></li>
	</ol>
	<div class="row get_in_touch"> 
						<form action="#" method="#" class="form" id="get_in_touch">
							<h2>Обратная связь</h2>
							<p>Если у Вас возникли какие-либо вопросы или предложения, напишите нам:</p>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" placeholder="Ваше имя" name="form_name" class="form-input" required>
								</div>
								<div class="form-group">
									<input type="email" placeholder="Ваш e-mail" name="form_email" class="form-input" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<textarea name="" placeholder="Текст сообщения" class="form-textarea" required></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<button type="submit" class="form-btn">Отправить</button>
								</div>
							</div>
						</form>
					</div>
    
	<?php echo getBannerBlockById(1); ?>
</div>