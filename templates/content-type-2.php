<?php if(!defined("_APP_START")) { exit(); }  ?>
<?php 
$this->page->breadcrumb = array();
//$this->page->addPathItem('Каталог', '/catalog/');
$data['UP_CATEGORY_TREE'] = array();
Content::getOpenContentTree($data['id'],$data['UP_CATEGORY_TREE']);
$count = 1;
foreach(array_reverse($data['UP_CATEGORY_TREE']) as $path_item)
{
  if($count == count($data['UP_CATEGORY_TREE']))
  {
    $this->page->addPathItem($path_item['NAME']);
  }
  else
  {
    $this->page->addPathItem($path_item['NAME'], $path_item['LINK']);
  }

  $count++;
}
$pageId=$data['id'];
include('sidebar.php');
?>
<div class="col-md-9 col-sm-8">
	<?php $this->page->getPath(); ?>
	<!--<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><?php //echo $data['name']; ?></li>
	</ol>-->
	<div class="row personal_area listing">
		<div id="listing_h2 clearfix">
			<div class="col-md-7 col-sm-7 col-xs-12">
				<h2><?php echo $data['name']; ?></h2>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12 sponsor">
				<p>Спонсор раздела</p>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 sorting">
			<ul class="ul0without-shadow">
<!--				<li <?php //if(!isset($_GET['sort'])) echo 'class="active"'; ?>><a href="/<?php //echo $data['url']; ?>/">Все</a></li>-->
				<li <?php if(isset($_GET['sort']) && $_GET['sort'] == 'rating') echo 'class="active"'; ?> ><a href="/<?php echo $data['url']; ?>/?sort=rating">По рейтингу тем</a></li>
				<li <?php if(isset($_GET['sort']) && $_GET['sort'] == 'latest') echo 'class="active"'; ?> ><a href="/<?php echo $data['url']; ?>/?sort=latest">Новые отзывы</a></li>
				<li <?php if(isset($_GET['sort']) && $_GET['sort'] == 'best') echo 'class="active"'; ?> ><a href="/<?php echo $data['url']; ?>/?sort=best">Лучшие отзывы</a></li>
			</ul>
		</div>
		<?php $this->includeComponent('otzovik/category.posts.list', array('CATEGORY' => $data['id'])); ?>
	</div>
<?php echo getBannerBlockById(1); ?>
</div>
