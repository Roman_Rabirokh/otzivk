<?php if(!defined("_APP_START")) { exit(); }

include('sidebar.php'); ?>
<div class="col-md-9 col-sm-8" style="padding:0;">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><?php echo $data['name']; ?></li>
	</ol>
	<div style="height:auto;padding:0 0 30px 0;" class="col-md-12">
		 <div style="height:400px;" id="map"></div>
	</div>

	<?php echo getBannerBlockById(1); ?>
</div>
