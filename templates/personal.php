﻿	<?php if(!defined("_APP_START")) { exit(); }
	global $user;
	global $app;
	if($user->Authorized())
	{
		$isset_email = dbGetOne('SELECT email FROM #__users WHERE id = :uid', array(':uid' => $user->getID()));
		$app->page->addPathItem('Личный кабинет','/personal/');
		if(isset($params[1]) && $params[1] == 'messages')
		{
			$app->page->addPathItem('Cooбщения');
		}
		else if(isset($params[1]) && $params[1] == 'reviews')
		{
			$app->page->addPathItem('Мои отзывы');
		}
		else if(isset($params[1]) && $params[1] == 'subs')
		{
			$app->page->addPathItem('Подписки');
		}
		else if(isset($params[1]) && $params[1] == 'bookmarks')
		{
			$app->page->addPathItem('Закладки');
		}
		else if(isset($params[1]) && $params[1] == 'drafts')
		{
			if(!empty($params[2]))
			{
				$app->page->addPathItem('Черновики отзывов', '/personal/drafts/');
				$app->page->addPathItem('Редактирование черновика');
			}
			else
			{
				$app->page->addPathItem('Черновики отзывов');
			}
		}
		else if(isset($params[1]) && $params[1] == 'money')
		{
			$app->page->addPathItem('Деньги');
		}
		else if(isset($params[1]) && $params[1] == 'profile')
		{
			if(isset($params[2]) && $params[2] == 'edit')
			{
				$app->page->addPathItem('Профиль', '/personal/');
				$app->page->addPathItem('Редактирование профиля');
			}

		}
		else
		{
			$app->page->addPathItem('Профиль');
		} ?>
		<div class="col-md-3 sidebar">
			<?php echo getBannerBlockById(2); ?>
			 <?php echo getBannerBlockById(3); ?>
		</div><!-- .sidebar -->
                <div class="col-md-9 col-sm-8<?php if(isset($params[1]) && $params[1] == 'reviews') { ?> topic <?php }?> ">
			<?php
			if(!empty($isset_email))
			{
			$app->page->getPath();

			if((isset($params[1]) && $params[1] == 'messages') && !empty($params[1]))
			{
				dbQuery('UPDATE #__users_messages
								SET isread = 1
								WHERE userid_to = :uid_to AND userid_from = :uid_from',
									array(
										':uid_to' => $user->getID(),
										':uid_from' => intval($params[2])
									));
			}
			$data['PERSONAL_REVIEWS_QNT'] = dbGetOne('SELECT COUNT(id) FROM #__content WHERE content_type = 3 AND userid = :uid', array(':uid' => $user->getID()));
			$data['PERSONAL_MSG_QNT'] = dbGetOne('SELECT COUNT(*) FROM #__users_messages WHERE userid_to = :uid AND isread = 0', array(':uid' => $user->getID()));
			$data['PERSONAL_SUBSCR_QNT'] = dbGetOne('SELECT COUNT(*) FROM #__subscribes_on_user WHERE userid = :uid', array(':uid'=> $user->getID()));
			$data['PERSONAL_DRAFT_QNT'] = dbGetOne('SELECT COUNT(id) FROM #__content WHERE content_type = 3 AND userid = :uid AND active = 0', array(':uid' => $user->getID()));
			$data['PERSONAL_BOOKMARKS_QNT'] = dbGetOne('SELECT COUNT(id) FROM #__users_bookmarks WHERE userid = :uid', array(':uid' => $user->getID()));

				?>
			<div class="personal_area">
				<ul>
					<li <?php if(empty($params[1])) echo 'class="active"'; ?> ><a href="/personal/">Профиль</a></li>
					<li <?php if(isset($params[1]) && $params[1] == 'reviews') echo 'class="active"'; ?> ><a href="/personal/reviews/">Мои отзывы (<span><?php echo $data['PERSONAL_REVIEWS_QNT']; ?></span>)</a></li>
					<li <?php if(isset($params[1]) && $params[1] == 'messages') echo 'class="active"'; ?> ><a href="/personal/messages/">Сообщения (<span><?php echo $data['PERSONAL_MSG_QNT']; ?></span>)</a></li>
					<li <?php if(isset($params[1]) && $params[1] == 'subs') echo 'class="active"'; ?> ><a href="/personal/subs/">Подписки (<span><?php echo $data['PERSONAL_SUBSCR_QNT']; ?></span>)</a></li>
					<li <?php if(isset($params[1]) && $params[1] == 'bookmarks') echo 'class="active"'; ?> ><a href="/personal/bookmarks/">Закладки (<span><?php echo $data['PERSONAL_BOOKMARKS_QNT']; ?></span>)</a></li>
					<li <?php if(isset($params[1]) && $params[1] == 'drafts') echo 'class="active"'; ?> ><a href="/personal/drafts/">Черновик отзывов (<span><?php echo $data['PERSONAL_DRAFT_QNT']; ?></span>)</a></li>
					<li <?php if(isset($params[1]) && $params[1] == 'money') echo 'class="active"'; ?> ><a href="/personal/money/">Деньги</a></li>
				</ul>
			</div><?php
			if(isset($params[1]) && $params[1] == 'messages')
			{
				if(!empty($params[2]))
				{

					$app->includeComponent('otzovik/personal.messages.chat', array('USER_FROM' => intval($params[2])));
				}
				else
				{
					$app->includeComponent('otzovik/personal.messages');
				}
			}
			else if(isset($params[1]) && $params[1] == 'reviews')
			{
				$app->includeComponent('otzovik/personal.reviews');
			}
			else if(isset($params[1]) && $params[1] == 'subs')
			{
				$app->includeComponent('otzovik/personal.subscribes');
			}
			else if(isset($params[1]) && $params[1] == 'drafts')
			{
				if(!empty($params[2]))
				{
					//echo $params[2]; die;
					$app->includeComponent('otzovik/personal.drafts.edit', array('REVIEW_ID' => intval($params[2])));
				}
				else
				{
					$app->includeComponent('otzovik/personal.drafts');
				}
			}
			else if(isset($params[1]) && $params[1] == 'bookmarks')
			{
				$app->includeComponent('otzovik/personal.bookmarks');
			}
			else if(isset($params[1]) && $params[1] == 'money')
			{
				$app->includeComponent('otzovik/personal.money');
			}
			else if(isset($params[1]) && $params[1] == 'profile')
			{
				if(isset($params[2]) && $params[2] == 'edit')
				{
					$app->includeComponent('otzovik/personal.profile.edit');
				}
			}
			else
			{
				$app->includeComponent('otzovik/personal.profile');
			} ?>
			<?php echo getBannerBlockById(1); ?>
		</div><?php
		}
		else
		{ ?>
			<div class="row personal_area profile">
				<div class="box">
					<div class="block">
						<div class="row">
							<div class="col-md-12" style="margin-bottom:-20px;margin-top:18px;" >
								<div style="width: 82.3%;margin-left: 15px;" id="registerMessages"></div>
								<iframe style="width:100%;height:150px;display:none;" name="user-email" ></iframe>
								<form class="form" method="POST" action="/save/email/" target="user-email">
								Укажите пожалуйста свой реальный адрес электронной почты.
С помощью него вы сможете востановить забытый пароль, а также получать оповещения о действиях в вашем аккаунте
<div style="height: 20px;width: 10px;">  </div>
                                                                    <div class="col-md-6 form-group">
										<input required type="email" class="form-input" placeholder="Укажите свой мейл для связи с вами" name="email" value="" style="height: 43px;padding-left:20px;width:100%" />
									</div>
									<div class="col-md-6 form-group"  >
                                                                            
										<input type="submit" value="завершить регистрацию" style="height: 43px;width: 261px;background-color:#417505; color:#FFFFFF; text-transform:uppercase;border:none;"/>
									</div>
                                                                    
								</form>
							</div>
						</div>
					</div>
				</div>
			</div><?php
		}
	}
	else
	{
           // var_dump($user->Authorized()); die;
           redirect('/login/');
	//$app->includeComponent("system/auth", array('TEMPLATE'=>'login'));
	}
