<?php include('sidebar.php');
	global $app;
	$token = $app->createFormToken('ar');
	echo '<script> var tokenUpload="'.$token.'"</script>'; ?>
	<div class="col-md-9 col-sm-8">
		<ol class="breadcrumb">
			<li><a href="/">Главная</a></li>
			<li class="active"><?php if(!empty($name_post)) echo $name_post; else redirect('/'); ?></li>
		</ol>
		<div class="row add_new_topic_review">
			<h2 style="margin-bottom: 35px;">Добавление отзыва</h2>
			<div id="registerMessages"></div>
			<iframe style="width:100%;height:150px;display:none;" name="save-topic-review" ></iframe>
			<p id="error_add_new_review" style="color: red;"></p>
			<form enctype="multipart/form-data" action="/add/topic/review/" method="post" id="topic-review" target="save-topic-review" >
				<div class="form-group review-group" >
					<label>Напишите лаконичный заголовок отзыва:</label>
					<input type="text" name="review_title" class="input-review" placeholder="заголовок отзыва"/>
					<input type="hidden" name="topic_id" value="<?php if(!empty($_POST['post_id'])) echo $_POST['post_id']; ?>" />
					<input type="hidden" name="token" value="<?php echo $token; ?>" >
					<div class="errors-add error-review-title" style="margin-top: -10px;"></div>
				</div>
				<div class="form-group review-group" >
					<label>Ваш отзыв:</label>
					<textarea id="text-review" name="review_text" placeholder="начните вводить тект..." ></textarea>
					<div class="errors-add error-review-details" style="margin-top: -16px;"></div>
				</div>
				<div class="form-group review-group review-rate" >
					<div class="col-md-6">
						<label>Ваша оценка:</label>
						<div class="add-rating"><?php
						for($i = 1; $i <=5; $i++)
						{ ?>
							<img height="35" class="star-rate-<?php echo $i; ?>" src="<?php echo _TEMPLATE_URL; ?>img/w-star.png" />
							<input type="radio" name="rating" value="<?php echo $i; ?>" /><?php
						} ?>

						</div>
						<div class="errors-add error-review-rating" style=""></div>
					</div>
					<div class="col-md-6">
						<label>Рекомендуете ли вы это своим друзьям?</label>
						<div class="recommend">
							<span class="recomend-yes">Да</span>
							<input type="radio" name="recommend" value="1"/>
							<span class="recommend-no">Нет</span>
							<input type="radio" name="recommend" value="0" />
						</div>
						<div class="errors-add error-review-recommend" style=""></div>
					</div>
				</div>
				<div class="form-group review-group review-foto" >
					<label>Добавьте фотографии:</label>
					<p>! Пожалуйста, добавляйте здесь только сделанные вами уникальные фотографии и только по теме отзыва!</p>
					<div style="margin-bottom:20px;" id="load_img_review"></div>
					<div id="dropzone" style="border:2px dashed #4A90E2; margin:30px 0;background-color: #F6F9FD; height: 355px; width:100%;">
						<div class="dz-message needsclick">
						    <p>Перетащите изображения сюда</p>
								<p>или</p>
						    <span class="note needsclick"><button type="button" class="add-topic-foto">Загрузить с компьютера</button></span>
						  </div>
					</div>
				</div>
				<!--<div id="map-block" class="form-group review-group review-video" style="display:none" >
					<label>Отметьте на карте своё местоположение:</label>
					<div id="map" style="height: 500px;"></div>
					<input id="review-lat" type="hidden" name="user_lat" value="" />
					<input id="review-lng" type="hidden" name="review_lng" value="" />
				</div>-->
				<div class="form-group review-group review-video" >
					<label>Добавьте видео:</label>
					<p>Вставьте в поле ссылку с YouTube</p>
					<input type="text" name="review_video" class="input-review" placeholder="вставьте ссылку"/>
				</div>
				<div class="action-review" ></div>
				<div class="form-group review-group review-buttons" >
					<div class="col-md-4 but-preview" >
						<button type="button" class="saveReview" data-page="topic-review" >Сохранить</button>
					</div>
					<div class="col-md-4 but-preview" style="margin-left:-14px;margin-right:5px;" >
						<button type="button" class="sendPreview" data-page="topic-review" >Предпросмотр</button>
					</div>
					<div class="col-md-4 but-publish" >
						<button type="submit" >Опубликовать</button>
					</div>
				</div>
			</form>
		</div>
		<?php echo getBannerBlockById(1); ?>
		<h2 class="see_also">Смотрите также</h2>
		<div class="top_news">
			<div class="row block">
				<div class="col-md-4 col-sm-4 left">
					<img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img5.jpg" alt="">
					<div class="comments_box">
						<div class="comments">
							<p>56</p>
						</div>
					</div>
				</div>
				<div class="col-md-8 right">
					<div class="row">
						<div class="col-md-7 col-sm-8">
							<h3>Обновленный седан Toyota Corola</h3>
						</div>
						<div class="col-md-5 col-sm-8 rating">
							<p>4.5 / 5</p>
							<div class="stars">
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="line"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h2>Блиц-тест обновленного седана Toyota Corolla. Десять отличий.</h2>
						</div>
						<div class="col-md-12">
							<p>Загибайте пальцы. Интерьер - раз, бамперы - два, оптика - три... Плавное обновление седана Toyota Corolla прошло...</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<span>+ 2 фото</span>
							<div class="autor">
								<p>Armir_12</p>
							</div>
						</div>
					</div>
				</div>
			</div><!-- .block -->
			<div class="row block">
				<div class="col-md-4 col-sm-4 left">
					<img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img6.jpg" alt="">
					<div class="comments_box">
						<div class="comments">
							<p>56</p>
						</div>
					</div>
				</div>
				<div class="col-md-8 right">
					<div class="row">
						<div class="col-md-7 col-sm-8">
							<h3>Обновленный седан Toyota Corola</h3>
						</div>
						<div class="col-md-5 col-sm-8 rating">
							<p>4.5 / 5</p>
							<div class="stars">
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="line"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h2>Блиц-тест обновленного седана Toyota Corolla. Десять отличий.</h2>
						</div>
						<div class="col-md-12">
							<p>Загибайте пальцы. Интерьер - раз, бамперы - два, оптика - три... Плавное обновление седана Toyota Corolla прошло...</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<span>+ 2 фото</span>
							<div class="autor">
								<p>Armir_12</p>
							</div>
						</div>
					</div>
				</div>
			</div><!-- .block -->
		</div><!-- .see_also -->
	</div>
	<script>
	/*function handleFileSelect(evt)
	{
		var files = evt.target.files; // FileList object
		// Loop through the FileList and render image files as thumbnails.
		document.getElementById('load_img_review').innerHTML = '';
		for (var i = 0, f; f = files[i]; i++)
		{
			// Only process image files.
			if (!f.type.match('image.*'))
			{
				alert("Image only please....");
			}
			var reader = new FileReader();
			// Closure to capture the file information.
			reader.onload = (function (theFile)
			{
				return function (e)
				{
					console.log(e);
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = ['<span onmouseover="showDelButton(this);" onmouseleave="hideDelButton(this);" class="img-container" ><img style="margin-right:20px; data-name="'+theFile.name+'" border-radius:5px;" class="load-img" height="100" width="150" class="thumb" title="', escape(theFile.name), '" src="', e.target.result, '" /><span>Удалить</span></span>'].join('');
					document.getElementById('load_img_review').insertBefore(span, null);
				};
			})(f);
			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
		}
	}

	document.getElementById('reviewFotos').addEventListener('change', handleFileSelect, false);*/
	</script>
