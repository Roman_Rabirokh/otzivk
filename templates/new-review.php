<?php include('sidebar.php'); ?>
<div class="col-md-9 col-sm-8">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active">Написать отзыв</li>
	</ol>
	<div class="row add_new_topic_review">
		<h2>Написать отзыв</h2>
		<p>Для написания отзыва введите о чем вы хотите написать отзыв. Если во всплывающей подсказке вы обнаружите нужную вам тему, то перейдите по ссылке, чтобы оставить свой отзыв в даннной теме:</p>
		<form action="/review/add/" id="add-review" method="post" class="search" >
			<input type="search" name="find_post_for_review" required class="input" placeholder="введите название объекта отзыва"/>
			<input type="hidden" name="post_id" value="" />
			<input type="submit" name="" value="Продолжить" class="submit" />
		</form>
                <div class="similar-theme">
                    
                </div>
		<div class="help">
			<h3>Примеры правильного оформления названий:</h3>
			<p>Смартфон Apple iPhone 5</p>
			<p>Отель Golden Five Emerald 5* (Египет, Хургада)</p>
			<p>Фильм “Мальчишник в Вегасе” (2009)</p>
			<p>Автомобиль Ford Focus 2 хетчбек</p>
			<p>Отдых в г.. Судак (Россия, Крым)</p>
		</div>
	</div>
	<?php echo getBannerBlockById(1); ?>
	<h2 class="see_also">Смотрите также</h2>
	<div class="top_news">
		<div class="row block">
			<div class="col-md-4 col-sm-4 left">
				<img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img5.jpg" alt="">
				<div class="comments_box">
					<div class="comments">
						<p>56</p>
					</div>
				</div>
			</div>
			<div class="col-md-8 right">
				<div class="row">
					<div class="col-md-7 col-sm-8">
						<h3>Обновленный седан Toyota Corola</h3>
					</div>
					<div class="col-md-5 col-sm-8 rating">
						<p>4.5 / 5</p>
						<div class="stars">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="line"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h2>Блиц-тест обновленного седана Toyota Corolla. Десять отличий.</h2>
					</div>
					<div class="col-md-12">
						<p>Загибайте пальцы. Интерьер - раз, бамперы - два, оптика - три... Плавное обновление седана Toyota Corolla прошло...</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<span>+ 2 фото</span>
						<div class="autor">
							<p>Armir_12</p>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .block -->
		<div class="row block">
			<div class="col-md-4 col-sm-4 left">
				<img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img6.jpg" alt="">
				<div class="comments_box">
					<div class="comments">
						<p>56</p>
					</div>
				</div>
			</div>
			<div class="col-md-8 right">
				<div class="row">
					<div class="col-md-7 col-sm-8">
						<h3>Обновленный седан Toyota Corola</h3>
					</div>
					<div class="col-md-5 col-sm-8 rating">
						<p>4.5 / 5</p>
						<div class="stars">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="line"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h2>Блиц-тест обновленного седана Toyota Corolla. Десять отличий.</h2>
					</div>
					<div class="col-md-12">
						<p>Загибайте пальцы. Интерьер - раз, бамперы - два, оптика - три... Плавное обновление седана Toyota Corolla прошло...</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
						<span>+ 2 фото</span>
						<div class="autor">
							<p>Armir_12</p>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .block -->
	</div><!-- .see_also -->
</div>
