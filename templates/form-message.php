<?php if(!defined("_APP_START")) { exit(); }
  include('sidebar.php');
  $token = $app->createFormToken('sm')?>

  <div class="col-md-9">
    <ol class="breadcrumb">
      <li><a href="#">Главная</a></li>
      <li class="active">Отправить сообщение</li>
    </ol>
    <div class="col-md-12 add-comment">
      <h4 style="margin-bottom:20px;">Отправьте своё сообщение:</h4>
      <iframe style="height:150px;width:100%; display:none;" name="msg-send" ></iframe>
      <form method="POST" name="send-message" action="/send/message/" target="msg-send" ><?php
      if(!empty($_SESSION['user']))
      { ?>
          <div class="comment-review" >
               <label>Тема сообщения:</label><br />
               <input style="width: 100%;" name="subject" type="text" />
               <p style="display:none; color:red;margin-bottom:0;font-size:12px;" class="error-subject">Введите название темы</p>
          </div><br/><?php
          
      } ?>
          
<!--        <div class="form-group review-group" >
					<label>Тема сообщения:</label>
					<input type="text" name="msg_subject" class="input-review" placeholder="тема сообщения"/>
          <span class="error-subj"></span>
				</div>-->
        <div class="comment-review message-text">
          <label>Текст сообщения:</label>
          <input type="hidden" name="user_to" value="<?php echo $user_to; ?>">
          <input type="hidden" name="token" value="<?php echo $token; ?>">
          <textarea required="" style="padding-left: 20px;" name="msg-text" id="msg-text-to-user" rows="10" placeholder="Текст сообщения" ></textarea>
          <span class="error-text"></span>
        </div>


        <div style="padding-left: 0;" class="col-md-10"><p style="color: red;" class="error-comment-text"></p></div>
        <div class="col-md-2">
          <button class="send-message" type="submit" >Отправить</button>
        </div>
      </form>
      </div>
    <?php echo getBannerBlockById(1); ?>
  </div>
