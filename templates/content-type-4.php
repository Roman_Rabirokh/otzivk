<?php // страница темы
if (!defined("_APP_START")) {
    exit();
}
include('sidebar.php');
$this->page->breadcrumb = array();
//$this->page->addPathItem('Каталог', '/catalog/');
$data['CATEGORY_ID'] = dbGetOne('SELECT f2 AS category FROM #__content_data_4 WHERE id = :id', array(':id' => $data['id']));
$data['CATEGORY_TREE'] = array();
Content::getOpenContentTree($data['CATEGORY_ID'], $data['CATEGORY_TREE']);
$data['CATEGORY_TREE'] = array_reverse($data['CATEGORY_TREE']);
$data['RECALL_BLOCK']['product_info'] = dbGetRow('SELECT c.id, c.name, c.mainimage, c.url, c.detail_text, cd.f2 AS category, cd.f7 AS rating FROM #__content c
                                                      LEFT JOIN #__content_data_4 cd ON cd.id = c.id
                                                      WHERE content_type = 4 AND c.id = :id', array(
    ':id' => $data['id']
        ));
if ($data['RECALL_BLOCK']['product_info']['mainimage'] != 0) {
    $data['RECALL_BLOCK']['product_info']['main_image'] = getImageById($data['RECALL_BLOCK']['product_info']['mainimage'], array('width' => 550, 'crop' => array(0, 0, 424, 346)));
}

foreach ($data['CATEGORY_TREE'] as $path_item) {
    $this->page->addPathItem($path_item['NAME'], $path_item['LINK']);
}
$this->page->addPathItem($this->page->getTitle());
?>
<div class="col-md-9 col-sm-8">
<?php $this->page->getPath(); ?>
    <div class="row topic" style="padding:0;">
        <h2><?php echo $this->page->getTitle(); ?></h2>
        <div class="col-md-6 col-sm-6  home"><?php
if (!empty($data['RECALL_BLOCK']['product_info']['main_image'])) {
    ?>
                <img height="346" src="<?php echo $data['RECALL_BLOCK']['product_info']['main_image']; ?>" alt=""><?php
            } else {
                ?>
                <img height="346" src="<?php echo _IMAGES_URL; ?>no-foto.png" alt=""><?php }
            ?>
            <div class="col-md-6">
                <h2><?php echo $data['RECALL_BLOCK']['product_info']['name']; ?></h2>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 topic-rating"><?php
            if (!empty($data['RECALL_BLOCK']['product_info']['rating'])) {
                ?>
                        <p class="col-md-4"><?php echo round($data['RECALL_BLOCK']['product_info']['rating'], 1, PHP_ROUND_HALF_UP); ?>/5</p>
                        <div class="stars col-md-8"style="padding:0 !important;width:65%;margin-top:15px;margin-left: -8px;"><?php
                for ($i = 1; $i <= 5; $i++) 
                { 
                    if($i <= round($data['RECALL_BLOCK']['product_info']['rating'], 1, PHP_ROUND_HALF_UP))
                    { ?>
                        <img style="width: 12px;" src="<?php echo _TEMPLATE_URL; ?>img/or-star.png" alt=""><?php 
                    }
                    else
                    {
                        if($i - round($data['RECALL_BLOCK']['product_info']['rating'], 1, PHP_ROUND_HALF_UP) < 1)
                        { ?>
                            <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/hp-star.png" alt=""><?php
                        }
                        else
                        { ?>
                            <img style="width:12px;" src="<?php echo _TEMPLATE_URL; ?>img/ow-star.png" alt=""><?php  
                        }
                    }
                } ?>
            </div><?php }
            ?>
                </div>
                <div class="col-md-12 add-review-button">
                    <form action="/review/add/" id="add-review-more" method="POST" class="search" >
                        <input type="hidden" name="post_id" value="<?php echo $data['RECALL_BLOCK']['product_info']['id']; ?>" />
                    </form>
                    <a href="javascript:void(0);" id="button-add-recall" >+ Добавить отзыв</a>
                </div>
            </div>

        </div>
<?php echo getBannerBlockById(5); ?>
    </div>
    <div class="col-md-12 block_detail_text">
<?php if (!empty($data['RECALL_BLOCK']['product_info']['detail_text'])) {
    ?>
    <?= $data['RECALL_BLOCK']['product_info']['detail_text']; ?>
        <?php } else {
            ?>
            Нет описания
        <?php } ?>
    </div>
    <div class="social-icons" style="margin-bottom:15px;">
        <?php include 'yandex-social.php'; ?>
    </div>




    <div class="row listing recall" >
        <?php $this->includeComponent('otzovik/post.recalls.list', array('CATEGORY' => $data['id'])); ?>
    </div>
 
    <div class="table">
        <?php echo getBannerBlockById(6); ?>
    </div>
    <h2 class="see_also">Смотрите также</h2>
    <div class="top_news">
        <?php echo getBannerBlockById(8); ?><?php
        $category = dbGetOne('SELECT f2 AS idc FROM #__content_data_4 WHERE id = :id', array(':id' => $data['id']));
        $this->includeComponent('otzovik/category.view.also', array('CATEGORY' => $category, 'PID' => $data['id']));
        ?>
    </div><!-- .see_also -->

        <?php echo getBannerBlockById(1); ?>
</div>
