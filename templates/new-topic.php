<?php include('sidebar.php');
	global $app;
	$token = $app->createFormToken('ar');
	echo '<script> var tokenUpload="'.$token.'"</script>'; ?>
	<div class="col-md-9 col-sm-8">
		<ol class="breadcrumb">
			<li><a href="/">Главная</a></li>
			<li class="active"><?php if(!empty($name_post)) echo $name_post; else redirect('/'); ?></li>
		</ol>
		<div class="row add_new_topic_review">
			<div id="registerMessages"></div>
			<h2 style="margin-bottom: 35px;">Добавление новой темы и отзыва</h2>
			<iframe style="display:block;width:100%;heigth:500px !important;" name="save-topic-review"  ></iframe>
			<form enctype="multipart/form-data" action="/add/topic/review/" method="post" id="new-topic-review" target="save-topic-review" >
				<div class="form-group review-group" >
					<label>Напишите название Темы, в которой будет размещён отзыв:</label>
					<input type="text" required name="topic_title" class="input-review" placeholder="название темы" value="<?php if(!empty($_POST['find_post_for_review'])) echo $_POST['find_post_for_review']; ?>"/>
					<div class="errors-add error-topic-title" style="margin-top: -9px;"></div>
				</div><?php
				$data['TOPIC_CATEGORIES'] = dbQuery('SELECT id, name FROM #__content WHERE content_type = 2 AND parentid = 0');
				//echo "<pre>"; print_r($data['TOPIC_CATEGORIES']); die;
				if(!empty($data['TOPIC_CATEGORIES']))
				{ ?>
				<div class="form-group review-group select-category" >
					<label>Выберите категорию Темы:</label>
					<select required name="topic_category" onchange="getSubCats(this);" class="input-review categories" >
						<option value="0" >Выберите категорию</option><?php
					foreach($data['TOPIC_CATEGORIES'] as $topic_category)
					{ ?>
						<option value="<?php echo $topic_category['id']; ?>"><?php echo $topic_category['name'] ?></option><?php
					} ?>
					</select>
				</div>
				<div class="errors-add error-topic-category" style="margin-top: -24px;color:red;"></div><?php
				} ?>
				<div class="form-group review-group topic-text" >
					<label>Описание темы:</label>
					<textarea required id="text-topic" name="topic_text" placeholder="начните вводить тект..." ></textarea>
					<div class="errors-add error-topic-details" style="margin-top: -16px;"></div>
				</div>
				<div class="form-group review-group topic-foto" >
					<div class="col-md-9 div-without-padding col-sm-9" >
						<label>Прикрепите изображение Темы:</label>
 <p class="gray-color">Вы создаёте тему которой возможно ещё нет на нашем сайте.
Мы будем очень благодарны если Вы предложите изображение подходящее к данной теме.
Это изображение будет отображаться сверху всех отзывов по этой теме.
Пожалуйста, не загружайте картинки, на которых присутствуют водяные знаки других сайтов.
 </p>
					</div>
					<div class="col-md-3" >
                                                
						<button id="btn-add-photo" type="button" class="add-topic-foto" >Загрузить с компьютера</button>
						<input type="file" onchange="readFotoTopicURL(this);" name="topic_foto" />
					</div>
					<!--<div class="col-md-4" id="load-img" >

					</div> -->
					<!-- onchange="readURL(this);" -->
				</div>
				<p class="line-review"></p>
				<div class="form-group review-group" >
					<label>Напишите лаконичный заголовок отзыва:</label>
					<input type="text" required name="review_title" class="input-review" placeholder="заголовок отзыва"/>
					<input type="hidden" name="topic_id" value="<?php if(!empty($_POST['post_id'])) echo $_POST['post_id']; ?>" />
					<input type="hidden" name="token" value="<?php echo $token; ?>" >
					<div class="errors-add error-review-title" style="margin-top: -10px;"></div>
				</div>
				<div class="form-group review-group" >
					<label>Ваш отзыв:</label>
					<textarea id="text-review" required name="review_text" placeholder="начните вводить тект..." ></textarea>
					<div class="errors-add error-review-details" style="margin-top: -16px;"></div>
				</div>
				<div class="form-group review-group review-rate" >
					<div class="col-md-6">
						<label>Ваша оценка:</label>
						<div class="add-rating"><?php
						for($i = 1; $i <=5; $i++)
						{ ?>
							<img height="35" class="star-rate-<?php echo $i; ?>" src="<?php echo _TEMPLATE_URL; ?>img/w-star.png" />
							<input type="radio" name="rating" value="<?php echo $i; ?>" /><?php
						} ?>
						</div>
						<div class="errors-add error-review-rating" style=""></div>
					</div>
					<div class="col-md-6">
						<label>Рекомендуете ли вы это своим друзьям?</label>
						<div class="recommend">
							<span class="recomend-yes">Да</span>
							<input type="radio" name="recommend" value="1"/>
							<span class="recommend-no">Нет</span>
							<input type="radio" name="recommend" value="0" />
						</div>
						<div class="errors-add error-review-recommend" style=""></div>
					</div>
				</div>
				<div style="height: auto;" class="form-group review-group review-foto" >
					<label>Добавьте фотографии:</label>
					<p>! Пожалуйста, добавляйте здесь только сделанные вами уникальные фотографии и только по теме отзыва!</p>
					<div style="margin-bottom:20px;" id="load_img_review"></div>
					<div id="dropzone" style="border:2px dashed #4A90E2; margin:30px 0;background-color: #F6F9FD; height: 355px; width:100%;">
						<div class="dz-message needsclick">
								<p>Перетащите изображения сюда</p>
								<p>или</p>
								<span class="note needsclick"><button type="button" class="add-topic-foto">Загрузить с компьютера</button></span>
							</div>
					</div>
				</div>
				<div class="form-group review-group review-video" >
					<label>Добавьте видео:</label>
					<p>Вставьте в поле ссылку с YouTube</p>
					<input type="text" name="review_video" class="input-review" placeholder="вставьте ссылку"/>
				</div>
				<div class="action-review" ></div>
				<div class="form-group review-group review-buttons" >
					<div class="col-md-4 but-preview" >
						<button type="button" class="saveReview" data-page="new-topic" >Сохранить</button>
					</div>
					<div class="col-md-4 but-preview" >
						<button type="button" class="sendPreview" data-page="new-topic" style="margin-right: 9px;" >Предпросмотр</button>
					</div>
					<div class="col-md-4 but-publish" >
						<button type="submit" >Опубликовать</button>
					</div>
				</div>
			</form>
		</div>
		<?php echo getBannerBlockById(1); ?>
		<h2 class="see_also">Смотрите также</h2>
		<div class="top_news">
			<div class="row block">
				<div class="col-md-4 col-sm-4 left">
					<img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img5.jpg" alt="">
					<div class="comments_box">
						<div class="comments">
							<p>56</p>
						</div>
					</div>
				</div>
				<div class="col-md-8 right">
					<div class="row">
						<div class="col-md-7 col-sm-8">
							<h3>Обновленный седан Toyota Corola</h3>
						</div>
						<div class="col-md-5 col-sm-8 rating">
							<p>4.5 / 5</p>
							<div class="stars">
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="line"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h2>Блиц-тест обновленного седана Toyota Corolla. Десять отличий.</h2>
						</div>
						<div class="col-md-12">
							<p>Загибайте пальцы. Интерьер - раз, бамперы - два, оптика - три... Плавное обновление седана Toyota Corolla прошло...</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<span>+ 2 фото</span>
							<div class="autor">
								<p>Armir_12</p>
							</div>
						</div>
					</div>
				</div>
			</div><!-- .block -->
			<div class="row block">
				<div class="col-md-4 col-sm-4 left">
					<img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img6.jpg" alt="">
					<div class="comments_box">
						<div class="comments">
							<p>56</p>
						</div>
					</div>
				</div>
				<div class="col-md-8 right">
					<div class="row">
						<div class="col-md-7 col-sm-8">
							<h3>Обновленный седан Toyota Corola</h3>
						</div>
						<div class="col-md-5 col-sm-8 rating">
							<p>4.5 / 5</p>
							<div class="stars">
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="line"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h2>Блиц-тест обновленного седана Toyota Corolla. Десять отличий.</h2>
						</div>
						<div class="col-md-12">
							<p>Загибайте пальцы. Интерьер - раз, бамперы - два, оптика - три... Плавное обновление седана Toyota Corolla прошло...</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<a href="#"><img src="<?php echo _TEMPLATE_URL; ?>img/top_news_img1_prev.jpg" alt=""></a>
							<span>+ 2 фото</span>
							<div class="autor">
								<p>Armir_12</p>
							</div>
						</div>
					</div>
				</div>
			</div><!-- .block -->
		</div><!-- .see_also -->
	</div>

	</div>
	<script>
	function handleFileSelect(evt)
	{
		var files = evt.target.files; // FileList object
		// Loop through the FileList and render image files as thumbnails.
		document.getElementById('load_img_review').innerHTML = '';
		for (var i = 0, f; f = files[i]; i++)
		{
			// Only process image files.
			if (!f.type.match('image.*'))
			{
				alert("Image only please....");
			}
			var reader = new FileReader();
			// Closure to capture the file information.
			reader.onload = (function (theFile)
			{
				return function (e)
				{
					console.log(e);
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = ['<span onmouseover="showDelButton(this);" onmouseleave="hideDelButton(this);" class="img-container" ><img style="margin-right:20px; data-name="'+theFile.name+'" border-radius:5px;" class="load-img" height="100" width="150" class="thumb" title="', escape(theFile.name), '" src="', e.target.result, '" /><span>Удалить</span></span>'].join('');
					document.getElementById('load_img_review').insertBefore(span, null);
				};
			})(f);
			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
		}
	}

	//document.getElementById('reviewFotos').addEventListener('change', handleFileSelect, false);
	</script>
