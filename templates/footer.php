</div>
</div><!-- .container -->
</main><!-- main -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 left">
                <h3>О нас</h3>
                <p>I have hinted that I would often jerk poor Queequeg from between the whale and the ship - where he would occasionally fall, from the incessant rolling and swaying of both. But this was.</p>
                <form action="/search/" method="get" id="search-form-topic-review-footer" class="footer_search">
                    <input type="search" name="s" id="search-review-footer" class="input" placeholder="Что вы ищите?" />
                    <input type="hidden" name="d" value="1" />
                    <input type="submit" name="" value="" class="submit" />
                </form>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-7 right">
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <h3>Помощь</h3><?php
                    $footer_menu = Content::getMenuByCode('footer');
                    if (!empty($footer_menu)) {
                        ?>
                        <ul><?php
                            foreach ($footer_menu as $menu_item) {
                                ?>
                                <li class="z-index-for-mob"><a href="<?php echo $menu_item['link']; ?>"><?php echo $menu_item['title']; ?></a></li><?php }
                    ?>
                        </ul><?php }
                        ?>
                </div>
                <div class="col-md-8 col-sm-7">
                    <h3>Каталог</h3>
                    <?php $this->includeComponent('otzovik/menu.main', array('PLACE' => 'footer')); ?>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-md-11 col-sm-11 col-xs-10 p0">
                <p>Copyright 2016. All rights reserved.</p>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2 p0">
                <a href="javascript:void(0);"><img src="<?php echo $this->page->getTemplateUrl(); ?>img/footer_counter.jpg" alt=""></a>
            </div>
        </div>
    </div>
</footer><!-- footer -->
<!--[if lt IE 9]>
    <script src="<?php echo $this->page->getTemplateUrl(); ?>libs/html5shiv/es5-shim.min.js"></script>
    <script src="<?php echo $this->page->getTemplateUrl(); ?>libs/html5shiv/html5shiv.min.js"></script>
    <script src="<?php echo $this->page->getTemplateUrl(); ?>libs/html5shiv/html5shiv-printshiv.min.js"></script>
    <script src="<?php echo $this->page->getTemplateUrl(); ?>libs/respond/respond.min.js"></script>
    <![endif]-->
<?php $this->includeComponent('system/js'); ?>
<?php $this->page->getFooter(); ?>

<script src="<?php echo $this->page->getTemplateUrl(); ?>js/jquery-migrate-1.4.1.min.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/jquery-mousewheel/jquery.mousewheel.min.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/waypoints/waypoints-1.6.2.min.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/scrollto/jquery.scrollTo.min.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/owl-carousel/owl.carousel.min.js"></script>
<!--<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/bootstrap/js/bootstrap.js"></script>--> 
<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/countdown/jquery.plugin.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/countdown/jquery.countdown.min.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/countdown/jquery.countdown-ru.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>libs/landing-nav/navigation.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDt5swuSODxoVxtPxdMIBkzWF6bE_htcEA&callback=initMap" async defer></script> -->
<script src="<?php echo $this->page->getTemplateUrl(); ?>js/dist/trumbowyg.min.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>js/jquery-ui.min.js"></script>
<script src="<?php echo $this->page->getTemplateUrl(); ?>js/dropzone.js"></script>

<script src="<?php echo $this->page->getTemplateUrl(); ?>js/common.js?"></script>



<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
<!-- Google Analytics counter --><!-- /Google Analytics counter -->
  
                    
</body>
</html>
