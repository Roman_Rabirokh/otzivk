<?php if(!defined("_APP_START")) { exit(); }
include('sidebar.php'); ?>
<div class="col-md-9 col-sm-8">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li class="active"><?php echo $data['name']; ?></li>
	</ol>
    <div class="row earn">
						<div class="col-md-12">
							<h2>Заработать</h2>
						</div>
						<div class="col-md-1 col-sm-1 col-xs-12 earn_logo">
							<img src="<?php echo $this->page->getTemplateUrl(); ?>img/earn_logo.jpg" alt="">
						</div>
						<div class="col-md-11 col-sm-11 col-xs-12">
							<p>На сервисе постоянно проводятся конкурсы на лучший отзыв с щедрыми денежными призами. Если Вы умеете хорошо писать, то можете поучавствовать в любом понравившемся конкурсе на знакомую Вам тематику.</p>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="table-responsive">
								<?php echo getBannerBlockById(6); ?>
							</div>
						</div>
						<div class="col-md-12">
							<ul>
								<li><a href="#" class="active">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">6</a></li>
								<li><a href="#">7</a></li>
								<li><a href="#">8</a></li>
								<li><a href="#">9</a></li>
								...
								<li><a href="#">21</a></li>
							</ul>
						</div>
					</div>
	<?php echo getBannerBlockById(1); ?>
</div>
