<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdvertisingBlocks
 *
 * @author Roman Rabirokh
 */
//class AdvertisingBlocks
//{
//    //put your code here
//    
//    public function getBlockById($id)
//    {
//        $code= dbGetOne('SELECT code FROM #__advertising_blocks WHERE id = :id', [':id' => $id]);
//        if(!empty($code))
//            return $code;
//        else
//            FALSE;
//    }
//}
//$banner= new AdvertisingBlocks();
//$banner=$banner->getBlockById(1);
//$banner[1]=$banner;
function getBannerBlockById($id)
{
    $code= dbGetOne('SELECT code FROM #__advertising_blocks WHERE id = :id', [':id' => $id]);
    return htmlspecialchars_decode($code);
}