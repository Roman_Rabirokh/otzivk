<?php
  include_once $_SERVER['DOCUMENT_ROOT'].'/api/config.php';
	define("GET_DATA","https://graph.facebook.com/me");
	if(isset($_GET['show']) && $_GET['show'] == 1)
	{
		define('_SHOW_PAGE', $_GET['show']);
	}
	else{
		define('_SHOW_PAGE', 0);
	}
  session_start();
  $app_id = "1747079585610673";
  $app_secret = "27023fcd202336705f4275f594afee98";
  $my_url = "http://"._SITE."/modules/social/facebook.php?show="._SHOW_PAGE;

  $url = 'https://www.facebook.com/dialog/oauth';
  $params = array(
    'client_id'     => $app_id,
    'redirect_uri'  => $my_url,
    'response_type' => 'code',
    'scope'         => 'email'
);

  if(!empty($_REQUEST["code"]))
  {
    $code = $_REQUEST["code"];
  }

  if(empty($code)) {
    $_SESSION['state'] = md5(uniqid(rand(), TRUE)); //CSRF protection
    $dialog_url = $url . "?" . urldecode(http_build_query($params))."&state="
      . $_SESSION['state'].'&show=1';

    echo("<script> top.location.href='" . $dialog_url . "'</script>");
  }

  if($_REQUEST['state'] == $_SESSION['state'])
	{
    $token_url = "https://graph.facebook.com/oauth/access_token?"
      . "client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url)
      . "&client_secret=" . $app_secret . "&code=" . $code;



    $response = file_get_contents($token_url);
//    $params = null;
//    parse_str($response, $params);
    $params = json_decode($response, true);

    $graph_url = "https://graph.facebook.com/me?access_token="
      . $params['access_token'].'&fields=name,email,link';

    $user = json_decode(file_get_contents($graph_url),true);
		$user =(array)$user;

		if(isset($user['name']))
		{
			$first_name = '';
			$last_name = '';
			$part = explode(' ',$user['name']);
			if(isset($part[0]))
			{
				$first_name = $part[0];
			}
			if(isset($part[1]))
			{
				$last_name = $part[1];
			}
		}

		if(isset($_GET['show']) && $_GET['show'] == 1)
		{
			$show_social_page = 1;
		}
		else
		{
			$show_social_page = 0;
		}

    $info = array('id' => $user['id'],
                  'socialservice' => 'facebook',
                  'last_name'  => $last_name,
                  'first_name' => $first_name,
									'foto'       => 'https://graph.facebook.com/'.$user['id'].'/picture?width=400',
									'link'       => $user['link'],
									'show'       => $show_social_page
									);
		
    if(!empty($user['email']))
    {
        $info['email'] = $user['email'];
    }
  }
  else
	{
    echo("The state does not match. You may be a victim of CSRF.");
  }

	require_once("auth.php");
