<?php
        
	include_once $_SERVER['DOCUMENT_ROOT'].'/api/config.php';
        global $url_callback;
          
	if(isset($_GET['show']) && $_GET['show'] == 1)
	{
		define('_SHOW_PAGE', $_GET['show']);
                define('URL_REDIRECT', 'http://'._SITE.'/modules/social/vk.php?source=vk&show=1');                
	}
	else{
		define('_SHOW_PAGE', 0);
                define('URL_REDIRECT', 'http://'._SITE.'/modules/social/vk.php?source=vk&show=0');   
	}

	class Utils
	{
		public static function redirect($uri = '')
		{
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: ".$uri, TRUE, 302);
			exit;
		}
	}

	class OAuthVK
	{
		
		const APP_ID = 5968803;
		const APP_SECRET = 'a2jUZxLyUbvxjfMiaJr3';
		const URL_CALLBACK = URL_REDIRECT;
		const URL_ACCESS_TOKEN = 'https://oauth.vk.com/access_token';
		const URL_AUTHORIZE = 'https://oauth.vk.com/authorize';
		const URL_GET_PROFILES = 'https://api.vk.com/method/getProfiles';
		const URL_GET_USER = 'https://api.vk.com/method/users.get';

		private static $token;
		public static $userId;
		public static $userData;
                public static $userEmail;
	
		private static function printError($error) {
		echo '#' . $error->error_code . ' - ' . $error->error_msg;
		}


		public static function goToAuth()
		{
                    
                       
			
			Utils::redirect(self::URL_AUTHORIZE .
			'?client_id=' . self::APP_ID .
			'&scope=offline,email' .
			'&redirect_uri=' . urlencode(self::URL_CALLBACK) .
			'&response_type=code');
		}
                
                public static function getCityCountry($city,$country)
                {
                    $URL_COUNTRY='https://api.vk.com/method/places.getCountryById';
                    $URL_CITY='https://api.vk.com/method/places.getCityById';
                    $url = $URL_COUNTRY.'?uid=' . self::$userId .'&cids='.$country.'&access_token=' . self::$token;
                    if (!($res = @file_get_contents($url)))
			{
				return false;
			}
                        $country = json_decode($res);
                        $data['country']=$country->response[0]->name;
                        
                        $url = $URL_CITY.'?uid=' . self::$userId .'&cids='.$city.'&access_token=' . self::$token;
                        if (!($res = @file_get_contents($url)))
			{
				return false;
			}
                        $city = json_decode($res);
                        $data['city']=$city->response[0]->name;
                        
                        return $data;
                    
                }
		public static function getToken($code)
		{
                    
			$url = self::URL_ACCESS_TOKEN .
			'?client_id=' . self::APP_ID .
			'&client_secret=' . self::APP_SECRET .
			'&code=' . $_GET['code'] .
			'&redirect_uri=' . urlencode(self::URL_CALLBACK);
                        

			if (!($res = @file_get_contents($url))) 
			{
				return false;
			}

			$res = json_decode($res);
//                        print_r($res); die;
			if (empty($res->access_token) || empty($res->user_id))
			{
				return false;
			}

			self::$token = $res->access_token;
			self::$userId = $res->user_id;
                        if(!empty($res->email))
                        {
                            self::$userEmail =  $res->email;
                        }

			return true;
		}


		public static function getUser()
		{
			if (!self::$userId)
			{
                            return false;
			}

			//fields=first_name,last_name,nickname,screen_name,sex,bdate,city, country,timezone,photo,photo_medium,photo_big,has_mobile,rate,contacts, education,online,counters&access_token=".self::$token;

			$url = self::URL_GET_PROFILES.
																		'?uid=' . self::$userId .
																		'&fields=photo_big,sex,country,city'.
																		'&access_token=' . self::$token;

			if (!($res = @file_get_contents($url)))
			{
				return false;
			}

			$user = json_decode($res);
  
                       
                        
			if (!empty($user->error))
			{
				self::printError($user->error);
				return false;
			}

			if (empty($user->response[0]))
			{
				return false;
			}

			$user = $user->response[0];
                        if(!empty($user->country) || !empty($user->city) )
                            $user->location =   self::getCityCountry($user->city,$user->country);
                         
			if (empty($user->uid) || empty($user->first_name) || empty($user->last_name))
			{
				return false;
			}

			return self::$userData = $user;
		}
	}


	if(!empty($_GET['error']))
	{
		die($_GET['error']);
	}
	elseif (empty($_GET['code']))
	{
		// Самый первый запрос
		OAuthVK::goToAuth();
	}
	else
	{
		if (!OAuthVK::getToken($_GET['code']))
		{
			die('Error - no token by code');
		}

		$user = OAuthVK::getUser();
//                echo "<pre>";print_r($user); die;
                $user =(array)$user;
		if(isset($_GET['show']) && $_GET['show'] == 1)
		{
			$show_social_page = 1;
		}
		else
		{
			$show_social_page = 0;
		}

		$info = array(
							'id' => $user['uid'],
							'socialservice' => 'vk',
							'last_name' => $user['last_name'],
							'first_name' => $user['first_name'],
							'foto'       => $user['photo_big'],
							'link'       => 'https://vk.com/id'.$user['uid'],
							'show'       => intval($_GET['show'])
                                                        
							);
                if(!empty($user['location']))
                {
                    $info['location'] = $user['location'];
                }
                else
                {
                    $info['location'] = '';
                }
                if(!empty(OAuthVK::$userEmail))
                {
                   $info['email'] =  OAuthVK::$userEmail;
                }
                else
                {
                    $info['email'] = '';
                }
	}
	
	require_once("auth.php");
	