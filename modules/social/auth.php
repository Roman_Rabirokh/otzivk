	<?php
	session_start();
	$url = $_SERVER['DOCUMENT_ROOT'];

	include_once($url."/api/config.php");
	include_once($url."/api/db.php");

	include_once($url."/api/functions.php");
	include_once($url."/api/image.php");
	include_once($url."/api/user.php");
	include_once($url."/api/application.php");
	include_once($url."/api/page.php");

	// print_r($info); die;

	if(isset($info) && isset($info['id']) && !empty($info['id']))
	{
		$first_name = '';
		$last_name = '';
		if(!empty($info['first_name']))
		{
			$first_name = $info['first_name'];
		}
		if(!empty($info['last_name']))
		{
			$last_name = $info['last_name'];
		}

		if(!empty($info['foto']))
		{
			$foto = $info['foto'];
		}
		else
		{
			$foto = '';
		}
		if(!empty($info['link']))
		{
			$link = $info['link'];
		}
		else
		{
			$link = '';
		}

		if(!empty($info['show']))
		{
			$show_link = $info['show'];
		}
		else
		{
			$show_link = 0;
		}
                
                                
                $country= !empty($info['location']['country']) ? $info['location']['country'] : '';
                $city= !empty($info['location']['city']) ? $info['location']['city'] : '';
                $email= !empty($info['email']) ? $info['email'] : '';

		global $user;
		$user = new User;
		// echo "<pre>"; print_r($info); die;
		// echo "SELECT id FROM rs_users WHERE socialservice = {$info['socialservice']} AND social_code = {$info['id']}"; die;
		$id = dbGetOne("SELECT id FROM #__users WHERE socialservice = :socialservice AND social_code = :code",
                                array(
                                    ":socialservice"=> $info['socialservice'], 
                                    ":code" => $info['id']
                                )
                               );
                
		if(!empty($id))
		{
			$user->AuthorizeByID($id);
                        if($user->Authorized())
                        {
                           header('location:http://'._SITE.'/personal/');
                        }
                        else
                        {
                            if(!empty($_SESSION['BLOCKED_USER']))
                            {
                                header('location:http://'._SITE.'/blocked/account/');
                            }
                            else
                            {
                                 header('location:http://'._SITE.'/registration/');
                            }
                           
                        }
			
		}
		else
		{
                    if(!empty($email))
                    {
                        $isset_email = dbGetOne('SELECT id FROM #__users WHERE email = :email', array(':email' => $email));
                        if(!empty($isset_email))
                        {
                            $email = '';
                        }
                    }
			$id = dbNonQuery("INSERT INTO #__users(created,email,socialservice,social_code,name, country,city) VALUES(NOW(),:email,:socialservice,:code,:name,:country,:city)",
                                            array(
						":code" => $info['id'],
                                                ":email"=> $email,
                                                ":socialservice" => $info['socialservice'],
                                                ':name' => $first_name,
                                                ':country'=> $country,
                                                ':city' => $city
						),true);
                        if(!empty($foto))
                        {
                            list($w_i, $h_i, $type) = getimagesize($foto);
                            $types = array("", "gif", "jpeg", "png");
                            $ext = $types[$type];
                            if ($ext)
                            {
                                $func = 'imagecreatefrom'.$ext;
                                $img_i = $func($foto);
                            }
                            else
                            {
                                echo 'Некорректное изображение'; // Выводим ошибку, если формат изображения недопустимый
                                return false;
                            }

                            if($w_i > $h_i)
                            {
                                $x_o = ($w_i - $h_i)/2;
                                $y_o = 0;
                                $w_o = $h_i;
                                $h_o = $h_i;
                            }
                            else
                            {
                                $x_o = 0;
                                $y_o = ($h_i - $w_i)/2;
                                $w_o = $w_i;
                                $h_o = $w_i;
                            }
                            $img_o = imagecreatetruecolor($w_o, $h_o);
                            imagecopy($img_o, $img_i, 0, 0, $x_o, $y_o, $w_o, $h_o);
                            if($type == 1)
                            {
                                $end = '.gif';
                            }
                            if($type == 2)
                            {
                                $end = '.jpg';
                            }
                            if($type == 3)
                            {
                                $end = '.png';
                            }
                            $name = uniqid().'-'.$id;
                            $src = '/files/images/'.$name.$end;
                            $func = 'image'.$ext;
                            $func($img_o, $_SERVER['DOCUMENT_ROOT'].$src);

                        }
                        
                        
        
			dbNonQuery("INSERT INTO #__profiles (created, userid, active, name, profile_foto, social_page_link, show_social_page, country, city) VALUES(NOW(), :id, 1, :name, :foto, :link, :show, :country, :city)",
                            array(
                                ':id' => $id,
                                ':name' => $first_name,
				':foto' => $src,
				':link'  => $link,
				':show'  => $show_link,
                                ':country'=> $country,
                                ':city' => $city
                        ));
            
            $data['SUBSCRIBES_TYPE'] = dbQuery('SELECT * FROM #__subscribe_types');
        
            foreach($data['SUBSCRIBES_TYPE'] as $subscribe)
            {
                dbNonQuery('INSERT INTO #__subscribes_profile (userid, subcribe_id)
                            VALUES(:uid, :sid)',
                                array(
                                    ':uid' => $id,
                                    ':sid' => $subscribe['id']));
            }
			$user->AuthorizeByID($id);
                        header('location:http://'._SITE.'/personal/');
                   

		}
 }

