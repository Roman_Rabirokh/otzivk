<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/api/config.php';
	if(isset($_GET['show']) && $_GET['show'] == 1)
	{
		define('_SHOW_PAGE', $_GET['show']);
                define('URL_REDIRECT', 'http://'._SITE.'/modules/social/odnoklassniki.php?source=vk&show=1');
	}
	else{
		define('_SHOW_PAGE', 0);
                define('URL_REDIRECT', 'http://'._SITE.'/modules/social/odnoklassniki.php?source=vk&show=0');  
	}
class Utils {
    public static function redirect($uri = '') {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".$uri, TRUE, 302);
        exit;
    }
}

/**
 * @url http://ok.ru/dk?st.cmd=appEdit&st._aid=Apps_Info_MyDev_AddApp добавить приложение
 */

class OAuthOK {

    const APP_ID = 1250605824; //ID приложения
    const APP_PUBLIC = 'CBAMLLILEBABABABA'; //Публичный ключ
    const APP_SECRET = 'DDF7588CB849CC05AD6CBBAB'; //Защищенный ключ
    const URL_CALLBACK = URL_REDIRECT; //URL, на который произойдет перенаправление после авторизации
    const URL_AUTHORIZE = 'https://connect.ok.ru/oauth/authorize';
    const URL_GET_TOKEN = 'https://api.ok.ru/oauth/token.do';
    const URL_ACCESS_TOKEN = 'http://api.odnoklassniki.ru/fb.do';

    private static $token;
    public static $userId;
    public static $userData;

    /**
     * @url http://apiok.ru/wiki/pages/viewpage.action?pageId=81822109
     */
    public static function goToAuth()
    {
        Utils::redirect(self::URL_AUTHORIZE .
            '?client_id=' . self::APP_ID .
            '&scope=GET_EMAIL'.
            '&response_type=code' .
            '&redirect_uri=' . urlencode(self::URL_CALLBACK));
    }

    public static function getToken($code) {

        $data = array(
            'code' => trim($code),
            'redirect_uri' => self::URL_CALLBACK,
            'client_id' => self::APP_ID,
            'client_secret' => self::APP_SECRET,
            'grant_type' => 'authorization_code'
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  =>"Content-type: application/x-www-form-urlencoded\r\n".
                    "Accept: */*\r\n",
                'content' => http_build_query($data)
                
            )
        );

        if (!($response = @file_get_contents(self::URL_GET_TOKEN, false, stream_context_create($opts)))) {
            return false;
        }

        $result = json_decode($response);
        if (empty($result->access_token)) {
            return false;
        }

        self::$token = $result->access_token;

        return true;
    }

    /**
     * Если данных недостаточно, то посмотрите что можно ещё запросить по этой ссылке
     * @url http://apiok.ru/wiki/display/api/users.getCurrentUser+ru
     */
    public static function getUser() {

        if (!self::$token) {
            return false;
        }

        $url = self::URL_ACCESS_TOKEN .
            '?access_token=' . self::$token .
            '&method=users.getCurrentUser' .
            '&fields=location,email,name,first_name,last_name,pic190x190'.
            '&application_key=' . self::APP_PUBLIC .
            '&sig=' . md5('application_key=' . self::APP_PUBLIC . 'fields=location,email,name,first_name,last_name,pic190x190method=users.getCurrentUser' . md5(self::$token . self::APP_SECRET));

        if (!($response = @file_get_contents($url))) {
            return false;
        }
     

        $user = json_decode($response);
//        echo "<pre>"; print_r($user); die;

        if (empty($user)) {
            return false;
        }

        self::$userId = $user->uid;
        return self::$userData = $user;
    }
}

// Пример использования класса:
if (!empty($_GET['error'])) {
    // Пришёл ответ с ошибкой. Например, юзер отменил авторизацию.
    die($_GET['error']);
} elseif (empty($_GET['code'])) {
    // Самый первый запрос
    OAuthOK::goToAuth();
} else {
    // Пришёл ответ без ошибок после запроса авторизации
    if (!OAuthOK::getToken($_GET['code'])) {
        die('Error - no token by code');
    }
    /*
     * На данном этапе можно проверить зарегистрирован ли у вас одноклассник с id = OAuthOK::$userId
     * Если да, то можно просто авторизовать его и не запрашивать его данные.
     */

    $user = OAuthOK::getUser();
    $user = (array)$user;
    $user['location']=(array)$user['location'];
		if(isset($_GET['show']) && $_GET['show'] == 1)
		{
			$show_social_page = 1;
		}
		else
		{
			$show_social_page = 0;
		}
		
    $info = array('id' => $user['uid'],
                  'socialservice' => 'ok',
                  'last_name'  => $user['last_name'],
                  'first_name' => $user['first_name'],
                  'foto'       => $user['pic190x190'],
                  'link'       => 'https://ok.ru/profile/'.$user['uid'],
                  'show'       => $show_social_page,
                  'location'   => ['country' => $user['location']['countryName'], 'city' => $user['location']['city']]
                );
//		echo "<pre>"; print_r($info); die;
}


require_once("auth.php");
