<?php

	$this->routes[] = array("pattern"=>'/personal\/([a-z]*)\/([a-z0-9]*)/',"action"=>"_showPersonalCabinet");
	$this->routes[] = array("pattern"=>'/redact\/profile/',"action"=>"_edit_pf");
	$this->routes[] = array("pattern"=>'/new/',"action"=>"_showNewReviewPage");
	$this->routes[] = array("pattern"=>'/find\/posts/',"action"=>"_showListPosts");
	$this->routes[] = array("pattern"=>'/review\/add/',"action"=>"_showFormReview");
	$this->routes[] = array("pattern"=>'/add\/topic\/review/',"action"=>"_addTopicReview");
	$this->routes[] = array("pattern"=>'/comment\/review\/leave/',"action"=>"_addCommentReview");
	$this->routes[] = array("pattern"=>'/measuring\/review/',"action"=>"_addMeasuringReview");
	$this->routes[] = array("pattern"=>'/search\/review\/topic/',"action"=>"_showSearchReviewsTopic");
	$this->routes[] = array("pattern"=>'/subscribe\/edit\/personal/',"action"=>"_editPersonalSubscribes");
        
        $this->routes[] = array("pattern"=>'/msg\/user\/block/',"action"=>"_sendMessageFromBlockedUser");
        
	$this->routes[] = array("pattern"=>'/user\/([0-9])*/',"action"=>"_showUserBlock");
	$this->routes[] = array("pattern"=>'/form\/message\/([0-9])*/',"action"=>"_showFormMessage");
	$this->routes[] = array("pattern"=>'/send\/message\/([0-9])*/',"action"=>"_sendMessage");
	$this->routes[] = array("pattern"=>'/subscribe\/on/',"action"=>"_subscribeOnUser");
	$this->routes[] = array("pattern"=>'/subscribe\/off/',"action"=>"_subscribeOffUser");
	$this->routes[] = array("pattern"=>'/get\/subcategories/',"action"=>"_getSubCategories");
	$this->routes[] = array("pattern"=>'/messages\/chat\/answer/',"action"=>"_sendMessageFromChat");
	$this->routes[] = array("pattern"=>'/preview\/([0-9]*)/',"action"=>"_showPreviewReview");
	$this->routes[] = array("pattern"=>'/draft\/image\/delete/',"action"=>"_delDraftImage");
	$this->routes[] = array("pattern"=>'/save\/draft\/review/',"action"=>"_saveDraftReview");
	$this->routes[] = array("pattern"=>'/bookmark\/add\/review/',"action"=>"_addBookmarkReview");
	$this->routes[] = array("pattern"=>'/bookmark\/del\/topic/',"action"=>"_delBookmarkReview");
	$this->routes[] = array("pattern"=>'/review\/publish/',"action"=>"_publishPreview");
	$this->routes[] = array("pattern"=>'/topic\/get\/subcats/',"action"=>"_getSubcategoriesForTopic");
	$this->routes[] = array("pattern"=>'/save\/email/',"action"=>"_setUserEmail");
	$this->routes[] = array("pattern"=>'/upload\/review\/images/',"action"=>"_uploadReviewImages");
	$this->routes[] = array("pattern"=>'/del\/upload/',"action"=>"_delUploadImage");
        $this->routes[] = array("pattern"=>'/admin\/send\/msg/',"action"=>"_sendMessageFromAdmin");
        
        
        $this->routes[] = array("pattern"=>' /blocked\/account/',"action"=>"_showBlockAccountPage");
        $this->routes[] = array("pattern"=>'/msg\/moderator/',"action"=>"_sendMessageModerator");
       

        
        $this->routes[] = array("pattern"=>'/clear-generate/',"action"=>"_clearGenerate");
        $this->routes[] = array("pattern"=>'/generate/',"action"=>"_generate");
        $this->routes[] = array("pattern"=>'/send-discontent/',"action"=>"_sendDiscontent");

        $this->routes[] = array("pattern"=>'/serch-similar-theme/',"action"=>"_serchSimilarTheme");
        $this->routes[] = array("pattern"=>'/send-request-money/',"action"=>"sendRequestMoney");
        
        
        $this->addEventHandler('afterUserCreated','add_all_subscribes');
        $this->routes[] = array("pattern"=>'/get\/previw\/avatar/',"action"=>"_getPreviewAvatar");
        
        
        $this->routes[] = array("pattern"=>'/message\/moderator\/read\/yes/',"action"=>"_messageToModeratorIsRead");
        $this->routes[] = array("pattern"=>'/message\/moderator\/read\/no/',"action"=>"_messageToModeratorNoRead");
        
        
        function _messageToModeratorIsRead()
        {
            global $app;
            
            if(!empty($_POST['report_id']))
            {
                $report_id = intval($_POST['report_id']);
            }
            if(!empty($report_id))
            {
               dbQuery('UPDATE #__message_to_moderator SET is_read = 1 WHERE id = :id', array(':id' => $report_id));
               echo 1;
            }
            $app->actionResult = TRUE;
            $app->loadTemplate = FALSE;
        }
        
         function _messageToModeratorNoRead()
        {
            global $app;
            
            if(!empty($_POST['report_id']))
            {
                $report_id = intval($_POST['report_id']);
            }
            if(!empty($report_id))
            {
               dbQuery('UPDATE #__message_to_moderator SET is_read = 0 WHERE id = :id', array(':id' => $report_id));
               echo 1;
            }
            $app->actionResult = TRUE;
            $app->loadTemplate = FALSE;
        }
        
        function _sendMessageModerator()
        {
            global $app;
            global $user;
            if($user->Authorized())
            {
                if(!empty($_POST['subject']))
                {
                   $subject = htmlspecialchars(strip_tags(trim($_POST['subject']), ENT_QUOTES));
                }
                else
                {
                    $subject = "";
                }

                 if(!empty($_POST['msg_text']))
                {
                   $msg_text = htmlspecialchars(strip_tags(trim($_POST['msg_text']), ENT_QUOTES)); ?>
                   <script>
                        window.parent.$('.error-text-msg').text('');
                    </script><?php
                }
                else
                { ?>
                    <script>
                        window.parent.$('.error-text-msg').text('Введите текст сообщения');
                    </script><?php
                    die;
                }
                $review_id = intval($_POST['review_id']);
                $user_id = $user->getID();

                $msg_id = dbNonQuery('INSERT INTO #__message_to_moderator (user_from, review_id,  subject, text_msg, created)
                                 VALUES(:uid, :rid, :subj, :msg, NOW())',
                                    array(
                                        ':uid' => $user_id,
                                        ':rid' => $review_id,
                                        ':subj'=> $subject,
                                        ':msg'  => $msg_text
                                    ), TRUE); 
                if(!empty($msg_id))
                { ?>
                    <script>
                    window.parent.$.fancybox.close();
                    //window.parent.$('#message-moderator-form').find('input[name="subject"]').val('');
                    window.parent.$('#message-moderator-form').find('textarea[name="msg_text"]').val('');
                    window.parent.$.fancybox.open('<div class="success-moderator-msg">Ваше сообщение отправлено</div>');
                </script><?php 
                } 
            }
            
            $app->actionResult = TRUE;
            $app->loadTemplate = FALSE;
        }
        
        function _sendMessageFromBlockedUser()
        {
            global $app;
            global $user;
            if(!empty($_POST['user_from']))
            {
                $user_from = intval($_POST['user_from']);
            }
            if(!empty($_POST['msg-text']))
            {
                $msg_text = htmlspecialchars(strip_tags(trim($_POST['msg-text']), ENT_QUOTES));
            }

            if(empty($_POST['subject']))
            { ?>
                <script>
                    window.parent.$('.error-subject').css('display', 'block');
                </script><?php
               die;
            }
            else
            { ?>
                <script>
                    window.parent.$('.error-subject').css('display', 'none');
                </script><?php
                $subject = htmlspecialchars(strip_tags(trim($_POST['subject']), ENT_QUOTES));
            }

            $msg_id = dbNonQuery('INSERT INTO #__messages_from_admin (user_from,created,subject,text_msg) VALUES(:uid, NOW(),:subject,:text)',array(':uid' => $user_from, ':subject'=> $subject, ':text' => $msg_text), TRUE);
            if(!empty($msg_id))
            {
                //dbNonQuery('UPDATE #__users SET qnt_warning =  qnt_warning + 1 WHERE id = :uid', array(':uid' => $user_to));
                //$app->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM, "" ,$to['email'], $subject, "_msg_from_admin.tpl", array("#SUBJECT#"=> $subject,"#MESSAGE#"=>$msg_text));

                echo "<script>var uid =".$user_from."</script>";
                ?><script>window.parent.location.href='/';</script><?php
            }
            
            if($app->checkToken('sm',$_POST['token']))
            {
                $user_from = $user->getID();

                if(!empty($user_to) && !empty($msg_text) && $user_from != $user_to)
                {
                    dbQuery('INSERT INTO #__users_messages (created, userid_from, userid_to,  msg_text)
                             VALUES(NOW(), :fr, :to, :msg )',
                                array(
                                    ':fr'   => $user_from,
                                    ':to'   => $user_to,
                                    ':msg'  => $msg_text
                                ));
                    echo "<script>var uid =".$user_to."</script>";
                    ?><script>window.parent.location.href='/user/'+ uid + '/';</script><?php
                }
            }

            $app->actionResult = TRUE;
            $app->loadTemplate = FALSE;
    
        }
        
        function _showBlockAccountPage($params)
	{
            global $app;
            global $user;

            $app->actionResult = TRUE;
            $app->page->meta_title = "Ваш аккаут заблокирован";
            include(_TEMPL . "blocked-account.php");
	}
        
        
        function _sendMessageFromAdmin()
        {
            global $app;
            $userid = intval($_POST['userid']);
            $to = dbGetRow('SELECT email,name FROM #__users WHERE id = :uid', array(':uid' => $userid));
            if(empty($_POST['text_msg']) || empty($_POST['subject']))
            { 
                if(empty($_POST['text_msg']))
                { ?>
                    <script>
                        window.parent.$('.msg_admin_error').css('display', 'block');
                    </script><?php
                }
                else
                { ?>
                    <script>
                        window.parent.$('.msg_admin_error').css('display', 'none');
                    </script><?php
                }
                if(empty($_POST['subject']))
                { ?>
                    <script>
                        window.parent.$('.subject_msg_admin_error').css('display', 'block');
                    </script><?php
                }
                else
                { ?>
                    <script>
                        window.parent.$('.subject_msg_admin_error').css('display', 'none');
                    </script><?php
                }
                die;   
            }
            else
            { ?>
                <script>
                    window.parent.$('.subject_msg_admin_error').css('display', 'none');
                    window.parent.$('.msg_admin_error').css('display', 'none');
                    window.parent.$('textarea[name="text_msg"]').val('');
                    window.parent.$('input[name="subject"]').val('');
                </script><?php
                $subject = htmlspecialchars(strip_tags(trim($_POST['subject']), ENT_QUOTES));
                $msg_text = htmlspecialchars(strip_tags(trim($_POST['text_msg']), ENT_QUOTES));
                $msg_id = dbNonQuery('INSERT INTO #__messages_from_admin (userid,created,subject,text_msg) VALUES(:uid, NOW(),:subject,:text)',array(':uid' => $userid, ':subject'=> $subject, ':text' => $msg_text), TRUE);
                
                if($msg_id)
                { 
                   
                    dbNonQuery('UPDATE #__users SET qnt_warning =  qnt_warning + 1 WHERE id = :uid', array(':uid' => $userid));
                    $app->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM,"",$to['email'],$subject, "_msg_from_admin.tpl",array("#SUBJECT#"=>$subject,"#MESSAGE#"=>$msg_text)); 
                    $last_msg = dbGetRow('SELECT * FROM #__messages_from_admin WHERE id = :mid', array(':mid' => $msg_id)); ?>
                    <script>
                       //window.parent.alert("Ваше сообщение отправлено!");
                       window.parent.location.reload();
                    </script><?php
                    die;
                }
            }
            
            $app->actionResult = TRUE;
            $app->loadTemplate = FALSE;
        }
        
        function _getPreviewAvatar()
        {
            global $app;
            
            if(!empty($_FILES['avatar']))
            {
                print_r($FILES['avatar']); die;
            }
            
            die('after');
            
            
            $app->actionResult = TRUE;
            $app->loadTemplate = FALSE;
        }
        
        function add_all_subscribes($userid)
        {
            $data['SUBSCRIBES_TYPE'] = dbQuery('SELECT * FROM #__subscribe_types');
        
            foreach($data['SUBSCRIBES_TYPE'] as $subscribe)
            {
                dbNonQuery('INSERT INTO #__subscribes_profile (userid, subcribe_id)
                            VALUES(:uid, :sid)',
                                array(
                                    ':uid' => $userid,
                                    ':sid' => $subscribe['id']));
                }
        }
        
        
        // отпавляет пиьсмо админу о запросе пользователя на вывод денег
        function sendRequestMoney()
        {
            global $user;
            global $app;
            $to= dbGetOne('SELECT email FROM #__direct_users WHERE login="admin"');
            $from= dbGetRow('SELECT p.*, u.money, u.email FROM #__profiles  as p '
                    . ' INNER JOIN #__users as u ON (p.userid=u.id) '
                    . ' WHERE u.id = :id ', [':id'=> $user->getID()]);
            
            if($from['money']>0){
                $app->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM,"",$to,"
				Уведомление о запросе вывода денег " . _SITE,"_money.tpl",array("#NAME#"=>$from['name'],"#BONUS#"=>$from['money'],"#EMAIL#"=>$from['email']));
                exit('Усьо ОК!');
            }
            else {
                exit('Запрос не отправлен, у вас нет бонусов');
            }
            
        }
        
        //отправляет  жалобу на комментарий
        function  _sendDiscontent()
        {
            $id=!empty($_POST['id']) ? $_POST['id'] : 0;
            global $user;
            $idUser=$user->getID();
            if($id)
            {
                $isset= dbGetOne('SELECT id FROM #__comments_reports  WHERE comment = :id AND user= :user ', [':id'=> $id, ':user'=> $idUser]);
                if(empty($isset))
                {
                    dbNonQuery('INSERT INTO #__comments_reports (comment, user) VALUES(:comment, :user) ',[':comment' =>$id,':user' => $idUser ] );
                }
                else
                    exit('Жалоба уже подана');
            }
            exit('Жалоба подана');
        }
        
        // получает строку и возвращает похожие имена 
        function _serchSimilarTheme()
        {
            $count=5;
            $name= !empty($_POST['name'])? $_POST['name'] : 0;
            $names=array();
            if($name)
            {
                $names= dbQuery('SELECT name, id FROM #__content WHERE name LIKE "%'. $name.'%" AND content_type=4 ');
            }
            exit(json_encode($names));
        }
        
        function _clearGenerate ()
        {
//            dbNonQuery('TRUNCATE #__content_data_3');
//            dbNonQuery('TRUNCATE #__content_data_4');
//            dbNonQuery('DELETE FROM #__content WHERE content_type=3');
//            dbNonQuery('DELETE FROM #__content WHERE content_type=4');
            $all= dbQuery('SELECT id FROM #__comments_review WHERE  review_id<41200');
            $count=0;
            foreach($all as $item){
//            dbNonQuery ('INSERT INTO #__measurings_review (review_id, userid, measuring, created ) VALUES(:review_id, 88, :rand, NOW())', [':review_id' => $item['id'],':rand' => rand(1,4) ]);
        dbNonQuery('DELETE FROM #__comments_review WHERE id= :id', [':id' => $item['id']]);
            $count++;
            }
            print_r('<pre>');
            print_r('Обновлено: '
                    . ' <br> Удалено:  '.$count);
            print_r('</pre>');
            exit();        
            
        }
        function _generate()
        {
            
            
            $categories= dbQuery('SELECT c1.urlname AS name1, c2.urlname AS name2, c3.* FROM #__content as c1  '
                    . ' INNER JOIN  #__content as c2 ON (c1.id=c2.parentid) '
                    . ' INNER JOIN  #__content as c3 ON (c2.id=c3.parentid) '
                    .  'WHERE c1.content_type=2 ');
            
            $data_theme= dbGetRow('SELECT c.*, f.f2, f.f7, f.f10, f.f11 FROM #__content as c  LEFT JOIN #__content_data_4 AS f  ON(c.id=f.id) WHERE c.id=41396');
            $data_otz=dbGetRow('SELECT c.*, f.f1, f.f3, f.f8 FROM #__content AS c  LEFT JOIN #__content_data_3 AS f  ON(c.id=f.id) WHERE c.id=41397');
            $count=0;
            $count_otz=0;
            foreach($categories as $category )
            {
                for($i=1; $i<11; $i++)
                    {
                        $name=$data_theme['urlname'] .' № '.$i;
                        $name_n=$data_theme['name'] .' № '.$i;
                        $id= dbNonQuery("INSERT INTO #__content (content_type, userid, name, mainimage, preview_text, detail_text, url, urlname,created, active) VALUES(4, :userid, :name, :mainimage, :preview_text, :detail_text, :url, :urlname, :created, 1) ", [
                            ':userid' => $data_theme['userid'], 
                            ':name' => $name_n,
                            ':mainimage' => $data_theme['mainimage'],
                            ':preview_text' => $data_theme['preview_text'],
                            ':detail_text' => $data_theme['detail_text'], 
                            ':url' => $category['name1'].'/'.$category['name2'].'/'.$category['urlname'].'/'.$name, 
                            ':urlname'=> $name,
                            ':created' =>$data_theme['created'] ], TRUE);
                        dbNonQuery('INSERT INTO #__content_data_4 (id, f2, f7, f10, f11) VALUES(:id, :f2, :f7, :f10, :f11)', [':id' => $id, ':f2' =>$category['id'], ':f7' => $data_theme['f7'], ':f10'=> $data_theme['f10'], ':f11' => $data_theme['f11']]);
                        $count++;

                        for ($j=1;$j<6;$j++)
                        {
                            $name_otz=$data_otz['urlname'] .' № '.$i.'.'.$j;
                            $name_otz_n=$data_otz['name'] .' № '.$i.'.'.$j;
                            $id_otz= dbNonQuery("INSERT INTO #__content (content_type, userid, name, mainimage, preview_text, detail_text, url, urlname, created, active) VALUES(3, :userid, :name, :mainimage, :preview_text, :detail_text, :url, :urlname, :created, 1) ", [
                                ':userid' => $data_otz['userid'], 
                                ':name' => $name_otz_n,
                                ':mainimage' => $data_otz['mainimage'],
                                ':preview_text' => $data_otz['preview_text'],
                                ':detail_text' => $data_otz['detail_text'], 
                                ':url' => $category['url'].'/'.$name_otz,
                                ':urlname'=> $name_otz,
                                ':created' =>$data_otz['created']], TRUE);
                        
                        dbNonQuery('UPDATE #__content SET url= :url WHERE id= :id  ', [':id'=>$id_otz, ':url' =>$category['name1'].'/'.$category['name2'].'/'.$category['urlname'].'/'.$name. '/'.$name_otz.'/'.$id_otz ]);
                        dbNonQuery('INSERT INTO #__content_data_3 ( id ,f1, f3, f8) VALUES(:id, :f1, :f3, :f8)', [':id' => $id_otz, ':f1' =>$id, ':f3' => $data_otz['f3'],':f8' => $data_otz['f8']]);
                        
                        for($k=1;$k<7;$k++)
                        {
                            $text='Комментарий '.$i.'.'.$j.'.'.$k;
                            dbNonQuery('INSERT INTO #__comments_review (review_id, userid, created, detail_text) VALUES (:review_id, :user, NOW(), :text )',
                                    [':review_id' =>$id_otz,
                                     ':user'=> $data_otz['userid'],
                                     ':text' => $text  ]);
                        }
                            
                        $count_otz++;
                        
                        }
                    }
            }
            print_r('<pre>');
            print_r('Сгенерировано: '
                    . ' <br> Тем:  '.$count.' '
                    . '<br> Отзывов :'.$count_otz);
            print_r('</pre>');
            exit();
        }

	function _delUploadImage()
	{
		global $app;
		$dir = _IMAGES_PATH.$_POST['token_folder'];

		if(file_exists($dir .'/'. $_POST['img_name']))
		{

			unlink($dir .'/'. $_POST['img_name']);
		}


		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _uploadReviewImages()
	{
		global $app;
		$dir = _IMAGES_PATH.$_POST['token_folder'];

		if(!is_dir($dir))
		{
			mkdir($dir);
		}

		$review_uploads = array();
		if(!empty($_FILES['file']))
		{
			foreach($_FILES['file'] as $key => $value)
			{
				foreach($value as $k => $val)
				{
					$review_uploads[$k][$key] = $val;
				}
			}
		}

		foreach($review_uploads as $upload)
		{
			//$upload_name = uniqid().$upload['name'];
			$upload_name = $upload['name'];
			copy($upload['tmp_name'], $dir."/".basename($upload_name));
		}
		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}


	function _setUserEmail()
	{
		global $app;
		global $user;
		if($user->Authorized())
		{
			$email = htmlspecialchars(strip_tags(trim($_POST['email']), ENT_QUOTES));
			$isset_email = dbGetOne('SELECT id FROM #__users WHERE email = :email', array(':email' => $email));
			if(empty($isset_email))
			{
				// echo $email; die;
				dbQuery('UPDATE #__users
								 SET email = :email
								 WHERE id = :uid',
									array(
										':uid' => $user->getID(),
										':email'   => $email,
										));
				?><script>window.parent.location.href= '/personal/'; </script><?php
			}
			else
			{ ?>
				<script> window.parent.$("#registerMessages").html('Такой е-mail уже существует. Введите другой уникальный e-mail.'); window.parent.$("#registerMessages").addClass('alert alert-danger');</script><?php
				die();
			}
		}

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _getSubcategoriesForTopic()
	{
		global $app;
		$subcats = dbQuery('SELECT id, name FROM #__content WHERE content_type = 2 AND parentid = :pid',
								array(
									':pid' => intval($_POST['cat_id'])
							));
		if(!empty($subcats)) echo json_encode($subcats);
		else echo false;
		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _publishPreview()
	{
		global $app;
		global $user;
		if($user->Authorized())
		{
			$userid = $user->getID();
			if(!empty($_POST['rid']))
			{
				$review_id = intval($_POST['rid']);
			}

			if(!empty($review_id))
			{
				dbNonQuery('UPDATE #__content SET active = 1
										WHERE id = :rid AND userid = :uid',
											array(
												':rid' => $review_id,
												':uid' => $userid
											));
				$topic_id = dbGetOne('SELECT f1 FROM #__content_data_3 WHERE id = :rid', array(':rid' => $review_id));
				$topic_url = dbGetOne('SELECT url FROM #__content WHERE id = :tid AND content_type = 4', array(':tid' => $topic_id));
				echo $topic_url;
			}
		}

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _delBookmarkReview()
	{
		global $app;
		global $user;

		if($user->Authorized())
		{
			$userid = intval($user->getID());
			if(!empty($_POST['tid']))
			{
				$topic_id = intval($_POST['tid']);

				dbQuery('DELETE FROM #__users_bookmarks WHERE userid = :uid AND topic_id = :tid',
									array(
										':uid' => $userid,
										':tid' => $topic_id
									));
			}
		}

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _addBookmarkReview()
	{
		global $app;
		global $user;
		if($user->Authorized())
		{
			$userid = intval($user->getID());
			if(!empty($_POST['tid']))
			{
				$tid = intval($_POST['tid']);
			}

			if(!empty($_POST['uid']))
			{
				$review_uid = intval($_POST['uid']);
			}

			if(!empty($tid) && !empty($review_uid) )
			{
				// if($userid == $review_uid)
				// {
				// 	echo "Вы можете добавить в закладки только отзывы других пользователей";
				// }
				// else
				// {
					$id =dbGetOne('SELECT id FROM #__users_bookmarks WHERE userid = :uid AND topic_id = :tid',
														array(
															':uid' => $userid,
															':tid' => $tid
														));

					if(empty($id))
					{
						$bookmark_id = dbNonQuery('INSERT INTO #__users_bookmarks (created, userid, topic_id)
																				VALUES (NOW(), :uid, :tid)',
																					array(
																						':uid' => $userid,
																						':tid' => $tid
																					), TRUE);
						if(!empty($bookmark_id))
						{
							echo $bookmark_id;
						}
						else
						{
							echo "Ошибка";
						}
					}
					else
					{
						echo "Эта тема уже находится у вас в закладках";
					}

				//}

			}
		}


		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _saveDraftReview()
	{
		global $app;
		global $user;
		//echo "<pre>"; print_r($_POST); die;
		if($user->Authorized() && $app->checkToken('de',$_POST['token']))
		{
			$userid = $user->getID();
			if(!empty($_POST['review_title']))
			{
				$review_title = htmlspecialchars(strip_tags(trim($_POST['review_title']), ENT_QUOTES));
			}
			else
			{
				$review_title = "";
			}

			if(!empty($_POST['review_text']))
			{
				$review_text = htmlspecialchars(strip_tags(trim($_POST['review_text']), ENT_QUOTES));
			}
			else
			{
				$review_text = "";
			}

			if(!empty($_POST['rating']))
			{
				$review_rating = intval($_POST['rating']);

			}
			else
			{
				$review_rating = 0;
			}
			//echo $review_rating; die;
			if(!empty($_POST['recommend']))
			{
				$review_recommend = intval($_POST['recommend']);
			}
			else
			{
				$review_recommend = 0;
			}

			if(!empty($_POST['review_video']))
			{
				$review_video = htmlspecialchars(strip_tags(trim($_POST['review_video']), ENT_QUOTES));
			}
			else
			{
				$review_video = '';
			}

			if(!empty($_POST['topic_id']))
			{

				$topic_id = intval($_POST['topic_id']);
			}
			else
			{
				$topic_id = 0;
			}

			if(!empty($_POST['review_id']))
			{

				$review_id = intval($_POST['review_id']);
			}
			else
			{
				$review_id = 0;
			}

			if(!empty($_POST['action']) && $_POST['action'] == 'save')
			{
				$active = 0;
			}
			else
			{
				$active = 1;
			}

			if(!empty($review_id) && !empty($topic_id))
			{
				dbQuery('UPDATE #__content
								 SET active      = :active,
								     name        = :review_title,
										 detail_text = :review_text
										 WHERE id = :id AND userid = :uid AND content_type = 3',
										 	 array(
												 ':active'       => $active,
												 ':review_title' => $review_title,
												 ':review_text'  => $review_text,
												 ':id'           => $review_id,
												 ':uid'          => $userid
											 ));

				dbQuery('UPDATE #__content_data_3
								 SET f3 = :rating,
								     f8 = :recommend,
										 f9 = :youtube
										 WHERE id = :id AND f1 = :topic_id',
											 	array(
												 ':rating'     => $review_rating,
												 ':recommend'  => $review_recommend,
												 ':youtube'    => $review_video,
												 ':id'         => $review_id,
												 ':topic_id'   => $topic_id
												));

				$topic_data = dbGetRow('SELECT AVG(cd.f3) AS rating, COUNT(c.id) AS qnt_reviews FROM #__content c
																				 LEFT JOIN #__content_data_3 cd ON cd.id = c.id
																				 WHERE c.content_type = 3 AND cd.f1 = :pid',
																					array(
																						':pid' => $topic_id
																					));
				dbQuery('UPDATE #__content_data_4
								 SET f7   = :rating,
										 f10  = :quantity,
										 f11  = NOW()
								 WHERE id = :topic_id',
									array(
										':topic_id' => $topic_id,
										':rating'   => $topic_data['rating'],
										':quantity' => $topic_data['qnt_reviews']
										));
			}

			$dir = _IMAGES_PATH.$_POST['token'];
			if(is_dir($dir))
			{
				$images = scandir($dir);
				foreach($images as $image)
				{
					if(strlen($image) > 2 )
					{
						$filename = $dir.'/'.$image;
						$value['title'] = "";
						$value['description'] = "";
						$value['showorder']  = 500;
						copyImageExt($review_id,$filename,$filename,$value, FALSE, 10);
						unlink($filename);
					}
				}
				rmdir($dir);
			}
			unset($_SESSION["token_de"]);
		}
		if(isset($_POST['action']) && $_POST['action'] == 'preview')
		{
			?><script>window.parent.location.href="/preview/<?php echo $review_id; ?>/";</script><?php
		}
		else
		{
			?><script>window.parent.location.href="/personal/drafts/";</script><?php
		}

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}


	function _delDraftImage()
	{
		global $app;
		global $user;

		if($user->Authorized())
		{
		//	echo "<pre>"; print_r($_POST);
			if(!empty($_POST['img_id']))
			{
				$img_id = intval($_POST['img_id']);
				$parent_id = intval($_POST['rid']);
				rmImageByIdAndParentId($img_id, $parent_id);
			}
		}

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;

	}

	function _showPreviewReview($params)
	{

		global $user;
		global $app;

		if($user->Authorized())
		{
			// echo "<pre>"; print_r($params); die;
			$userid = $user->getID();
			$review = dbGetRow('SELECT userid, name FROM #__content WHERE id = :review_id AND content_type = 3', array(':review_id' => intval($params[1])));
			// echo "<pre>"; print_r($review); die;
			if($review['userid'] == $userid)
			{
				$app->actionResult = TRUE;
				$app->page->meta_title = $review['name'] ." - предпросмотр";
				include(_TEMPL . "preview-page.php");
			}
			else
			{
				# code...
			}

		}
	}

	function _sendMessageFromChat()
	{
		global $user;
		global $app;
		if($user->Authorized() && $app->checkToken('mc',$_POST['token']))
		{
			$text_msg = htmlspecialchars(strip_tags(trim($_POST['text_msg']), ENT_QUOTES));
			$user_to = intval($_POST['user_to']);
			$user_from = $user->getID();
			$id = dbNonQuery('INSERT INTO #__users_messages (created, userid_from, userid_to, msg_subject, msg_text)
								VALUES(NOW(), :uid_from, :uid_to, "", :msg_text)',
								array(
									':uid_from' => $user_from,
									':uid_to' => $user_to,
									':msg_text' => $text_msg
								), TRUE);
			echo $id;
		}

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;

	}

	function _getSubCategories()
	{
		global $app;
		$data['SUBCATS'] = array();
		getCategoriesChilds($_POST['cat'], $data['SUBCATS'], 2);
		renderFullCatalog($data['SUBCATS'], 0);

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function getCategoriesChilds($parentid, &$arr, $content_type)
	{
		$arr = dbQuery('SELECT id, name, parentid, url FROM #__content WHERE content_type = :content_type AND parentid = :parentid', array(':parentid' => $parentid, ':content_type' => $content_type));
//                if($parentid != 0)
//                {
//                    $parent_url = dbGetOne("SELECT url FROM #__content WHERE id = '$parentid'");
//                    foreach($arr as &$par)
//                    {
//                       $par['new_url'] =  $parent_url.'/'.$par['url'];
//                       $urlname = $par['id'];
//                       $url = dbGetOne("SELECT urlname FROM #__content WHERE id = '$urlname'");
//                       dbQuery('UPDATE #__content
//					SET url  = :new_url
//					WHERE id = :cat_id AND content_type = 2',
//                                            array(
//						':cat_id' => $par['id'],
//						':new_url'=> $par['new_url']
//                                            ));
//                    }
//                }
		if(!empty($arr))
		{
                    foreach($arr as &$subcat)
                    {
                        getCategoriesChilds($subcat['id'], $subcat['subcats'], $content_type);
                    }
		}
	}

	function renderFullCatalog($array)
	{
		echo '<ul>';
		foreach($array as $it)
		{

			echo '<li>';
			echo '<a href="'.Content::contentUrl($it['url']).'" >'.$it['name'].'</a>';
			if(!empty($it['subcats'])) renderFullCatalog($it['subcats']);
			echo '</li>';
		}
		echo '</ul>';

	}

	function _showPersonalCabinet($params)
	{
		global $app;
		global $user;
                
		if($user->Authorized())
		{
			$app->actionResult = TRUE;
			$app->page->meta_title = "Персональный кабинет";
			include(_TEMPL . "personal.php");
		}
		else
		{
			redirect('/login/');
		}
	}

	function _edit_pf()
	{
		global $user;
		global $app;
		// echo "<pre>";print_r($_POST); die;
		if($user->Authorized() && $app->checkToken('pf',$_POST['token']))
		{
			$userid = $user->getID();
			if(!empty($_POST['user_name']))
			{
				$user_name = htmlspecialchars(strip_tags(trim($_POST['user_name']), ENT_QUOTES));
			}
			else
			{
				$user_name = '';
			}

			if(!empty($_POST['user_email']))
			{
				$user_email = htmlspecialchars(strip_tags(trim($_POST['user_email']), ENT_QUOTES));
			}
			else
			{
				$user_email = '';
			}
                        
                        if(!empty($_POST['city']))
			{
				$city = htmlspecialchars(strip_tags(trim($_POST['city']), ENT_QUOTES));
			}
			else
			{
				$city = '';
			}
                        
                        if(!empty($_POST['country']))
			{
				$country= htmlspecialchars(strip_tags(trim($_POST['country']), ENT_QUOTES));
			}
			else
			{
				$country = '';
			}
                        
			if(!empty($_POST['pf_id']))
			{
				$profile_id = intval($_POST['pf_id']);
			}

			if(!empty($user_email))
			{
				$isset_email = dbQuery('SELECT email FROM #__users WHERE email = :email', array(':email' => $user_email));
				if(!empty($isset_email))
				{ ?>
					 <script> window.parent.$("#registerMessages").html('Такой е-mail уже существует. Введите другой уникальный e-mail.'); window.parent.$("#registerMessages").addClass('alert alert-danger');</script><?php
					die();
				}
				else
				{
					dbQuery('UPDATE #__users
											SET email     = :email
											WHERE id = :userid',
												array(
													':email'    	=> $user_email,
													':userid'     => $userid,
													));
				}
			}

			if(empty($profile_id))
			{
                            $profile_id = dbNonQuery('INSERT INTO #__profiles (created, active, userid, name, country, city) VALUES (NOW(), 1, :uid, :name, :country, city)',
								array(
									':uid' => $userid,
									':name' => $user_name,
                                                                        ':country' => $country,
                                                                        'city' => $city,
								), TRUE);
			}
			else
			{
                            dbNonQuery('UPDATE #__profiles
                                        SET name     = :name,
                                            country  = :country,
                                            city     = :city
                                            WHERE userid = :userid AND id = :profileid',
                                                array(
                                                        ':name'    	  => $user_name,
                                                        ':userid'     => $userid,
                                                        ':profileid'  => $profile_id,
                                                        ':country'    => $country,
                                                        ':city'       => $city,
                                                        ));
                            dbNonQuery('UPDATE #__users
                                        SET name     = :name,
                                            country  = :country,
                                            city     = :city
                                            WHERE id = :userid',
                                                array(
                                                    ':name'    	  => $user_name,
                                                    ':userid'     => $userid,
                                                    ':country'    => $country,
                                                    ':city'       => $city,
                                                ));

			}
		} 
                
                
                if(!empty($_FILES['avatar_profile']['name']))
                {
                   /* $image = new Imagick($_FILES['avatar_profile']['tmp_name']);
                    $width  = $image->getImageWidth();
                    $height = $image->getImageHeight();

                    if($width > $height)
                    {
                        $x = ($width - $height)/2;
                        $y = 0;
                        $w = $height; 
                        $h = $height;
                    }
                    else
                    {
                        $x = 0;
                        $y = ($height - $width)/2;
                        $w = $width;
                        $h = $width;
                    }
	
                    $image->cropImage($w, $h, $x, $y);
                    if($w > 190 || $h > 190)
                    {
                        $image->resizeImage(300,300,Imagick::FILTER_LANCZOS, 1);
                    }
	
                    unlink($_FILES['avatar_profile']['tmp_name']);
                    $image->writeImage($_FILES['avatar_profile']['tmp_name']);*/
                           /*
                  $x_o и $y_o - координаты левого верхнего угла выходного изображения на исходном
                  $w_o и h_o - ширина и высота выходного изображения
                  */
                    crop($_FILES['avatar_profile']['tmp_name']);
                    $value['title'] = "";
                    $value['description'] = "";
                    $value['showorder']  = 500;
                    
                    $img_id = uploadImageExt($profile_id,$_FILES['avatar_profile']['name'], $_FILES['avatar_profile']['tmp_name'], $value, FALSE, 1);
                    
                    dbQuery('UPDATE #__profiles SET upload_foto = :fid WHERE id= :pid', array(':fid' => $img_id, ':pid'=> $profile_id ));
                    
                    
                }
?>

		<script>window.parent.location.href='/personal/'</script><?php

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _showNewReviewPage()
	{
		global $app;
		global $user;
		if($user->Authorized())
		{
			$app->actionResult = TRUE;
			$app->page->meta_title = "Добавить новый отзыв";
			include(_TEMPL . "new-review.php");
		}
		else
		{ 
                    redirect('/login/');
		}


	}

	function _showListPosts()
	{
		global $app;
		$name = htmlspecialchars(strip_tags(trim(mb_strtolower($_POST['q'],'UTF-8'))), ENT_QUOTES);
		$posts = dbQuery('SELECT id, name, url FROM #__content WHERE content_type = 4 AND LOWER(name) RLIKE :name', array(':name' => $name));
		echo json_encode($posts);

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _showFormReview()
	{

		global $app;
                global $user;
                if($user->Authorized())
                {
                    if(!empty($_POST['find_post_for_review']))
                    {
                            $name_post = htmlspecialchars(strip_tags(trim($_POST['find_post_for_review']), ENT_QUOTES));
                    }
                    $app->actionResult = TRUE;
                    if(!empty($_POST['post_id']))
                    {
                            if(empty($name_post))
                            {
                                    $name_post = dbGetOne('SELECT name FROM #__content WHERE content_type = 4 AND id = :id', array(':id' => intval($_POST['post_id'])));
                            }

                            $app->page->meta_title = $name_post;
                            include(_TEMPL . "topic-review.php");
                    }
                    else
                    {
                        $name=!empty($_POST['find_post_for_review']) ? trim($_POST['find_post_for_review'])  : 0;
                        if($name)
                        {
                            $post= dbGetRow('SELECT c.id, c.name FROM #__content AS c WHERE c.name= :name AND c.content_type=4', [':name' => $name]);
                            if(!empty($post))
                            {
                                $_POST['id']=$post['id'];
                                $app->page->meta_title = $post['name'];
                                include(_TEMPL . "topic-review.php");
                                
                            }
                        }
                            $app->page->meta_title = "Добавить новый товар и отзыв нему";
                            include(_TEMPL . "new-topic.php");
                    }
                }
                else {
                    redirect('/registration/');
                }

	}

	function _addTopicReview()
	{
		global $app;
		global $user;
		// echo "<pre>"; print_r($_POST); die;
		if($user->Authorized())
		{
			if(!empty($_POST['topic_title']))
			{
				$topic_title = htmlspecialchars(strip_tags(trim($_POST['topic_title']), ENT_QUOTES));
				if(isset($_POST['action']) && $_POST['action'] == 'save')
				{
					$isset_topic_title = dbGetOne('SELECT name FROM #__content WHERE name = :title AND content_type = 4', array(':title'=> $topic_title));
					if(!empty($isset_topic_title))
					{
						?> <script> window.parent.$("#registerMessages").html('Такая тема уже существует. Чтобы отредактировать отзыв, вы можете перейти в личный кабинет.'); window.parent.$("body").animate({scrollTop:0}, '500', 'swing'); window.parent.$("#registerMessages").addClass('alert alert-danger');</script><?php
						die();
					}
				}
				if(isset($_POST['action']) && $_POST['action'] == 'preview' && !empty($_POST['review_id']))
				{

					echo '<script> var preview_id ='.intval($_POST['review_id']).'</script>';
					?><script>window.parent.location.href='/preview/' + preview_id +'/'</script><?php
						die('preview after save');
					return false;
				}
				?><script>
						//window.parent.$("#registerMessages").html('');
						window.parent.$('input[name="topic_title"]').css('border', '1px solid #B9B9B9');
						window.parent.$(".error-topic-title").html('');
					</script><?php
			}
			else
			{
				if(empty($_POST['topic_id']))
				{
					?> <script>
						var pos = window.parent.$('input[name="topic_title"]').offset();
								window.parent.$('input[name="topic_title"]').css('border', '1px solid red');
								window.parent.$(".error-topic-title").html('Введите название темы.');
								// window.parent.$("#registerMessages").html('Введите название темы.');
								window.parent.$("#registerMessages").removeClass('alert alert-success');
								window.parent.$("body").animate({scrollTop:pos.top-50}, '500', 'swing');
								window.parent.$("#registerMessages").addClass('alert alert-danger');
							</script><?php
							die();
				}
				$topic_title = "";
			}

			if(!empty($_POST['topic_category']))
			{
                            $topic_category = intval($_POST['topic_category']);
                            $parent_url = dbGetOne('SELECT url FROM #__content WHERE id = :cid AND content_type = 2', array(':cid' => $topic_category));
                            $topic_url = $parent_url.'/'.create_urlname($topic_title);

				?><script>
                                    window.parent.$('.select-category').find('select:first').css('border', '1px solid #B9B9B9');
                                    window.parent.$('.select-category').find('select:first').next().css('border', '1px solid #B9B9B9');
                                    window.parent.$('.select-category').find('select:first').next().next().css('border', '1px solid #B9B9B9');
                                    window.parent.$(".error-topic-category").html('');
				</script><?php
			}
			else
			{
				if(empty($_POST['topic_id']))
				{
					?> <script>
								var pos = window.parent.$('.select-category').find('select:first').offset();
								window.parent.$(".error-topic-category").html('Выберите категорию для темы.');
								window.parent.$('.select-category').find('select:first').css('border', '1px solid #B9B9B9');
								window.parent.$('.select-category').find('select:first').next().css('border', '1px solid #B9B9B9');
								window.parent.$('select[name="topic_category"]').css('border', '1px solid red');
								// window.parent.$("#registerMessages").html('Выберите категорию для темы.');
								// window.parent.$("#registerMessages").removeClass('alert alert-success');
								window.parent.$("body").animate({scrollTop:pos.top-50}, '500', 'swing');
							//	window.parent.$("#registerMessages").addClass('alert alert-danger');
							</script><?php
							die();
					}
			}

			if(!empty($_POST['topic_text']) && empty($_POST['topic_id']))
			{
				$topic_text = htmlspecialchars(strip_tags(trim($_POST['topic_text']), ENT_QUOTES));
				?><script>
					window.parent.$('#text-topic').prev().css('border', 'none');
					//window.parent.$("#registerMessages").html('');
					window.parent.$(".error-topic-details").html('');
				</script><?php
			}
			else
			{
				if(empty($_POST['topic_id']))
				{
					?> <script>
								var pos = window.parent.$('[name="topic_text"]').offset();
								window.parent.$('#text-topic').prev().css('border', '1px solid red');
								//window.parent.$("#registerMessages").html('Введите краткое описание темы.');
								window.parent.$(".error-topic-details").html('Введите краткое описание темы.');
								//window.parent.$("#registerMessages").removeClass('alert alert-success');
								//window.parent.$("body").animate({scrollTop:0}, '500', 'swing');
								window.parent.$("body").animate({scrollTop:pos.top-220}, '500', 'swing');
								//window.parent.$("#registerMessages").addClass('alert alert-danger');
							</script><?php
							die();
					}
			}


			$user_id = $user->getID();
			if(!empty($_POST['review_title']))
			{
				$review_title = htmlspecialchars(strip_tags(trim($_POST['review_title']), ENT_QUOTES));
				?><script>
						//window.parent.$("#registerMessages").html('');
						window.parent.$('input[name="review_title"]').css('border', '1px solid #B9B9B9');
						window.parent.$(".error-review-title").html('');
					</script><?php
			}
			else
			{
				?> <script>
							var pos = window.parent.$('input[name="review_title"]').offset();
							window.parent.$(".error-review-title").html('Введите название отзыва.');
							//window.parent.$("#registerMessages").html('Введите название отзыва.');
							window.parent.$('input[name="review_title"]').css('border', '1px solid red');
							//window.parent.$("#registerMessages").removeClass('alert alert-success');
							//window.parent.$("body").animate({scrollTop:0}, '500', 'swing');
							window.parent.$("body").animate({scrollTop:pos.top - 50}, '500', 'swing');
							//window.parent.$("#registerMessages").addClass('alert alert-danger');
						</script><?php
					die();
			}

			if(!empty($_POST['review_text']))
			{
				$review_text = $_POST['review_text'];
				?><script>
						window.parent.$('#text-review').prev().css('border', 'none');
						window.parent.$(".error-review-details").html('');
						//window.parent.$("#registerMessages").html('');
					</script><?php
			}
			else
			{
				?> <script>
							var pos = window.parent.$('[name="review_text"]').offset();
							window.parent.$(".error-review-details").html('Введите текст отзыва.');
							//window.parent.$("#registerMessages").html('Введите текст отзыва.');
							window.parent.$('#text-review').prev().css('border', '1px solid red');
							//window.parent.$("#registerMessages").removeClass('alert alert-success');
							window.parent.$("body").animate({scrollTop:pos.top - 220}, '500', 'swing');
							//window.parent.$("body").animate({scrollTop:0}, '500', 'swing');
							//window.parent.$("#registerMessages").addClass('alert alert-danger');
						</script><?php
					die();
			}

			if(!empty($_POST['rating']))
			{
				$review_rating = intval($_POST['rating']);
			}
			else
			{
				?> <script>
							var pos = window.parent.$('.review-rate').offset();
							window.parent.$(".error-review-rating").html('Поставьте оценку теме.');
							//window.parent.$("#registerMessages").html('Поставьте оценку теме.');
							//window.parent.$("#registerMessages").removeClass('alert alert-success');
							window.parent.$("body").animate({scrollTop:pos.top - 50}, '500', 'swing');
							//window.parent.$("body").animate({scrollTop:0}, '500', 'swing');
							//window.parent.$("#registerMessages").addClass('alert alert-danger');
						</script><?php
					die();
				$review_rating = 0;
			}

			if(isset($_POST['recommend']))
			{
				$review_recommend = intval($_POST['recommend']);
				?>
					<script>
						window.parent.$(".error-review-recommend").html('');
						window.parent.$(".recomend-yes").css('border', '1px solid #B9B9B9');
						window.parent.$(".recommend-no").css('border', '1px solid #B9B9B9');
					</script><?php
			}
			else
			{
				?> <script>
							var pos = window.parent.$('.recommend').offset();
							window.parent.$(".error-review-recommend").html('Отметьте, рекомендуете ли вы другим эту тему или нет.');
							//window.parent.$("#registerMessages").html('Отметьте, рекомендуете ли вы другим эту тему или нет.');
							window.parent.$(".recomend-yes").css('border', '1px solid red');
							window.parent.$(".recommend-no").css('border', '1px solid red');
							//window.parent.$("#registerMessages").removeClass('alert alert-success');
							window.parent.$("body").animate({scrollTop:pos.top -50}, '500', 'swing');
							//window.parent.$("body").animate({scrollTop:0}, '500', 'swing');
							//window.parent.$("#registerMessages").addClass('alert alert-danger');
						</script><?php
						die();
			}

			if(!empty($_POST['review_video']))
			{
				$review_video = $_POST['review_video'];
                                $review_video= createYoutubeIframe($review_video);
			}
			else
			{
				$review_video = '';
			}

			if(!empty($_POST['topic_id']))
			{

				$topic_id = intval($_POST['topic_id']);
				$topic_url = dbGetOne('SELECT url FROM #__content WHERE content_type = 4 AND id =:id', array(':id' => $topic_id));
			}
			else
			{   
                                $isset_topic_title = dbGetOne('SELECT id FROM #__content WHERE name = :title AND content_type = 4', array(':title'=> $topic_title));                          
                                        print_r('<pre>');
                                        print_r($isset_topic_title);
                                        print_r('</pre>');
					if(!empty($isset_topic_title))
					{
						?> <script> window.parent.$("#registerMessages").html('Такая тема уже существует.'); window.parent.$("body").animate({scrollTop:0}, '500', 'swing'); window.parent.$("#registerMessages").addClass('alert alert-danger');</script><?php
						die();
					}
				$topic_id = dbNonQuery('INSERT INTO #__content (created, userid, active, name, detail_text, urlname, url, content_type, content_date)
																 VALUES(NOW(), :uid, 1, :name, :details, :urlname, :url, 4, NOW())',
																		array(
																			':uid'     => $user_id,
																			':name'    => $topic_title,
																			':url'     => $topic_url,
																			':urlname' => create_urlname($topic_title),
																			':details' => $topic_text
																		), TRUE);
                                dbQuery('UPDATE #__users SET qnt_topic = qnt_topic + 1 WHERE id = :uid', array(':uid' => $user_id));
                                // добавить баллы за добавление темы
                                $rating= new Ratings($user_id);
                                $rating->addRatingBalls(2, $topic_id);
				dbQuery('INSERT INTO #__content_data_4 (id, f2, f7)
								 VALUES (:id, :category, :rating)',
									array(
										':id'        => $topic_id,
										':category'  => $topic_category,
										':rating'    => $review_rating
									));
				if(!empty($_FILES['topic_foto']))
				{
					$value['title'] = "";
					$value['description'] = "";
					$value['showorder']  = 500;
                                        cropTopicFoto($_FILES['topic_foto']['tmp_name']);

					$img_id = uploadImageExt($topic_id,$_FILES['topic_foto']['name'], $_FILES['topic_foto']['tmp_name'], $value, TRUE, 10);

				}
			}

			$review = dbGetOne('SELECT c.id FROM #__content c
													LEFT JOIN #__content_data_3 cd ON cd.id = c.id
													WHERE c.content_type = 3 AND c.userid = :uid AND cd.f1 = :topic_id',
														array(
															':uid'      => $user_id,
															':topic_id' => $topic_id
														));
			if(empty($review))
			{
				if(!empty($_POST['action']) && ($_POST['action'] == 'preview' || $_POST['action'] == 'save'))
				{
					$active = 0;
				}
				else
				{
					$active = 1;
				}

				if(!empty($_POST['review_lat']))
				{
					$review_lat = htmlspecialchars(strip_tags(trim($_POST['review_lat']), ENT_QUOTES));
				}
				else
				{
					$review_lat = '';
				}

				if(!empty($_POST['review_lng']))
				{
					$review_lng = htmlspecialchars(strip_tags(trim($_POST['review_lng']), ENT_QUOTES));
				}
				else
				{
					$review_lng = '';
				}

				$review_id = dbNonQuery('INSERT INTO #__content (created, userid, active, name, detail_text, urlname, url, content_type, content_date)
							VALUES(NOW(), :uid, :active, :name, :details, :urlname, :url,  3, NOW())',
                                                            array(
                                                                    ':uid'     => $user_id,
                                                                    ':active'  => $active,
                                                                    ':name'    => $review_title,
                                                                    ':urlname' => create_urlname($review_title),
                                                                    ':url'     => $topic_url.'/'.create_urlname($review_title),
                                                                    ':details' => $review_text
                                                            ), TRUE);
                                
                                dbQuery('UPDATE #__users SET qnt_review = qnt_review + 1 WHERE id = :uid', array(':uid' => $user_id));
                                // добавляем баллы автору темы
                                $author= dbGetOne('SELECT userid FROM #__content WHERE id= :id', [':id' => $topic_id]); // TODO: Вынести в клас rating
                                if($author!=$user_id) // баллы добавляем только если авторы темы и отзыва разные люди
                                {
                                    $rating= new Ratings($author);
                                    $rating->addRatingBalls(4, $review_id);
                                }
                                dbNonQuery('UPDATE #__content SET url = :url WHERE id = :id AND content_type = 3',array(
					':id'=> $review_id,
					':url' => $topic_url.'/'.create_urlname($review_title).'/'.$review_id
				));

				dbQuery('INSERT INTO #__content_data_3 (id, f1, f3, f8, f9, f12, f13)
								 VALUES (:id, :topic_id, :rating, :recommend, :video, :lat, :lng)',
									array(
										':id'        => $review_id,
										':topic_id'  => $topic_id,
										':rating'    => $review_rating,
										':recommend' => $review_recommend,
										':video'     => $review_video,
										':lat'       => $review_lat,
										':lng'       => $review_lng
									));
				$dir = _IMAGES_PATH.$_POST['token'];
				if(is_dir($dir))
				{
					$images = scandir($dir);
					foreach($images as $image)
					{
						if(strlen($image) > 2 )
						{
							$filename = $dir.'/'.$image;
							$value['title'] = "";
							$value['description'] = "";
							$value['showorder']  = 500;
							copyImageExt($review_id,$filename,$filename,$value, FALSE, 10);
							unlink($filename);
						}
					}
					rmdir($dir);
				}

				$topic_data = dbGetRow('SELECT AVG(cd.f3) AS rating, COUNT(c.id) AS qnt_reviews FROM #__content c
																				 LEFT JOIN #__content_data_3 cd ON cd.id = c.id
																				 WHERE c.content_type = 3 AND cd.f1 = :pid',
																					array(
																						':pid' => $topic_id
																					));

				dbQuery('UPDATE #__content_data_4
                                        SET f7   = :rating,
                                                        f10  = :quantity,
                                                        f11  = NOW()
                                        WHERE id = :topic_id',
                                               array(
                                                       ':topic_id' => $topic_id,
                                                       ':rating'   => $topic_data['rating'],
                                                       ':quantity' => $topic_data['qnt_reviews']
                                                       ));
				unset($_SESSION["token_ar"]);
				if(!empty($_POST['action']) && $_POST['action'] == 'preview')
				{
					echo '<script> var preview_id ='.$review_id.'</script>';
					?><script>window.parent.location.href='/preview/' + preview_id +'/'</script><?php
				}
				else if(!empty($_POST['action']) && $_POST['action'] == 'save')
				{
					echo "<script> var review_id = '".$review_id."';</script>";
					?> <script>
								window.parent.$("#registerMessages").removeClass('alert alert-danger');
								window.parent.$('#new-topic-review').append('<input type="hidden" name="review_id" value="' + review_id + '"/>');
								window.parent.$("#registerMessages").html('Ваш тема и отзыв сохранены. Чтобы отредактировать отзыв, вы можете перейти в <a href="/personal/drafts/" >личный кабинет</a>.');
								window.parent.$("body").animate({scrollTop:0}, '500', 'swing'); window.parent.$("#registerMessages").addClass('alert alert-success');
							</script><?php
				}
				else
				{
					$topic_review_id = dbGetOne('SELECT f1 FROM #__content_data_3 WHERE id = :rid', array(':rid' => $review_id));
					$topic_url = dbGetOne('SELECT url FROM #__content WHERE id = :pid', array(':pid' => $topic_review_id));
					echo "<script> var topic_url = '".$topic_url."';</script>";
					?><script>window.parent.location.href='/' + topic_url +'/';</script><?php
				}
			}
			else
			{
				?> <script> window.parent.$("#registerMessages").html('Вы уже оставляли отзыв об этом товаре'); window.parent.$("body").animate({scrollTop:0}, '500', 'swing'); window.parent.$("#registerMessages").addClass('alert alert-danger');</script><?php
			}
		}

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _addCommentReview()
	{
		global $app;
		global $user;
		if($user->Authorized())
		{
			$result = array();
			$userid = $user->getID();
			if(!empty($_POST['comment']))
			{
				$comment_text = htmlspecialchars(strip_tags(trim($_POST['comment']), ENT_QUOTES));
			}

			if(!empty($_POST['rid']))
			{
				$review_id = intval($_POST['rid']);
			}
                        
                        if(!empty($_POST['rate']))
                        {
                            $rate = intval($_POST['rate']);
                        }
                        

			if(!empty($comment_text) && !empty($review_id) && !empty($rate))
			{
                            $isset_comment = dbQuery('SELECT id FROM #__comments_review WHERE userid = :uid AND review_id = :rid', array(':uid' => $userid, ':rid' => $review_id));
                            if(empty($isset_comment))
                            {
                                $idComment=dbNonQuery('INSERT INTO #__comments_review (review_id, userid, created, detail_text)
									VALUES(:rid, :uid, NOW(), :comment)',
										array(
											':rid' 		=> $review_id,
											':uid' 		=> $userid,
											':comment' => $comment_text
										),TRUE);
                                $id = dbNonQuery('INSERT INTO #__measurings_review (review_id, userid, measuring, created)
								 VALUES(:rid, :uid, :rate, NOW())',
									array(
										':rid' => $review_id,
										':uid' => $userid,
										':rate'=> $rate
									), TRUE);
                                
                                dbQuery('UPDATE #__users SET qnt_comments = qnt_comments + 1 WHERE id = :uid', array(':uid' => $userid));
				$result['SUCCESS'] = true;
                                // добавляем баллы в рейтинг, за комментарий
                                $balls= new Ratings($userid);
                                $balls->addRatingBalls(1,$idComment);
                                $author= dbGetOne('SELECT userid FROM #__content WHERE id = :id', [':id' => $review_id]); // TODO: возможно добавить в клас рейтинга, поиск автора контента 
                                $balls=new Ratings($author);
                                $balls->addRatingBalls(3, $idComment);
                                // добавить баллов в рейтинг автору отзыва
                                $balls->addRatingBalls(5, $id);
                                
                            }
                            else
                            {
                                $result['MESSAGE'] = "Вы уже давали комментарий и оценку этому отзыву";
                                $result['SUCCESS'] = false;
                            }
				
			}

			echo json_encode($result);
		}
		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _addMeasuringReview()
	{
		global $app;
		global $user;
		if($user->Authorized())
		{
			$result = array();
			$userid = $user->getID();
			if(!empty($_POST['rate'])) $rate = intval($_POST['rate']); else $rate = 0;
			if(!empty($_POST['rid'])) $rid = intval($_POST['rid']);

			if(!empty($rid))
			{
				$mid = dbGetOne('SELECT id FROM #__measurings_review WHERE userid = :uid AND review_id = :rid', array(':uid' => $userid,':rid' => $rid));
				if(empty($mid))
				{
                                    $id=dbNonQuery('INSERT INTO #__measurings_review (review_id, userid, measuring, created)
								 VALUES(:rid, :uid, :rate, NOW())',
									array(
										':rid' => $rid,
										':uid' => $userid,
										':rate'=> $rate
									), TRUE);
                                        // добавить баллов в рейтинг автору отзыва
                                        $author= dbGetOne('SELECT userid FROM #__content WHERE id= :id',[':id' => $rid]);
                                        $rating= new Ratings($author);
                                        $rating->addRatingBalls(5, $id);
				}
				else
				{
					$result['MESSAGE'] = "Вы уже давали оценку этому отзыву";
				}
			}
		}
		echo json_encode($result);

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;

	}

	function _showSearchReviewsTopic()
	{
		global $app;
		$app->actionResult = TRUE;
		$app->page->meta_title = "Добавить новый отзыв";
		include(_TEMPL . "content-type-4.php");
	}

	function _editPersonalSubscribes()
	{
		global $user;
		global $app;

		if($user->Authorized() && $app->checkToken('se',$_POST['token']))
		{
			$userid = $user->getID();
			$subscribes_personal = array();
			if(isset($_POST['checkboxG1']))
			{
				$subscribes_personal['sbc_site_news'] = intval($_POST['checkboxG1']);
			}
			if(isset($_POST['checkboxG2']))
			{
				$subscribes_personal['sbc_new_msg'] = intval($_POST['checkboxG2']);
			}
			if(isset($_POST['checkboxG3']))
			{
				$subscribes_personal['sbc_new_review'] = intval($_POST['checkboxG3']);
			}
			if(isset($_POST['checkboxG4']))
			{
				$subscribes_personal['sbc_new_comment'] = intval($_POST['checkboxG4']);
			}

			if(!empty($subscribes_personal))
			{
				dbQuery('DELETE FROM #__subscribes_profile WHERE userid = :uid', array(':uid' => $userid));
				foreach($subscribes_personal as $subcribe)
				{
					dbQuery('INSERT INTO #__subscribes_profile (userid, subcribe_id) VALUES(:uid, :sbcrid )',
										array(
											':uid' => $userid,
											':sbcrid' => $subcribe
									));
				}
			}

		} ?><script>window.parent.location.href = '/personal/';</script><?php

		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _showUserBlock($params)
	{
		global $app;
		$userid = explode('/', $params[0])[1];
		$app->actionResult = TRUE;
		$app->page->meta_title = "Страница профиля";
		include(_TEMPL . "profile-page.php");

	}

	function _showFormMessage($params)
	{
		if(!empty($params))
		{
			$user_to = intval(explode('/', $params[0])[2]);
		}

		global $app;
		global $user;
		if($user->Authorized() || !empty($_SESSION['user']))
		{
			$app->actionResult = TRUE;
			$app->page->meta_title = "Отправить сообщение";
			include(_TEMPL . "form-message.php");
		}
		else
		{
			redirect('/login/');
		}
	}

	function _sendMessage()
	{
		global $app;
		global $user;
                if(!empty($_POST['user_to']))
                {
                    $user_to = $_POST['user_to'];
                }
                if(!empty($_POST['msg-text']))
                {
                    $msg_text = htmlspecialchars(strip_tags(trim($_POST['msg-text']), ENT_QUOTES));
                }
                if(!empty($_SESSION['user']))
                {
                    $to = dbGetRow('SELECT email,name FROM #__users WHERE id = :uid', array(':uid' => $user_to));
                    if(empty($_POST['subject']))
                    { ?>
                        <script>
                            window.parent.$('.error-subject').css('display', 'block');
                            
                        </script><?php
                        die;
                        
                    }
                    else
                    { ?>
                        <script>
                            window.parent.$('.error-subject').css('display', 'none');
                        </script><?php
                        $subject = htmlspecialchars(strip_tags(trim($_POST['subject']), ENT_QUOTES));
                        
                    }
            
                    $msg_id = dbNonQuery('INSERT INTO #__messages_from_admin (userid,created,subject,text_msg) VALUES(:uid, NOW(),:subject,:text)',array(':uid' => $user_to, ':subject'=> $subject, ':text' => $msg_text), TRUE);
                    if(!empty($msg_id))
                    {
                        dbNonQuery('UPDATE #__users SET qnt_warning =  qnt_warning + 1 WHERE id = :uid', array(':uid' => $user_to));
                        $app->sendEmailTemplate(_SITE,_DEFAULT_EMAIL_FROM, "" ,$to['email'], $subject, "_msg_from_admin.tpl", array("#SUBJECT#"=> $subject,"#MESSAGE#"=>$msg_text));
                        
                        echo "<script>var uid =".$user_to."</script>";
                        ?><script>window.parent.location.href='/user/'+ uid + '/';</script><?php
                    }
                }
                else
                {
                    if($user->Authorized() && $app->checkToken('sm',$_POST['token']))
                    {
                        $user_from = $user->getID();

                        if(!empty($user_to) && !empty($msg_text) && $user_from != $user_to)
                        {
                                dbQuery('INSERT INTO #__users_messages (created, userid_from, userid_to,  msg_text)
                                                                        VALUES(NOW(), :fr, :to, :msg )',
                                                                        array(
                                                                                ':fr'   => $user_from,
                                                                                ':to'   => $user_to,
                                                                                ':msg'  => $msg_text
                                                                        ));
                                echo "<script>var uid =".$user_to."</script>";
                                ?><script>window.parent.location.href='/user/'+ uid + '/';</script><?php
                        }
                    }
                    
                }
		
		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _subscribeOnUser()
	{
		global $app;
		global $user;
		if($user->Authorized())
		{
			$userid = $user->getID();
			if(!empty($_POST['user_on']))
			{
				$user_on = intval($_POST['user_on']);
			}

			if(!empty($user_on))
			{
				$id = dbNonQuery('INSERT INTO #__subscribes_on_user (userid, userid_subscr) VALUES(:uid, :user_on)',
									array(
										':uid'		 => $userid,
										':user_on' => $user_on
									), TRUE);
				if(!empty($id))
				{
					echo 1;
				}
				else {
					echo 0;
				}
			}
		}
		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function _subscribeOffUser()
	{
		global $app;
		global $user;
		if($user->Authorized())
		{
			$userid = $user->getID();
			if(!empty($_POST['user_on']))
			{
				$user_on = intval($_POST['user_on']);
			}

			if(!empty($user_on))
			{
				$res = dbQuery('DELETE FROM #__subscribes_on_user WHERE userid = :uid AND userid_subscr = :user_on',
									array(
										':uid'		 => $userid,
										':user_on' => $user_on
									));
				var_dump($res); die;
				if(!empty($res))
				{
					echo 1;
				}
				else {
					echo 0;
				}
			}
		}
		$app->actionResult = TRUE;
		$app->loadTemplate = FALSE;
	}

	function uploadImageExt($id,$originalname,$filename,$value,$setMain = FALSE, $source = 1)
	{
		if(empty($filename)) {  return; }

		$format = explode(".",$originalname);
		$strFormat = $format[count($format) - 1];
		$newName = $id . '_' . uniqid() . '.' . $strFormat;
		$size = getimagesize($filename);
		$width = $size[0];
		$height = $size[1];
		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
		$filesize = filesize($filename);

		if(move_uploaded_file($filename,_IMAGES_PATH . $newName))
		{
			$imgID = dbNonQuery(
					'INSERT INTO #__images (source,parentid,title,description,originalname,filename,width,height,mime,filesize,showorder)
					 VALUES(:source,:parentid,:title,:description,:originalname,:filename,:width,:height,:mime,:filesize,:showorder)',
						array(
							":source"       =>$source,
							":parentid"     =>$id,
							":title"        =>$value["title"],
							":description"  =>$value["description"],
							":originalname" =>$originalname,
							":filename"     =>$newName,
							":width"        =>$width,
							":height"       =>$height,
							":mime"         =>$format,
							":filesize"     =>$filesize,
							":showorder"    =>$value["showorder"]
						),TRUE);

			if($setMain)
			{
				dbNonQuery('UPDATE #__content SET mainimage = :imgID WHERE id = :id',array(
					':imgID'=>$imgID,
					':id'=>$id
				));
			}

			return $imgID;
		}
	}

	function copyImageExt($id,$originalname,$filename,$value,$setMain = FALSE, $source = 1)
	{
		if(empty($filename)) {  return; }

		$format = explode(".",$originalname);
		$strFormat = $format[count($format) - 1];
		$newName = $id . '_' . uniqid() . '.' . $strFormat;
		$size = getimagesize($filename);
		$width = $size[0];
		$height = $size[1];
		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
		$filesize = filesize($filename);

		if(copy($filename,_IMAGES_PATH . $newName))
		{
			$imgID = dbNonQuery(
					'INSERT INTO #__images (source,parentid,title,description,originalname,filename,width,height,mime,filesize,showorder)
					 VALUES(:source,:parentid,:title,:description,:originalname,:filename,:width,:height,:mime,:filesize,:showorder)',
						array(
							":source"       =>$source,
							":parentid"     =>$id,
							":title"        =>$value["title"],
							":description"  =>$value["description"],
							":originalname" =>$originalname,
							":filename"     =>$newName,
							":width"        =>$width,
							":height"       =>$height,
							":mime"         =>$format,
							":filesize"     =>$filesize,
							":showorder"    =>$value["showorder"]
						),TRUE);

			if($setMain)
			{
				dbNonQuery('UPDATE #__content SET mainimage = :imgID WHERE id = :id',array(
					':imgID'=>$imgID,
					':id'=>$id
				));
			}

			return $imgID;
		}
	}

        
        function sendEmailTemplate($name_from,$email_from,$name_to,$email_to,$subject,$template,$params = array())
	{
		$mailTemplate = _MAIL_TEMPLATE . $template;
		$body = "";
		if(file_exists($mailTemplate))
		{
			$tpl = file_get_contents($mailTemplate);
			foreach($params as $key=>$param)
			{
				$tpl = str_replace($key,$param,$tpl);
			}
			$body = $tpl;
		}
			
		sendEmail($name_from,$email_from,$name_to,$email_to,$subject,$body);
		
	}
        
        function sendEmail($name_from,$email_from,$name_to,$email_to,$subject,$body)
	{
		$data_charset = "UTF-8"; 
		$send_charset = "UTF-8"; 
		$to = mime_header_encode($name_to, $data_charset, $send_charset)
		. ' <' . $email_to . '>';
		$subjectOriginal = $subject;
		$subject = mime_header_encode($subject, $data_charset, $send_charset);
		$from =  mime_header_encode($name_from, $data_charset, $send_charset) 
		.' <' . $email_from . '>'; 
		if($data_charset != $send_charset) {
			$body = iconv($data_charset, $send_charset, $body); 
		}
		$headers = "From: $from\r\n";
		$headers .= "Content-type: text/html; charset=$send_charset\r\n";
		$headers .= "Mime-Version: 1.0\r\n";
		$headers .= "X-Mailer: PHP/".phpversion()."\r\n";  
		
		if(!_MAIL_TO_LOG)
		{	
			return mail($to, $subject, $body, $headers);
		}
		else
		{
			dbNonQuery("INSERT INTO `". _DB_TABLE_PREFIX ."mail_log` (`created`, `name_form`, `email_form`, `name_to`, `email_to`, `subject`, `body`) VALUES (now(), :name_form, :email_from, :name_to, :email_to, :subject, :body);
			
			",array(
				":name_form"=>$name_from,
				":email_from"=>$email_from,
				":name_to"=>$name_to,
				":email_to"=>$email_to,
				":subject"=>$subjectOriginal,
				":body"=>$body
			));
		}
		
	
	}
        
        function mime_header_encode($str, $data_charset, $send_charset) {
	if($data_charset != $send_charset) {
		$str = iconv($data_charset, $send_charset, $str);
	}
	return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
	}


	function GetPeriodOnSite($qnt_days)
	{
		$years = intval(intval($qnt_days/365));
		$months = intval(intval(($qnt_days%365)/30));
		$weeks = intval(intval(($qnt_days%365)%30)/7);
		$days  = intval(intval(($qnt_days%365)%30)%7);

		if($years > 0)
		{
			echo $years;
			if(substr($years,strlen($years)-1) == 1 && ($years < 10 || $years > 20) ) echo " год ";
			elseif ((substr($years,strlen($years)-1) == 2 && ($years < 10 || $years > 20) ) ||
						 (substr($years,strlen($years)-1) == 3 && ($years < 10 || $years > 20) ) ||
						 (substr($years,strlen($years)-1) == 4 && ($years < 10 || $years > 20) )) echo " года ";
			elseif (substr($years,strlen($years)-1) == 0 ||
							substr($years,strlen($years)-1) == 5 ||
							substr($years,strlen($years)-1) == 6 ||
							substr($years,strlen($years)-1) == 7 ||
							substr($years,strlen($years)-1) == 8 ||
							substr($years,strlen($years)-1) == 9 ||
							($years > 10 && $years < 20) ) echo " лет ";
		}
		if($months > 0)
		{
			echo $months;
			if($months == 1) echo ' месяц ';
			if($months > 1 && $months < 5) echo ' месяца ';
			if($months > 4) echo ' месяцев ';
		}
		if($weeks > 0 && ($months == 0 || $years == 0))
		{
			echo $weeks;
			if($weeks == 1) echo ' неделя ';
			if($weeks > 1 && $weeks < 5) echo ' недели ';
			if($weeks > 4) echo ' недель ';
		}
		if($days > 0 && (($months == 0 && $weeks == 0) || ($years == 0 && $months == 0) || ($years == 0 && $weeks == 0) ))
		{
			echo $days;
			if(substr($days,strlen($days)-1) == 1 && ($days < 10 || $days > 20) ) echo " день ";
			elseif ((substr($days,strlen($days)-1) == 2 && ($days < 10 || $days > 20) ) ||
						 (substr($days,strlen($days)-1) == 3 && ($days < 10 || $days > 20) ) ||
						 (substr($days,strlen($days)-1) == 4 && ($days < 10 || $days > 20) )) echo " дня ";
			elseif (substr($days,strlen($days)-1) == 0 ||
							substr($days,strlen($days)-1) == 5 ||
							substr($days,strlen($days)-1) == 6 ||
							substr($days,strlen($days)-1) == 7 ||
							substr($days,strlen($days)-1) == 8 ||
							substr($days,strlen($days)-1) == 9 ||
							($days > 10 && $days < 20) ) echo " дней ";
		}
	}
        
        /*
  $x_o и $y_o - координаты левого верхнего угла выходного изображения на исходном
  $w_o и h_o - ширина и высота выходного изображения
  */
  function crop($image) {
//    if (($x_o < 0) || ($y_o < 0) || ($w_o < 0) || ($h_o < 0)) {
//      echo "Некорректные входные параметры";
//      return false;
//    }
    list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
    $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
    $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
    if ($ext) {
      $func = 'imagecreatefrom'.$ext; // Получаем название функции, соответствующую типу, для создания изображения
      $img_i = $func($image); // Создаём дескриптор для работы с исходным изображением
    } else {
      echo 'Некорректное изображение'; // Выводим ошибку, если формат изображения недопустимый
      return false;
    }
    if($w_i > $h_i)
    {
        $w_o = $h_i;
        $h_o = $h_i;
        $x_o = ($w_i - $h_i)/2;
        $y_o = 0;
        
    }
    else
    {
        $w_o = $w_i;
        $h_o = $w_i;
        $x_o = 0;
        $y_o = ($h_i - $w_i)/2;
        
    }
    //if ($x_o + $w_o > $w_i) $w_o = $w_i - $x_o; // Если ширина выходного изображения больше исходного (с учётом x_o), то уменьшаем её
    //if ($y_o + $h_o > $h_i) $h_o = $h_i - $y_o; // Если высота выходного изображения больше исходного (с учётом y_o), то уменьшаем её
    $img_o = imagecreatetruecolor($w_o, $h_o); // Создаём дескриптор для выходного изображения
    imagecopy($img_o, $img_i, 0, 0, $x_o, $y_o, $w_o, $h_o); // Переносим часть изображения из исходного в выходное
    $func = 'image'.$ext; // Получаем функция для сохранения результата
    return $func($img_o, $image); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
  }
  
  function cropTopicFoto($image) {
//    if (($x_o < 0) || ($y_o < 0) || ($w_o < 0) || ($h_o < 0)) {
//      echo "Некорректные входные параметры";
//      return false;
//    }
    list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
    $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
    $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
    if ($ext) {
      $func = 'imagecreatefrom'.$ext; // Получаем название функции, соответствующую типу, для создания изображения
      $img_i = $func($image); // Создаём дескриптор для работы с исходным изображением
    } else {
      echo 'Некорректное изображение'; // Выводим ошибку, если формат изображения недопустимый
      return false;
    }
    if($w_i > $h_i)
    {
        $w_o = $h_i* 1.22543352601;
        $h_o = $h_i;
        $x_o = ($w_i - $h_i)/2;
        $y_o = 0;
        
    }
    else
    {
        $w_o = $w_i;
        $h_o = $w_i * 0.816037735849;
        $x_o = 0;
        $y_o = ($h_i - $w_i)/2;
        
    }
    //if ($x_o + $w_o > $w_i) $w_o = $w_i - $x_o; // Если ширина выходного изображения больше исходного (с учётом x_o), то уменьшаем её
    //if ($y_o + $h_o > $h_i) $h_o = $h_i - $y_o; // Если высота выходного изображения больше исходного (с учётом y_o), то уменьшаем её
    $img_o = imagecreatetruecolor($w_o, $h_o); // Создаём дескриптор для выходного изображения
    imagecopy($img_o, $img_i, 0, 0, $x_o, $y_o, $w_o, $h_o); // Переносим часть изображения из исходного в выходное
    $func = 'image'.$ext; // Получаем функция для сохранения результата
    return $func($img_o, $image); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
  }
