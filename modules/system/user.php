<?php if(!defined("_APP_START")) { exit(); }

$this->routes[] = array("pattern"=>'/login\/form/',"action"=>"_showLoginFormAjax","contentType"=>"component");
$this->routes[] = array("pattern"=>'/registration\/form/',"action"=>"_showRegistrationFormAjax","contentType"=>"component");
$this->routes[] = array("pattern"=>'/login\/$/',"action"=>"_showLoginForm");
$this->routes[] = array("pattern"=>'/logout\/$/',"action"=>"_Logout");
$this->routes[] = array("pattern"=>'/registration\/$/',"action"=>"_showRegistrationForm");
$this->routes[] = array("pattern"=>'/personal\/$/',"action"=>"_showPersonalPage");
$this->routes[] = array("pattern"=>'/registration\/create/',"action"=>"_registrationCreate");

function _showLoginForm()
{
	 global $app;
	 $app->actionResult = TRUE;
	 $app->page->meta_title = "Авторизация";
	 $app->IncludeComponent("system/auth");
}
function _showLoginFormAjax()
{
	global $app;
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
	$app->IncludeComponent("system/auth");
}
function _showRegistrationForm()
{
	 global $app;
	 $app->actionResult = TRUE;
	 $app->page->meta_title = "Регистрация";
	 $app->IncludeComponent("system/register");
}
function _showRegistrationFormAjax()
{
	global $app;
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
	$app->IncludeComponent("system/register");
}
function _showPersonalPage()
{
	 global $app;
	 $app->actionResult = TRUE;
	 $app->page->meta_title = "Персональный кабинет";
	 include(_TEMPL . "personal.php");

}
function _registrationCreate()
{
	$app->loadTemplate = FALSE;
	$app->actionResult = TRUE;
}
function _Logout()
{
	global $user;
	$user->Logout();
	redirect(APP::getServerProtocol() .  "://" . _SITE);
}
