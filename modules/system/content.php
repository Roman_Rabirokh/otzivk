<?php if(!defined("_APP_START")) { exit(); }

$this->routes[] = array("pattern"=>'/sitemap\.xml/',"action"=>"sitemap","contentType"=>"content");

class Content
{

    static function Insert($params)
    {

    }

    static function Update($id,$params)
    {

    }

    static function Delete($id,$contentType = 0)
    {
        if($contentType == 0)
        {
            $contentType = dbGetOne('SELECT content_type FROM #__content WHERE id = :id',array(':id'=>$id));
        }
        dbNonQuery('DELETE FROM #__content WHERE id = :id',array(':id'=>$id));
        dbNonQuery('DELETE FROM #__content_data_" . $contentType . " WHERE id = :id',array(':id'=>$id));

        $images = dbQuery('SELECT id FROM #__images WHERE parentid = :id AND source = 1',array(':id'=>$id));
    	foreach($images as $file)
    	{
    		//rmImage($file['id']);
    	}
    	$files = dbQuery('SELECT id FROM #__files WHERE parentid = :id AND source = 1',array(':id'=>$id));
    	foreach($files as $file)
    	{
    		//rmFile($file['id']);
    	}
    }

    static function GetList($typeID,$fields = array(),$where = array(),$order = array(),$limit = array(),&$allCount = FALSE)
	{

			$selectQuery = '';

			$joinQuery = '';

			$whereQuery = ' WHERE c.content_type = :typeID ';

			$orderQuery = ' ORDER BY c.content_date DESC';

			$limitQuery = ' LIMIT 0,20';

			$params = array(':typeID'=>$typeID);

			if(!empty($fields['USER']))
			{
				$joinQuery .= ' LEFT JOIN #__users u ON u.id = c.userid ';
				$selectQuery  .= implode(',',$fields['USER']);

				unset($fields['USER']);
			}
			if(!empty($fields['PROFILE']))
			{
				$joinQuery .= ' LEFT JOIN #__profiles p ON p.userid = c.userid ';
				$selectQuery  .= implode(',',$fields['PROFILE']);

				unset($fields['PROFILE']);
			}

			if(!empty($fields))
			{
				if(!empty($selectQuery))
				{
					$selectQuery .= ', ';
				}
				$selectQuery  .= implode(',',$fields);
			}

			if(empty($selectQuery))
			{
				$selectQuery = 'c.*';
			}


			if(!empty($where['FIELDS']))
			{

				$fieldsInfo = dbQuery('SELECT id,fieldtype,multiple FROM  #__content_fields WHERE content_type = :typeID AND id IN(' . implode(',',array_keys($where['FIELDS'])) . ')',array(
					':typeID'=>$typeID
				));



				foreach($fieldsInfo as $current)
				{
                    $value = $where['FIELDS'][$current['id']];
					$pname = ':f'.$current['id'];
					$fname = 'f'.$current['id'];
					switch($current['fieldtype'])
					{
						case 4:

							if($current['multiple'] == 1)
							{
								$whereQuery .= ' AND c.id IN ( SELECT parentid FROM #__content_field_' . $current['id'] . ' WHERE valueid ' . (is_array($value) ? ' IN (' . implode(',',$value) . ') ' : ' = ' . $pname) . ' )';
							}
							else
							{
								if(is_array($value))
								{
									$inQuery = array();
									$z = 0;
									foreach($value as $current)
									{
										$z++;
										$inQuery[] = $pname  . $z;
									}

									$whereQuery .= ' AND cd.' . $fname .' IN (' . implode(',',$inQuery)  . ') ';



									foreach($inQuery as $k=>$v)
									{
										$params[$v] =  $value[$k];
									}
								}
								else
								{
									$whereQuery .= ' AND cd.' . $fname .  ' = ' . $pname;
								}
							}
							break;
					}
					if(!is_array($value))
					{
						$params[$pname] =  $value;
					}
				}

				unset($where['FIELDS']);
			}



			if(empty($where))
			{
				$whereQuery .= ' AND c.active = 1';
			}
			else
			{
				$i = 0;

				foreach($where as $key=>$value)
				{
					if($value == '')
					{
						$whereQuery .= ' AND ' . $key;
					}
					else
					{
						$whereQuery .= ' AND ' . $key . '= :param' . $i;
						$params[':param' . $i] = $value;
					}
					$i++;
				}
			}

			if(!empty($order))
			{
				$orderQuery = ' ORDER BY ';
				$orderQueryPrepare = array();
				foreach($order as $key=>$value)
				{
					$orderQueryPrepare[] = $key . ' ' . $value;
				}
				$orderQuery .= implode(',',$orderQueryPrepare);
			}

			if(!empty($limit))
			{
				$limitQuery = ' LIMIT ' . intval(isset($limit['OFFSET']) ? $limit['OFFSET'] : 0) . ',' .  intval($limit['ROWS']);
			}


			$sql = 'SELECT ' . $selectQuery   . ' FROM #__content c
			LEFT JOIN #__content_data_' . $typeID . ' cd ON cd.id = c.id
			' . $joinQuery . $whereQuery . $orderQuery . $limitQuery ;
			
			//echo $sql;

			return  dbQuery($sql, $params, (!empty($limit) && isset($limit['COUNT_ALL'])? TRUE : FALSE), $allCount);
	}
	static function GetByUrlname($url) //iData
	{
		$data = dbGetRow("SELECT c.*, ct.urlname, ct.urlname as content_type_url  FROM " . _DB_TABLE_PREFIX . "content c
		INNER JOIN #__content_types ct ON  ct.id = c.content_type
		WHERE c.active = 1 AND c.urlname = :url LIMIT 0,1",array(
	    ":url"=>$url));
		return $data;
	}
	static function GetByUrl($url) //iData
	{

		$data = dbGetRow("SELECT c.*, ct.urlname, ct.urlname as content_type_url,ct.url404  FROM " . _DB_TABLE_PREFIX . "content c
		INNER JOIN #__content_types ct ON  ct.id = c.content_type
		WHERE active = 1 AND url = :url LIMIT 0,1",array(
	    ":url"=>$url));
		return $data;
	}
	static function GetById($id) //iData
	{
		$data = dbGetRow("SELECT * FROM " . _DB_TABLE_PREFIX . "content WHERE active = 1 AND  id = :id LIMIT 0,1",array(
	    ":id"=>$id));
		return $data;
	}

	static function getFieldsData($id,$contentType,$code = array())
	{
		$result = array();

		$select = '*';

		if(!empty($code))
		{

			$keys = array();
			$params = array(
				':content_type'=>$contentType
			);
			foreach($code as $current)
			{
				$keys[] = ':' . $current;
				$params[':' . $current] = $current;
			}




			$fields = dbQueryToArray('SELECT CONCAT("f",id," as ",code) as fieldName FROM  #__content_fields WHERE content_type = :content_type AND code IN(' . implode(',' , $keys) . ')',
			$params
			);
			if(!empty($fields))
			{
				$select = implode(',',$fields);
			}
		}

		$result = dbGetRow('SELECT ' . $select . ' FROM #__content_data_' . intval($contentType).' WHERE id = :id',array(':id'=>$id));

		return $result;
	}

	static function getMultipleFieldsData($id,$contentType)
	{

	}

	static function contentUrl($url)
	{
        return '/' . $url . '/';
	}

	static function loadModules($contentId)
	{
		global $app;
		global $page;
		global $user;

		$modules = $app->IncludeComponent("system/get.modules",array("CONTENT_ID"=>$contentId));

		foreach($modules as $module)
		{
			if(file_exists($module["path"]))
			{
				if($module["vars"] != "")
				{
					eval(htmlspecialchars_decode($module["vars"],ENT_QUOTES));
				}
				include($module["path"]);
			}
		}
	}

	static function  getFiles($contentId,$limit = 0,$source = 1)
	{
		return dbQuery('SELECT * FROM #__files WHERE parentid = :parentid AND source = :source ORDER BY showorder ASC' . ($limit > 0 ? ' LIMIT 0,' . intval($limit)  : ''),array(':parentid'=>$contentId,':source'=>$source));
	}

	static function loadContent($url = "")
	{
		global $app;
		$params = array();
		if(!empty($url))
		{
			$params["URL"] = $url;
		}

		$app->actionResult = $app->IncludeComponent("system/content.page",$params);
	}

	static function getOpenContentTree($id,&$arr)
	{
		$data = dbGetRow('SELECT name,parentid,url FROM #__content WHERE id = :id LIMIT 0,1',array(':id'=>intval($id)));
		$arr[$id] = array("NAME"=>$data['name'],"LINK"=>self::contentUrl($data['url']));
		if($data['parentid'] > 0)
		{
			self::getOpenContentTree($data['parentid'],$arr);
		}
	}

	static function getBlock($code,$html_decode = FALSE)
	{
		if($html_decode)
		{
				return htmlspecialchars_decode(dbGetOne('SELECT content FROM #__blocks WHERE code = :code', array(':code'=>$code)));
		}
		else
		{
			return dbGetOne('SELECT content FROM #__blocks WHERE code = :code', array(':code'=>$code));
		}

	}

	static function getMenuByID($id)
	{
		$items = dbQuery('SELECT title,link FROM #__menu_items WHERE menuid = :id ORDER BY showorder',array(
			':id'=>$id
		));

		foreach($items as &$current)
		{
			$current['active'] = FALSE;
			if($current['link'] != '/' && stripos($_SERVER['REQUEST_URI'],$current['link']) !== false)
			{
				$current['active'] = TRUE;
			}
			if($current['link'] == '/' && $_SERVER['REQUEST_URI'] == '/')
			{
				$current['active'] = TRUE;
			}

		}

		return $items;

	}
	static function getMenuByCode($code)
	{
		$menuID = dbGetOne('SELECT id FROM #__menu WHERE code = :code',array(':code'=>$code));
		if(!empty($menuID))
		{
			return  Content::getMenuByID($menuID);
		}
		else
		{
			return array();
		}
	}
}

function sitemap()
{
	global $app;
	$app->loadTemplate = false;
	$app->actionResult = true;
	header('Content-Type: text/xml; charset=UTF-8');
	echo "<?xml version='1.0' encoding='UTF-8'?>";

	$data = dbQuery("SELECT url FROM #__content_types ct
	INNER JOIN #__content c ON c.content_type = ct.id
	WHERE ct.sitemap = 1 AND c.urlname NOT IN ('index','404') AND parentid = 0 AND active = 1
	");

	?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<?php foreach($data as $current) {?>
	<url><loc><?php echo _SITE.Content::contentUrl($current['url']); ?></loc></url>
	<?php } ?>
	</urlset>
	<?php

}
