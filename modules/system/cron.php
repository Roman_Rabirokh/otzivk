<?php if(!defined("_APP_START")) { exit(); }

$this->routes[] = array("pattern"=>'/cron\/run/',"action"=>"runCron","contentType"=>"cron");
function runCron()
{
	 global $app;
	 global $page;
	 $app->actionResult = TRUE;
	 $app->loadTemplate = FALSE;

	 $events = $app->getEventHandlers("cronActions");
	 foreach($events as $event)
	 {
		call_user_func($event);	
	 }
	 
	 exit();
}
