<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ratings
 *
 * @author Roman Rabirokh
 */
class Ratings
{

    private $id;

    public function __construct($id)
    {
        if (!empty($id))
        {
            $this->id = $id;
        }
    }

    public function addRatingBalls($type, $content)
    {
        $type = $this->getTypeRating($type);
     
        if(!empty($type))
        {
            // если зависит от оценки рейтинга то баллы взять из оценки
            if ($type['id'] == 5)
                $type['balls'] = $this->getBallsForType($content);
            $this->insertLogs($type, $content);
            $this->updateBalls($type['balls']);
        }
    }

    private function getTypeRating($type)
    {
        if (is_numeric($type))
        {
            $rating = dbGetRow('SELECT id, balls FROM #__rating_types WHERE id= :id', [':id' => $type]);
        } else if (is_string($type))
        {
            $rating = dbGetRow('SELECT id, balls FROM #__rating_types WHERE description= :description', [':description' => $type]);
        }

        if (!empty($rating))
            return $rating;
        else
            return FALSE;
    }

    private function updateBalls($balls)
    {
        dbNonQuery('UPDATE #__profiles SET rating = rating + :balls WHERE userid = :id', [':balls' => $balls, ':id' => $this->id]);
        dbNonQuery('UPDATE #__users SET rating = rating + :balls WHERE id = :id', [':balls' => $balls, ':id' => $this->id]);
    }

    private function getBallsForType($content)
    {
        $balls = dbGetOne('SELECT measuring FROM #__measurings_review WHERE id = :id', [':id' => $content]);
        return $balls;
    }

    private function insertLogs($type, $content)
    {
        dbNonQuery('INSERT INTO #__rating_logs (rating_type, balls, cdate, content, user) VALUES (:type, :balls, NOW(), :content, :user)', [
            ':type' => $type['id'],
            ':balls' => $type['balls'],
            ':content' => $content,
            ':user' => $this->id,
        ]);
    }

}
